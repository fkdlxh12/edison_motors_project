@echo off

@REM 
@REM Copy srec file to firmware folder
@REM
SET CM0_PATH=%cd%
SET CM0_FILE=cm0plus.elf

::SET CM4_PATH=%cd%\env
SET CM7_PATH=%cd%
SET CM7_FILE=cm7_0.elf

SET OPEN_OCD=..\bin\openocd

@echo CM0_PATH=%CM0_PATH%
@echo CM7_0_PATH=%CM7_PATH%

@REM 
@REM Select Tool 
@REM
@REM miniProg4
SET INTERFACE=-f interface/kitprog3.cfg
@REM J-link
@REM SET INTERFACE=-f interface/jlink.cfg -c "transport select swd"


@REM 
@REM Select config file for device 
@REM

SET CFG=traveo2_c2d_4m.cfg


@REM 
@REM ELF to SREC TOOL 
@REM

SET GHS_SREC="C:\GHS\ARM.V2017.1.4\comp_201714\gsrec.exe"