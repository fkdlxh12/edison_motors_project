@echo off

@call env.bat

if "%1"=="" goto error


@echo on
%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "program %1 verify exit"

goto exit


:error
echo.
echo USAGE : program [filename]
echo.

:exit
