@echo off

@call env.bat

SET Voltage=
SET onoff=

set /p Voltage=Voltage Input:
set /p onoff=onoff Input:

@echo on
%OPEN_OCD% -s ../scripts -f interface/kitprog3.cfg -c "kitprog3 power_config on %Voltage% ; init; kitprog3 power_control %onoff%; shutdown"

goto exit

:error

echo.
echo USAGE : power [on/off]
echo.
:exit