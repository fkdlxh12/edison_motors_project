@echo off

@call env.bat

@echo on

@echo erase_sector

%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash erase_sector 0 0 last; shutdown"


