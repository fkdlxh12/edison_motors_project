@echo off

@call env.bat

@echo on
%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "program %1 verify exit 0x10000000"

goto exit
