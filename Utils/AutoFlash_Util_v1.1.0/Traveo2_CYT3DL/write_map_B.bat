@echo off

@call env.bat


@echo on

if "%CFG%"=="traveo2_2m.cfg" (
	%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash erase_sector 1 48 48; flash fillw 0x14018000 0xBBBBBBBB 0x1; shutdown"
) 

@REM if "%CFG%"=="traveo2_1m_a0.cfg" (
else (
	%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash erase_sector 1 36 36; flash fillw 0x14012000 0xBBBBBBBB 0x1; shutdown"
)