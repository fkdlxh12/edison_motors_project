@echo off

@call env.bat


@echo on

set SEMPERFLASH_LOADER_0 TV2C_6M_HyperFlash.elf

%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash fillw 0x60000000 0xBBBBBBBB 0x1; shutdown"
