@echo off

@call env.bat

@echo on
if "%CFG%"=="traveo2_2m.cfg" (
	%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash erase_address 0x14000000 0x20000; shutdown"
) else if "%CFG%"=="traveo2_1m_a0.cfg" (
	%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash erase_address 0x14000000 0x18000; shutdown"
) else if "%CFG%"=="traveo2_6m_psvp.cfg" (
	%OPEN_OCD% -s ../scripts %INTERFACE% -f target/%CFG% -c "init; reset init; flash erase_address 0x14000000 0x20000; shutdown"
) 
else ( echo "CFG error" )

