@echo off

@call env.bat

if "%1"=="" goto error

@echo on

if "%CFG%"=="traveo2_2m.cfg" (
	%GHS_SREC% -romaddr 0x100F8000 -start 0x10000000 "%1" -o "%1_b.srec" -bytes 28
) 

if "%CFG%"=="traveo2_1m_a0.cfg" (
	%GHS_SREC% -romaddr 0x10078000 -start 0x10000000 "%1" -o "%1_b.srec" -bytes 28
)


goto exit


:error

echo.
echo USAGE : program [filename]
echo.

:exit
