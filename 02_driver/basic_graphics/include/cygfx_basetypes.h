/*******************************************************************************
 * Product: SW_TVII_VGFX_DRV
 *
 * (c) 2017-2022, Cypress Semiconductor Corporation. All rights reserved.
 *
 * Warranty and Disclaimer
 *
 * This software product is property of Cypress Semiconductor Corporation or
 * its subsidiaries.
 * Any use and/or distribution rights for this software product are provided
 * only under the Cypress Software License Agreement.
 * Any use and/or distribution of this software product not in accordance with
 * the terms of the Cypress Software License Agreement are unauthorized and
 * shall constitute an infringement of Cypress intellectual property rights.
 */
/*****************************************************************************/


/**
 * \file    cygfx_basetypes.h
 */


#ifndef CYGFX_BASETYPES_H
#define CYGFX_BASETYPES_H


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include <stdint.h>
#ifndef CY_C_MODEL
#include <stdbool.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/* Define SILICON, PSVP, FPGA, C_MODEL */
#if defined(CY_USE_PSVP) && !(CY_USE_PSVP)
#define CYGFX_SILICON
#elif defined(CY_USE_PSVP) && (CY_USE_PSVP)
#define CYGFX_PSVP
#elif defined(CY_FPGA)
#define CYGFX_FPGA
#elif defined(C_MODEL)
#define CYGFX_C_MODEL
#endif

/* Check that not 2 or more systems defined */
#if (defined(CYGFX_SILICON) && ((defined(CYGFX_FPGA) || (defined(CYGFX_C_MODEL)))))
#error "Select just one system"
#elif (defined(CYGFX_PSVP) && ((defined(CYGFX_FPGA) || (defined(CYGFX_C_MODEL)))))
#error "Select just one system"
#elif (defined(CYGFX_FPGA) &&  (defined(CYGFX_C_MODEL)))
#error "Select just one system"
#endif

/**
\addtogroup cygfx_basetypes
\code #include "cygfx_basetypes.h" \endcode
\brief Generic type definitions
*/
/** \{ */

#if (defined(CYGFX_SILICON) || defined (CYGFX_PSVP))
/**
    Macro to call asm command DSB
**/
#define GCCGDX_GEN_ASMDSB __asm("dsb")
#else
#define GCCGDX_GEN_ASMDSB CYGFX_NULL_FUNCTION
#endif


/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/
typedef uint8_t CYGFX_U08; /**< Unsigned 8-bit integer */

typedef int8_t CYGFX_S08; /**< Signed 8-bit integer */

typedef uint16_t CYGFX_U16; /**< Unsigned 16-bit integer */

typedef int16_t CYGFX_S16; /**< Signed 16-bit integer */

typedef uint32_t CYGFX_U32; /**< Unsigned 32-bit integer */

typedef int32_t CYGFX_S32; /**< Signed 32-bit integer */

typedef uint64_t CYGFX_U64; /**< Unsigned 64-bit integer */

typedef int64_t CYGFX_S64; /**< Signed 64-bit integer */

typedef char CYGFX_CHAR; /**< String character */

typedef float CYGFX_FLOAT; /**< 32-bit IEEE float */

typedef double CYGFX_DOUBLE; /**< 64-bit IEEE float */

typedef CYGFX_U32 CYGFX_BOOL; /**< Boolean */

typedef CYGFX_U32 CYGFX_ADDR; /**< Physical memory address */

typedef CYGFX_S32 CYGFX_ERROR; /**< Function return code */

typedef CYGFX_S32 CYGFX_MODULE; /**< Module ID */

/** \} end addtogroup */

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* CYGFX_BASETYPES_H */

