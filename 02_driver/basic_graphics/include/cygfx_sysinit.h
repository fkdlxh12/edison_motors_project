/*******************************************************************************
 * Product: SW_TVII_VGFX_DRV
 *
 * (c) 2017-2022, Cypress Semiconductor Corporation. All rights reserved.
 *
 * Warranty and Disclaimer
 *
 * This software product is property of Cypress Semiconductor Corporation or
 * its subsidiaries.
 * Any use and/or distribution rights for this software product are provided
 * only under the Cypress Software License Agreement.
 * Any use and/or distribution of this software product not in accordance with
 * the terms of the Cypress Software License Agreement are unauthorized and
 * shall constitute an infringement of Cypress intellectual property rights.
 */
/*****************************************************************************/


/**
 * \file    cygfx_sysinit.h
 */


#ifndef CYGFX_SYSINIT_H
#define CYGFX_SYSINIT_H


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/


#ifdef __cplusplus
extern "C"
{
#endif

/**
* \addtogroup cygfx_sysinit
* \code #include "cygfx_sysinit.h" \endcode
* \brief The Driver Initialization API exposes the functions to initialize and de-initialize the driver.
*/


/** \{ */

/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/** \name Default initializer
 * \{ */
#define CYGFX_SYS_INIT_INITIALIZER  \
    { \
        0u, /* no safety driver */ \
        0u, /* Display 0 PLL in Hz  */ \
        0u, /* Display 1 PLL in Hz  */ \
        CYGFX_TRUE /* Erasing enabled */ \
    } /**< Default Initializer */
/** \} */
/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/** \name Resource names */
/** \{ */
/* Displays */
    typedef CYGFX_U32 CYGFX_SYS_RES;                               /**< Type for resources */
#define CYGFX_SYS_RES_DISP0        ((CYGFX_SYS_RES)(1uL))      /**< Resource Display 0 (includes framegen, Clut, optional color matrix, optional histogram, dither for display 0)
                                                                        \note If Display 0 should be configured in dual channel mode, CYSD_SYS_RES_DISP1 must be also selected but no CYSD_SYS_RES_DISP1WINDOWS */
#define CYGFX_SYS_RES_DISP1        ((CYGFX_SYS_RES)(2uL))      /**< Resource Display 1 (includes framegen, Clut, optional color matrix, optional histogram, dither for display 1) */
#define CYGFX_SYS_RES_DISP0WINDOWS ((CYGFX_SYS_RES)(5uL))      /**< Window 0 - 7 for display 0 (Inludes LayerBlend 5, FetchLayer 0, ConstFrame0, Extdest0) */
#define CYGFX_SYS_RES_DISP1WINDOWS ((CYGFX_SYS_RES)(10uL))     /**< Window 0 - 7 for display 1 (Inludes LayerBlend 4, FetchLayer 1, ConstFrame1, Extdest1) */
                                                                        /** \} */

/** \name Allowed PLL frequency range */
/** \{ */
typedef CYGFX_U32 CYGFX_SYS_INIT_DISP_PLL;                               /**< Type that represents a display PLL frequency */
#define CYGFX_SYS_INIT_DISP_PLL_MIN ((CYGFX_SYS_INIT_DISP_PLL)110000000) /**< Minimum display PLL frequency of 110 MHz */
#define CYGFX_SYS_INIT_DISP_PLL_MAX ((CYGFX_SYS_INIT_DISP_PLL)220000000) /**< Maximum display PLL frequency of 220 MHz */

/** \} */

/**
    Data type used to specify PLL values for the two display controllers
**/
typedef struct
{
    CYGFX_SYS_RES           ResourceLock; /**< Bitmask that describes the resources allocated by the safety driver; default: 0 */
    CYGFX_SYS_INIT_DISP_PLL PllDsp0;      /**< Frequency of the display 0 PLL in Hz. Used for the configuration of
        clock dividers for pixel clock generation. Assumed to have been configured by the system. The default value
        is 0. */
    CYGFX_SYS_INIT_DISP_PLL PllDsp1;      /**< Frequency of the display 1 PLL in Hz. Used for the configuration of
        clock dividers for pixel clock generation. Assumed to have been configured by the system. The default value
        is 0.
        Note: May not be available on all devices. */
    CYGFX_BOOL              bErase;       /**< Controls whether a system-wide erasing is performed during startup. 
        The default value is CYGFX_TRUE; all GFX registers and the complete VRAM are erased. */
} CYGFX_SYSINIT_INFO_S;

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/* N/A */

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

/* Deviation from MISRA C:2012 Rule-3.1.
   Justification: In Doxygen comments, character sequences such as double slash are used. 
                  This cannot be avoided without losing information. */
/* PRQA S 5133 1 */
/**
    Used to initialize the driver at startup.
    Applications must initialize the driver before they can call other driver
    functions except for the \ref cygfx_erp "error reporting API".

    \note
    The VIDEOSS hardware must be in the default state, i.e., no registers may be
    altered between HW reset and the call of CyGfx_SysInitializeDriver().

    The registers reserved for a safety driver may be altered.

    \note
    If a pDriverInitInfo parameter is used, it is recommended to initialize this
    structure with CYGFX_SYS_INIT_INITIALIZER, and then set the fields that are to
    be changed:

    \code
    CYGFX_SYSINIT_INFO_S DriverInitInfo = CYGFX_SYS_INIT_INITIALIZER;
    DriverInitInfo.PllDsp0 = (200 * 1000000); // Set value for display 0 PLL to 200 MHz
    DriverInitInfo.ResourceLock = CYGFX_SYS_RES_DISP0; //Reserves the FetchLayer0 Unit
    CyGfx_SysInitializeDriver(&DriverInitInfo);
    \endcode

    For driver default values, refer to \a ::CYGFX_SYS_INIT_INITIALIZER. Note that
    the driver will not configure the PLL frequencies specified with
    pDriverInitInfo, but relies on the system to have configured them.

    \param [in] pDriverInitInfo  Can be NULL or a pointer to a
                                 ::CYGFX_SYSINIT_INFO_S driver initialization structure.

    \retval  ::CYGFX_OK Successfully initialized the driver.
    \retval  ::CYGFX_ERP_ERR_SYS_DEVICE_INVALID_PARAMETER ResourceLock parameter
             invalid
    \retval  ::CYGFX_ERP_ERR_SYS_DEVICE_ALREADY_INITIALIZED Already initialized
    \retval  ::CYGFX_ERP_ERR_SYS_DEVICE_WRONG_ID The graphics driver is not valid
             for the hardware.
**/
CYGFX_EXTERN CYGFX_ERROR CyGfx_SysInitializeDriver( const CYGFX_SYSINIT_INFO_S* pDriverInitInfo);

/**
    Used to de-initialize the driver.

    Applications must de-initialize the driver if it is no longer used.

    \retval  ::CYGFX_OK Successfully shut down the driver.
    \retval  ::CYGFX_ERP_ERR_SYS_DEVICE_NOT_YET_INITIALIZED Not yet initialized (
             ::CyGfx_SysInitializeDriver was not called).
**/
CYGFX_EXTERN CYGFX_ERROR CyGfx_SysDeInitializeDriver(void);


/** \} end addtogroup */

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* CYGFX_SYSINIT_H */

