/*******************************************************************************
 * Product: SW_TVII_VGFX_DRV
 *
 * (c) 2017-2022, Cypress Semiconductor Corporation. All rights reserved.
 *
 * Warranty and Disclaimer
 *
 * This software product is property of Cypress Semiconductor Corporation or
 * its subsidiaries.
 * Any use and/or distribution rights for this software product are provided
 * only under the Cypress Software License Agreement.
 * Any use and/or distribution of this software product not in accordance with
 * the terms of the Cypress Software License Agreement are unauthorized and
 * shall constitute an infringement of Cypress intellectual property rights.
 */
/*****************************************************************************/


/**
 * \file    cygfx_erp_types.h
 */


#ifndef CYGFX_ERP_TYPES_H
#define CYGFX_ERP_TYPES_H


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/


#ifdef __cplusplus
extern "C"
{
#endif

/**
 * \addtogroup cygfx_erp
 * \code #include "cygfx_erp_types.h" \endcode
 */


/** \{ */


/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/* N/A */

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/
/** \name Error reporting levels */
/** \{ */
typedef CYGFX_U08 CYGFX_ERP_LEVEL;                   /**< Type for erp level */
#define CYGFX_ERP_LEVEL_NOTHING ((CYGFX_ERP_LEVEL)0) /**< Report no messages */
#define CYGFX_ERP_LEVEL_ERROR   ((CYGFX_ERP_LEVEL)1) /**< Report error messages */
#define CYGFX_ERP_LEVEL_WARNING ((CYGFX_ERP_LEVEL)2) /**< Report error+warning messages */
#define CYGFX_ERP_LEVEL_INFO    ((CYGFX_ERP_LEVEL)3) /**< Report error+warning+info messages */
/** \} */

/** \name Error reporting channel properties */
/** \{ */
typedef CYGFX_U08 CYGFX_ERP_CHANNEL;                 /**< Type for erp channel setting */
#define CYGFX_ERP_CHANNEL_OFF ((CYGFX_ERP_CHANNEL)0) /**< Message channel off */
#define CYGFX_ERP_CHANNEL_ON  ((CYGFX_ERP_CHANNEL)1) /**< Message channel on */
/** \} */

/** \name Error reporting destinations */
/** \{ */
typedef CYGFX_U08 CYGFX_ERP_DEST;                 /**< Type for erp destination setting */
#define CYGFX_ERP_DEST_STDOUT ((CYGFX_ERP_DEST)0) /**< Report to stdout */
#define CYGFX_ERP_DEST_BUFFER ((CYGFX_ERP_DEST)1) /**< Report to buffer */
/** \} */

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/* N/A */

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

/* N/A */

/** \} end addtogroup */

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* CYGFX_ERP_TYPES_H */
