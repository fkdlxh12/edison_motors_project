/*******************************************************************************
 * Product: SW_TVII_VGFX_DRV
 *
 * (c) 2017-2022, Cypress Semiconductor Corporation. All rights reserved.
 *
 * Warranty and Disclaimer
 *
 * This software product is property of Cypress Semiconductor Corporation or
 * its subsidiaries.
 * Any use and/or distribution rights for this software product are provided
 * only under the Cypress Software License Agreement.
 * Any use and/or distribution of this software product not in accordance with
 * the terms of the Cypress Software License Agreement are unauthorized and
 * shall constitute an infringement of Cypress intellectual property rights.
 */
/*****************************************************************************/


/**
 * \file    cygfx_driver_api.h
 */


#ifndef CYGFX_DRIVER_API_H
#define CYGFX_DRIVER_API_H


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

/* ATTENTION: The order of the includes matters. */
/*            Don't change unless you really know what you do. */
#include "cygfx_basetypes.h"
#include "cygfx_defines.h"
#include "cygfx_erp_types.h"
#include "cygfx_errors.h"
#include "cygfx_module_id.h"
#include "cygfx_interrupthandler.h"

#include "cygfx_types.h"
#include "cygfx_sync.h"           /* Types used by nearly every module */
#include "cygfx_palette.h"
#include "cygfx_surfman.h"        /* Types used by several modules */
#ifndef GRAPHICS_PACKAGE_BASIC
#include "cygfx_capture.h"          /* Types used by several modules */
#endif
#include "cygfx_display.h"        /* Types used by several modules */
#include "cygfx_window.h"        /* Types used by several modules */
#ifndef GRAPHICS_PACKAGE_BASIC
#include "cygfx_bliteng.h"
#endif

#include "cygfx_config.h"
#ifndef GRAPHICS_PACKAGE_BASIC
#include "cygfx_draweng.h"
#endif
#include "cygfx_erp.h"
#include "cygfx_sysinit.h"
#ifndef GRAPHICS_PACKAGE_BASIC
#include "cygfx_writeback.h"
#include "cygfx_cm.h"
#endif


#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/* N/A */

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/* N/A */

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/* N/A */

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

/* N/A */

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* CYGFX_DRIVER_API_H */

