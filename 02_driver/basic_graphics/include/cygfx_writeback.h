/*******************************************************************************
 * Product: SW_TVII_VGFX_DRV
 *
 * (c) 2017-2022, Cypress Semiconductor Corporation. All rights reserved.
 *
 * Warranty and Disclaimer
 *
 * This software product is property of Cypress Semiconductor Corporation or
 * its subsidiaries.
 * Any use and/or distribution rights for this software product are provided
 * only under the Cypress Software License Agreement.
 * Any use and/or distribution of this software product not in accordance with
 * the terms of the Cypress Software License Agreement are unauthorized and
 * shall constitute an infringement of Cypress intellectual property rights.
 */
/*****************************************************************************/


/**
 * \file    cygfx_writeback.h
 */


#ifndef CYGFX_WRITEBACK_H
#define CYGFX_WRITEBACK_H


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/


#ifdef __cplusplus
extern "C"
{
#endif

/**
* \addtogroup cygfx_writeback
* \code #include "cygfx_writeback.h" \endcode
* \brief The WriteBack API allows an application to store one frame of a
*        display stream in a surface buffer using the frame dump functionality.
*
*/


/** \{ */

/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/
/**
    Dumps a single display frame into a surface.
    This function returns immediately. Use CyGfx_SyncWaitSync() to determine
    whether the frame dump is finished.

    \note
    The dump area size is determined by the surface dimensions.

    \note
    Functionality is not available if Capture-to-Surface or Capture-to-Window
    mode is used.

    \param [in] display  An ::CYGFX_DISP returned from a previous call to
                         CyGfx_DispOpenDisplay().
    \param [out] surf  The ::CYGFX_SURFACE object to save the frame
    \param [in] sync  Sync object that signals completion
    \param [in] offsetx  Horizontal offset of the dump area
    \param [in] offsety  Vertical offset of the dump area

    \retval ::CYGFX_OK On success
    \retval ::CYGFX_ERP_ERR_WB_INVALID_PARAMETER Invalid argument was passed.
    \retval ::CYGFX_ERP_ERR_DISP_CLOSED Display is closed.
    \retval ::CYGFX_ERP_ERR_BE_INVALID_TARGET Invalid surface
    \retval ::CYGFX_ERP_ERR_BE_INVALID_SURFACE_OBJECT Invalid surface 
                                                      CYGFX_BE_SURF_MASK_ALPHA.
    \retval ::CYGFX_ERP_ERR_BE_INVALID_ADDRESS Wrong address.
    \retval ::CYGFX_ERP_ERR_BE_INVALID_STRIDE Invalid stride
    \retval ::CYGFX_ERP_ERR_BE_INVALID_DIMENSION Surface dimensions are 0.
    \retval ::CYGFX_ERP_ERR_WB_DEVICE_BUSY Requested hardware is in use.
**/
CYGFX_EXTERN CYGFX_ERROR CyGfx_WbDumpFrame( CYGFX_DISP    display,
                                            CYGFX_SURFACE surf,
                                            CYGFX_SYNC    sync,
                                            CYGFX_U16     offsetx,
                                            CYGFX_U16     offsety);
/** \} end addtogroup */

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* CYGFX_WRITEBACK_H */

