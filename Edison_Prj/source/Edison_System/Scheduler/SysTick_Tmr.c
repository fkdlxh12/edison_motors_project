
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "SysTick_Tmr.h"

#include "Edison_System.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/
cy_stc_tcpwm_counter_config_t const Timer_clock =
{                           
    .period             = 16-1ul,//(520ul) - 1ul,                //  PERIOD / (CLK/prescale) = 16/15625 = 1msec (0.001024)
    .clockPrescaler     = CY_TCPWM_COUNTER_PRESCALER_DIVBY_128, //CY_TCPWM_COUNTER_PRESCALER_DIVBY_128,  // 2,000,000Hz / 128 = 15625Hz
    .runMode            = CY_TCPWM_PWM_CONTINUOUS,
    .countDirection     = CY_TCPWM_COUNTER_COUNT_UP,
    .debug_pause        = 0uL,
    .CompareOrCapture   = CY_TCPWM_COUNTER_MODE_COMPARE,    // COMPARE MOD
    .compare0           = 0,
    .compare0_buff      = 0,
    .compare1           = 0,
    .compare1_buff      = 0,
    .enableCompare0Swap = false,
    .enableCompare1Swap = false,
    .interruptSources   = 0ul,
    .capture0InputMode  = 3ul,
    .capture0Input      = 0ul,
    .reloadInputMode    = 3ul,                                 // NOT
    .reloadInput        = 0ul,
    .startInputMode     = 3ul,
    .startInput         = 0ul,
    .stopInputMode      = 3ul,
    .stopInput          = 0ul,
    .capture1InputMode  = 3ul,
    .capture1Input      = 0ul,
    .countInputMode     = 3ul,
    .countInput         = 1ul,
    .trigger1           = CY_TCPWM_COUNTER_OVERFLOW,           // OVERFLOW INT //  trigger1EventCfg
};

cy_stc_sysint_irq_t irq_cfg = 
{
    .sysIntSrc  = tcpwm_0_interrupts_0_IRQn,
    .intIdx     = CPUIntIdx3_IRQn,
    .isEnabled  = true,
};
/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
static void Timer_Handler(void);
void vTimer_Init(void);
/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/


/*******************************************************************************
* Function Name: vTimer_Init
********************************************************************************
*
* \brief 
* System Timer init
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* P_CLK=80Mhz
* SYS_CLK = 2Mhz
* TIMER_Clock = 15625Hz
* TIMER_PERIOD = about 1msec
*******************************************************************************/
void vTimer_Init(void)
{
    uint32_t periFreq = 80000000ul;
    uint32_t targetFreq = 2000000ul;
    uint32_t divNum = (periFreq / targetFreq);
    CY_ASSERT((periFreq % targetFreq) == 0ul); // inaccurate target clock

    Cy_SysClk_PeriphAssignDivider(PCLK_TCPWM0_CLOCKS0, CY_SYSCLK_DIV_16_BIT, 1);
    Cy_SysClk_PeriphSetDivider(Cy_SysClk_GetClockGroup(PCLK_TCPWM0_CLOCKS0), CY_SYSCLK_DIV_16_BIT, 1, (divNum-1ul));  
    Cy_SysClk_PeriphEnableDivider(Cy_SysClk_GetClockGroup(PCLK_TCPWM0_CLOCKS0), CY_SYSCLK_DIV_16_BIT, 1);

    Cy_SysInt_InitIRQ(&irq_cfg);
    Cy_SysInt_SetSystemIrqVector(irq_cfg.sysIntSrc, Timer_Handler);

    NVIC_SetPriority(irq_cfg.intIdx, 3ul);
    NVIC_EnableIRQ(irq_cfg.intIdx);

    Cy_Tcpwm_Counter_Init(TCPWM0_GRP0_CNT0, &Timer_clock);
    Cy_Tcpwm_Counter_Enable(TCPWM0_GRP0_CNT0);
    Cy_Tcpwm_Counter_SetTC_IntrMask(TCPWM0_GRP0_CNT0); /* Enable Interrupt */
    Cy_Tcpwm_TriggerStart(TCPWM0_GRP0_CNT0);
}


/*******************************************************************************
* Function Name: Timer_Handler
********************************************************************************
*
* \brief 
* System Timer Interrupt Functions
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* None.
*******************************************************************************/
static void Timer_Handler(void)
{
    if(Cy_Tcpwm_Counter_GetTC_IntrMasked(TCPWM0_GRP0_CNT0) == 1u)
    {
// 1msec        
        if(Edison_Data.Buzzer.Buz_OnTime_1msec!=0)
        {
            Edison_Data.Buzzer.Buz_OnTime_1msec--;
        }
        Edison_Data.Timer.m_1msec_flag=1;
// 10msec
        Edison_Data.Timer.m_10msec++;
        if(Edison_Data.Timer.m_10msec>9)
        {
            Edison_Data.Timer.m_10msec=0;
            Edison_Data.Timer.m_10msec_flag=1;
        }    
// 100msec 
        Edison_Data.Timer.m_100msec++;
        if(Edison_Data.Timer.m_100msec>99)
        {
            if(Edison_Data.Can.Status.Can_Response_Time!=0)
            {
                Edison_Data.Can.Status.Can_Response_Time--; // 1000msec        
            }
            Edison_Data.Timer.m_100msec=0;
            Edison_Data.Timer.m_100msec_flag=1;
        }         
        Cy_Tcpwm_Counter_ClearTC_Intr(TCPWM0_GRP0_CNT0);
    }
}


/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/

