/*
 * File: EdisionMotors_HmiCan.c
 *
 * Code generated for Simulink model 'EdisionMotors_HmiCan'.
 *
 * Model version                  : 1.18
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Mon Jan 17 19:02:23 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Infineon->TriCore
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */
#include "stdio.h"
#include "EdisionMotors_HmiCan.h"
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

/* Skipping ulong_long/long_long check: insufficient preprocessor integer range. */

/* Block signals and states (default storage) */
DW rtDW;

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
static RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;

/* Model step function */
void EdisionMotors_HmiCan_step(void)
{
  /* S-Function (scanunpack): '<Root>/CAN Unpack' incorporates:
   *  Inport: '<Root>/Input'
   *  Outport: '<Root>/Output'
   *  Outport: '<Root>/Output1'
   *  Outport: '<Root>/Output10'
   *  Outport: '<Root>/Output11'
   *  Outport: '<Root>/Output12'
   *  Outport: '<Root>/Output13'
   *  Outport: '<Root>/Output14'
   *  Outport: '<Root>/Output15'
   *  Outport: '<Root>/Output2'
   *  Outport: '<Root>/Output3'
   *  Outport: '<Root>/Output4'
   *  Outport: '<Root>/Output5'
   *  Outport: '<Root>/Output6'
   *  Outport: '<Root>/Output7'
   *  Outport: '<Root>/Output8'
   *  Outport: '<Root>/Output9'
   *  Outport: '<Root>/Output18'
   *  Outport: '<Root>/Output19'
   *  Outport: '<Root>/Output20'
   *  Outport: '<Root>/Output21'
   *  Outport: '<Root>/Output22'
   *  Outport: '<Root>/Output23'
   */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack' */
    if ((8 == rtU.Autonomous_Display_INFO.Length) &&
        (rtU.Autonomous_Display_INFO.ID != INVALID_CAN_ID) ) {
      if ((419419631 == rtU.Autonomous_Display_INFO.ID) && (1U ==
           rtU.Autonomous_Display_INFO.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 4
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0x10U)) >>
                    4);
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output = result;
            }
          }

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 40
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[5]);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output1 = result;
            }
          }

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 0
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0x1U));
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output2 = result;
            }
          }

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 37
           *  length                  = 3
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[4]) & (uint8_T)(0xE0U)) >>
                    5);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output3 = result;
            }
          }

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 12
           *  length                  = 3
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0x70U)) >>
                    4);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output4 = result;
            }
          }

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 6
           *  length                  = 2
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0xC0U)) >>
                    6);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output5 = result;
            }
          }

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 10
           *  length                  = 2
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0xCU)) >>
                    2);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output6 = result;
            }
          }

          /* --------------- START Unpacking signal 7 ------------------
           *  startBit                = 9
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0x2U)) >>
                    1);
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output7 = result;
            }
          }

          /* --------------- START Unpacking signal 8 ------------------
           *  startBit                = 8
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0x1U));
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output8 = result;
            }
          }

          /* --------------- START Unpacking signal 9 ------------------
           *  startBit                = 48
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[6]) & (uint8_T)(0x1U));             
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output9 = result;
            }
          }          

          /* --------------- START Unpacking signal 10 ------------------
           *  startBit                = 1
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0x2U)) >>
                    1);
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output10 = result;
            }
          }

          /* --------------- START Unpacking signal 11 ------------------
           *  startBit                = 5
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0x20U)) >>
                    5);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output11 = result;
            }
          }

          /* --------------- START Unpacking signal 12 ------------------
           *  startBit                = 2
           *  length                  = 2
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0xCU)) >>
                    2);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              rtY.Output12 = result;
            }
          }

          /* --------------- START Unpacking signal 13 ------------------
           *  startBit                = 16
           *  length                  = 16
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 0.5
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            real64_T outValue = 0;

            {
              uint16_T unpackedValue = 0;

              {
                uint16_T tempValue = (uint16_T) (0);

                {
                  tempValue = tempValue | (uint16_T)
                    (rtU.Autonomous_Display_INFO.Data[2]);
                  tempValue = tempValue | (uint16_T)((uint16_T)
                    (rtU.Autonomous_Display_INFO.Data[3]) << 8);
                }

                unpackedValue = tempValue;
              }

              outValue = (real64_T) (unpackedValue);
            }

            {
              real64_T result = (real64_T) outValue;
              result = result * 0.5;
              rtY.Output13 = result;
            }
          }

          /* --------------- START Unpacking signal 14 ------------------
           *  startBit                = 32
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[4]) & (uint8_T)(0x1U));
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              boolean_T result = (boolean_T) outValue;
              rtY.Output14 = result;
            }
          }

          /* --------------- START Unpacking signal 15 ------------------
           *  startBit                = 33
           *  length                  = 4
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 10.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[4]) & (uint8_T)(0x1EU)) >>
                    1);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result * 10U;
              rtY.Output15 = result;
            }
          }
          /* --------------- START Unpacking signal 16 ------------------
           *  startBit                = 49
           *  length                  = 7
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[6]) & (uint8_T)(0xFEU)) >>
                    1);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result;
              rtY.Output16 = result;
            }
          }
          /* --------------- START Unpacking signal 17 ------------------
           *  startBit                = 56
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)(rtU.Autonomous_Display_INFO.Data[7]);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result ;
              rtY.Output17 = result;
            }
          }
        }
      }
    }
  }

  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack' */
    if ((8 == rtU.Autonomous_Display_INFO.Length) &&
        (rtU.Autonomous_Display_INFO.ID != INVALID_CAN_ID) ) {
      if ((419419375 == rtU.Autonomous_Display_INFO.ID) && (1U ==
           rtU.Autonomous_Display_INFO.Extended) ) {
        {
          /* --------------- START Unpacking signal 18 ------------------
           *  startBit                = 0
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)(rtU.Autonomous_Display_INFO.Data[0]);
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result ;
              rtY.Output18 = result;
            }
          }

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 8
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                    (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0xFEU)) >>
                    1);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result;
              rtY.Output19 = result;
            }
          }

          /* --------------- START Unpacking signal HMI_Reset ------------------
           *  startBit                = 14
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          // {
          //   uint8_T outValue = 0;

          //   {
          //     uint8_T unpackedValue = 0;

          //     {
          //       uint8_T tempValue = (uint8_T) (0);

          //       {
          //         // tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
          //         //   (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0x2U)) >>
          //         //   1);

          //         tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
          //           (rtU.Autonomous_Display_INFO.Data[1]) & (uint8_T)(0x40U)) >>
          //           6);
          //       }

          //       unpackedValue = tempValue;
          //     }

          //     outValue = (uint8_T) (unpackedValue);
          //   }

          //   {
          //     uint8_T result = (uint8_T) outValue;
          //     rtY.Output23 = result;
          //   }
          // }

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 16
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)(rtU.Autonomous_Display_INFO.Data[2]);
                }

                unpackedValue = tempValue;
              }

              outValue = (boolean_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result ;
              rtY.Output20 = result;
            }
          }

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 24
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            uint8_T outValue = 0;

            {
              uint8_T unpackedValue = 0;

              {
                uint8_T tempValue = (uint8_T) (0);

                {
                  tempValue = tempValue | (uint8_T)(rtU.Autonomous_Display_INFO.Data[3]);
                }

                unpackedValue = tempValue;
              }

              outValue = (uint8_T) (unpackedValue);
            }

            {
              uint8_T result = (uint8_T) outValue;
              result = result ;
              rtY.Output21 = result;
            }
          }

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 32
           *  length                  = 32
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            // real64_T outValue = 0;
            uint32_T outValue = 0;

            {
              uint32_T unpackedValue = 0;

              {
                uint32_T tempValue = (uint32_T) (0);

                // printf("Get Data 4 %02x\r\n", rtU.Autonomous_Display_INFO.Data[4]);
                // printf("Get Data 5 %02x\r\n", rtU.Autonomous_Display_INFO.Data[5]);
                // printf("Get Data 6 %02x\r\n", rtU.Autonomous_Display_INFO.Data[6]);
                // printf("Get Data 7 %02x\r\n", rtU.Autonomous_Display_INFO.Data[7]);

                {
                  tempValue = tempValue | (uint32_T)
                    (rtU.Autonomous_Display_INFO.Data[4]);
                  tempValue = tempValue | (uint32_T)((uint32_T)
                    (rtU.Autonomous_Display_INFO.Data[5]) << 8);
                  tempValue = tempValue | (uint32_T)((uint32_T)
                    (rtU.Autonomous_Display_INFO.Data[6]) << 16);
                  tempValue = tempValue | (uint32_T)((uint32_T)
                    (rtU.Autonomous_Display_INFO.Data[7]) << 24);
                }

                unpackedValue = tempValue;
                // printf("Get tempValue %lu\r\n", tempValue);
                // printf("Get tempValue %lu\r\n", (uint32_T)tempValue);
                // printf("Get tempValue %lu\r\n", (int32_T)tempValue);
                // printf("Get tempValue %lu\r\n", (real64_T)tempValue);
              }

              // outValue = (real64_T) (unpackedValue);
              outValue = (uint32_T) (unpackedValue);
            }

            {
              // real64_T result = (real64_T) outValue;
              uint32_T result = (uint32_T) outValue;              
              // result = result * 0.1;
              rtY.Output22 = result;
              // printf("Get Data %f\r\n", rtY.Output22);
            }            
          }
        }
      }
    }
  }

  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack' */
    if ((8 == rtU.Autonomous_Display_INFO.Length) &&
        (rtU.Autonomous_Display_INFO.ID != INVALID_CAN_ID) ) {
      if ((2030 == rtU.Autonomous_Display_INFO.ID) && (0U ==
            rtU.Autonomous_Display_INFO.Extended) ) {
        {
            /* --------------- START Unpacking signal HMI_Reset ------------------
            *  startBit                = 14
            *  length                  = 1
            *  desiredSignalByteLayout = LITTLEENDIAN
            *  dataType                = UNSIGNED
            *  factor                  = 1.0
            *  offset                  = 0.0
            * -----------------------------------------------------------------------*/
            {
              uint8_T outValue = 0;

              {
                uint8_T unpackedValue = 0;

                {
                  uint8_T tempValue = (uint8_T) (0);

                  {
                    tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                      (rtU.Autonomous_Display_INFO.Data[0]) & (uint8_T)(0x01U)));
                  }

                  unpackedValue = tempValue;
                }

                outValue = (uint8_T) (unpackedValue);
              }

              {
                uint8_T result = (uint8_T) outValue;
                rtY.Output23 = result;
              }
            }
        }
      }
    }
  }
}

/* Model initialize function */
void EdisionMotors_HmiCan_initialize(void)
{
  /* Start for S-Function (scanunpack): '<Root>/CAN Unpack' incorporates:
   *  Inport: '<Root>/Input'
   *  Outport: '<Root>/Output'
   *  Outport: '<Root>/Output1'
   *  Outport: '<Root>/Output10'
   *  Outport: '<Root>/Output11'
   *  Outport: '<Root>/Output12'
   *  Outport: '<Root>/Output13'
   *  Outport: '<Root>/Output14'
   *  Outport: '<Root>/Output15'
   *  Outport: '<Root>/Output2'
   *  Outport: '<Root>/Output3'
   *  Outport: '<Root>/Output4'
   *  Outport: '<Root>/Output5'
   *  Outport: '<Root>/Output6'
   *  Outport: '<Root>/Output7'
   *  Outport: '<Root>/Output8'
   *  Outport: '<Root>/Output9'
   */

  /*-----------S-Function Block: <Root>/CAN Unpack -----------------*/
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
