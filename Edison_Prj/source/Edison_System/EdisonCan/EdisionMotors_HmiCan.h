/*
 * File: EdisionMotors_HmiCan.h
 *
 * Code generated for Simulink model 'EdisionMotors_HmiCan'.
 *
 * Model version                  : 1.18
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Mon Jan 17 19:02:23 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Infineon->TriCore
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_EdisionMotors_HmiCan_h_
#define RTW_HEADER_EdisionMotors_HmiCan_h_
#ifndef EdisionMotors_HmiCan_COMMON_INCLUDES_
#define EdisionMotors_HmiCan_COMMON_INCLUDES_
#include <string.h>
#include "rtwtypes.h"
#include "BSP_Can.h"
#endif                               /* EdisionMotors_HmiCan_COMMON_INCLUDES_ */

/* Model Code Variants */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* user code (top of export header file) */
/* Block signals and states (default storage) for system '<Root>' */
typedef struct {
  int_T CANUnpack_ModeSignalID;        /* '<Root>/CAN Unpack' */
  int_T CANUnpack_StatusPortID;        /* '<Root>/CAN Unpack' */
} DW;

// ADD Struct
typedef struct 
{
    uint8_T  Length;
    uint32_T ID;
    uint8_T  Data[8];
    uint8_T  Extended;
}_Autonomous_Display_INFO_;
///


/* External inputs (root inport signals with default storage) */
typedef struct {
  _Autonomous_Display_INFO_ Autonomous_Display_INFO;    /* '<Root>/Input' */
} ExtU;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  boolean_T Output;                    /* '<Root>/Output' */
  uint8_T Output1;                     /* '<Root>/Output1' */
  boolean_T Output2;                   /* '<Root>/Output2' */
  uint8_T Output3;                     /* '<Root>/Output3' */
  uint8_T Output4;                     /* '<Root>/Output4' */
  uint8_T Output5;                     /* '<Root>/Output5' */
  uint8_T Output6;                     /* '<Root>/Output6' */
  boolean_T Output7;                   /* '<Root>/Output7' */
  boolean_T Output8;                   /* '<Root>/Output8' */
  boolean_T Output9;                    /* '<Root>/Output9' */
  boolean_T Output10;                  /* '<Root>/Output10' */
  uint8_T Output11;                    /* '<Root>/Output11' */
  uint8_T Output12;                    /* '<Root>/Output12' */
  real_T Output13;                     /* '<Root>/Output13' */
  boolean_T Output14;                  /* '<Root>/Output14' */
  uint8_T Output15;                    /* '<Root>/Output15' */
  uint8_T Output16;                    /* '<Root>/Output16' */
  uint8_T Output17;                    /* '<Root>/Output17' */

  real_T Output18;                    /* '<Root>/Output18' */
  real_T Output19;                    /* '<Root>/Output19' */
  real_T Output20;                    /* '<Root>/Output20' */
  real_T Output21;                    /* '<Root>/Output21' */
  uint32_T Output22;                  /* '<Root>/Output22' */
  boolean_T Output23;                 /* '<Root>/Output23' */
} ExtY;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;
};

/* Block signals and states (default storage) */
extern DW rtDW;

/* External inputs (root inport signals with default storage) */
extern ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY rtY;

/* Model entry point functions */
extern void EdisionMotors_HmiCan_initialize(void);
extern void EdisionMotors_HmiCan_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'EdisionMotors_HmiCan'
 */

extern ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY rtY;

#endif                                 /* RTW_HEADER_EdisionMotors_HmiCan_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
