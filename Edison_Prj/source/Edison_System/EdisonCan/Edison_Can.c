
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "stdio.h"

#include "cy_project.h"
#include "cy_device_headers.h"
#include "Edison_System.h"
#include "BSP_Can.h"
#include "Device_Driver.h"

#include "EdisionMotors_HmiCan.h"
#include "rtwtypes.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/

/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
static void vEdison_Can_Data_Status_Passing(void);
static void vEdison_Can_Err_Buzzer(void);
void vEdison_Can_Task(void);

static uint32_t Aebs_Cnt=0;

// uint8_t prev_HMI_reset = 0;

/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: vEdison_Can_Data_Status_Passing
********************************************************************************
*
* \brief 
* Peripheral CAN Data Passing
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* None.
*******************************************************************************/
static void vEdison_Can_Data_Status_Passing(void)
{
    uint8_t cnt=0;
    uint8_t flag=CAN_CONNECT;

    for(cnt=0;cnt<1;cnt++)
    {
        if(Peri_Can_Info[cnt].Status_Info.Bus_Input_Flag==1)
        {
            rtU.Autonomous_Display_INFO.ID=Peri_Can_Info[cnt].Data_Info.CAN_ID;
            rtU.Autonomous_Display_INFO.Length=Peri_Can_Info[cnt].Data_Info.CAN_Length;
            rtU.Autonomous_Display_INFO.Extended=Peri_Can_Info[cnt].Data_Info.CAN_Extended_Type;
            rtU.Autonomous_Display_INFO.Data[0]=(Peri_Can_Info[cnt].Data_Info.CAN_Data[0]) & 0xff;
            rtU.Autonomous_Display_INFO.Data[1]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[0]) & 0xff00)>>8;
            rtU.Autonomous_Display_INFO.Data[2]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[0]) & 0xff0000)>>16;
            rtU.Autonomous_Display_INFO.Data[3]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[0]) & 0xff000000)>>24;
            rtU.Autonomous_Display_INFO.Data[4]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[1]) & 0xff);
            rtU.Autonomous_Display_INFO.Data[5]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[1]) & 0xff00)>>8;
            rtU.Autonomous_Display_INFO.Data[6]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[1]) & 0xff0000)>>16;
            rtU.Autonomous_Display_INFO.Data[7]=((Peri_Can_Info[cnt].Data_Info.CAN_Data[1]) & 0xff000000)>>24;

            // printf("%lx\r\n", rtU.Autonomous_Display_INFO.ID);

            EdisionMotors_HmiCan_step();
            // printf("EvcuOdometer %f\r\n", rtY.Output22);
            // memcpy(&Edison_Data.Can.Data[cnt].ActivaionAvailable_Status,&rtY,sizeof(Edison_Data.Can.Data[cnt]));  
            memcpy(&Edison_Data.Can.Data[cnt],&rtY,sizeof(Edison_Data.Can.Data[cnt])); 
            Edison_Data.Can.Data[cnt].EvcuOdometer = rtY.Output22;
            Edison_Data.Can.Data[cnt].HMI_Reset = rtY.Output23;

            // Reset HMI Milleage
            // if (prev_HMI_reset != Edison_Data.Can.Data[cnt].HMI_Reset)
            // {
            //     if (Edison_Data.Can.Data[cnt].HMI_Reset == 1)
            //     {
            //         printf("%d\r\n", Edison_Data.Can.Data[cnt].HMI_Reset);
            //         prev_HMI_reset = Edison_Data.Can.Data[cnt].HMI_Reset;
            //         uint8_t testData1[4] = {0};
            //         writeToFlash(CY_WFLASH_SM_SBM_TOP, (uint32_t*)(testData1));
            //         global_totalOdometer = readFromFlash();
            //         // NVIC_SystemReset();
            //     }
            // }
            
            // printf("EvcuOdometer %f\r\n", Edison_Data.Can.Data[cnt].EvcuOdometer);  
            Peri_Can_Info[cnt].Status_Info.Bus_Input_Flag=0;   

            Edison_Data.Can.Status.Can_Response_Time=10;
        }
    }

// Can Response Time
    if(Edison_Data.Can.Status.Can_Response_Time==0)
    {
        flag=CAN_DISCONNECT;
    }

    if(flag!=Edison_Data.Can.Status.Connect_Status)
    {
        if(flag==CAN_DISCONNECT)
        {
            memset(&Edison_Data.Can.Data[0].ActivaionAvailable_Status,0,sizeof(Edison_Data.Can.Data[0]));
            memset(&Edison_Data.Can.Data[1].ActivaionAvailable_Status,0,sizeof(Edison_Data.Can.Data[1]));

            memset(&Edison_Data.Can.Data[0].Sensor_Fail_Info,1,sizeof(Edison_Data.Can.Data[0])); 
        }
        Edison_Data.Can.Status.Connect_Status=flag;
    }    
}


/*******************************************************************************
* Function Name: vEdison_Can_Err_Buzzer
********************************************************************************
*
* \brief 
* Edison Can Data Error Input -> Buzzer Active
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* None.
*******************************************************************************/

static void vEdison_Can_Err_Buzzer(void)
{
    uint8_t Buz_Type=0;
    uint8_t count=0;

    for(count=0;count<1;count++)
    {
        if(Edison_Data.Can.Data[count].AebsActive==0x1) // AebsActive
        { 
            Buz_Type=BUZ_MIX;                
            break;
        }            
        else if(Edison_Data.Can.Data[count].SW_Estop==1) // SW_ESTOP
        {
            Buz_Type=BUZ_WARN;
            break;
        }
        else if(Edison_Data.Can.Data[count].Takeover_Status==0x03) // Takeover
        {
            Buz_Type=BUZ_CAUTION;
            break;            
        }
        else if(Edison_Data.Can.Data[count].ActivaionAvailable_Status==0x01) // ActivaionAvailable_Status
        {
            Buz_Type=BUZ_CAUTION;
            break;            
        }
        else if(Edison_Data.Can.Data[count].Sensor_Fail_Info==0x01) // Sensor Error
        {
            Buz_Type=BUZ_CAUTION;        
            break;            
        }      
        
    }

    if(Buz_Type!=0)
    {
        if (Buz_Type == BUZ_MIX)
            Aebs_Cnt++;
        else
            Aebs_Cnt = 0;
        
        Edison_Data.Buzzer.Onoff=BUZ_ON;
        if (Aebs_Cnt > 800) {
            Edison_Data.Buzzer.Onoff=BUZ_OFF;
            Buz_Type = BUZ_OFF;
        }

        if(Edison_Data.Buzzer.Buz_State_Temp!=Buz_Type)
        {
            Edison_Data.Buzzer.Buz_State=Buz_Type;
        }
    }
    else
    {
        Aebs_Cnt = 0;
        Edison_Data.Buzzer.Onoff=BUZ_OFF;
    }
}


/*******************************************************************************
* Function Name: vEdison_Can_Task
********************************************************************************
*
* \brief 
* Edison Can Data Processing
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* None.
*******************************************************************************/
void vEdison_Can_Task(void)
{
    vEdison_Can_Data_Status_Passing();
    vEdison_Can_Err_Buzzer();
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/

