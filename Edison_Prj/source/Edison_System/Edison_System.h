#ifndef _DABO_EDISON_SYSTEM_H_
#define _DABO_EDISON_SYSTEM_H_


/*****************************************************************************/
/*                                VERSION                                     /
/*****************************************************************************/
#define HW_VER  "v0.0.1"
#define SW_VER  "v0.0.1"

/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
// Peripheral
#include "Dabo_HW_Init.h"

// Driver
#include "Device_Driver.h"

// Edison System
#include "SysTick_Tmr.h"
/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/
#define CAN_CONNECT     0
#define CAN_DISCONNECT  1
/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/
typedef struct {
  unsigned char ActivaionAvailable_Status;
  unsigned char Alive_Count;
  unsigned char Driving_Mode;
  unsigned char Front_Object;
  unsigned char GPS_Quality;
  unsigned char Gear_Pos_Disp;
  unsigned char Lane_Change_Det;
  unsigned char Lane_Change_State;
  unsigned char Lane_Det_Mode;
  unsigned char AebsActive;
  unsigned char SW_Estop;
  unsigned char Sensor_Fail_Info;
  unsigned char Takeover_Status;
  double Vehicle_Speed_Disp;
  unsigned char Vehicle_Speed_Limit_Info;
  unsigned char Vehicle_Speed_Limit_Info_Speed;
  unsigned short Reserve;
  unsigned short Reserve1;
  unsigned short Reserve2;
  unsigned short Reserve3;
  unsigned short Reserve4;
  unsigned short Reserve5;
  uint32_t EvcuOdometer;
  unsigned char HMI_Reset; // txcao hmi reset CAN message_220720
}Edison_Can_Data;

typedef struct {
  unsigned char Can_Response_Time;
  unsigned char Connect_Status;
}Edison_Can_Status;

typedef struct{
    Edison_Can_Data     Data[2];
    Edison_Can_Status   Status;
}Edison_Can_Info;

typedef struct
{   
    Edison_Can_Info    Can;
    Edison_Buzzer_Info Buzzer;
    Edison_Timer       Timer;
}Edison_Info_Data;

/*****************************************************************************/
/***                                EXTERN VARIABLE                        ***/
/*****************************************************************************/
extern Edison_Info_Data Edison_Data;
/*****************************************************************************/
/***                                EXTERN FUNCTIONS                       ***/
/*****************************************************************************/
extern void vEdsion_System_Init(void);
extern void vEdsion_System_Task_Scheduler(void);
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/

#endif

