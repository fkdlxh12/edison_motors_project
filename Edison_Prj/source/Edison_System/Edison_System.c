
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "Edison_System.h"
#include "cygfx_driver_api.h"

// Edison System Headers
#include "SysTick_Tmr.h"
#include "Edison_Can.h"
#include "Edison_Graphics.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/
Edison_Info_Data Edison_Data;
/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
void Edsion_System_Init(void);
void Edison_System_Task(void);
static void vEdison_System_1msec_Task(void);
static void vEdison_System_10msec_Task(void);
void vEdsion_System_Task_Scheduler(void);
/*****************************************************************************/
/***                                FUNCTIONS                              ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: vEdsion_System_Init
********************************************************************************
*
* \brief 
* Edison Motors System Initial ( Driver / Preipheral / Graphics / etc)
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* None.
*******************************************************************************/
void vEdsion_System_Init(void)
{
    // Edison System Data Clear
    memset(&Edison_Data,0,sizeof(Edison_Data));

    // Edison Driver & HW Init
    vEdison_HW_Init();
    vDevice_Driver_Init();
    vEdison_Graphics_Init();
    vTimer_Init(); // Must Graphics Drawing Time Set ( Scheduler\SysTick_tmr.h)
}
/*******************************************************************************
* Function Name: vEdison_System_1msec_Task
********************************************************************************
*
* \brief 
* Edison Motors System Scheduler 1msec
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* about 1msec System Timer
*******************************************************************************/
static void vEdison_System_1msec_Task(void)
{
    vPeripheral_Fault_Task();
    vEdison_Can_Task();
    vEdison_Graphics_Task();
    vBuzzer_Task();
}
/*******************************************************************************
* Function Name: vEdison_System_10msec_Task
********************************************************************************
*
* \brief 
* Edison Motors System Scheduler 10msec
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* about 10msec
*******************************************************************************/
static void vEdison_System_10msec_Task(void)
{
    vUserLed_Flip();
}
/*******************************************************************************
* Function Name: vEdsion_System_Task_Scheduler
********************************************************************************
*
* \brief 
* Edison Motors System Scheduler
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* about 1msec System Timer
*******************************************************************************/
void vEdsion_System_Task_Scheduler(void) // Scheduler
{   
    if(Edison_Data.Timer.m_1msec_flag==1)
    {
        Edison_Data.Timer.m_1msec_flag=0;
        vEdison_System_1msec_Task();
    }

    if(Edison_Data.Timer.m_10msec_flag==1)
    {
        Edison_Data.Timer.m_10msec_flag=0;
        vEdison_System_10msec_Task();
    }      
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/

