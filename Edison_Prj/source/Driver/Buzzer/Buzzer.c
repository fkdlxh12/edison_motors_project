
/*****************************************************************************/
/*                                NOTICE                                      /
 *      Version : V0.0.1                                                      /
 *      Author : Dabo-Coperation ( Kang In Jun )                              /
 *                                                                            /
/*****************************************************************************/


/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "BSP_Pwm.h"
#include "Buzzer.h"

#include "Edison_System.h"
#include "Edison_Can.h"


/*****************************************************************************/
/***                                OPTION DEFINED                         ***/
/*****************************************************************************/
#define BUZ_WARN_DUTY_VAR      (0x800)
#define BUZ_CAUTION_DUTY_VAR   (0x500)
#define BUZ_NO_DUTY            (0x0)

#define BUZ_ACTIVE_MS_TIME     (250) // ms

/*****************************************************************************/
/***                                Variables                              ***/
/*****************************************************************************/

/*****************************************************************************/
/***                             Function List                             ***/
/*****************************************************************************/
void vBuzzer_Init(void);
void vBuzzer_Task(void);
/*****************************************************************************/
/***                               FUNCTIONS                               ***/
/*****************************************************************************/
void vBuzzer_Init(void)
{
    vPWM_Control(BUZ_OFF,BUZ_NO_DUTY);
}
/*****************************************************************************/
static void vBuzzer_Activation(void)
{
    if(Edison_Data.Buzzer.Old_Onoff_State!=Edison_Data.Buzzer.Onoff)
    {
        Edison_Data.Buzzer.Old_Onoff_State=Edison_Data.Buzzer.Onoff;

        if(Edison_Data.Buzzer.Old_Onoff_State==BUZ_OFF)
        {
            vPWM_Control(BUZ_OFF,BUZ_NO_DUTY);   
            Edison_Data.Buzzer.Buz_OnTime_1msec=0;
            Edison_Data.Buzzer.Buz_State=BUZ_OFF;
        }
    }
    else if(Edison_Data.Buzzer.Old_Onoff_State==Edison_Data.Buzzer.Onoff)
    {
        if(Edison_Data.Buzzer.Old_Onoff_State>BUZ_OFF)
        {
            if(Edison_Data.Buzzer.Buz_OnTime_1msec==0)
            {
                Edison_Data.Buzzer.Buz_OnTime_1msec=BUZ_ACTIVE_MS_TIME;

                switch(Edison_Data.Buzzer.Buz_State)
                {
                    case BUZ_MIX:

                    vPWM_Control(BUZ_ON, 0x1000); // (0x8000 = 100% dutycycle)
                    Edison_Data.Buzzer.Buz_State_Temp=Edison_Data.Buzzer.Buz_State;
                    // Edison_Data.Buzzer.Buz_State=BUZ_MIX;
                    break;
                    case BUZ_CAUTION:
                    vPWM_Control(BUZ_ON,BUZ_CAUTION_DUTY_VAR);
                    Edison_Data.Buzzer.Buz_State_Temp=Edison_Data.Buzzer.Buz_State;
                    Edison_Data.Buzzer.Buz_State=BUZ_OFF;
                    break;

                    case BUZ_WARN:
                    vPWM_Control(BUZ_ON,BUZ_WARN_DUTY_VAR);
                    Edison_Data.Buzzer.Buz_State_Temp=Edison_Data.Buzzer.Buz_State;
                    Edison_Data.Buzzer.Buz_State=BUZ_OFF;
                    break;

                    case BUZ_OFF:
                    vPWM_Control(BUZ_OFF,BUZ_NO_DUTY);
                    Edison_Data.Buzzer.Buz_State=Edison_Data.Buzzer.Buz_State_Temp;
                    break;

                    default:
                    vPWM_Control(BUZ_OFF,BUZ_NO_DUTY);
                    break;
                }                
            }
        }
    }
}

void vBuzzer_Task(void)
{
    vBuzzer_Activation();
}

/* [] END OF FILE */
