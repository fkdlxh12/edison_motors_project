#ifndef __DABO_DRV_TOUCH_PANEL_H_
#define __DABO_DRV_TOUCH_PANEL_H_


/*****************************************************************************/
/*                                NOTICE                                      /
 *      Version : V0.0.1                                                      /
 *      Author : Dabo-Coperation ( Kang In Jun )                              /
 *                                                                            /
/*****************************************************************************/

/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "BSP_I2c.h"
/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/

#define TOUCH_PANEL_DRIVER_UNUSE    0
#define TOUCH_PANEL_DRIVER_USE      1

#define TOUCH_PANEL_DRIVER_STAT     TOUCH_PANEL_DRIVER_UNUSE 


#define FT5436_Write(id,addr,data)     Scb_I2C_Byte_Write(id,addr,data)
#define FT5436_Read(id,addr,data,cnt)  Scb_I2C_Bytes_Receive(id,addr,data,cnt)


#define TOUCH_I2C_ADDR          0x38 // or 0xB8
#define MAX_TOUCH_POINT         5

// Setting Point
#define TOUCH_DEV_MOD           0x00
#define TOUCH_GEST_ID           0x01
#define TOUCH_CNT_POINT         0x02
#define ID_G_THGROUP            0x80 
#define ID_G_THPEAK             0x81
#define ID_G_THCAL              0x82
#define ID_G_THWATER            0x83
#define ID_G_THTEMP             0x84
#define ID_G_THDIFF             0x85   
#define ID_G_CTRL               0x86 // Power Mod
#define ID_G_TIME_ENTER_MONITOR 0x87
#define ID_G_PERIODACTIVE       0x88
#define ID_G_PERIOD_MONITOR     0x89

#define ID_G_AUTO_CLB_MODE      0xA0
#define ID_G_LIB_VERSION_H      0xA1
#define ID_G_LIB_VERSION_L      0xA2
#define ID_G_CIPHER             0xA3
#define ID_G_MODE               0xA4
#define ID_G_PMODE              0xA5
#define ID_G_FIRMID             0xA6
#define ID_G_STATE              0xA7
#define ID_G_FT5201ID           0xA8
#define ID_G_ERR                0xA9
#define ID_G_CLB                0xAA

#define ID_G_B_AREA_TH          0xAE
#define LOG_MSG_CNT             0xFE
#define LOG_CUR_CHA             0xFF

// Touch Read Point
#define TOUCHn_XH(n)            (0x03+(0x06*n))
#define TOUCHn_XL(n)            (0x04+(0x06*n))
#define TOUCHn_YH(n)            (0x05+(0x06*n))
#define TOUCHn_YL(n)            (0x06+(0x06*n))
/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/

typedef enum {
    OPER_MOD=0,
    TEST_MOD=0x40,
    SYS_INFO_MOD=0x10
}FT5436_Oper_Mod;

typedef enum 
{
    ACTIVE_MOD=0,
    MONITOR_MOD=0x01,
    DEEPSLEEP_MOD=0x03
}FT5436_Power_Mod;

typedef enum 
{
    FT5436_OFF=0,
    FT5436_ON=0x40,
}FT5436_ONOFF;

typedef enum 
{
    NO_GESTURE=0x00,
    MOVE_UP=0x10,
    MOVE_LEFT=0x14,
    MOVE_DOWN=0x18,
    MOVE_RIGHT=0x1C,
    ZOOM_IN=0x48,
    ZOOM_OUT=0x49,
}FT5436_Gesture_Type;

typedef enum 
{
    FT5436_INTERRUPT_MOD=0,
    FT5436_POLLING_MOD=1, // Read Address -> Write Access err
}FT5436_Interrupt_Type;

typedef enum 
{
    FT5436_OK=0,
    FT5436_REG_DIFF_I2C_TYPE=0x03, // Read Address -> Write Access err
    FT5436_CHIP_START_FAIL = 0x05,
    FT5436_NO_MATCH_INPUT  = 0x1A,
}FT5436_Err_Code_Type;

typedef struct
{   
    unsigned char  connect_status;
    unsigned char  gesture;
    unsigned char  err_code;
    unsigned short coord_x[MAX_TOUCH_POINT];
    unsigned short coord_y[MAX_TOUCH_POINT];
    unsigned char  evt_flag[MAX_TOUCH_POINT];
    unsigned char  touch_id[MAX_TOUCH_POINT];
}__attribute__((packed)) Touch_Info;
/*****************************************************************************/
/***                                EXTERN VARIABLE                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                EXTERN FUNCTIONS                       ***/
/*****************************************************************************/
extern void vTouch_Panel_Init(void);
extern void vTouch_Panel_Task(void);
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/
#endif
