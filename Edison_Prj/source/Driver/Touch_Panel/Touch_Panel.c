
/*****************************************************************************/
/*                                NOTICE                                      /
 *      Version : V0.0.1                                                      /
 *      Author : Dabo-Coperation ( Kang In Jun )                              /
 *                                                                            /
/*****************************************************************************/

/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "BSP_I2c.h"
#include "Touch_Panel.h"

#define TOUCH_DEBUG_UART  0

#if (TOUCH_DEBUG_UART==1)
    #include "stdio.h"
#endif
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/
Touch_Info Touch_Status;
/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
void vTouch_Panel_Task(void);
void vTouch_Panel_Init(void);
static void FT5436_IC_ERR_DET(void);
static void FT5436_Touch_Data_Read(void);
static void FT5436_Init_Register(void);
/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: FT5436_Init_Register
********************************************************************************
*
* \brief 
* Touch Panel Initial I2C Settings
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* None.
*******************************************************************************/
static void FT5436_Init_Register(void)
{
    FT5436_Write(TOUCH_I2C_ADDR,TOUCH_DEV_MOD,(FT5436_Oper_Mod)OPER_MOD);
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_PMODE,(FT5436_Power_Mod)ACTIVE_MOD); 
    // Threshold
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_THGROUP,0x2e); // Touch Detect threshold
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_THPEAK,0x3c);  // Touch Peak Detect Threshold
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_THCAL,0x10);   // the threshold when calculating the focus of touching
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_THWATER,0x3c); // the threshold when there is surface water.
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_THTEMP,0x0a);  // the threshold of temperature compensation.
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_THDIFF,0x50);  // the threshold of temperature compensation.
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_B_AREA_TH,0x0c);  // the threshold of temperature compensation.
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_PERIODACTIVE,0x0c); // This register describes the period of active status
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_AUTO_CLB_MODE,(FT5436_ONOFF)FT5436_OFF);
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_MODE,(FT5436_Interrupt_Type)FT5436_POLLING_MOD); 
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_TIME_ENTER_MONITOR,2); // Touch Detect threshold
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_PERIODACTIVE,12);  // Touch Peak Detect Threshold
    FT5436_Write(TOUCH_I2C_ADDR,ID_G_PERIOD_MONITOR,40);  // Touch Peak Detect Threshold
}

/*******************************************************************************
* Function Name: FT5436_Touch_Data_Read
********************************************************************************
*
* \brief 
* Touch Panel Gesture & Touch X, Y Data Read
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* None.
*******************************************************************************/
static void FT5436_Touch_Data_Read(void)
{
    uint8_t read_cnt=0;
    uint8_t read_data[4];
    uint8_t cnt;
 
    FT5436_Read(TOUCH_I2C_ADDR,0x02,&read_cnt,1);

    if(read_cnt>3)
    {
        return ;
    }
    else
    {  
        if(read_cnt!=0)
        {
            uint8_t gesture_data=0;
            FT5436_Read(TOUCH_I2C_ADDR,TOUCH_GEST_ID,&gesture_data,1);

            if(Touch_Status.gesture!=gesture_data)
            {
                Touch_Status.gesture=gesture_data;
            }

            for(cnt=0;cnt<read_cnt;cnt++)
            {
                FT5436_Read(TOUCH_I2C_ADDR,TOUCHn_XH(0),&read_data[0],1);
                FT5436_Read(TOUCH_I2C_ADDR,TOUCHn_XL(0),&read_data[1],1);
                FT5436_Read(TOUCH_I2C_ADDR,TOUCHn_YH(0),&read_data[2],1);
                FT5436_Read(TOUCH_I2C_ADDR,TOUCHn_YL(0),&read_data[3],1);

                Touch_Status.coord_x[cnt]=( (read_data[2]&0x0f)<<8) | read_data[3];
                Touch_Status.coord_y[cnt]=( (read_data[0]&0x0f)<<8) | read_data[1];
                Touch_Status.evt_flag[cnt]=(read_data[0]&0xc0)>>6;
                Touch_Status.touch_id[cnt]=(read_data[2]&0xf0)>>4;
    #if (TOUCH_DEBUG_UART==1)            
                printf("=====================================================");
                printf("\r\n Touch Num[%x] / touch_id[%x] / EVT_FLAG[%x] / coord_x[%x] / coord_y[%x] "
                        ,cnt, Touch_Status.touch_id[cnt], Touch_Status.evt_flag[cnt], Touch_Status.coord_x[cnt], Touch_Status.coord_y[cnt]);
                printf("=====================================================");
    #endif            
            }
        }
    }
}
/*******************************************************************************
* Function Name: FT5436_IC_ERR_DET
********************************************************************************
*
* \brief 
* Touch Panel Error Data Check
* 
* \param 
* None.
*
* \return
* int
*
* \note 
        Error Code
        FT5436_OK
        FT5436_REG_DIFF_I2C_TYPE
        FT5436_CHIP_START_FAIL
        FT5436_NO_MATCH_INPUT
*******************************************************************************/
static void FT5436_IC_ERR_DET(void)
{
    uint8_t err_stat;
    FT5436_Read(TOUCH_I2C_ADDR,ID_G_ERR,&err_stat,1);

    if(Touch_Status.err_code!=err_stat)
    {
        Touch_Status.err_code=err_stat;

#if (TOUCH_DEBUG_UART==1)
        switch(err_stat)
        {
            case FT5436_OK:

                break;

            case FT5436_REG_DIFF_I2C_TYPE:
                printf("\r\n FT5436_REG_DIFF_I2C_TYPE");
                break;

            case FT5436_CHIP_START_FAIL:
                printf("\r\n FT5436_CHIP_START_FAIL");
                break;

            case FT5436_NO_MATCH_INPUT:
                printf("\r\n FT5436_NO_MATCH_INPUT");
                break;
        }
#endif        
    }
}
/*******************************************************************************
* Function Name: vTouch_Panel_Init
********************************************************************************
*
* \brief 
* Touch Panel Initial
* 
* \param 
* None.
*
* \return
* int
*
* \note 
*******************************************************************************/
void vTouch_Panel_Init(void)
{
#if (TOUCH_PANEL_DRIVER_STAT==TOUCH_PANEL_DRIVER_USE)
    FT5436_Init_Register();
#endif    
}

/*******************************************************************************
* Function Name: vTouch_Panel_Task
********************************************************************************
*
* \brief 
* Touch Panel Task Read & ERR Data Check
* 
* \param 
* None.
*
* \return
* int
*
* \note 
*******************************************************************************/
void vTouch_Panel_Task(void)
{
#if (TOUCH_PANEL_DRIVER_STAT==TOUCH_PANEL_DRIVER_USE)
    FT5436_Touch_Data_Read();
    FT5436_IC_ERR_DET();
#endif    
}

/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/

/* [] END OF FILE */
