
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/
#define TEST_W_SS_ADDR    (CY_WFLASH_SM_SBM_TOP)
/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/

/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/
void vFlash_Init(void)
{
    Cy_FlashInit(true);
}

void writeToFlash(uint32_t SectorAddr,  uint32_t* programData)
{
    Cy_FlashSectorErase(SectorAddr, CY_FLASH_DRIVER_NON_BLOCKING);

    // Wait for completion with counting
    while(Cy_Is_SROM_API_Completed() == false){}

    Cy_FlashWriteWork(SectorAddr, programData, CY_FLASH_DRIVER_NON_BLOCKING);

    while (Cy_Is_SROM_API_Completed() == false)
    {
        /* code */
    }
}

void vFlashErase(uint32_t SectorAddr)
{

    Cy_FlashSectorErase(SectorAddr, CY_FLASH_DRIVER_NON_BLOCKING);

    // Wait for completion with counting
    while(Cy_Is_SROM_API_Completed() == false){}
}

uint32_t readFromFlash(void)
{
    // Get the total odometer from flash memory
    uint32_t* p_TestFlsTop = (uint32_t*)CY_WFLASH_SM_SBM_TOP;
    return p_TestFlsTop[0];
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/
