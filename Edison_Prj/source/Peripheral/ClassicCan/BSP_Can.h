#ifndef __DABO_CAN_H_
#define __DABO_CAN_H_

/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/
#define INVALID_CAN_ID  (0xfff)
#define CAN_MAX_VAR     (2)

#define CLASSIC_CAN_DATA_LEN     (2)  // 8-Byte
#define CAN_FD_DATA_LEN          (8)  // 64-Byte

#define CAN_MAX_MESSAGE          (CLASSIC_CAN_DATA_LEN)
/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/
typedef struct {
  unsigned long CAN_ID;
  unsigned char CAN_Length;
  unsigned char CAN_Extended_Type;
  unsigned long CAN_Data[CAN_MAX_MESSAGE];
}CAN_Data;

typedef struct 
{
  unsigned char Bus_Err_Code;
  unsigned char Bus_Input_Flag;
}CAN_Status;

typedef struct 
{
  CAN_Data         Data_Info;
  CAN_Status       Status_Info;
}CAN_Info;

/*****************************************************************************/
/***                                EXTERN VARIABLE                        ***/
/*****************************************************************************/
extern CAN_Info     Peri_Can_Info[CAN_MAX_VAR];
/*****************************************************************************/
/***                                EXTERN FUNCTIONS                       ***/
/*****************************************************************************/
extern void vCan_Init(void);
extern void vCan_fault_task(void);
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/

#endif
