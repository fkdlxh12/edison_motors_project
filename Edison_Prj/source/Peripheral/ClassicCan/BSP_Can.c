
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "BSP_Can.h"
/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/

/*****************************************************************************/
/***                             FUNCTIONS LIST                            ***/
/*****************************************************************************/

// Can_Peripheral_Functions
void vCan_Init(void);
static void vCANErrorDet(cy_pstc_canfd_type_t pstcCanFD);
void vCan_fault_task(void);

// CAN0_0
static void CAN_RxMsgCallback(bool bRxFifoMsg, uint8_t u8MsgBufOrRxFifoNum, cy_stc_canfd_msg_t* pstcCanFDmsg);
static void CanfdInterruptHandler(void);

// CAN1_0
static void CAN1_RxMsgCallback(bool bRxFifoMsg, uint8_t u8MsgBufOrRxFifoNum, cy_stc_canfd_msg_t* pstcCanFDmsg);
static void Can1fdInterruptHandler(void);

/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/
CAN_Info     Peri_Can_Info[CAN_MAX_VAR];

/* Standard ID Filter configration */
static const cy_stc_id_filter_t stdIdFilter[] = 
{
    CANFD_CONFIG_STD_ID_FILTER_CLASSIC_RXBUFF(0x7EEu, 0u),      /* ID=0x7EE, store into RX buffer Idx0 */
};


/* Extended ID Filter configration */
static const cy_stc_extid_filter_t extIdFilter[] = 
{
    CANFD_CONFIG_EXT_ID_FILTER_CLASSIC_RXBUFF(0x18FFD5EF, 0u),   
    CANFD_CONFIG_EXT_ID_FILTER_CLASSIC_RXBUFF(0x18FFD4EF, 1u),
};

static cy_stc_canfd_config_t canCfg = 
{
    .txCallback     = NULL, // Unused.
    .rxCallback     = CAN_RxMsgCallback,
    .rxFifoWithTopCallback = NULL, //CAN_RxFifoWithTopCallback,
    .statusCallback = NULL, // Un-supported now
    .errorCallback  = NULL, // Un-supported now
    .canFDMode      = false, // Use standard CAN mode
    // 40 MHz
    .bitrate        =       // Nominal bit rate settings (sampling point = 75%)
    {   
        // 500kbps
        .prescaler      = 10u - 1u,  // cclk/10, When using 500kbps, 1bit = 8tq
        .timeSegment1   = 5u - 1u, // tseg1 = 5tq
        .timeSegment2   = 2u - 1u,  // tseg2 = 2tq
        .syncJumpWidth  = 2u - 1u,  // sjw   = 2tq
    },
    .tdcConfig      =       // Transceiver delay compensation, unused.
    {
        .tdcEnabled     = false,
        .tdcOffset      = 0,
        .tdcFilterWindow= 0,
    },    
    .sidFilterConfig    =   // Standard ID filter
    {
        .numberOfSIDFilters = sizeof(stdIdFilter) / sizeof(stdIdFilter[0]), //0,
        .sidFilter          = stdIdFilter, //0, // NOT USED
    },  
    .extidFilterConfig  =   // Extended ID filter
    {
        .numberOfEXTIDFilters   = sizeof(extIdFilter) / sizeof(extIdFilter[0]),
        .extidFilter            = extIdFilter,
        .extIDANDMask           = 0x1fffffff,   // No pre filtering.
    },
    .globalFilterConfig =   // Global filter
    {     
        .nonMatchingFramesStandard = CY_CANFD_REJECT_NON_MATCHING,  // Reject none match IDs
        .nonMatchingFramesExtended = CY_CANFD_REJECT_NON_MATCHING,  // Reject none match IDs
        .rejectRemoteFramesStandard = true, // No remote frame
        .rejectRemoteFramesExtended = true, // No remote frame
    },
    .rxBufferDataSize = CY_CANFD_BUFFER_DATA_SIZE_64,  
    .rxFifo1DataSize  = CY_CANFD_BUFFER_DATA_SIZE_8,
    .rxFifo0DataSize  = CY_CANFD_BUFFER_DATA_SIZE_8,
    .txBufferDataSize = CY_CANFD_BUFFER_DATA_SIZE_64,
    .rxFifo0Config    = // RX FIFO0, unused.
    {
        .mode = CY_CANFD_FIFO_MODE_BLOCKING,     
        .watermark = 0u,
        .numberOfFifoElements = 0u,
        .topPointerLogicEnabled = false,
    },
    .rxFifo1Config    = // RX FIFO1, unused.
    {
        .mode = CY_CANFD_FIFO_MODE_BLOCKING,      
        .watermark = 0u,
        .numberOfFifoElements = 0u,
        .topPointerLogicEnabled = false, // true,
    },
    .noOfRxBuffers  = 2u,
    .noOfTxBuffers  = 2u,
};
///////////// CAN1_0

/* Extended ID Filter configration */
static const cy_stc_extid_filter_t extIdFilter1[] = 
{
    CANFD_CONFIG_EXT_ID_FILTER_CLASSIC_RXBUFF(0x18FFD5EF, 0u),   
};

static cy_stc_canfd_config_t canCfg1 = 
{
    .txCallback     = NULL, // Unused.
    .rxCallback     = CAN1_RxMsgCallback,
    .rxFifoWithTopCallback = NULL, //CAN_RxFifoWithTopCallback,
    .statusCallback = NULL, // Un-supported now
    .errorCallback  = NULL, // Un-supported now
    .canFDMode      = false, // Use standard CAN mode
    // 40 MHz
    .bitrate        =       // Nominal bit rate settings (sampling point = 75%)
    {   
        .prescaler      = 10u - 1u,  // cclk/10, When using 500kbps, 1bit = 8tq
        .timeSegment1   = 5u - 1u, // tseg1 = 5tq
        .timeSegment2   = 2u - 1u,  // tseg2 = 2tq
        .syncJumpWidth  = 2u - 1u,  // sjw   = 2tq
    },
    .tdcConfig      =       // Transceiver delay compensation, unused.
    {
        .tdcEnabled     = false,
        .tdcOffset      = 0,
        .tdcFilterWindow= 0,
    },    
    .sidFilterConfig    =   // Standard ID filter
    {
        .numberOfSIDFilters = 0,
        .sidFilter          = 0, // NOT USED
    },  
    .extidFilterConfig  =   // Extended ID filter
    {
        .numberOfEXTIDFilters   = sizeof(extIdFilter1) / sizeof(extIdFilter1[0]),
        .extidFilter            = extIdFilter1,
        .extIDANDMask           = 0x1fffffff,   // No pre filtering.
    },
    .globalFilterConfig =   // Global filter
    {     
        .nonMatchingFramesStandard = CY_CANFD_REJECT_NON_MATCHING,  // Reject none match IDs
        .nonMatchingFramesExtended = CY_CANFD_REJECT_NON_MATCHING,  // Reject none match IDs
        .rejectRemoteFramesStandard = true, // No remote frame
        .rejectRemoteFramesExtended = true, // No remote frame
    },
    .rxBufferDataSize = CY_CANFD_BUFFER_DATA_SIZE_64,  
    .rxFifo1DataSize  = CY_CANFD_BUFFER_DATA_SIZE_8,
    .rxFifo0DataSize  = CY_CANFD_BUFFER_DATA_SIZE_8,
    .txBufferDataSize = CY_CANFD_BUFFER_DATA_SIZE_64,
    .rxFifo0Config    = // RX FIFO0, unused.
    {
        .mode = CY_CANFD_FIFO_MODE_BLOCKING,     
        .watermark = 0u,
        .numberOfFifoElements = 0u,
        .topPointerLogicEnabled = false,
    },
    .rxFifo1Config    = // RX FIFO1, unused.
    {
        .mode = CY_CANFD_FIFO_MODE_BLOCKING,      
        .watermark = 0u,
        .numberOfFifoElements = 0u,
        .topPointerLogicEnabled = false, // true,
    },
    .noOfRxBuffers  = 4,
    .noOfTxBuffers  = 4u,
};


/*****************************************************************************/
/***                                FUNCTIONS                              ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: CAN_RxMsgCallback
********************************************************************************
*
* \brief 
* CAN0_0 Rx Messge Callback
* 
* \param 
* bool bRxFifoMsg : Fifo Message Input Checker
* uint8_t u8MsgBufOrRxFifoNum : Rx Message Input Number 
* cy_stc_canfd_msg_t* pstcCanFDmsg : CAN Frame Msg
*
* \return
* None.
*
* \note 
* CAN0_0 RX Interrupt Message Progress. 
* Edison CAN Protocol Passing Action
*******************************************************************************/
static void CAN_RxMsgCallback(bool bRxFifoMsg, uint8_t u8MsgBufOrRxFifoNum, cy_stc_canfd_msg_t* pstcCanFDmsg)
{
    Peri_Can_Info[0].Data_Info.CAN_ID=pstcCanFDmsg->idConfig.identifier;
    Peri_Can_Info[0].Data_Info.CAN_Length=pstcCanFDmsg->dataConfig.dataLengthCode;
    Peri_Can_Info[0].Data_Info.CAN_Extended_Type=pstcCanFDmsg->idConfig.extended;
    // 32-Bit Data 
    Peri_Can_Info[0].Data_Info.CAN_Data[0]=pstcCanFDmsg->dataConfig.data[0];
    Peri_Can_Info[0].Data_Info.CAN_Data[1]=pstcCanFDmsg->dataConfig.data[1];

    // Update Can Receive Timer 
    Peri_Can_Info[0].Status_Info.Bus_Input_Flag=1;
}
/*******************************************************************************
* Function Name: CanfdInterruptHandler
********************************************************************************
*
* \brief 
* CAN0_0 Interrupt Call Functions
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* CAN0_0 CallBack Functions Call ( Error / Status / RX / ETC)
*******************************************************************************/
static void CanfdInterruptHandler(void)
{
    Cy_CANFD_IrqHandler(CY_CANFD0_0_TYPE);
}
/*******************************************************************************
* Function Name: CAN1_RxMsgCallback
********************************************************************************
*
* \brief 
* CAN1_0 Rx Messge Callback
* 
* \param 
* bool bRxFifoMsg : Fifo Message Input Checker
* uint8_t u8MsgBufOrRxFifoNum : Rx Message Input Number 
* cy_stc_canfd_msg_t* pstcCanFDmsg : CAN Frame Msg
*
* \return
* None.
*
* \note 
* CAN1_0 RX Interrupt Message Progress. 
* Edison CAN Protocol Passing Action
*******************************************************************************/
static void CAN1_RxMsgCallback(bool bRxFifoMsg, uint8_t u8MsgBufOrRxFifoNum, cy_stc_canfd_msg_t* pstcCanFDmsg)
{
    Peri_Can_Info[1].Data_Info.CAN_ID=pstcCanFDmsg->idConfig.identifier;
    Peri_Can_Info[1].Data_Info.CAN_Length=pstcCanFDmsg->dataConfig.dataLengthCode;
    Peri_Can_Info[1].Data_Info.CAN_Extended_Type=pstcCanFDmsg->idConfig.extended;
    // 32-Bit Data 
    Peri_Can_Info[1].Data_Info.CAN_Data[0]=pstcCanFDmsg->dataConfig.data[0];
    Peri_Can_Info[1].Data_Info.CAN_Data[1]=pstcCanFDmsg->dataConfig.data[1];

    // Update Can Receive Timer 
    Peri_Can_Info[1].Status_Info.Bus_Input_Flag=1;    
}

/*******************************************************************************
* Function Name: CanfdInterruptHandler
********************************************************************************
*
* \brief 
* CAN0_0 Interrupt Call Functions
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* CAN0_0 CallBack Functions Call ( Error / Status / RX / ETC)
*******************************************************************************/
static void Can1fdInterruptHandler(void)
{
    Cy_CANFD_IrqHandler(CY_CANFD1_0_TYPE);
}
/*******************************************************************************
* Function Name: vCan_Init
********************************************************************************
*
* \brief 
* CAN0_0 & CAN0_1 Configuration 
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* CAN0_0 & CAN1_0 Configuration
*******************************************************************************/
void vCan_Init(void)
{
    // CAN Clock Init CAN0_0
    // 40MHZ
    Cy_SysClk_HfClkEnable(CY_SYSCLK_HFCLK_2);
    Cy_SysClk_PeriphAssignDivider(PCLK_CANFD0_CLOCK_CAN0 , CY_SYSCLK_DIV_8_BIT, 0);
    Cy_SysClk_PeriphSetDivider(Cy_SysClk_GetClockGroup(PCLK_CANFD0_CLOCK_CAN0), CY_SYSCLK_DIV_8_BIT, 0, 1u);
    Cy_SysClk_PeriphEnableDivider(Cy_SysClk_GetClockGroup(PCLK_CANFD0_CLOCK_CAN0), CY_SYSCLK_DIV_8_BIT, 0);

    // CAN Deinit
    Cy_CANFD_DeInit(CY_CANFD0_0_TYPE);

    Cy_SysClk_HfClkEnable(CY_SYSCLK_HFCLK_2);
    Cy_SysClk_PeriphAssignDivider(PCLK_CANFD1_CLOCK_CAN0 , CY_SYSCLK_DIV_8_BIT, 1);
    Cy_SysClk_PeriphSetDivider(Cy_SysClk_GetClockGroup(PCLK_CANFD1_CLOCK_CAN0), CY_SYSCLK_DIV_8_BIT, 1, 1u);
    Cy_SysClk_PeriphEnableDivider(Cy_SysClk_GetClockGroup(PCLK_CANFD1_CLOCK_CAN0), CY_SYSCLK_DIV_8_BIT, 1);

    // CAN Deinit
    /* Cy_CANFD_DeInit(CY_CANFD1_0_TYPE); */    

    const cy_stc_gpio_pin_prt_config_t Can_port[4] =
    {
    //  {       port,              pin,   outVal,        driveMode,          hsiom, intEdge, intMask, vtrip, slewRate, driveSel, },
        {     GPIO_PRT5,            4,     1,         CY_GPIO_DM_STRONG ,    P5_4_CANFD0_TTCAN_TX0,       0,       0,     0,        0,        0, },  // CAN0_0_TX
        {     GPIO_PRT5,            5,     0,         CY_GPIO_DM_HIGHZ,      P5_5_CANFD0_TTCAN_RX0 ,      0,       0,     0,        0,        0, }, // CAN0_0_RX
        {     GPIO_PRT4,            6,     1,         CY_GPIO_DM_STRONG,     P4_6_CANFD1_TTCAN_TX0,       0,       0,     0,        0,        0, }, // CAN1_0_TX
        {     GPIO_PRT4,            7,     0,         CY_GPIO_DM_HIGHZ,      P4_7_CANFD1_TTCAN_RX0,       0,       0,     0,        0,        0, }, // CAN1_0_RX   
    };

    for (int i = 0; i < (sizeof(Can_port) / sizeof(Can_port[0])); i++)
    {
        Cy_GPIO_Pin_Init(Can_port[i].portReg, Can_port[i].pinNum, &Can_port[i].cfg);
    }     

    // Interrupt Setting
    cy_stc_sysint_irq_t irq_cfg;
    irq_cfg = (cy_stc_sysint_irq_t){
        .sysIntSrc  = canfd_0_interrupts0_0_IRQn, /* Use interrupt LINE0 */
        .intIdx     = CPUIntIdx3_IRQn,
        .isEnabled  = true,
    };
    Cy_SysInt_InitIRQ(&irq_cfg);
    Cy_SysInt_SetSystemIrqVector(irq_cfg.sysIntSrc, CanfdInterruptHandler);
    
    NVIC_SetPriority(CPUIntIdx3_IRQn, 0);
    NVIC_ClearPendingIRQ(CPUIntIdx3_IRQn);
    NVIC_EnableIRQ(CPUIntIdx3_IRQn);    
    Cy_CANFD_Init(CY_CANFD0_0_TYPE, &canCfg);

    irq_cfg = (cy_stc_sysint_irq_t){
        .sysIntSrc  = canfd_1_interrupts0_0_IRQn, /* Use interrupt LINE1 */
        .intIdx     = CPUIntIdx3_IRQn,
        .isEnabled  = true,
    };
    /*
    Cy_SysInt_InitIRQ(&irq_cfg);
    Cy_SysInt_SetSystemIrqVector(irq_cfg.sysIntSrc, Can1fdInterruptHandler);
    
    NVIC_SetPriority(CPUIntIdx3_IRQn, 0);
    NVIC_ClearPendingIRQ(CPUIntIdx3_IRQn);
    NVIC_EnableIRQ(CPUIntIdx3_IRQn);    

    Cy_CANFD_Init(CY_CANFD1_0_TYPE, &canCfg1);    */
}

/*******************************************************************************
* Function Name: vCANErrorDet
********************************************************************************
*
* \brief 
* CAN0_0 & CAN0_1 Bus Fault Detection
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* Not Support Traveo2 SDL Error CallBack Function
*******************************************************************************/
static void vCANErrorDet(cy_pstc_canfd_type_t pstcCanFD)
{
    uint8_t index = 0;
    uint8_t tmp_status=0;
    uint8_t status=0;

    if(pstcCanFD==CY_CANFD0_0_TYPE)
    {
        index=0;
    }
    else if(pstcCanFD==CY_CANFD1_0_TYPE)
    {
        index=1;
    }

    tmp_status=Peri_Can_Info[index].Status_Info.Bus_Err_Code;
    
    /* Access to Reserved Address */
    if (pstcCanFD->M_TTCAN.unIR.stcField.u1ARA == 1)
    {
        status = 0x01;
    }

    /* Protocol Error in Data Phase */
    if (pstcCanFD->M_TTCAN.unIR.stcField.u1PED == 1)
    {
        status = 0x02;
    }

    /* Protocol Error in Arbitration Phase */
    if (pstcCanFD->M_TTCAN.unIR.stcField.u1PEA == 1)
    {
        status = 0x03;
    }

    /* Watchdog Interrupt */
    if (pstcCanFD->M_TTCAN.unIR.stcField.u1WDI == 1)
    {
        status = 0x04;
    }

    /* Bus_Off Status Interrupt */
    if (pstcCanFD->M_TTCAN.unIR.stcField.u1BO_ == 1)
    {
        status = 0x05;
    }

    /* Bit Error Uncorrected Interrupt */
    if (pstcCanFD->M_TTCAN.unIR.stcField.u1BEU == 1)
    {
        status = 0x06;
    }

    if(status!=tmp_status)
    {
        Peri_Can_Info[index].Status_Info.Bus_Err_Code=status;

        if(pstcCanFD==CY_CANFD0_0_TYPE)
        {
            Cy_CANFD_DeInit(CY_CANFD0_0_TYPE);
            Cy_CANFD_Init(CY_CANFD0_0_TYPE, &canCfg);
        }
        else if(pstcCanFD==CY_CANFD1_0_TYPE)
        {
            Cy_CANFD_DeInit(CY_CANFD1_0_TYPE);
            Cy_CANFD_Init(CY_CANFD1_0_TYPE, &canCfg);
        }
    }
} /* ErrorHandling */

/*******************************************************************************
* Function Name: vCan_fault_task
********************************************************************************
*
* \brief 
* CAN0_0 & CAN0_1 Bus Fault Detection 
* 
* \param 
* None.
*
* \return
* None.
*
* \note 
* CAN0_0 & CAN1_0 Fault Detection
* If Can Bus Fault -> CAN ReInit
*******************************************************************************/
void vCan_fault_task(void)
{    
    vCANErrorDet(CY_CANFD0_0_TYPE);
    /* vCANErrorDet(CY_CANFD1_0_TYPE); */
}
/*****************************************************************************/
/***                              END FILE                                ***/
/*****************************************************************************/

