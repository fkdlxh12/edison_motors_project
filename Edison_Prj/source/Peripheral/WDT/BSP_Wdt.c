
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/

/*****************************************************************************/
/***                             FUNCTIONS LIST                            ***/
/*****************************************************************************/
void vWdt_Init(void);
void vWdt_Update(void);
/*****************************************************************************/
/***                                FUNCTIONS                              ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: vWdt_Init
********************************************************************************
*
* \brief 
* ILO Clock (32Khz) WatchDog 
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* None.
*******************************************************************************/
void vWdt_Init(void)
{    
    // Clock 
    Cy_WDT_Init();                      /* Upper Limit: 1sec and reset */
    Cy_WDT_Unlock();
    // Cy_WDT_SetUpperLimit(32000);        /* Upper Limit: 2sec (override) */
    Cy_WDT_SetDebugRun(CY_WDT_ENABLE);  /* This is necessary when using debugger */
    Cy_WDT_Lock();
    Cy_WDT_Enable();
}

/*******************************************************************************
* Function Name: vWdt_Update
********************************************************************************
*
* \brief 
* PWM Duty Value 
* 
* \param 
* unsigned char onoff : onoff
* unsigned long duty  : 
*
* \return
* None.
*
* \note 
* None.
*******************************************************************************/
void vWdt_Update(void)
{    
    Cy_WDT_ClearWatchdog();
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/
