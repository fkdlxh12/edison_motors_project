
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "cygfx_driver_api.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/
static const cy_gfxenv_stc_cfg_t m_stcGfxEnvCfg =
{
    .bInitSwTimer         = false,
    .bInitSemihosting     = true, // Uart Debugger Message
    .pstcInitPortPins     = &(cy_gfxenv_stc_init_portpins_t)
                            {
                                .bInitDisplay0Ttl       = false, // usually the FPD-Link is used on silicon
                                .bInitDisplay1Ttl       = true,  // FPD-Link #1 is not available on PSVP or on silicon for this device
                                .bInitCapture0Ttl       = false,
                                .bInitSmif0             = false,
                                .bInitSmif1             = false,
                                .bInitBacklightDisp0    = false, // backlights are enabled by jumper setting on CY boards, would only be needed for PWM dimming
                                .bInitBacklightDisp1    = false, // backlights are enabled by jumper setting on CY boards, would only be needed for PWM dimming
                                .bInitBacklightFpdLink0 = false, // backlights are enabled by jumper setting on CY boards, would only be needed for PWM dimming
                                .bInitBacklightFpdLink1 = false, // backlights are enabled by jumper setting on CY boards, would only be needed for PWM dimming
                                .bInitButtonGpios       = false,
                            },
    .pstcInitExtMem       = NULL,
    .pstcInitButtons      = NULL
};
/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
void vGraphicsIP_Init(void);
static void ConfigureVideoSSInterrupts(const cy_stc_sysint_irq_t *pIrqCfg);
/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: vGraphicsIP_Init
********************************************************************************
*
* \brief 
* Traveo2 GraphicsIP Init
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* Graphics Env Init & Graphics Kernel Interrupt Init
*******************************************************************************/
void vGraphicsIP_Init(void)
{
    static const cy_stc_sysint_irq_t irq_cfg [3] =
    {
        {
        //VIDEOSS_0_INTERRUPT_GFX2D_HANDLER
        .sysIntSrc  = videoss_0_interrupt_gfx2d_IRQn, //150
        .intIdx     = CPUIntIdx3_IRQn,
        .isEnabled  = true
        },
        {
        //VIDEOSS_0_INTERRUPT_VIDEOIO0_HANDLER
        .sysIntSrc  = videoss_0_interrupt_videoio0_IRQn, //152
        .intIdx     = CPUIntIdx3_IRQn,
        .isEnabled  = true
        },
        {
        //VIDEOSS_0_INTERRUPT_VIDEOIO1_HANDLER
        .sysIntSrc  = videoss_0_interrupt_videoio1_IRQn, //153
        .intIdx     = CPUIntIdx3_IRQn,
        .isEnabled  = true
        }
    };

    ConfigureVideoSSInterrupts(irq_cfg);
    Cy_GfxEnv_Init(&m_stcGfxEnvCfg);
}

/*******************************************************************************
* Function Name: vGraphicsIP_Init
********************************************************************************
*
* \brief 
* Traveo2 GraphicsIP Init
* 
* \param 
* pIrqCfg : Graphics Interrupt Settings
*
* \return
* int
*
* \note 
* Graphics Env Init & Graphics Kernel Interrupt Init
*******************************************************************************/
static void ConfigureVideoSSInterrupts(const cy_stc_sysint_irq_t pIrqCfg [] )
{
    /* Initialize VideoSS interrupts */
    Cy_SysInt_InitIRQ(&pIrqCfg[0]); // GFX_2D
    Cy_SysInt_InitIRQ(&pIrqCfg[1]); // VideoIO0
    Cy_SysInt_InitIRQ(&pIrqCfg[2]); // VideoIO1

    /* Set Handlers */
    Cy_SysInt_SetSystemIrqVector(pIrqCfg[0].sysIntSrc, CyGfx_kInterruptHandlerGfx2d);
    Cy_SysInt_SetSystemIrqVector(pIrqCfg[1].sysIntSrc, CyGfx_kInterruptHandlerVideoio0);
    Cy_SysInt_SetSystemIrqVector(pIrqCfg[2].sysIntSrc, CyGfx_kInterruptHandlerVideoio1);

    /* Set Prio/Enable IRQ */
    NVIC_SetPriority(CPUIntIdx3_IRQn, 3);
    NVIC_ClearPendingIRQ(CPUIntIdx3_IRQn);
    NVIC_EnableIRQ(CPUIntIdx3_IRQn);
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/
