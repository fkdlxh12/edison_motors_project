#ifndef __DABO_GRAPHICS_IP_H_
#define __DABO_GRAPHICS_IP_H_

/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/


/*****************************************************************************/
/***                                EXTERN VARIABLE                        ***/
/*****************************************************************************/

/*****************************************************************************/
/***                                EXTERN FUNCTIONS                       ***/
/*****************************************************************************/
extern void vGraphicsIP_Init(void);
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/


#endif
