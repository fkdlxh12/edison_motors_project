
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
#include "BSP_I2c.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/
#define E_SOURCE_CLK_FREQ               (80000000) // fixed

#define DATARATE_100K           0
#define DATARATE_400K           1
#define DATARATE_1M             2 

#define DATARATE_TARGET         DATARATE_400K

#if(DATARATE_TARGET==DATARATE_100K)
    #define E_I2C_INCLK_TARGET_FREQ         (2000000ul)//(12000000)  // modifiable
    #define E_I2C_DATARATE                  (100000ul)//(400000)   // modifiable
#elif(DATARATE_TARGET==DATARATE_400K)
    #define E_I2C_INCLK_TARGET_FREQ         (10000000u)
    #define E_I2C_DATARATE                  (400000u)
#else
    #define E_I2C_INCLK_TARGET_FREQ         (25800000u)
    #define E_I2C_DATARATE                  (1000000u)
#endif    

#define I2C_TIME_OUT 50

#define TOUCH_I2C_SDA_PORT              GPIO_PRT9
#define TOUCH_I2C_SDA_PIN               (4u)
#define TOUCH_I2C_SDA_MUX               (P9_4_SCB8_I2C_SDA)
#define TOUCH_I2C_SDA_MOD               (CY_GPIO_DM_OD_DRIVESLOW)

#define TOUCH_I2C_SCL_PORT              GPIO_PRT9
#define TOUCH_I2C_SCL_PIN               (5u)
#define TOUCH_I2C_SCL_MUX               (P9_5_SCB8_I2C_SCL)
#define TOUCH_I2C_SCL_MOD               (CY_GPIO_DM_OD_DRIVESLOW)
/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/
static cy_stc_scb_i2c_context_t g_stc_i2c_context;
I2C_Info Peri_I2C_Info;

static const cy_stc_scb_i2c_config_t  g_stc_i2c_config =
{
    .i2cMode             = CY_SCB_I2C_MASTER,
    .useRxFifo           = true,
    .useTxFifo           = true,
    .slaveAddress        = 0,
    .slaveAddressMask    = 0,
    .acceptAddrInFifo    = false,
    .ackGeneralAddr      = false,
    .enableWakeFromSleep = false
};
/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
unsigned long Scb_I2C_Byte_Write(unsigned char ID, unsigned char addr, unsigned char data);
unsigned long Scb_I2C_Bytes_Receive(unsigned char ID, unsigned char addr, unsigned char *data, unsigned char cnt);
void vI2c_Init(void);
void vI2c_Fault_task(void);
/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: Scb_I2C_Byte_Write
********************************************************************************
*
* \brief 
* I2C 1-Byte Write
* 
* \param 
* unsigned char ID   = Slave ID
* unsigned char addr = Slave Address
* unsigned char data.= Byte Data
*
* \return
* Peri_I2C_Info.Stat : I2C Status Return
*    CY_SCB_I2C_SUCCESS,
*    CY_SCB_I2C_BAD_PARAM,
*    CY_SCB_I2C_MASTER_NOT_READY,
*    CY_SCB_I2C_MASTER_MANUAL_TIMEOUT,
*    CY_SCB_I2C_MASTER_MANUAL_ADDR_NAK,
*    CY_SCB_I2C_MASTER_MANUAL_NAK,
*    CY_SCB_I2C_MASTER_MANUAL_ARB_LOST,
*    CY_SCB_I2C_MASTER_MANUAL_BUS_ERR,
*    CY_SCB_I2C_MASTER_MANUAL_ABORT_START
*
* \note 
* None.
*******************************************************************************/
unsigned long Scb_I2C_Byte_Write(unsigned char ID, unsigned char addr, unsigned char data)
{
    uint8_t count=0;
    cy_en_scb_i2c_status_t i2c_stat;

    /* Make sure TX FIFO empty */
    while(Cy_SCB_GetNumInTxFifo(SCB8) != 0ul);

    /* Send START and Receive ACK/NACK */
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterSendStart(SCB8, ID, CY_SCB_I2C_WRITE_XFER, I2C_TIME_OUT, &g_stc_i2c_context);
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterWriteByte(SCB8, addr, I2C_TIME_OUT, &g_stc_i2c_context);
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterWriteByte(SCB8, data, I2C_TIME_OUT, &g_stc_i2c_context);
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterSendWriteStop(SCB8, I2C_TIME_OUT, &g_stc_i2c_context);    

    return Peri_I2C_Info.Stat;
}

/*******************************************************************************
* Function Name: Scb_I2C_Bytes_Receive
********************************************************************************
*
* \brief 
* I2C 1-Byte Write
* 
* \param 
* unsigned char ID   = Slave ID
* unsigned char addr = Slave Address
* unsigned char *data.= Read Save Data (Pointer)
* unsigned char cnt  = Read Count
*
* \return
* Peri_I2C_Info.Stat : I2C Status Return
*    CY_SCB_I2C_SUCCESS,
*    CY_SCB_I2C_BAD_PARAM,
*    CY_SCB_I2C_MASTER_NOT_READY,
*    CY_SCB_I2C_MASTER_MANUAL_TIMEOUT,
*    CY_SCB_I2C_MASTER_MANUAL_ADDR_NAK,
*    CY_SCB_I2C_MASTER_MANUAL_NAK,
*    CY_SCB_I2C_MASTER_MANUAL_ARB_LOST,
*    CY_SCB_I2C_MASTER_MANUAL_BUS_ERR,
*    CY_SCB_I2C_MASTER_MANUAL_ABORT_START
*
* \note 
* None.
*******************************************************************************/
unsigned long Scb_I2C_Bytes_Receive(unsigned char ID, unsigned char addr, unsigned char *data, unsigned char cnt)
{
    cy_en_scb_i2c_status_t status;
    /* Make sure TX FIFO empty */
    while(Cy_SCB_GetNumInTxFifo(SCB8) != 0ul);

// CALL DEV I2C ADDRESS
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterSendStart(SCB8, ID, CY_SCB_I2C_WRITE_XFER, I2C_TIME_OUT, &g_stc_i2c_context);
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterWriteByte(SCB8, addr, I2C_TIME_OUT, &g_stc_i2c_context);
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterSendWriteStop(SCB8, I2C_TIME_OUT, &g_stc_i2c_context);

// READ
    /* Make sure RX FIFO empty */
    if(Cy_SCB_GetNumInRxFifo(SCB8) != 0ul); 

    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterSendStart(SCB8, ID, CY_SCB_I2C_READ_XFER, I2C_TIME_OUT, &g_stc_i2c_context);     
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterReadByte(SCB8, CY_SCB_I2C_NAK, data, I2C_TIME_OUT, &g_stc_i2c_context);
    Peri_I2C_Info.Stat=Cy_SCB_I2C_MasterSendReadStop(SCB8, I2C_TIME_OUT, &g_stc_i2c_context);

    return Peri_I2C_Info.Stat;
}
/*******************************************************************************
* Function Name: vI2c_Init
********************************************************************************
*
* \brief 
* I2C Initialize
* 
* \param 
* None.
* \return
* None.
*
* \note 
* None.
*******************************************************************************/
void vI2c_Init(void)
{
    Cy_SysClk_HfClkEnable(CY_SYSCLK_HFCLK_2);
    Cy_SysClk_PeriphAssignDivider(PCLK_SCB8_CLOCK, CY_SYSCLK_DIV_24_5_BIT, 0u);
    // DIV_24.5
    uint64_t temp = ((uint64_t)E_SOURCE_CLK_FREQ << 5ull);
    uint32_t divSetting;

    divSetting = (uint32_t)(temp / E_I2C_INCLK_TARGET_FREQ);
    Cy_SysClk_PeriphSetFracDivider(Cy_SysClk_GetClockGroup(PCLK_SCB8_CLOCK), 
                                   CY_SYSCLK_DIV_24_5_BIT, 0u, 
                                   (((divSetting >> 5u) & 0x00000FFF) - 1u), 
                                   (divSetting & 0x0000001F));

    Cy_SysClk_PeriphEnableDivider(Cy_SysClk_GetClockGroup(PCLK_SCB8_CLOCK), CY_SYSCLK_DIV_24_5_BIT, 0u);
    Cy_SCB_I2C_DeInit(SCB8);

    cy_stc_gpio_pin_prt_config_t I2C_Port[2] =
    {
        {     TOUCH_I2C_SDA_PORT,            TOUCH_I2C_SDA_PIN,            0,    TOUCH_I2C_SDA_MOD ,      TOUCH_I2C_SDA_MUX,      0,      0,     0,        0,        0, },
        {     TOUCH_I2C_SCL_PORT,            TOUCH_I2C_SCL_PIN,            0,    TOUCH_I2C_SCL_MOD ,      TOUCH_I2C_SCL_MUX,      0,      0,     0,        0,        0, },
    };

    for (uint8_t i = 0; i < (sizeof(I2C_Port) / sizeof(I2C_Port[0])); i++)
    {
        Cy_GPIO_Pin_Init(I2C_Port[i].portReg, I2C_Port[i].pinNum, &I2C_Port[i].cfg);
    }   

    Cy_SCB_I2C_Init(SCB8, &g_stc_i2c_config, &g_stc_i2c_context);
    Cy_SCB_I2C_SetDataRate(SCB8, E_I2C_DATARATE, E_I2C_INCLK_TARGET_FREQ);
    Cy_SCB_I2C_Enable(SCB8);
}

/*******************************************************************************
* Function Name: vI2c_Fault_task
********************************************************************************
*
* \brief 
* I2C Fault Detection
* 
* \param 
* None.
* \return
* None.
*
* \note 
* None.
*******************************************************************************/
void vI2c_Fault_task(void)
{
    if(Peri_I2C_Info.Stat!=CY_SCB_I2C_SUCCESS)
    {
        Cy_SCB_I2C_DeInit(SCB8);
        Cy_SCB_I2C_Init(SCB8, &g_stc_i2c_config, &g_stc_i2c_context);
        Cy_SCB_I2C_SetDataRate(SCB8, E_I2C_DATARATE, E_I2C_INCLK_TARGET_FREQ);
        Cy_SCB_I2C_Enable(SCB8);        
        Peri_I2C_Info.Stat=CY_SCB_I2C_SUCCESS;
    }
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/
