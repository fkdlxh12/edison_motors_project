
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
/*****************************************************************************/
/***                                DEFINED                                ***/
/*****************************************************************************/
#define BUZ_PWM_PORT                    GPIO_PRT9
#define BUZ_PWM_PIN                     (3u)
#define BUZ_PWM_MUX                     (P9_3_TCPWM0_LINE20)
#define BUZ_PWM_MOD                     (CY_GPIO_DM_STRONG_IN_OFF)
/*****************************************************************************/
/***                                VARIABLE                               ***/
/*****************************************************************************/
static cy_stc_tcpwm_pwm_config_t const PWM_P9_3_config =
{
    .pwmMode            = CY_TCPWM_PWM_MODE_PWM,
    .clockPrescaler     = CY_TCPWM_PWM_PRESCALER_DIVBY_128, // 2,000,000 / 128 = 15,625Hz //   CY_TCPWM_PRESCALER_DIVBY_128
    .debug_pause        = 0uL,
    .Cc0MatchMode       = CY_TCPWM_PWM_TR_CTRL2_CLEAR,
    .OverflowMode       = CY_TCPWM_PWM_TR_CTRL2_SET,
    .UnderflowMode      = CY_TCPWM_PWM_TR_CTRL2_NO_CHANGE,
    .Cc1MatchMode       = CY_TCPWM_PWM_TR_CTRL2_NO_CHANGE,
    .deadTime           = 0uL,
    .deadTimeComp       = 0uL,
    .runMode            = CY_TCPWM_PWM_CONTINUOUS,
    .period             = 0x1000-1,//16u - 1,                        
    .period_buff        = 0u,
    .enablePeriodSwap   = false,
    .compare0           = 0u,//0x800,//8u,                               
    .compare1           = 0u,
    .enableCompare0Swap = false,
    .enableCompare1Swap = false,
    .interruptSources   = 0uL,
    .invertPWMOut       = 0uL,
    .invertPWMOutN      = 0uL,
    .killMode           = CY_TCPWM_PWM_NOT_STOP_ON_KILL,
    .switchInputMode    = 3uL,
    .switchInput        = 0uL,
    .reloadInputMode    = 3uL,
    .reloadInput        = 0uL,
    .startInputMode     = 3uL,
    .startInput         = 0uL,
    .kill0InputMode     = 3uL,
    .kill0Input         = 0uL,
    .kill1InputMode     = 3uL,
    .kill1Input         = 0uL,
    .countInputMode     = 3uL,
    .countInput         = 1uL,
};
/*****************************************************************************/
/***                             FUNCTIONS LIST                            ***/
/*****************************************************************************/
void vPWM_Init(void);
void vPWM_Control(unsigned char onoff,unsigned long duty);
/*****************************************************************************/
/***                                FUNCTIONS                              ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: vPWM_Init
********************************************************************************
*
* \brief 
* PWM P9_3 Initial 
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* None.
*******************************************************************************/
void vPWM_Init(void)
{    
    uint32_t sourceFreq = 80000000ul;
    uint32_t targetFreq = 2000000ul;
    uint32_t divNum = (sourceFreq / targetFreq);

    Cy_SysClk_PeriphAssignDivider(PCLK_TCPWM0_CLOCKS20, CY_SYSCLK_DIV_16_BIT, 0);
    Cy_SysClk_PeriphSetDivider(Cy_SysClk_GetClockGroup(PCLK_TCPWM0_CLOCKS20),CY_SYSCLK_DIV_16_BIT, 0u, (divNum-1ul)); // Divider 39 --> 80MHz / (39+1) = 2MHz
    Cy_SysClk_PeriphEnableDivider(Cy_SysClk_GetClockGroup(PCLK_TCPWM0_CLOCKS20),CY_SYSCLK_DIV_16_BIT, 0);
    
    cy_stc_gpio_pin_prt_config_t Buzzer_Port[1] =
    {
    //  {              port,              pin,                outVal,      driveMode,            hsiom,         intEdge,                intMask, vtrip, slewRate, driveSel, },
        {     BUZ_PWM_PORT,            BUZ_PWM_PIN,            0,      BUZ_PWM_MOD,       BUZ_PWM_MUX,      0,      0,     0,        0,        0, },
    };

    for (uint8_t i = 0; i < (sizeof(Buzzer_Port) / sizeof(Buzzer_Port[0])); i++)
    {
        Cy_GPIO_Pin_Init(Buzzer_Port[i].portReg, Buzzer_Port[i].pinNum, &Buzzer_Port[i].cfg);
    }

    /* Initialize TCPWM0_GPR0_CNT0 as PWM & Enable */
    Cy_Tcpwm_Pwm_Init(TCPWM0_GRP0_CNT20, &PWM_P9_3_config);
    Cy_Tcpwm_Pwm_Enable(TCPWM0_GRP0_CNT20);
    Cy_Tcpwm_TriggerStart(TCPWM0_GRP0_CNT20);
}

/*******************************************************************************
* Function Name: vPWM_Control
********************************************************************************
*
* \brief 
* PWM Duty Value 
* 
* \param 
* unsigned char onoff : onoff
* unsigned long duty  : 
*
* \return
* None.
*
* \note 
* None.
*******************************************************************************/
void vPWM_Control(unsigned char onoff,unsigned long duty)
{
    if(onoff&0x01==0)
    {
        Cy_Tcpwm_Pwm_SetCompare0(TCPWM0_GRP0_CNT20,0);        
    }
    else
    {
        Cy_Tcpwm_Pwm_SetCompare0(TCPWM0_GRP0_CNT20,  duty);
    }
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/
