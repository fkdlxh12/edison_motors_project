
/*****************************************************************************/
/***                                INCLUDE                                ***/
/*****************************************************************************/
#include "cy_project.h"
#include "cy_device_headers.h"
/*****************************************************************************/
/***                                DEFINED                                        ***/
/*****************************************************************************/
#define USR_LED_PORT                    GPIO_PRT7
#define USR_LED_PIN                     (2u)
#define USR_LED_MUX                     (P7_2_GPIO)
#define USR_LED_MOD                     (CY_GPIO_DM_STRONG)

#define TOUCH_INT_PORT                  GPIO_PRT6
#define TOUCH_INT_PIN                   (1u)
#define TOUCH_INT_MUX                   (P6_1_GPIO)
#define TOUCH_INT_MOD                   (CY_GPIO_DM_HIGHZ)

#define SMIF0_INT_PORT                  GPIO_PRT6
#define SMIF0_INT_PIN                   (2u)
#define SMIF0_INT_MUX                   (P6_2_GPIO)
#define SMIF0_INT_MOD                   (CY_GPIO_DM_HIGHZ)

#define USR_SWITCH_INT_PORT             GPIO_PRT6
#define USR_SWITCH_INT_PIN              (3u)
#define USR_SWITCH_INT_MUX              (P6_3_GPIO)
#define USR_SWITCH_INT_MOD              (CY_GPIO_DM_HIGHZ)
/*****************************************************************************/
/***                                VARIABLE                                       ***/
/*****************************************************************************/

/*****************************************************************************/
/***                             FUNCTIONS LIST                                 ***/
/*****************************************************************************/
void vGpio_In_Out_Init(void);
static void vOutput_gpio_Init(void);
static void vInput_gpio_Init(void);
/*****************************************************************************/
/***                                FUNCTIONS                                    ***/
/*****************************************************************************/

/*******************************************************************************
* Function Name: vOutput_gpio_Init
********************************************************************************
*
* \brief 
* Output GPIO Initialize ( Only Output Pin )
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* Output GPIO Initialize
*******************************************************************************/
static void vOutput_gpio_Init(void)
{
    cy_stc_gpio_pin_prt_config_t Gpio_Out_port[1] =
    {
    //  {              port,              pin,                outVal,      driveMode,            hsiom,         intEdge,                intMask, vtrip, slewRate, driveSel, },
        {     
            GPIO_PRT7,
                    2,
                    0,
            CY_GPIO_DM_STRONG ,
            P7_2_GPIO,
                  0,
                  0,
                  0,
                  0,
                  0
        }
    };

    for (uint8_t i = 0; i < (sizeof(Gpio_Out_port) / sizeof(Gpio_Out_port[0])); i++)
    {
        Cy_GPIO_Pin_Init(Gpio_Out_port[i].portReg, Gpio_Out_port[i].pinNum, &Gpio_Out_port[i].cfg);
    }    
}
/*******************************************************************************
* Function Name: vInput_gpio_Init
********************************************************************************
*
* \brief 
* Input GPIO Initialize ( Only Input Pin )
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* Input GPIO Initialize
*******************************************************************************/
static void vInput_gpio_Init(void)
{
    cy_stc_gpio_pin_prt_config_t Gpio_Int_port[2] =
    {
    //  {              port,              pin,                outVal,      driveMode,            hsiom,         intEdge,                intMask, vtrip, slewRate, driveSel, },
        {     TOUCH_INT_PORT,            TOUCH_INT_PIN,            0,     TOUCH_INT_MOD,       TOUCH_INT_MUX,      CY_GPIO_INTR_RISING,      1,     0,        0,        0, },
        {     USR_SWITCH_INT_PORT,       USR_SWITCH_INT_PIN,       0,      USR_SWITCH_INT_MOD, USR_SWITCH_INT_MUX, CY_GPIO_INTR_RISING,      1,     0,        0,        0, },
        
        
    };

    for (uint8_t i = 0; i < (sizeof(Gpio_Int_port) / sizeof(Gpio_Int_port[0])); i++)
    {
        Cy_GPIO_Pin_Init(Gpio_Int_port[i].portReg, Gpio_Int_port[i].pinNum, &Gpio_Int_port[i].cfg);
    }      
}
/*******************************************************************************
* Function Name: vGpio_In_Out_Init
********************************************************************************
*
* \brief 
* Input/Output GPIO Initialize
* 
* \param 
* None.
*
* \return
* int
*
* \note 
* Input/Output GPIO Initialize
*******************************************************************************/
void vGpio_In_Out_Init(void)
{
    vOutput_gpio_Init();
    vInput_gpio_Init();
}
/*****************************************************************************/
/***                                END FILE                               ***/
/*****************************************************************************/


