/*******************************************************************************
NEED SCENARIOS TO CREATE VARIABLE AND DATA CRAWLING FUNCTIONS
*******************************************************************************/
#include "demodata.h"

Engine g_engine;

static const Engine::POS s_data[] = {
    {   0,    0,         714},
    {   1,    0,         720},
    {   2,    0,         719},
    {   3,    0,         1050 },
    {   4,    4.8984f,    987  },
    {   5,    9.6484f,    1669 },
    {   6,    21.305f,    3963 },
    {   7,    42.617f,    7500 },
    {   8,    61.734f,    5605  },
    {   9,    78.258f,    6899.5f},
    {  10,    93.43f,     7970  },
    {  11,    106.08f,    6265.5f},
    {  12,    118.05f,    6931.5f},
    {  13,    129.45f,    7560.5f},
    {  14,    139.62f,    6162.5f},
    {  15,    148.84f,    6473.5f},
    {  16,    157.4f,     6849  },
    {  17,    165.94f,    7239  },
    {  18,    174.13f,    7583  },
    {  19,    181.72f,    6388  },
    {  20,    188.68f,    6353  },
    {  21,    195.55f,    6518.5f},
    {  22,    196.79f,    5040  },
    {  23,    194.09f,    5177.5f},
    {  24,    190.38f,    5091.5f},
    {  25,    186.45f,    4966.5f},
    {  26,    182.27f,    4843.5f},
    {  27,    178.33f,    4737.5f},
    {  28,    174.45f,    4642.5f},
    {  29,    170.68f,    4549.5f},
    {  30,    167.15f,    4447  },
    {  31,    163.95f,    4096  },
    {  33,    0,         714.5f  },
};

static const Engine::POS s_data2[] = {
    {   0,    0,         0},
    {   0.6f,    240,    8000},
    {   2.5f,    0,         0},
};

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

Engine::Engine()
{
    Init();
}

Engine::~Engine()
{
}

void Engine::Init()
{
    Restart();
    m_end = 0;
    m_start = 0;
    SetData(s_data, sizeof(s_data)/sizeof(POS));
}

CYGFX_BOOL Engine::Tick()
{
    if (IsEnd())
    {
        Restart();
    }
#ifdef WIN32
    m_time += 0.1f;
#else
    m_time += 1.0f/60;
#endif
    CYGFX_U32 i;

    for (i = m_pos; i < m_max; i++)
    {
        if (m_pData[i].time > m_time) {
            m_pos = i;
            break;
        }
    }
    if (i == 0)
    {
        m_Speed = (CYGFX_FLOAT)m_pData[0].speed;
    }
    else if (i < m_max)
    {
        m_Speed = (CYGFX_FLOAT)(m_pData[i].speed - m_pData[i-1].speed) / (m_pData[i].time - m_pData[i-1].time) * (m_time - m_pData[i-1].time);
        m_Speed = m_Speed + m_pData[i-1].speed;
    }
    else
    {
        m_Speed = m_pData[m_max-1].speed;
    }

    return CYGFX_TRUE;
}

CYGFX_BOOL Engine::IsEnd()
{
    if (m_end > 0 && m_end < m_time)
        return CYGFX_TRUE;
    return m_pData[m_max - 1].time < m_time;
}

void Engine::SetData(const POS *pData, CYGFX_U32 max)
{
    m_pData = pData;
    m_max = max;
}

void Engine::Restart()
{
    m_time = 0;
    m_pos = 0;
}
