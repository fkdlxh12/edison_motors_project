/**
 * \file        hud.h
 * \brief       Head up unit interface
 *              This file contains the interface for the Head up display
 */

#include "cygfx_defines.h"
#include "cygfx_basetypes.h"

#ifdef __cplusplus
extern "C"
{
#endif

CYGFX_ERROR HudInit(void);
CYGFX_ERROR HudUpdate(void);
void     HudClose(void);
void     HudHandleKey(int key);

#ifdef __cplusplus
} /* extern "C" */
#endif
