extern const unsigned char gps_signal1[296];

#ifndef GPS_SIGNAL1
#define GPS_SIGNAL1
#ifdef BITMAP_DATA

#include "flash_resource.h"

const unsigned char gps_signal1[296] = {
    0x28, 0x01, 0x00, 0x00, /* nSize in bytes (296) */
    0x28, 0x00, /* nWidth (40) */
    0x23, 0x00, /* nHeight (35) */
    0x00, 0x00, /* strideInByte (0) */
    0x04, /* totalBits */
    0x04, /* flags */
    0x84, 0x80, 0x80, 0x80,  /* ColorComponentBits (0x80808084) */
    0x00, 0x00, 0x00, 0x00,  /* ColorComponentShift (0x00000000) */
    /* color / index data */
    0x3e, 0xc0, 0x01, 0xf8, 0x00, 0x07, 0xe0, 0x03, 0x1c, 0x80, 0x0f, 0x70, 0x00, 0x3e, 0xc0, 0x01, 
    0xf8, 0x00, 0x07, 0xe0, 0x03, 0x1c, 0x80, 0x0f, 0x70, 0x00, 0x3e, 0xc0, 0x01, 0xf8, 0x00, 0x07, 
    0xe0, 0x03, 0x1c, 0x80, 0x0f, 0x70, 0x00, 0x3e, 0xc0, 0x01, 0x98, 0x00, 0x41, 0x23, 0x62, 0x20, 
    0x00, 0x00, 0x13, 0x70, 0xcc, 0x7d, 0xc1, 0x03, 0x98, 0x00, 0x64, 0xc8, 0xff, 0xe0, 0x02, 0x01, 
    0x00, 0x13, 0x90, 0x4c, 0x30, 0x13, 0xce, 0x12, 0xff, 0xff, 0x9f, 0x01, 0xa8, 0x80, 0x63, 0x81, 
    0x1f, 0xd0, 0xc0, 0x00, 0x00, 0x00, 0x08, 0xc0, 0x0a, 0x00, 0xa0, 0x31, 0x4d, 0x09, 0x47, 0x20, 
    0x00, 0x40, 0x4c, 0xd1, 0x21, 0x02, 0x01, 0x1c, 0x80, 0x05, 0x40, 0xa6, 0x68, 0x3b, 0x6a, 0x7e, 
    0x4d, 0x92, 0x8a, 0xdc, 0x02, 0x58, 0x00, 0x00, 0x85, 0xc4, 0x1f, 0xa1, 0x6c, 0x94, 0xad, 0x5b, 
    0x83, 0xcc, 0x12, 0xff, 0xff, 0x2b, 0x00, 0x0b, 0x20, 0x6c, 0x0d, 0x05, 0xef, 0x90, 0xa1, 0x88, 
    0x08, 0x19, 0x2d, 0x00, 0x06, 0x60, 0xd6, 0x86, 0x24, 0x49, 0x99, 0xc2, 0xb6, 0xad, 0x01, 0x34, 
    0x00, 0x80, 0x52, 0xe2, 0x1f, 0x80, 0x12, 0x01, 0x1a, 0x00, 0xc0, 0x30, 0x71, 0x1f, 0x80, 0x01, 
    0x01, 0x1a, 0x00, 0xc0, 0x31, 0x78, 0x00, 0x2b, 0x09, 0x04, 0x00, 0x80, 0x01, 0x60, 0x00, 0x61, 
    0x6b, 0x38, 0x78, 0x00, 0xe9, 0x01, 0x30, 0x00, 0x32, 0x2f, 0x92, 0x38, 0x4b, 0xc8, 0x76, 0x7f, 
    0x0d, 0x80, 0x01, 0x00, 0x9c, 0x81, 0x27, 0x26, 0x81, 0x11, 0x39, 0xa1, 0x1a, 0x00, 0x03, 0x00, 
    0x50, 0x03, 0xaf, 0x0c, 0x06, 0xec, 0xf6, 0x00, 0x12, 0x80, 0x90, 0xd0, 0x48, 0xdc, 0x03, 0xc0, 
    0x49, 0xa4, 0xa0, 0xaa, 0x14, 0x00, 0x02, 0x00, 0xb8, 0x28, 0xcd, 0xc0, 0x58, 0x27, 0x38, 0x08, 
    0x80, 0x00, 0x00, 0xd6, 0x4a, 0x1f, 0x3c, 0x80, 0x03, 0x01, 0x3e, 0xc0, 0x01, 0xf8, 0x00, 0x07, 
    0x00, 0x00, 0x00, 0x00
};

#endif /* BITMAP_DATA */
#endif /* GPS_SIGNAL1 */
