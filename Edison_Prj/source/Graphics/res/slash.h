extern const unsigned char slash[60];

#ifndef SLASH
#define SLASH
#ifdef BITMAP_DATA

#include "flash_resource.h"

const unsigned char slash[60] = {
    0x3c, 0x00, 0x00, 0x00, /* nSize in bytes (60) */
    0x07, 0x00, /* nWidth (7) */
    0x0f, 0x00, /* nHeight (15) */
    0x00, 0x00, /* strideInByte (0) */
    0x04, /* totalBits */
    0x04, /* flags */
    0x84, 0x80, 0x80, 0x80,  /* ColorComponentBits (0x80808084) */
    0x00, 0x00, 0x00, 0x00,  /* ColorComponentShift (0x00000000) */
    /* color / index data */
    0x4c, 0xfe, 0xc3, 0xe4, 0x3f, 0x4c, 0xfe, 0xc9, 0xe4, 0x9f, 0x4c, 0xfe, 0xc9, 0xe4, 0xcf, 0x4c, 
    0xfe, 0xcc, 0xe4, 0xcf, 0x4c, 0x7e, 0xce, 0xe4, 0xe7, 0x4c, 0x3e, 0xce, 0xe4, 0xf3, 0x4c, 0x3e, 
    0xcf, 0xe4, 0xf9, 0x4c, 0x9e, 0x0f, 0x00, 0x00
};

#endif /* BITMAP_DATA */
#endif /* SLASH */
