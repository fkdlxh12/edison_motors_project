/**
 * \file        hud.cpp
 * \brief       Head up unit driver
 *              This file contains the code to drive a Head up display

 */
#include <stdlib.h>
#include "cygfx_driver_api.h"
#include "pe_matrix.h"
#include "res.h"
#include "cy_project.h"
#include "demodata.h"

#include "ut_class_display.h"
#include "ut_class_ctx.h"
#include "ut_class_device.h"
#include "ut_class_window.h"
#include "hud.h"
#include "edisonmotors.h"

#include "Edison_System.h"

#define DISPLAY_HUD     0

static struct RESOLUTION_TYPES {
    const wchar_t* pName;
    CYGFX_U32 width;
    CYGFX_U32 height;
} s_resolution_types[] = {    
    {L"800*480", 800, 480},
};

#define SPEED_MAX        250
#define SPEEDLIMIT_MAX   150

static CHudDisplay              s_hud;
static CDevice                  s_device;
static CCtx                     s_draw_ctx;
static Engine                   s_engine;

static CYGFX_U32                   s_nFrame = 0;
static CYGFX_FLOAT                 s_fDrivenKm = 0.0f;

static CYGFX_U32                   s_fAutoDrivenKm = 0;
static CYGFX_U32                   preAutoDrivenKm = s_fAutoDrivenKm;
static CYGFX_U32 currentMileage = 0, preMileage = 0, totalMileage = 0;
static CYGFX_U32 curODO = 0,  subODO = 0;
static CYGFX_U32 curStatus = 0;
static CYGFX_U08  preGear = P;
uint8_t testData[4] = {0};
CYGFX_U32 preODO = 0;
uint8_t prev_HMI_reset = 0;

static CYGFX_SYNC_OBJECT_S   s_sync;
/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

extern "C" {
#include "BSP_Flash.h"

CYGFX_ERROR HudInit(void)
{
    CYGFX_ERROR ret = CYGFX_OK;

    s_engine.Init();

    /* First we initialize all classes required for all situations. */
    /* Init driver, setup command sequencer and open the display */
    UTIL_SUCCESS(ret, s_device.Open(128*1024));
    /* open a draw context */
    UTIL_SUCCESS(ret, s_draw_ctx.OpenDrawCtx(250, 250, 4 ));
    UTIL_SUCCESS(ret, CyGfx_BeSetAttribute(s_draw_ctx, CYGFX_BE_CTX_ATTR_GAMMA, CYGFX_BE_GAMMA_CORRECTED));

    // Access just the first resolution, based on SUPPORT_XXX definition
    UTIL_SUCCESS(ret, s_hud.OpenHud(s_resolution_types[0].width, s_resolution_types[0].height, 
        (CYGFX_DISP_CONTROLLER)DISPLAY_HUD, s_draw_ctx));

    UTIL_SUCCESS(ret, CyGfx_SyncReset(&s_sync));
    UTIL_SUCCESS(ret, CyGfx_DispGetVSync(s_hud.GetHandle(), &s_sync, 1));

    {
        CYGFX_U32 val, all;
        utVideoGetFreeTotal(&val);
        utVideoGetSize(&all);
        printf("%.2f of %.2f MB VRAM free\n", (CYGFX_FLOAT)val / (1024 * 1024), (CYGFX_FLOAT)all / (1024 * 1024));
    }

    return ret;
}

void writeFlashMemory(uint32_t addr, uint32_t* writeData)
{
    // Cy_FlashWriteWork(addr, writeData, CY_FLASH_DRIVER_NON_BLOCKING);
    Cy_FlashSectorErase(addr, CY_FLASH_DRIVER_NON_BLOCKING);

    // Wait for completion with counting
    while(Cy_Is_SROM_API_Completed() == false){}


    Cy_FlashWriteWork(addr, writeData, CY_FLASH_DRIVER_NON_BLOCKING);

    while (Cy_Is_SROM_API_Completed() == false)
    {
        /* code */
    }
}

void HudClose(void)
{
    s_hud.Close();
    s_draw_ctx.Close();
    s_device.Close();
}

CYGFX_ERROR HudUpdate(void)
{
    CYGFX_ERROR ret = CYGFX_OK;
#ifndef C_MODEL
    //do not continue if we don't have a new frame
    if (CyGfx_SyncWaitSync(&s_sync, 0) == CYGFX_ERP_ERR_SYNC_TIMEOUT)
        return ret;
#endif

    UTIL_SUCCESS(ret, CyGfx_SyncIncr(&s_sync, 1));

#if 1
    s_nFrame++;
    s_engine.Tick();
    CYGFX_FLOAT fSpeed = s_engine.GetSpeed();

    CYGFX_FLOAT fDist = fSpeed * 1.0f / 60.0f / 3600.0f;
    s_fDrivenKm += fDist;
    if (s_fDrivenKm > 999)
        s_fDrivenKm = 0;

    UTIL_SUCCESS(ret, s_hud.SetBusMatrix(((s_nFrame>>8)%8)));
    UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(((s_nFrame>>8)%4)));
    UTIL_SUCCESS(ret, s_hud.SetFrontObject(((s_nFrame>>8)%8)));
    UTIL_SUCCESS(ret, s_hud.SetGpsSignal(((s_nFrame>>8)%8)));

    UTIL_SUCCESS(ret, s_hud.SetSpeedLimit((int)(fSpeed/60) * 30));
    // UTIL_SUCCESS(ret, s_hud.SetDGauge(fSpeed));
    UTIL_SUCCESS(ret, s_hud.SetSpeed(fSpeed));
    UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(fSpeed/25));
    UTIL_SUCCESS(ret, s_hud.SetDrivingMode(fSpeed/70));
    UTIL_SUCCESS(ret, s_hud.SetLaneDirection(fSpeed/70));
    UTIL_SUCCESS(ret, s_hud.SetGear(fSpeed/50));
    UTIL_SUCCESS(ret, s_hud.SetOdometer(s_fDrivenKm*100, s_fDrivenKm*10000));
#else
// CAN No Error
if (Edison_Data.Can.Status.Connect_Status==CAN_CONNECT) {

    // Reset HMI Milleage
    if (prev_HMI_reset != Edison_Data.Can.Data[0].HMI_Reset)
    {
        if (Edison_Data.Can.Data[0].HMI_Reset == 1)
        {
            printf("%d\r\n", Edison_Data.Can.Data[0].HMI_Reset);
            prev_HMI_reset = Edison_Data.Can.Data[0].HMI_Reset;
            uint8_t testData1[4] = {0};
            // vFlashErase(CY_WFLASH_SM_SBM_TOP);
            writeToFlash(CY_WFLASH_SM_SBM_TOP, (uint32_t*)(testData1));
            global_totalOdometer = readFromFlash();
            // NVIC_SystemReset();

            subODO = 0;
            curODO = 0;
            preODO = 0;
            currentMileage = 0;
        }
    }

    // Front Object   
    switch (Edison_Data.Can.Data[0].Front_Object)
    {
    case 2: case 3: case 4: // car for 2, bicycle for 3, and pedestrian for 4
        UTIL_SUCCESS(ret, s_hud.SetFrontObject(Edison_Data.Can.Data[0].Front_Object));
        break;
    case 1: case 5: case 6: // unknow object for 1, 5, 6
        UTIL_SUCCESS(ret, s_hud.SetFrontObject(FRONT_OBJECT_UNKNOW));
        break;
    default: // no object for 0
        UTIL_SUCCESS(ret, s_hud.SetFrontObject(FRONT_OBJECT_NONE));
        break;
    }

    // GPS Signal: noSignal if GPS quality is invalid, in other cases, show a full signal image
    if (Edison_Data.Can.Data[0].GPS_Quality == 0)
    {
        UTIL_SUCCESS(ret, s_hud.SetGpsSignal(GPS_LEVEL_ERR));
    }
    else if (Edison_Data.Can.Data[0].GPS_Quality > 0 && Edison_Data.Can.Data[0].GPS_Quality < 7)
    {
        UTIL_SUCCESS(ret, s_hud.SetGpsSignal(GPS_LEVEL_4));
    }
    
    // Gear

    if (Edison_Data.Can.Data[0].Lane_Change_Det >= 0 && Edison_Data.Can.Data[0].Lane_Change_Det < 4)
    {
        UTIL_SUCCESS(ret, s_hud.SetLaneDirection(Edison_Data.Can.Data[0].Lane_Change_Det)); //
    }

    if (Edison_Data.Can.Data[0].Vehicle_Speed_Disp > SPEED_MAX)
    {
        Edison_Data.Can.Data[0].Vehicle_Speed_Disp = SPEED_MAX;
    }
    UTIL_SUCCESS(ret, s_hud.SetSpeed(Edison_Data.Can.Data[0].Vehicle_Speed_Disp));

    if (Edison_Data.Can.Data[0].Takeover_Status >= 0 && Edison_Data.Can.Data[0].Takeover_Status < 4)
    {
        UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(Edison_Data.Can.Data[0].Takeover_Status));
    }

    if (Edison_Data.Can.Data[0].Driving_Mode >= 0 && Edison_Data.Can.Data[0].Driving_Mode < 2)
    {
        UTIL_SUCCESS(ret, s_hud.SetDrivingMode(Edison_Data.Can.Data[0].Driving_Mode));
        if (Edison_Data.Can.Data[0].Driving_Mode == 0)
        {
            // if (Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info == 0 && Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed > 0)
            {
                if (Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed > SPEEDLIMIT_MAX)
                    Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed = SPEEDLIMIT_MAX;
                UTIL_SUCCESS(ret, s_hud.SetSpeedLimit(Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed));
            }   
            UTIL_SUCCESS(ret, s_hud.SetBusMatrix(GRAY_MATRIX));
            UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(E_WHEEL_NONE));

            preODO = Edison_Data.Can.Data[0].EvcuOdometer;
        }
        else 
        {
            // if (Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info == 1 && Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed > 0)
            {
                if (Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed > SPEEDLIMIT_MAX)
                    Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed = SPEEDLIMIT_MAX;
                UTIL_SUCCESS(ret, s_hud.SetSpeedLimit(Edison_Data.Can.Data[0].Vehicle_Speed_Limit_Info_Speed));
            } 
            UTIL_SUCCESS(ret, s_hud.SetBusMatrix(BLUE_MATRIX));
            UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(E_WHEEL_BLUE));

            curODO = Edison_Data.Can.Data[0].EvcuOdometer;
            if ((curODO != preODO) && (curODO > preODO))
            {
                subODO = curODO - preODO;
                currentMileage += subODO;
                if ((global_totalOdometer + subODO) < 10000000) // txcao_limit of total Odometer
                {
                    global_totalOdometer += subODO;
                }
                else
                {    global_totalOdometer = 9999999;  }
                
                preODO = Edison_Data.Can.Data[0].EvcuOdometer;
            }
        }   
    }

    // Gear Pos
    if (Edison_Data.Can.Data[0].Gear_Pos_Disp >= 0 && Edison_Data.Can.Data[0].Gear_Pos_Disp < 4)
    {
        UTIL_SUCCESS(ret, s_hud.SetGear(Edison_Data.Can.Data[0].Gear_Pos_Disp));
    }

    if (Edison_Data.Can.Data[0].AebsActive == 1) 
    {
        UTIL_SUCCESS(ret, s_hud.SetBusMatrix(RED_MATRIX));
        UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(2));
        UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(ABES_ACTION));
    } 
    else if (Edison_Data.Can.Data[0].SW_Estop == 1) 
    {
        UTIL_SUCCESS(ret, s_hud.SetBusMatrix(RED_MATRIX));
        UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(2));
        UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(ATTENTION_ACTION));
    } 
    else if (Edison_Data.Can.Data[0].Takeover_Status > 0 && Edison_Data.Can.Data[0].Takeover_Status < 3) 
    {
        UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(Edison_Data.Can.Data[0].Takeover_Status));
    }
    else if (Edison_Data.Can.Data[0].Takeover_Status == 3) 
    {
        UTIL_SUCCESS(ret, s_hud.SetBusMatrix(WARNING_MATRIX));
        UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(E_WHEEL_BLUE));
        UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(0));
    }    
    else if (Edison_Data.Can.Data[0].ActivaionAvailable_Status == 1 || Edison_Data.Can.Data[0].Sensor_Fail_Info == 1 || Edison_Data.Can.Data[0].Lane_Det_Mode == 1)
    {
        if (Edison_Data.Can.Data[0].ActivaionAvailable_Status == 1)
        {
            UTIL_SUCCESS(ret, s_hud.SetDrivingMode(3)); // no mode character when activationAvalable Status fail(AUTO, NORMAL)
        }
        
        UTIL_SUCCESS(ret, s_hud.SetBusMatrix(WARNING_MATRIX));
        UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(E_WHEEL_NONE));
        UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(SYSTEM_FAIL));
        UTIL_SUCCESS(ret, s_hud.SetLaneDirection(LANE_DIRECTION_NONE));
    } 

    // Odometer
    {
        if (global_totalOdometer > 9999999)
            global_totalOdometer = 9999999;
        if (currentMileage > 99999)
            currentMileage = 99999;
        UTIL_SUCCESS(ret, s_hud.SetOdometer(currentMileage, global_totalOdometer));
    }      
} 
else // CAN Status == ERROR
{
    UTIL_SUCCESS(ret, s_hud.SetFrontObject(FRONT_OBJECT_NONE));
    UTIL_SUCCESS(ret, s_hud.SetGpsSignal(GPS_LEVEL_4));
    UTIL_SUCCESS(ret, s_hud.SetSpeed(0));
    UTIL_SUCCESS(ret, s_hud.SetSpeedLimit(0));
    UTIL_SUCCESS(ret, s_hud.SetDrivingMode(3));
    UTIL_SUCCESS(ret, s_hud.SetGear(0));
    UTIL_SUCCESS(ret, s_hud.SetBusMatrix(WARNING_MATRIX));
    UTIL_SUCCESS(ret, s_hud.SetSteeringWheel(E_WHEEL_NONE));
    UTIL_SUCCESS(ret, s_hud.SetTakeoverSign(SYSTEM_FAIL));
    UTIL_SUCCESS(ret, s_hud.SetLaneDirection(LANE_DIRECTION_NONE));
}
#endif

    UTIL_SUCCESS(ret, s_hud.Draw());
#if 0
    if ((Edison_Data.Can.Data[0].Gear_Pos_Disp == 0) && (preGear != 0))
    {
        preGear = 0;
        // totalMileage += currentMileage;
        testData[0] = (global_totalOdometer & 0xff);
        testData[1] = (global_totalOdometer & 0xff00) >> 8;
        testData[2] = (global_totalOdometer & 0xff0000) >> 16;
        testData[3] = (global_totalOdometer & 0xff000000) >> 24;

        // writeFlashMemory(CY_WFLASH_SM_SBM_TOP, (uint32_t*)testData);
        writeToFlash(CY_WFLASH_SM_SBM_TOP, (uint32_t*)testData);
        // printf("Write OK\r\n");

        printf("global_totalOdometer = %ld, Gear = %d\r\n", global_totalOdometer, preGear);
    }
    else if ((Edison_Data.Can.Data[0].Gear_Pos_Disp != 0) && (preGear == 0))
    {
        preGear = Edison_Data.Can.Data[0].Gear_Pos_Disp;
        // uint32_t* p_TestFlsTop = (uint32_t*)CY_WFLASH_SM_SBM_TOP;
        // totalMileage = p_TestFlsTop[0];
        printf("ReadMillage = %ld, Gear = %d\r\n", global_totalOdometer, preGear);
    }
#endif
    return ret;
}
} /* extern "C" */
