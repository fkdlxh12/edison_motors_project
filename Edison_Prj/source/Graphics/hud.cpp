#include <string.h>
#include <math.h>
#include "cygfx_driver_api.h"

#include "pe_matrix.h"
#include "res.h"
#include "demodata.h"
#include "ut_class_window.h"
#include "ut_class_display.h"
#include "ut_class_device.h"

#include "hud.h"
#include "hweb.h"

#define SOURCE_WIDTH    800
#define SOURCE_HEIGHT   480

#define FADE_STEP 10
#define MAX_TEXT 64

#define BPP 8
#define TEXT_BPP 4

static struct WINDOW_DEFS{
    CYGFX_U32 x;
    CYGFX_U32 y;
    CYGFX_U32 w;
    CYGFX_U32 h;
} s_wins[] = {
	{0, 59, 503, 421},    // bus matrix
	{ 391, 86, 111, 64},  // steering wheel
    {222, 93, 61, 57}, 	  // Front Object
    { 737, 10, 40, 35},   // gps status
    { 5, 86, 105, 64},    //speed limit  
    {528, 87, 250, 61},   //speed signal
	{ 653, 171, 129, 26}, // gear
    {510, 280, 279, 167}, // Takeover Sign
    { 528, 174, 118, 21}, // driving mode
    { 391, 171, 111, 63}, // dial gauge
    { 60, 270, 387, 44},   // Lane direction
    {528, 210, 279, 40}    // Odometer
};

static CYGFX_DISP_TCON_PROPERTIES_S    ss_panel_XXX_800_480_tcon[] = {
    { 0x0408, 0x00000108}, /* CTRL (ENLVDS | BYPASS) */
    { 0x0410, 0x03040508}, /* MapBit3_0   ( R3, R4, R5, G0)     */
    { 0x0414, 0x11000102}, /* MapBit7_4   ( B1, R0, R1, R2)     */
    { 0x0418, 0x0b0c0d10}, /* MapBit11_8  ( G3, G4, G5, B0)     */
    { 0x041c, 0x191a090a}, /* MapBit15_12 ( VSYNC, EN, G1, G2)  */
    { 0x0420, 0x13141518}, /* MapBit19_16 ( B3, B4, B5, HSYNC)  */
    { 0x0424, 0x16171b12}, /* MapBit23_20 ( B6, B7, RES, B2)    */
    { 0x0428, 0x06070e0f}, /* MapBit27_24 ( R6, R7, G6, G7)     */
};

const CYGFX_U08 *mode_image[] = {
    manual_mode, auto_mode, abnormal_mode,
};

const CYGFX_U08 *steering_wheel_image[] = {
    wheel_white, wheel_blue, wheel_red,
};

const CYGFX_U08 *bus_matrix_image[] = {
    gray_503x421, blue_503x421, red_503x421, red_warning,
};

const CYGFX_U08 *speed_number[] = {
    big_0, big_1, big_2, big_3, big_4, big_5, big_6, big_7, big_8, big_9, kmh,
};

const CYGFX_U08 *speedlimit_number[] = {
    small_0, small_1, small_2, small_3, small_4, small_5, small_6, small_7, small_8, small_9,
};

const CYGFX_U08 *odometer_number[] = {
    tiny_0, tiny_1, tiny_2, tiny_3, tiny_4, tiny_5, tiny_6, tiny_7, tiny_8, tiny_9, period, slash, tiny_km, bg_mileage,
};

const CYGFX_U08 *gear_image[] = {
    gearP, gearR, gearN, gearD,
};

const CYGFX_U08 *takeoverstatus[] = {
    takeover_brake, takeover_accel, takeover_handle, takeover_estop, takeover_cancel, takeover_systemfail, takeover_attention, takeover_aebs,
};

const CYGFX_U08 *frontobject_image[] = {
    fo_unknow, fo_car, fo_bike, fo_people, fo_motorbike, fo_bigdog, fo_smalldog,
};

const CYGFX_U08 *gps_signal_image[] = {
    gps_signal0, gps_signal1, gps_signal2, gps_signal3, gps_nosignal,
};

const CYGFX_U08 *lane_direction_image[] = {
    left_direction, right_direction,
};

CYGFX_ERROR CImgWnd::Open(CYGFX_DISP display, CYGFX_S32 x, CYGFX_S32 y, CYGFX_S32 w, CYGFX_S32 h, 
    CYGFX_DISP_LAYER layer, CYGFX_DISP_SUB_LAYER subLayer, 
    CYGFX_U32 features, CYGFX_U32 blend_mode)
{
    CYGFX_U32 ret = CYGFX_OK;
    m_nSignLevel = 0;
    m_sSign = 0;
    m_sSignTarget = 0;
    UTIL_SUCCESS(ret, CSurfaceWindow<1>::Open(display, x, y, w, h, layer, subLayer, features, blend_mode));
    return ret;
}

CHudDisplay::CHudDisplay()
{
}

CHudDisplay::~CHudDisplay()
{
    Close();
}

void CHudDisplay::Close()
{
    CYGFX_U32 i;
    m_wndBusMatrix.Close();
    for (i = 0; i < MAX_STATUSWINDOW; i++)
    {
        m_wndStatus[i].Close();
    }
    m_wndSteeringWheels.Close();
    m_wndBgr.Close();
    m_wndFrontObject.Close();
    m_wndGpsSignal.Close();
    CDisplay::Close();
}

CYGFX_ERROR CHudDisplay::OpenHud(CYGFX_U32 nWidth, CYGFX_U32 nHeight, CYGFX_DISP_CONTROLLER display, CYGFX_BE_CONTEXT ctx)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 i;
    CYGFX_U32 PosID;

#if (defined(CY_USE_PSVP) && !CY_USE_PSVP)
    static CYGFX_DISP_PROPERTIES_S dispProps = CYGFX_DISP_PROPERTIES_INITIALIZER;
#endif

#if (defined(CY_USE_PSVP) && !CY_USE_PSVP)
    s_panel_XXX_800_480.pTconProps = ss_panel_XXX_800_480_tcon;
	UTIL_SUCCESS(ret, CDisplay::Open(&s_panel_XXX_800_480)); /* initialize FPD Link outport */
#else
    UTIL_SUCCESS(ret, CDisplay::Open(nWidth, nHeight, display));
#endif
    m_ctx = ctx;

    /* load all bitmaps */
    UTIL_SUCCESS(ret, m_sHudBgr.SurfLoadBitmap(Background, CYGFX_FALSE));
    UTIL_SUCCESS(ret, m_sSpeedLimit.SurfLoadBitmap(bg_speedlimit, CYGFX_FALSE));

    for (i = 0; i < 3; i++) {
        UTIL_SUCCESS(ret, m_sSteeringWheels[i].SurfLoadBitmap(steering_wheel_image[i], CYGFX_FALSE));
    }

    for (i = 0; i < MAX_FRONTOBJECTS; i++) {
        UTIL_SUCCESS(ret, m_sFrontObjects[i].SurfLoadBitmap(frontobject_image[i], CYGFX_FALSE));
    }

    for (i = 0; i < 5; i++) {
        UTIL_SUCCESS(ret, m_sGpsSignals[i].SurfLoadBitmap(gps_signal_image[i], CYGFX_FALSE));
    }

    for (i = 0; i < 4; i++) {
        UTIL_SUCCESS(ret, m_sBusMatrixs[i].SurfLoadBitmap(bus_matrix_image[i], CYGFX_FALSE));
    }

    UTIL_SUCCESS(ret, m_sDGaugeBG.SurfLoadBitmap(dial_gauge_bg, CYGFX_FALSE));
    UTIL_SUCCESS(ret, m_sDGaugeNeedle.SurfLoadBitmap(dial_gauge_needle, CYGFX_FALSE));
    UTIL_SUCCESS(ret, m_sDGaugeRound.SurfLoadBitmap(dial_gauge_round, CYGFX_FALSE));

    for (i = 0; i < 11; i++) {
        UTIL_SUCCESS(ret, m_sBigNumbers[i].SurfLoadBitmap(speed_number[i], CYGFX_FALSE));
    }

    for (i = 0; i < 11; i++) {
        UTIL_SUCCESS(ret, m_sSmallNumbers[i].SurfLoadBitmap(speedlimit_number[i], CYGFX_FALSE));
    }

    for (i = 0; i < 14; i++) {
        UTIL_SUCCESS(ret, m_sTinyNumbers[i].SurfLoadBitmap(odometer_number[i], CYGFX_FALSE));
    }

    for (i = 0; i < 11; i++) {
        UTIL_SUCCESS(ret, m_sGear[i].SurfLoadBitmap(gear_image[i], CYGFX_FALSE));
    }

    for (i = 0; i < 8; i++) {
        UTIL_SUCCESS(ret, m_sTakeoverStatus[i].SurfLoadBitmap(takeoverstatus[i], CYGFX_FALSE));
    }

    for (i = 0; i < 3; i++) {
        UTIL_SUCCESS(ret, m_sDrivingModes[i].SurfLoadBitmap(mode_image[i], CYGFX_FALSE));
    }

    for (i = 0; i < 2; i++) {
        UTIL_SUCCESS(ret, m_sLaneDirection[i].SurfLoadBitmap(lane_direction_image[i], CYGFX_FALSE));
    }

    UTIL_SUCCESS(ret, m_wndBusMatrix.Open(CDisplay::GetHandle(), s_wins[0].x, s_wins[0].y, s_wins[0].w, s_wins[0].h, 
        CYGFX_DISP_LAYER_1, CYGFX_DISP_SUB_LAYER_1, CYGFX_DISP_FEATURE_MULTI_LAYER, 
        CYGFX_WIN_BLEND_GLOBAL_ALPHA | CYGFX_WIN_BLEND_SOURCE_ALPHA | CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
    UTIL_SUCCESS(ret, m_wndBusMatrix.CreateBuffer(BPP, BPP, BPP, BPP));

    UTIL_SUCCESS(ret, m_wndSteeringWheels.Open(CDisplay::GetHandle(), s_wins[1].x, s_wins[1].y, s_wins[1].w, s_wins[1].h, 
        CYGFX_DISP_LAYER_2, CYGFX_DISP_SUB_LAYER_1, CYGFX_DISP_FEATURE_MULTI_LAYER, 
        CYGFX_WIN_BLEND_GLOBAL_ALPHA | CYGFX_WIN_BLEND_SOURCE_ALPHA | CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
    UTIL_SUCCESS(ret, m_wndSteeringWheels.CreateBuffer(BPP, BPP, BPP, BPP));

    UTIL_SUCCESS(ret, m_wndFrontObject.Open(CDisplay::GetHandle(), s_wins[2].x, s_wins[2].y, s_wins[2].w, s_wins[2].h, 
        CYGFX_DISP_LAYER_1, CYGFX_DISP_SUB_LAYER_2, CYGFX_DISP_FEATURE_MULTI_LAYER, 
        CYGFX_WIN_BLEND_GLOBAL_ALPHA | CYGFX_WIN_BLEND_SOURCE_ALPHA | CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
    UTIL_SUCCESS(ret, m_wndFrontObject.CreateBuffer(BPP, BPP, BPP, BPP));

    UTIL_SUCCESS(ret, m_wndGpsSignal.Open(CDisplay::GetHandle(), s_wins[3].x, s_wins[3].y, s_wins[3].w, s_wins[3].h, 
        CYGFX_DISP_LAYER_1, CYGFX_DISP_SUB_LAYER_3, CYGFX_DISP_FEATURE_MULTI_LAYER, 
        CYGFX_WIN_BLEND_GLOBAL_ALPHA | CYGFX_WIN_BLEND_SOURCE_ALPHA | CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
    UTIL_SUCCESS(ret, m_wndGpsSignal.CreateBuffer(BPP, BPP, BPP, BPP));
    
    for (i = 0; i < ODOMETER + 1; i++) {
        PosID = i + 4;
        if (i < DIAL_GAUGE) {
            UTIL_SUCCESS(ret, m_wndStatus[i].Open(CDisplay::GetHandle(), s_wins[PosID].x, s_wins[PosID].y, s_wins[PosID].w, s_wins[PosID].h, 
            CYGFX_DISP_LAYER_1, (CYGFX_DISP_SUB_LAYER)(PosID + CYGFX_DISP_SUB_LAYER_DEFAULT), CYGFX_DISP_FEATURE_MULTI_LAYER, 
            CYGFX_WIN_BLEND_GLOBAL_ALPHA | CYGFX_WIN_BLEND_SOURCE_ALPHA | CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
        } else {
            UTIL_SUCCESS(ret, m_wndStatus[i].Open(CDisplay::GetHandle(), s_wins[PosID].x, s_wins[PosID].y, s_wins[PosID].w, s_wins[PosID].h, 
            CYGFX_DISP_LAYER_2, (CYGFX_DISP_SUB_LAYER)(PosID - CYGFX_DISP_SUB_LAYER_7), CYGFX_DISP_FEATURE_MULTI_LAYER, 
            CYGFX_WIN_BLEND_GLOBAL_ALPHA | CYGFX_WIN_BLEND_SOURCE_ALPHA | CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
        }
        if (i == LANE_DIRECTION)
            UTIL_SUCCESS(ret, m_wndStatus[i].CreateGrayBuffer(0, TEXT_BPP));
        else
            UTIL_SUCCESS(ret, m_wndStatus[i].CreateBuffer(CYGFX_SM_FORMAT_R5G6B5));
        UTIL_SUCCESS(ret, CyGfx_WinSetAttribute(m_wndStatus[i], CYGFX_WIN_ATTR_COLOR, 0xffffffff)); 
    }

    UTIL_SUCCESS(ret, m_wndBgr.Open(this->GetHandle(), 0, 0, GetWidth(), GetHeight(), CYGFX_DISP_LAYER_0, 
        CYGFX_DISP_SUB_LAYER_DEFAULT, CYGFX_DISP_FEATURE_DECODE, CYGFX_WIN_BLEND_SOURCE_MULTIPLY_ALPHA));
    UTIL_SUCCESS(ret, CyGfx_WinSetAttribute(m_wndBgr, CYGFX_WIN_ATTR_COLOR, 0xffffffff));
    /* The Background buffer will show a compressed pre-warped full size image
       For dynamic changing of warping buffer */
    UTIL_SUCCESS(ret, CyGfx_SmAssignBuffer(m_wndBgr.m_surface, GetWidth(), GetHeight(), CYGFX_SM_FORMAT_A8, 0, 0));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(m_wndBgr.m_surface, CYGFX_SM_ATTR_COMPRESSION_FORMAT, CYGFX_SM_COMP_RLAD_UNIFORM));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(m_wndBgr.m_surface, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS, 0x00000003));

    CYGFX_U32 size;
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(m_wndBgr.m_surface, CYGFX_SM_ATTR_SIZEINBYTES, &size));
    void* addr = utVideoAlloc(size, 32, 0);
    if (addr == NULL)
        return CYGFX_ERR;
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(m_wndBgr.m_surface, CYGFX_SM_ATTR_VIRT_ADDRESS, (CYGFX_U32)addr));
    UTIL_SUCCESS(ret, UpdateBgr());

    m_fSpeed = 0;
    m_uTakeoverSign = NO_ACTION;

    m_bUpdateStatus[SPEED_LIMIT] = CYGFX_TRUE;
    m_bUpdateStatus[SPEED_SIGNAL] = CYGFX_TRUE;
    m_bUpdateStatus[GEAR] = CYGFX_TRUE;
    m_bUpdateStatus[TAKEOVER_SIGN] = CYGFX_TRUE;
    m_bUpdateStatus[DRIVING_MODE] = CYGFX_TRUE;
    m_bUpdateStatus[DIAL_GAUGE] = CYGFX_TRUE;
    m_bUpdateStatus[LANE_DIRECTION] = CYGFX_TRUE;
    m_bUpdateStatus[ODOMETER]       = CYGFX_TRUE;

    UTIL_SUCCESS(ret, DrawStatusData(SPEED_SIGNAL));
    UTIL_SUCCESS(ret, DrawStatusData(GEAR));
    UTIL_SUCCESS(ret, DrawStatusData(ODOMETER));

    return ret;
}

CYGFX_ERROR CHudDisplay::Draw()
{
    CYGFX_ERROR ret = CYGFX_OK;

    UTIL_SUCCESS(ret, m_wndBusMatrix.Draw(NO_FADE));
    UTIL_SUCCESS(ret, m_wndSteeringWheels.Draw(FADE));
    UTIL_SUCCESS(ret, m_wndFrontObject.Draw(FADE));
    UTIL_SUCCESS(ret, m_wndGpsSignal.Draw(NO_FADE));

    UTIL_SUCCESS(ret, DrawStatusData(SPEED_LIMIT));
    
    UTIL_SUCCESS(ret, DrawStatusData(SPEED_SIGNAL));
    UTIL_SUCCESS(ret, DrawStatusData(GEAR));
    UTIL_SUCCESS(ret, DrawStatusData(TAKEOVER_SIGN));
    UTIL_SUCCESS(ret, DrawStatusData(DRIVING_MODE));
    // UTIL_SUCCESS(ret, DrawStatusData(DIAL_GAUGE));
    UTIL_SUCCESS(ret, DrawStatusData(LANE_DIRECTION));
    UTIL_SUCCESS(ret, DrawStatusData(ODOMETER));

    return ret;
}

CYGFX_ERROR CHudDisplay::SetFrontObject(CYGFX_U32 id)
{
    if (id == 0)
        m_wndFrontObject.SetSign(0);
    else
        m_wndFrontObject.SetSign(m_sFrontObjects[(id-1)%MAX_FRONTOBJECTS]);
    return CYGFX_OK;
}

CYGFX_ERROR CHudDisplay::SetGpsSignal(CYGFX_U32 id)
{
    m_wndGpsSignal.SetSign(m_sGpsSignals[(id-1)%MAX_GPSSIGNALS]);
    return CYGFX_OK;
}

CYGFX_ERROR CHudDisplay::SetSteeringWheel(CYGFX_U32 id)
{
    if (id > 2)
        m_wndSteeringWheels.SetSign(0);
    else
        m_wndSteeringWheels.SetSign(m_sSteeringWheels[id%MAX_WHEELSIGNS]);

    return CYGFX_OK;
}

CYGFX_ERROR CHudDisplay::SetBusMatrix(CYGFX_U32 id)
{
    CYGFX_ERROR ret = CYGFX_OK;
    m_wndBusMatrix.SetSign(m_sBusMatrixs[id % MAX_BUSMATRIXS]);
    return ret;
}

CYGFX_ERROR CHudDisplay::UpdateBgr()
{
    CYGFX_ERROR ret = CYGFX_OK;

    if (m_ctx == 0) return CYGFX_OK;

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_STORE, m_wndBgr.m_surface));
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sHudBgr));
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_DST, 0));

    UTIL_SUCCESS(ret, CyGfx_BeSetAttribute(m_ctx, CYGFX_BE_CTX_ATTR_DITHER_ALPHA, CYGFX_TRUE));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));
    UTIL_SUCCESS(ret, CyGfx_BeSetAttribute(m_ctx, CYGFX_BE_CTX_ATTR_DITHER_ALPHA, CYGFX_FALSE));

    /* Get a sysnc from the Bliteng and push it in the win pipe */
    UTIL_SUCCESS(ret, CyGfx_BeGetSync(m_ctx, m_wndBgr.GetSync()));
    UTIL_SUCCESS(ret, CyGfx_WinWaitSync(m_wndBgr, m_wndBgr.GetSync()));
    SEND_SURFACE(m_wndBgr.m_surface);
    m_wndBgr.Swap();

    return ret;
}

CYGFX_ERROR CHudDisplay::DrawStatusData(CYGFX_U32 winId)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 x[3] = {0};
    CYGFX_U32 y[5] = {0};
    CYGFX_U32 z[7] = {0};

    if (m_bUpdateStatus[winId] == CYGFX_FALSE)
        return ret;

    if (m_wndStatus[winId].GetWindowHandle() == 0)
        return CYGFX_OK;

    /* Do nothing if the last change is not yet finished */
    if (m_wndStatus[winId].SyncReady() == CYGFX_FALSE)
    {
        return CYGFX_OK;
    }

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_STORE | CYGFX_BE_TARGET_DST, m_wndStatus[winId].m_surface));
    UTIL_SUCCESS(ret, CyGfx_BeFill(m_ctx, 0, 0, m_wndStatus[winId].GetWidth(), m_wndStatus[winId].GetHeight()));

    switch (winId)
    {
    case SPEED_LIMIT:
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sSpeedLimit));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));

        x[0] = (CYGFX_U32)(m_fSpeedLimit) / 100;
        x[1] = (CYGFX_U32)(m_fSpeedLimit) % 100 / 10;
        x[2] = (CYGFX_U32)(m_fSpeedLimit) % 10;

        for (int i = 0; i < 3; i++)
        {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sSmallNumbers[x[i]]));
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, (m_sSmallNumbers[0].GetWidth() * i + i) + 28, 27));
        }
        break;

    case SPEED_SIGNAL:
        // printf ("\nm_fSpeed = %f", m_fSpeed);
        x[0] = (CYGFX_U32)(m_fSpeed) / 100;
        x[1] = (CYGFX_U32)(m_fSpeed) % 100 / 10;
        x[2] = (CYGFX_U32)(m_fSpeed) % 10;

        for (int i = 0; i < 3; i++)
        {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sBigNumbers[x[i]]));
            if (m_uDrivingMode == 0)
                CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(128, 128, 128, 128));
            else if (m_uDrivingMode == 1)
                CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(20, 147, 245, 240));
            
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, (m_sBigNumbers[0].GetWidth() + 1) * i, 0));
        }

        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sBigNumbers[10]));
        if (m_uDrivingMode == 0)
            CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(128, 128, 128, 128));
        else if (m_uDrivingMode == 1)
            CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(20, 147, 245, 240));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 194, 0));
        break;

    case GEAR:
        for (int i = 0; i < 4; i++)
        {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sGear[i]));
            if (i == m_fGear) {
                if (i == R)
                {
                    CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(255, 0, 0, 255));
                } else {
                    CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(255, 255, 255, 255));
                }
            } else {
                if (i == R)
                {
                    CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(128, 0, 0, 128));
                } else {
                    CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(128, 128, 128, 128));
                }
            }
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, (m_sGear[0].GetWidth() + 2) * i, 0));
        }
        break;

    case TAKEOVER_SIGN:
        TakeoverSign(m_uTakeoverSign);
        break;

    case DRIVING_MODE:      
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sDrivingModes[m_uDrivingMode]));

        if (m_uDrivingMode < 3)
        {
            if (m_uDrivingMode == 1)
                CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(20, 147, 245, 240));
            else if (m_uDrivingMode == 2)
                CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(255, 0, 0, 255));
            else
                CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(128, 128, 128, 128));
            
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));
        }
        break;

    case DIAL_GAUGE:
    /*  UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sDGaugeBG));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sDGaugeRound));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 15, 20));

        // -97 degree to 97 degree
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sDGaugeNeedle));
        CYGFX_FLOAT fAngle = (CYGFX_FLOAT)(100.0f - m_fSpeed);
        SetMatrix(m_ctx, fAngle); // SetMatrix(m_ctx, 270 + i * (-54));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));

        swprintf (szBuffer, MAX_TEXT, L"%03.0f", m_fSpeed);
        DrawText(m_fDeckerB17, szBuffer, 23, 6, 250, 129, 40);*/
        break;

    case LANE_DIRECTION:
        // txcao _ Add blink when using Warning Mode
        if (m_uLaneDirection > 0)
        {
            if (m_uLaneDirection == 3)
            { // no need count frame for 1s blinking
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sLaneDirection[0]));
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sLaneDirection[1]));
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 343, 0));
            }
            else
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sLaneDirection[m_uLaneDirection - 1]));
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 343 * (m_uLaneDirection - 1), 0));                
            }            
        }
        break;

    case ODOMETER:
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[13]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 2, 17));
/*
        int point_pos = 0;
        y[0] = (CYGFX_U32)(m_fOdometer) / 10000;
        y[1] = (CYGFX_U32)(m_fOdometer) % 10000 / 1000;
        y[2] = (CYGFX_U32)(m_fOdometer) % 1000 / 100;
        y[3] = (CYGFX_U32)(m_fOdometer) % 100 / 10;
        y[4] = (CYGFX_U32)(m_fOdometer) % 10;

        if (y[0] > 0) {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[0]]));            
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));
        }
        if (y[1] > 0) {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[1]]));            
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, m_sTinyNumbers[0].GetWidth() + 1, 0));
        }
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[2]]));            
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, (m_sTinyNumbers[0].GetWidth() + 1) * 2, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[3]]));            
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, (m_sTinyNumbers[0].GetWidth() + 1) * 3, 0));

        point_pos = (m_sTinyNumbers[0].GetWidth() + 1) * 4;
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[10]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[4]]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos + m_sTinyNumbers[10].GetWidth() + 1, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[12]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos + m_sTinyNumbers[0].GetWidth() + m_sTinyNumbers[10].GetWidth() + 2, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[11]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos + m_sTinyNumbers[0].GetWidth() + m_sTinyNumbers[10].GetWidth() + m_sTinyNumbers[12].GetWidth() + 4, 0));

        // Total Mileage 
        z[0] = (CYGFX_U32)(m_fTotalOdometer) / 1000000;
        z[1] = (CYGFX_U32)(m_fTotalOdometer) % 1000000 / 100000;
        z[2] = (CYGFX_U32)(m_fTotalOdometer) % 100000 / 10000;
        z[3] = (CYGFX_U32)(m_fTotalOdometer) % 10000 / 1000;
        z[4] = (CYGFX_U32)(m_fTotalOdometer) % 1000 / 100;
        z[5] = (CYGFX_U32)(m_fTotalOdometer) % 100 / 10;
        z[6] = (CYGFX_U32)(m_fTotalOdometer) % 10;

        point_pos = (m_sTinyNumbers[0].GetWidth() + 1) * 5 + (m_sTinyNumbers[10].GetWidth() + 1) + (m_sTinyNumbers[11].GetWidth() + 1) + (m_sTinyNumbers[12].GetWidth() + 1) + 2;
        for (int i = 0; i < 6; i++)
        {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[i]]));       // z[i]     
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos, 0));
            point_pos += m_sTinyNumbers[0].GetWidth() + 1;
        }
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[10]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[6]]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos + m_sTinyNumbers[10].GetWidth() + 1, 0));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[12]));
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, point_pos + m_sTinyNumbers[0].GetWidth() + m_sTinyNumbers[10].GetWidth() + 2, 0)); */

        int x_x = 84; // first = 81, add 3 for the space in new request
        int y_y = 0;
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[11])); // slash
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, y_y));
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[12])); // if odometer = 0
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1), 0));

        // Odometer
        if (m_fOdometer == 0)  // if odometer = 0
        {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[0]));
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1), 0));
        }
        else
        {
            y[0] = (CYGFX_U32)(m_fOdometer) / 10000;
            y[1] = (CYGFX_U32)(m_fOdometer) % 10000 / 1000;
            y[2] = (CYGFX_U32)(m_fOdometer) % 1000 / 100;
            y[3] = (CYGFX_U32)(m_fOdometer) % 100 / 10;
            y[4] = (CYGFX_U32)(m_fOdometer) % 10;
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[4]])); // y[4] at 0
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1), 0));
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[10])); // period
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1) - (m_sTinyNumbers[10].GetWidth() + 1), 0));
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[3]])); // y[3] at 1
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1)*2 - (m_sTinyNumbers[10].GetWidth() + 1), 0));
            if ((y[0] > 0) || (y[1] > 0) || (y[2] > 0))
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[2]])); // y[2] at 2
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1)*3 - (m_sTinyNumbers[10].GetWidth() + 1), 0));
            }
            if ((y[0] > 0) || (y[1] > 0))
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[1]])); // y[1] at 3
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1)*4 - (m_sTinyNumbers[10].GetWidth() + 1), 0));
            }
            if ((y[0] > 0))
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[y[0]])); // y[0] at 4
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x - (m_sTinyNumbers[12].GetWidth() + 1) - 3 - (m_sTinyNumbers[0].GetWidth() + 1)*5 - (m_sTinyNumbers[10].GetWidth() + 1), 0));
            }
        }

        x_x = x_x + (m_sTinyNumbers[11].GetWidth() + 1);
        int zz = 0;

        // Total Odometer
        if (m_fTotalOdometer == 0)
        {
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[0]));
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
            x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
        }
        else
        {
            z[0] = (CYGFX_U32)(m_fTotalOdometer) / 1000000;
            z[1] = (CYGFX_U32)(m_fTotalOdometer) % 1000000 / 100000;
            z[2] = (CYGFX_U32)(m_fTotalOdometer) % 100000 / 10000;
            z[3] = (CYGFX_U32)(m_fTotalOdometer) % 10000 / 1000;
            z[4] = (CYGFX_U32)(m_fTotalOdometer) % 1000 / 100;
            z[5] = (CYGFX_U32)(m_fTotalOdometer) % 100 / 10;
            z[6] = (CYGFX_U32)(m_fTotalOdometer) % 10;

            if (z[0] > 0) {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[0]])); // z[0] at 0
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
                x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
                zz = 1;
            }
            if (z[1] > 0 || zz == 1)
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[1]])); // z[1] at 1
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
                x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
                zz = 1;
            }
            if (z[2] > 0 || zz == 1)
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[2]])); // z[2] at 2
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
                x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
                zz = 1;
            }
            if (z[3] > 0 || zz == 1)
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[3]])); // z[3] at 3
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
                x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
                zz = 1;
            }
            if (z[4] > 0 || zz == 1)
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[4]])); // z[4] at 4
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
                x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
                zz = 1;
            }
            // if (z[5] > 0 || zz == 1)
            {
                UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[5]])); // z[5] at 5
                UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
                x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
            }
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[10])); // z[5] at 5
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
            x_x = x_x + (m_sTinyNumbers[10].GetWidth() + 1);
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[z[6]])); // z[5] at 5
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));
            x_x = x_x + (m_sTinyNumbers[0].GetWidth() + 1);
        }
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTinyNumbers[12])); // z[5] at 5
        UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, x_x, 0));                    

        break;

    default:
        return ret;
    }

    UTIL_SUCCESS(ret, CyGfx_BeGetSync(m_ctx, m_wndStatus[winId].GetSync()));
    UTIL_SUCCESS(ret, CyGfx_WinWaitSync(m_wndStatus[winId], m_wndStatus[winId].GetSync()));

    UTIL_SUCCESS(ret, m_wndStatus[winId].Swap());

    m_bUpdateStatus[winId] = CYGFX_FALSE;

    return ret;
}

void CHudDisplay::SetMatrix(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT angle)
{
    Mat3x2 mat;
    utMat3x2LoadIdentity(mat);
    utMat3x2Translate(mat, 38.0f, 29.0f);
    utMat3x2Rot(mat, angle);
    utMat3x2Translate(mat, - 4, - 2);
    CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);
}

void CHudDisplay::DrawText(FT_CONTEXT_CONTAINER fontType, const wchar_t *strText, CYGFX_S32 x, CYGFX_S32 y, CYGFX_S32 r, CYGFX_S32 g, CYGFX_S32 b)
{
    CYGFX_ERROR ret = CYGFX_OK;
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, 0));
    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(r, g, b, 255)));
    UTIL_SUCCESS(ret, utFtTextOut(&fontType, x, y, strText));
}

void CHudDisplay::TakeoverSign(CYGFX_U32 signs)
{
    CYGFX_ERROR ret = CYGFX_OK;

    switch (signs)
    {
        case BRAKE_PEDAL_PRESS:
        case ACC_PEDAL_PRESS:
        case HANDLE_ACTION:
        case EMERGENCY_BUTTON:
        case CANCEL_ACTION:
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTakeoverStatus[signs - 1]));
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 13));
            break;

        case SYSTEM_FAIL:
        case ATTENTION_ACTION:
        case ABES_ACTION:
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(m_ctx, CYGFX_BE_TARGET_SRC, m_sTakeoverStatus[signs - 1]));
            UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(m_ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(255, 0, 0, 255)));
            UTIL_SUCCESS(ret, CyGfx_BeBlt(m_ctx, 0, 0));
            break;

        default:
            break;
    }
}

CYGFX_ERROR CImgWnd::Draw(CYGFX_U32 fade_method)
{
    CYGFX_U32 ret = CYGFX_OK;

    if (CWindow::GetWindowHandle() == 0) return CYGFX_OK;

    /* Do nothing if the last change is not yet finished */
    if (!CWindow::SyncReady())
    {
        return CYGFX_OK;
    }

    if ((m_sSignTarget == m_sSign) && (m_nSignLevel == 255))
        return ret;
    if ((m_sSignTarget == m_sSign) && (m_sSign == 0) && (m_nSignLevel == 0))
        return ret;

    if (fade_method)
    {
        /* fade out */
        if ((m_nSignLevel > 0) && (m_sSignTarget != m_sSign))
        {
            if (m_nSignLevel > FADE_STEP)
                m_nSignLevel -= FADE_STEP;
            else
                m_nSignLevel = 0;
                
            //use global alpha to blend the window. alpha output = surface alpha * m_nSignLevel / 255.
            UTIL_SUCCESS(ret, CyGfx_WinSetAttribute(m_win, CYGFX_WIN_ATTR_COLOR,  m_nSignLevel));
            UTIL_SUCCESS(ret, CWindow::Commit());
            return ret;
        }
    }

    m_sSign = m_sSignTarget;

    if (m_nSignLevel == 0 || fade_method == NO_FADE)
    {
        if (m_surface.HasBuffer())
        {
            CCtx ctx;
            ctx.Init();
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, m_sSignTarget));
            UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_STORE, m_surface));
            if (m_sSignTarget)
            {
                CYGFX_U32 val;
                UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(m_surface, CYGFX_SM_ATTR_COLORBITS, &val));
                if ((val & 0xff) == 0)
                {
                    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLORMULTI, CYGFX_TRUE));
                }
                UTIL_SUCCESS(ret, CyGfx_BeSelectArea(ctx, CYGFX_BE_TARGET_STORE));
                UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, 
                    (utSurfWidth(m_surface)- utSurfWidth(m_sSignTarget)) * 0.5f, 
                    (utSurfHeight(m_surface)- utSurfHeight(m_sSignTarget)) * 0.5f));
                UTIL_SUCCESS(ret, CyGfx_BeSelectArea(ctx, CYGFX_BE_TARGET_SRC));
                /* Get a sync from the Bliteng and push it in the win pipe */
                UTIL_SUCCESS(ret, CyGfx_BeGetSync(ctx, GetSync()));
                UTIL_SUCCESS(ret, CyGfx_WinWaitSync(m_win, GetSync()));
            }
            else
            {
                UTIL_SUCCESS(ret, CyGfx_BeFill(ctx, 0, 0, utSurfWidth(m_surface), utSurfHeight(m_surface)));
            }
        }
        else
        {
            UTIL_SUCCESS(ret, CSurfaceWindow<1>::m_surface.Copy(m_sSignTarget)); 
        }
        UTIL_SUCCESS(ret, CSurfaceWindow<1>::Swap());
    }
    switch (fade_method)
    {
        case NO_FADE:
            m_nSignLevel = 255;
            break;
        case FADE:
            if ((m_nSignLevel + FADE_STEP) > 255)
                m_nSignLevel = 255;
            else
                m_nSignLevel += FADE_STEP;
            break;
        default:
            break;
    }
    
    //use global alpha to blend the display. alpha output = surface alpha * m_nSignLevel / 255.
    UTIL_SUCCESS(ret, CyGfx_WinSetAttribute(m_win, CYGFX_WIN_ATTR_COLOR,  m_nSignLevel));
    UTIL_SUCCESS(ret, CWindow::Commit());

    return ret;
}
