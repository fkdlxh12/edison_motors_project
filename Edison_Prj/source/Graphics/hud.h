/**
 * \file        hud.h
 * \brief       Head up unit interface
 */

#ifdef __cplusplus
extern "C"
{
#endif

#define MAX_WHEELSIGNS      3
#define MAX_BUSMATRIXS      4
#define MAX_DRIVINGSIGNS    4
#define MAX_FRONTOBJECTS    7
#define MAX_GPSSIGNALS      5
#define MAX_STATUSWINDOW    8//7

enum status_index{
    SPEED_LIMIT,
    SPEED_SIGNAL,
    GEAR,
    TAKEOVER_SIGN,
    DRIVING_MODE,
	DIAL_GAUGE,
    LANE_DIRECTION,
    ODOMETER
};

enum action_list{
    NO_ACTION,
    BRAKE_PEDAL_PRESS,
    ACC_PEDAL_PRESS,
    HANDLE_ACTION,
    EMERGENCY_BUTTON,
    CANCEL_ACTION,
    SYSTEM_FAIL,
    ATTENTION_ACTION,
    ABES_ACTION
};

enum fade_methods{
    NO_FADE,
    FADE
};

enum driving_modes{
    MODE_NORMAL,
    MODE_AUTO,
    MODE_ABNORMAL
};

enum gears{
    P,
    R,
    N,
    D
};

enum e_gps_signal {
    GPS_NONE,
    GPS_LEVEL_1,
    GPS_LEVEL_2,
    GPS_LEVEL_3,
    GPS_LEVEL_4,
    GPS_LEVEL_ERR
};

enum e_front_object {
    FRONT_OBJECT_NONE,
    FRONT_OBJECT_UNKNOW,
    FRONT_OBJECT_CAR,
    FRONT_OBJECT_BIKE,
    FRONT_OBJECT_PEOPLE,
    FRONT_OBJECT_BIGDOG,
    FRONT_OBJECT_SMALLDOG
};

enum e_lane_direction {
    LANE_DIRECTION_NONE,
    LANE_DIRECTION_LEFT,
    LANE_DIRECTION_RIGHT
};

enum e_busmatrix {
    GRAY_MATRIX,
    BLUE_MATRIX,
    RED_MATRIX,
    WARNING_MATRIX
};

enum e_steering_wheel {
    E_WHEEL_WHITE,
    E_WHEEL_BLUE,
    E_WHEEL_RED,
    E_WHEEL_NONE
};

class CImgWnd: public  CSurfaceWindow<1>
{
private:    
    CYGFX_SURFACE m_sSign;
    CYGFX_SURFACE m_sSignTarget;
    CYGFX_S32     m_nSignLevel;
    CYGFX_U32     m_sValue;
    CYGFX_U32     m_sDGLevel;
    FT_CONTEXT_CONTAINER m_scSmall;
public:
    CYGFX_ERROR Open(CYGFX_DISP display, int x, int y, int w, int h, 
        CYGFX_DISP_LAYER layer, CYGFX_DISP_SUB_LAYER subLayer,
        CYGFX_U32 features, CYGFX_U32 blend_mode);
    void SetSign(CYGFX_SURFACE sSign){m_sSignTarget = sSign;}
    void SetDgauge(CYGFX_SURFACE sSign, CYGFX_S32 m_sValue, FT_CONTEXT_CONTAINER wndFont){m_sSignTarget = sSign;m_sDGLevel = m_sValue; m_scSmall = wndFont;}
    CYGFX_ERROR Draw(CYGFX_U32 fade_method);
};

class CHudDisplay:public CDisplay
{
public:
public:
    CHudDisplay();
    ~CHudDisplay();
    CYGFX_ERROR OpenHud(CYGFX_U32 nWidth, CYGFX_U32 nHeight, CYGFX_DISP_CONTROLLER display, CYGFX_BE_CONTEXT ctx);
    CYGFX_ERROR Draw();
    void Close();
    void SetMatrix(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT fAngle);
    void DrawText(FT_CONTEXT_CONTAINER fontType, const wchar_t *strText, CYGFX_S32 x, CYGFX_S32 y, CYGFX_S32 r, CYGFX_S32 g, CYGFX_S32 b);
    CYGFX_ERROR SetSteeringWheel(CYGFX_U32 id);
    CYGFX_ERROR SetBusMatrix(CYGFX_U32 id);
    CYGFX_ERROR SetFrontObject(CYGFX_U32 id);
    CYGFX_ERROR SetGpsSignal(CYGFX_U32 id);
    CYGFX_ERROR SetSpeedLimit(float fSpeedLimit){m_fSpeedLimit = fSpeedLimit; m_bUpdateStatus[SPEED_LIMIT] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetDGauge(CYGFX_U32 nDgauge){m_bUpdateStatus[DIAL_GAUGE] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetSpeed(float fSpeed1){m_fSpeed = fSpeed1; m_bUpdateStatus[SPEED_SIGNAL] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetGear(CYGFX_U32 nGear){m_fGear = nGear; m_bUpdateStatus[GEAR] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetTakeoverSign(CYGFX_U32 takeoverSign){m_uTakeoverSign = takeoverSign; m_bUpdateStatus[TAKEOVER_SIGN] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetDrivingMode(CYGFX_U32 drivingMode){m_uDrivingMode = drivingMode; m_bUpdateStatus[DRIVING_MODE] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetLaneDirection(CYGFX_U32 laneDirection){m_uLaneDirection = laneDirection; m_bUpdateStatus[LANE_DIRECTION] = CYGFX_TRUE; return CYGFX_OK;}
    CYGFX_ERROR SetOdometer(CYGFX_U32 fOdometer, CYGFX_U32 fTotalOdometer){m_fOdometer = fOdometer; m_fTotalOdometer = fTotalOdometer; m_bUpdateStatus[ODOMETER] = CYGFX_TRUE; return CYGFX_OK;}
private: 
    CYGFX_ERROR DrawSpeed();
    CYGFX_ERROR DrawStatusData(CYGFX_U32 winId);
    CYGFX_ERROR UpdateBgr();
    void TakeoverSign(CYGFX_U32 signs);

    CYGFX_BE_CONTEXT m_ctx;

    // Window variables
    CSurfaceWindow<1> m_wndBgr;
    CSurfaceWindow<2> m_wndStatus[MAX_STATUSWINDOW];
    CImgWnd m_wndBusMatrix;
    CImgWnd m_wndSteeringWheels;
    CImgWnd m_wndFrontObject;
    CImgWnd m_wndGpsSignal;

    CSurface<1> m_sHudBgr, m_sSpeedLimit;
    CSurface<1> m_sSteeringWheels[MAX_WHEELSIGNS];
    CSurface<1> m_sBusMatrixs[MAX_BUSMATRIXS];
    CSurface<1> m_sFrontObjects[MAX_FRONTOBJECTS];
    CSurface<1> m_sGpsSignals[MAX_GPSSIGNALS];
    CSurface<1> m_sBigNumbers[11];
    CSurface<1> m_sSmallNumbers[10];
    CSurface<1> m_sTinyNumbers[14];
    CSurface<1> m_sTakeoverStatus[8];
    CSurface<1> m_sGear[4];
    CSurface<1> m_sDrivingModes[3];
    CSurface<1> m_sDGaugeBG, m_sDGaugeNeedle, m_sDGaugeRound;
    CSurface<1> m_sLaneDirection[2];

    CYGFX_FLOAT m_fSpeed;
    CYGFX_FLOAT m_fSpeedLimit;
    CYGFX_U32 m_uDGauge;
    CYGFX_U32 m_uTakeoverSign;
    CYGFX_U32 m_uDrivingMode;
    CYGFX_U32 m_fGear;
    CYGFX_U32 m_uLaneDirection;
    CYGFX_BOOL m_bUpdateStatus[MAX_STATUSWINDOW];
    CYGFX_DOUBLE m_fOdometer, m_fTotalOdometer;
    
};

#ifdef __cplusplus
} /* extern "C" */
#endif
