/*******************************************************************************
NEED SCENARIOS TO CREATE VARIABLE AND DATA CRAWLING FUNCTIONS
*******************************************************************************/

#include <vector>
#include "cygfx_basetypes.h"
#include "cygfx_defines.h"
#include "BSP_Can.h"
#include "Edison_System.h"

using namespace std;

class Engine
{
public:
struct POS
{
    CYGFX_FLOAT time;
    CYGFX_FLOAT speed;
    CYGFX_FLOAT spin;
};
public:
    Engine();
    ~Engine();
    void Init();
    CYGFX_FLOAT GetSpeed(){return m_Speed;}
    CYGFX_BOOL Tick();
    CYGFX_BOOL IsEnd();
    void SetData(const POS *pData, CYGFX_U32 max);
    void Restart();
private:
    vector<POS> m_data;
    const POS *m_pData;
    CYGFX_U32 m_max;
    CYGFX_FLOAT m_time;
    CYGFX_FLOAT m_start;
    CYGFX_FLOAT m_end;
    CYGFX_U32 m_pos;
    CYGFX_FLOAT m_Speed;
};

extern Engine g_engine;
