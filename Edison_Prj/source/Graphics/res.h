
#ifndef __RES_H__
#define __RES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "res\Background.h"

#include "res\red_503x421.h"
#include "res\blue_503x421.h"
#include "res\gray_503x421.h"
#include "res\red_warning.h"

#include "res\wheel_white.h"
#include "res\wheel_red.h"
#include "res\wheel_blue.h"

#include "res\bg_speedlimit.h"
#include "res\dial_gauge_bg.h"
#include "res\dial_gauge_round.h"
#include "res\dial_gauge_needle.h"

#include "res\gps_nosignal.h"
#include "res\gps_signal0.h"
#include "res\gps_signal1.h"
#include "res\gps_signal2.h"
#include "res\gps_signal3.h"

#include "res\fo_bigdog.h"
#include "res\fo_smalldog.h"
#include "res\fo_bike.h"
#include "res\fo_car.h"
#include "res\fo_motorbike.h"
#include "res\fo_people.h"
#include "res\fo_unknow.h"

#include "res\0_big.h"
#include "res\1_big.h"
#include "res\2_big.h"
#include "res\3_big.h"
#include "res\4_big.h"
#include "res\5_big.h"
#include "res\6_big.h"
#include "res\7_big.h"
#include "res\8_big.h"
#include "res\9_big.h"
#include "res\kmh.h"

#include "res\0_small.h"
#include "res\1_small.h"
#include "res\2_small.h"
#include "res\3_small.h"
#include "res\4_small.h"
#include "res\5_small.h"
#include "res\6_small.h"
#include "res\7_small.h"
#include "res\8_small.h"
#include "res\9_small.h"

#include "res\0_tiny.h"
#include "res\1_tiny.h"
#include "res\2_tiny.h"
#include "res\3_tiny.h"
#include "res\4_tiny.h"
#include "res\5_tiny.h"
#include "res\6_tiny.h"
#include "res\7_tiny.h"
#include "res\8_tiny.h"
#include "res\9_tiny.h"
#include "res\period.h"
#include "res\slash.h"
#include "res\tiny_km.h"
#include "bg_mileage.h"

#include "res\gearD.h"
#include "res\gearN.h"
#include "res\gearP.h"
#include "res\gearR.h"

#include "takeover_accel.h"
#include "takeover_brake.h"
#include "takeover_cancel.h"
#include "takeover_estop.h"
#include "takeover_handle.h"
#include "takeover_systemfail.h"
#include "takeover_attention.h"
#include "takeover_aebs.h"

#include "auto.h"
#include "manual.h"
#include "abnormal.h"

#include "left.h"
#include "right.h"

#ifdef __cplusplus
}
#endif

#endif /* __RES_H__ */
