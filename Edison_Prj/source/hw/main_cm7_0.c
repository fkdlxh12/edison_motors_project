/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/
#include <stdio.h>

#include "cy_project.h"
#include "cy_device_headers.h"
#include "cygfx_driver_api.h"

#include "Edison_System.h"

/*****************************************************************************/
/*** GLOBAL FUNCTIONS ********************************************************/
/*****************************************************************************/
extern void _main(void);

/* Main entry point */
int main(void)
{
#ifndef __ICCARM__
    _main(); /* for ctors & dtors */
#endif

    SystemInit();
    SCB_DisableICache();
    SCB_DisableDCache();

    __enable_irq();

    global_totalOdometer = 0;

    vEdsion_System_Init();

    printf("totalMileage = %d\r\n", global_totalOdometer);

    for (;;)
    {
        // TODO : Applications Adding
        vEdsion_System_Task_Scheduler();
    }
}


/* [] END OF FILE */
