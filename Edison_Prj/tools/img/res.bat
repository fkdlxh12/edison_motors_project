@REM SET BIN="../../../08_tool/basic_graphics/bin/windows/Bin2Text.exe"
SET RES="../../../08_tool/basic_graphics/bin/windows/ResourceGenerator.exe"

SET PATH_SRC_IMG="..\..\img"
SET PATH_DST_IMG="..\..\source\Graphics\res"

%BIN% %PATH_SRC_IMG%\Aero.ttf 		%PATH_DST_IMG%\Aero.h
%BIN% %PATH_SRC_IMG%\DeckerB.ttf 	%PATH_DST_IMG%\DeckerB.h

GOTO LABEL
%BIN% %PATH_SRC_IMG%\HYGTRE.ttf 	%PATH_DST_IMG%\HYGTRE.h
%BIN% %PATH_SRC_IMG%\MyriadPro.ttf 	%PATH_DST_IMG%\MyriadPro.h
%BIN% %PATH_SRC_IMG%\YGD360.ttf 	%PATH_DST_IMG%\YGD360.h
%BIN% %PATH_SRC_IMG%\ygo140.ttf 	%PATH_DST_IMG%\ygo140.h
:LABEL

%RES% %PATH_SRC_IMG%\Background.png     -nBackground    -o%PATH_DST_IMG%\Background.h          -fiR8A8B8G8 -foA8 
%RES% %PATH_SRC_IMG%\clock.png          -nclockpic      -o%PATH_DST_IMG%\clockpic.h      -cRLA -foA4
%RES% %PATH_SRC_IMG%\flag.png           -nflag          -o%PATH_DST_IMG%\flag.h          -cRLA -foA4
%RES% %PATH_SRC_IMG%\km_flag.png        -nkm_flag       -o%PATH_DST_IMG%\km_flag.h       -cRLA -foA4

%RES% %PATH_SRC_IMG%\blue_503x421.png  -nblue_503x421 -o%PATH_DST_IMG%\blue_503x421.h -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\gray_503x421.png  -ngray_503x421 -o%PATH_DST_IMG%\gray_503x421.h -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\red_503x421.png   -nred_503x421  -o%PATH_DST_IMG%\red_503x421.h  -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\red_warning.png   -nred_warning  -o%PATH_DST_IMG%\red_warning.h  -cRLA -foR8G8B8A8  -s

%RES% %PATH_SRC_IMG%\wheel_red.png     -nwheel_red    -o%PATH_DST_IMG%\wheel_red.h    -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\wheel_blue.png    -nwheel_blue   -o%PATH_DST_IMG%\wheel_blue.h   -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\wheel_white.png   -nwheel_white  -o%PATH_DST_IMG%\wheel_white.h  -cRLA -foR8G8B8A8  -s

%RES% %PATH_SRC_IMG%\bg_speedlimit.png   	-nbg_speedlimit  	-o%PATH_DST_IMG%\bg_speedlimit.h       -cRLA -foA4
%RES% %PATH_SRC_IMG%\dial_gauge_bg.png   	-ndial_gauge_bg  	-o%PATH_DST_IMG%\dial_gauge_bg.h       -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\dial_gauge_round.png   -ndial_gauge_round  -o%PATH_DST_IMG%\dial_gauge_round.h    -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\dial_gauge_needle.png  -ndial_gauge_needle -o%PATH_DST_IMG%\dial_gauge_needle.h   		 -foR8G8B8A8  -s

%RES% %PATH_SRC_IMG%\sgn_person.png     -nsgn_person    -o%PATH_DST_IMG%\sgn_person.h   -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\sgn_car.png        -nsgn_car       -o%PATH_DST_IMG%\sgn_car.h      -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\sgn_bike.png       -nsgn_bike      -o%PATH_DST_IMG%\sgn_bike.h     -cRLA -foR8G8B8A8  -s

%RES% %PATH_SRC_IMG%\fo_bigdog.png      -nfo_bigdog     -o%PATH_DST_IMG%\fo_bigdog.h    -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\fo_smalldog.png    -nfo_smalldog   -o%PATH_DST_IMG%\fo_smalldog.h  -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\fo_bike.png        -nfo_bike       -o%PATH_DST_IMG%\fo_bike.h      -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\fo_car.png         -nfo_car        -o%PATH_DST_IMG%\fo_car.h       -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\fo_motorbike.png   -nfo_motorbike  -o%PATH_DST_IMG%\fo_motorbike.h -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\fo_people.png      -nfo_people     -o%PATH_DST_IMG%\fo_people.h    -cRLA -foR8G8B8A8  -s
%RES% %PATH_SRC_IMG%\fo_unknow.png      -nfo_unknow     -o%PATH_DST_IMG%\fo_unknow.h    -cRLA -foR8G8B8A8  -s

%RES% %PATH_SRC_IMG%\handle.png           -nhandle          -o%PATH_DST_IMG%\handle.h          -cRLA -foA4
%RES% %PATH_SRC_IMG%\gShortPedal.png      -ngShortPedal     -o%PATH_DST_IMG%\gShortPedal.h     -cRLA -foA4
%RES% %PATH_SRC_IMG%\wShortPedal.png      -nwShortPedal     -o%PATH_DST_IMG%\wShortPedal.h     -cRLA -foA4
%RES% %PATH_SRC_IMG%\gLongPedal.png       -ngLongPedal      -o%PATH_DST_IMG%\gLongPedal.h      -cRLA -foA4
%RES% %PATH_SRC_IMG%\wLongPedal.png       -nwLongPedal      -o%PATH_DST_IMG%\wLongPedal.h      -cRLA -foA4
%RES% %PATH_SRC_IMG%\touchHandle.png      -ntouchHandle     -o%PATH_DST_IMG%\touchHandle.h     -cRLA -foA4
%RES% %PATH_SRC_IMG%\cancel_icon.png      -ncancel_icon     -o%PATH_DST_IMG%\cancel_icon.h     -cRLA -foA4

%RES% %PATH_SRC_IMG%\gps_nosignal.png     -ngps_nosignal  	-o%PATH_DST_IMG%\gps_nosignal.h    -cRLA -foR8G8B8A8
%RES% %PATH_SRC_IMG%\gps_signal0.png      -ngps_signal0     -o%PATH_DST_IMG%\gps_signal0.h     -cRLA -foA4
%RES% %PATH_SRC_IMG%\gps_signal1.png      -ngps_signal1     -o%PATH_DST_IMG%\gps_signal1.h     -cRLA -foA4
%RES% %PATH_SRC_IMG%\gps_signal2.png      -ngps_signal2     -o%PATH_DST_IMG%\gps_signal2.h     -cRLA -foA4
%RES% %PATH_SRC_IMG%\gps_signal3.png      -ngps_signal3     -o%PATH_DST_IMG%\gps_signal3.h     -cRLA -foA4

%RES% %PATH_SRC_IMG%\big_number\0_big.png        -nbig_0   -o%PATH_DST_IMG%\0_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\1_big.png        -nbig_1   -o%PATH_DST_IMG%\1_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\2_big.png        -nbig_2   -o%PATH_DST_IMG%\2_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\3_big.png        -nbig_3   -o%PATH_DST_IMG%\3_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\4_big.png        -nbig_4   -o%PATH_DST_IMG%\4_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\5_big.png        -nbig_5   -o%PATH_DST_IMG%\5_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\6_big.png        -nbig_6   -o%PATH_DST_IMG%\6_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\7_big.png        -nbig_7   -o%PATH_DST_IMG%\7_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\8_big.png        -nbig_8   -o%PATH_DST_IMG%\8_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\9_big.png        -nbig_9   -o%PATH_DST_IMG%\9_big.h -cRLA -fiA8R8B8G8 -foA4
%RES% %PATH_SRC_IMG%\big_number\kmh.png        	 -nkmh     -o%PATH_DST_IMG%\kmh.h   -cRLA -fiA8R8B8G8 -foA4

%RES% %PATH_SRC_IMG%\small_number\0m.png        -nsmall_0   -o%PATH_DST_IMG%\0_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\1m.png        -nsmall_1   -o%PATH_DST_IMG%\1_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\2m.png        -nsmall_2   -o%PATH_DST_IMG%\2_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\3m.png        -nsmall_3   -o%PATH_DST_IMG%\3_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\4m.png        -nsmall_4   -o%PATH_DST_IMG%\4_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\5m.png        -nsmall_5   -o%PATH_DST_IMG%\5_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\6m.png        -nsmall_6   -o%PATH_DST_IMG%\6_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\7m.png        -nsmall_7   -o%PATH_DST_IMG%\7_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\8m.png        -nsmall_8   -o%PATH_DST_IMG%\8_small.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\small_number\9m.png        -nsmall_9   -o%PATH_DST_IMG%\9_small.h -cRLA -foA4

%RES% %PATH_SRC_IMG%\tiny_number\0t.png        -ntiny_0   -o%PATH_DST_IMG%\0_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\1t.png        -ntiny_1   -o%PATH_DST_IMG%\1_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\2t.png        -ntiny_2   -o%PATH_DST_IMG%\2_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\3t.png        -ntiny_3   -o%PATH_DST_IMG%\3_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\4t.png        -ntiny_4   -o%PATH_DST_IMG%\4_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\5t.png        -ntiny_5   -o%PATH_DST_IMG%\5_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\6t.png        -ntiny_6   -o%PATH_DST_IMG%\6_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\7t.png        -ntiny_7   -o%PATH_DST_IMG%\7_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\8t.png        -ntiny_8   -o%PATH_DST_IMG%\8_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\9t.png        -ntiny_9   -o%PATH_DST_IMG%\9_tiny.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\period.png    -nperiod   -o%PATH_DST_IMG%\period.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\slash.png     -nslash    -o%PATH_DST_IMG%\slash.h  -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\tiny_km.png     -ntiny_km    -o%PATH_DST_IMG%\tiny_km.h  -cRLA -foA4
%RES% %PATH_SRC_IMG%\tiny_number\bg_mileage.png     -nbg_mileage    -o%PATH_DST_IMG%\bg_mileage.h  -cRLA -foA4

%RES% %PATH_SRC_IMG%\gear\D.png        -ngearD   -o%PATH_DST_IMG%\gearD.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\gear\N.png        -ngearN   -o%PATH_DST_IMG%\gearN.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\gear\P.png        -ngearP   -o%PATH_DST_IMG%\gearP.h -cRLA -foA4
%RES% %PATH_SRC_IMG%\gear\R.png        -ngearR   -o%PATH_DST_IMG%\gearR.h -cRLA -foA4

%RES% %PATH_SRC_IMG%\takeoverstatus\accel.png        -ntakeover_accel   -o%PATH_DST_IMG%\takeover_accel.h  -cRLA -foRGB8
%RES% %PATH_SRC_IMG%\takeoverstatus\brake.png        -ntakeover_brake   -o%PATH_DST_IMG%\takeover_brake.h  -cRLA -foRGB8
%RES% %PATH_SRC_IMG%\takeoverstatus\cancel.png       -ntakeover_cancel  -o%PATH_DST_IMG%\takeover_cancel.h -cRLA -foRGB8
%RES% %PATH_SRC_IMG%\takeoverstatus\estop.png        -ntakeover_estop   -o%PATH_DST_IMG%\takeover_estop.h  -cRLA -foRGB8
%RES% %PATH_SRC_IMG%\takeoverstatus\handle.png       -ntakeover_handle  -o%PATH_DST_IMG%\takeover_handle.h -cRLA -foRGB8
%RES% %PATH_SRC_IMG%\takeoverstatus\systemfail.png   -ntakeover_systemfail   -o%PATH_DST_IMG%\takeover_systemfail.h  -cRLA -foA4
%RES% %PATH_SRC_IMG%\takeoverstatus\AEB.png          -ntakeover_attention     -o%PATH_DST_IMG%\takeover_attention.h    -cRLA -foA4
%RES% %PATH_SRC_IMG%\takeoverstatus\AEBS.png          -ntakeover_aebs     -o%PATH_DST_IMG%\takeover_aebs.h    -cRLA -foA4

%RES% %PATH_SRC_IMG%\driving_mode\auto.png          -nauto_mode     -o%PATH_DST_IMG%\auto.h      -cRLA -foA4
%RES% %PATH_SRC_IMG%\driving_mode\manual.png        -nmanual_mode   -o%PATH_DST_IMG%\manual.h    -cRLA -foA4
%RES% %PATH_SRC_IMG%\driving_mode\abnormal.png      -nabnormal_mode -o%PATH_DST_IMG%\abnormal.h  -cRLA -foA4

%RES% %PATH_SRC_IMG%\left.png        -nleft_direction   -o%PATH_DST_IMG%\left.h    -cRLA -foA4
%RES% %PATH_SRC_IMG%\right.png       -nright_direction  -o%PATH_DST_IMG%\right.h   -cRLA -foA4