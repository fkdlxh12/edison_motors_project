SET BIN="../../../../08_tool/basic_graphics/bin/windows/Bin2Text.exe"
SET RES="../../../../08_tool/basic_graphics/bin/windows/ResourceGenerator.exe"

%BIN% ..\..\resource\www.fonts2u.com\Decker.ttf res\Decker.h
%RES% res\hud\Background.png -nBackground -fiR8A8B8G8 -foA2 -ores\Background_DBG.h
%RES% res\hud\clock.png -nclockpic -ores\clockpic_DBG.h -cRLA -foA2
%RES% res\hud\flag.png -nflag -ores\flag_DBG.h -cRLA -foA2
%RES% res\hud\km_flag.png -nkm_flag -ores\km_flag_DBG.h -cRLA -foA2
%RES% res\hud\Pfeil_0Grad.png -nPfeil_0Grad -ores\Pfeil_0Grad_DBG.h -cRLA -foR2G2B2A2 -s
%RES% res\hud\Pfeil_45GradL.png -nPfeil_45GradL -ores\Pfeil_45GradL_DBG.h  -cRLA -foR2G2B2A2 -s
%RES% res\hud\Pfeil_45GradR.png -nPfeil_45GradR -ores\Pfeil_45GradR_DBG.h  -cRLA -foR2G2B2A2 -s
%RES% res\hud\Pfeil_90GradL.png -nPfeil_90GradL -ores\Pfeil_90GradL_DBG.h  -cRLA -foR2G2B2A2 -s
%RES% res\hud\Pfeil_90GradR.png -nPfeil_90GradR -ores\Pfeil_90GradR_DBG.h  -cRLA -foR2G2B2A2 -s
%RES% res\hud\Pfeil_LR.png -nPfeil_LR -ores\Pfeil_LR_DBG.h  -cRLA -fa -s
%RES% res\hud\Pfeil_RL.png -nPfeil_RL -ores\Pfeil_RL_DBG.h  -cRLA -fa -s
%RES% res\hud\Schild_80.png -nSchild_80 -ores\Schild_80_DBG.h  -cRLA -foR2G2B2A2
%RES% res\hud\Schild_Bahn.png -nSchild_Bahn -ores\Schild_Bahn_DBG.h -cRLA -foR2G2B2A2
%RES% res\hud\Schild_Wildwechsel.png -nSchild_Wildwechsel -ores\Schild_Wildwechsel_DBG.h -cRLA -foR2G2B2A2
