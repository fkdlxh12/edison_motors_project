@echo OFF
for %%m in (flash sram) do (
    for %%c in (cm0plus cm7 cm7_0 cm7_1) do (
        for %%d in (debug release production) do (
            rmdir /Q /S %%m\%%c\%%d\objs
            rmdir /Q /S %%m\%%c\%%d
        )
        del   /Q    %%m\%%c\*.elf
        del   /Q    %%m\%%c\*.dep
        del   /Q    %%m\%%c\*.dla
        del   /Q    %%m\%%c\*.dle
        del   /Q    %%m\%%c\*.dnm
        del   /Q    %%m\%%c\*.map
        del   /Q    %%m\%%c\*.srec
        del   /Q    %%m\%%c\*.siz
        del   /Q    %%m\%%c\*.a
        del   /Q    %%m\%%c\*.dba
    )
)

@echo ON