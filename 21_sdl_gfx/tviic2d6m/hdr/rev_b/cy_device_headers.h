/***************************************************************************//**
* \file cy_device_headers.h
*
* \brief
* Common header file to be included by the drivers.
*
* \note
* Generator version: 1.6.0.453
* Database revision: TVIIC2D6M_B0CFR_MTO
*
********************************************************************************
* \copyright
* Copyright 2016-2021, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#ifndef _CY_DEVICE_HEADERS_H_
#define _CY_DEVICE_HEADERS_H_

#if defined (CYT4DNDBAS)
    #include "cyt4dndbas.h"
#elif defined (CYT4DNDBBS)
    #include "cyt4dndbbs.h"
#elif defined (CYT4DNDBCS)
    #include "cyt4dndbcs.h"
#elif defined (CYT4DNDBDS)
    #include "cyt4dndbds.h"
#elif defined (CYT4DNDBES)
    #include "cyt4dndbes.h"
#elif defined (CYT4DNDBFS)
    #include "cyt4dndbfs.h"
#elif defined (CYT4DNDBGS)
    #include "cyt4dndbgs.h"
#elif defined (CYT4DNDBHS)
    #include "cyt4dndbhs.h"
#elif defined (CYT4DNDBJS)
    #include "cyt4dndbjs.h"
#elif defined (CYT4DNDBKS)
    #include "cyt4dndbks.h"
#elif defined (CYT4DNDBLS)
    #include "cyt4dndbls.h"
#elif defined (CYT4DNDBMS)
    #include "cyt4dndbms.h"
#elif defined (CYT4DNDBNS)
    #include "cyt4dndbns.h"
#elif defined (CYT4DNDBPS)
    #include "cyt4dndbps.h"
#elif defined (CYT4DNDBQS)
    #include "cyt4dndbqs.h"
#elif defined (CYT4DNDBRS)
    #include "cyt4dndbrs.h"
#elif defined (CYT4DNJBAS)
    #include "cyt4dnjbas.h"
#elif defined (CYT4DNJBBS)
    #include "cyt4dnjbbs.h"
#elif defined (CYT4DNJBCS)
    #include "cyt4dnjbcs.h"
#elif defined (CYT4DNJBDS)
    #include "cyt4dnjbds.h"
#elif defined (CYT4DNJBES)
    #include "cyt4dnjbes.h"
#elif defined (CYT4DNJBFS)
    #include "cyt4dnjbfs.h"
#elif defined (CYT4DNJBGS)
    #include "cyt4dnjbgs.h"
#elif defined (CYT4DNJBHS)
    #include "cyt4dnjbhs.h"
#elif defined (CYT4DNJBJS)
    #include "cyt4dnjbjs.h"
#elif defined (CYT4DNJBKS)
    #include "cyt4dnjbks.h"
#elif defined (CYT4DNJBLS)
    #include "cyt4dnjbls.h"
#elif defined (CYT4DNJBMS)
    #include "cyt4dnjbms.h"
#elif defined (CYT4DNJBNS)
    #include "cyt4dnjbns.h"
#elif defined (CYT4DNJBPS)
    #include "cyt4dnjbps.h"
#elif defined (CYT4DNJBQS)
    #include "cyt4dnjbqs.h"
#elif defined (CYT4DNJBRS)
    #include "cyt4dnjbrs.h"
#else
    #error Undefined part number
#endif

#endif /* _CY_DEVICE_HEADERS_H_ */


/* [] END OF FILE */
