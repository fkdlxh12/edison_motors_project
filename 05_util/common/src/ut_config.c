/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        ut_config.c
 * \brief       This is the implementation of a function to get special attributes.
 */

/*******************************************************************************
 Includes
*******************************************************************************/

#include <stdio.h>

#include "cygfx_driver_api.h"

#include "ut_config.h"

#include "ut_memman.h"


/*******************************************************************************
 Macro Definitions
*******************************************************************************/

/*******************************************************************************
 Data Types
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Variables
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Prototypes
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Definitions
*******************************************************************************/

CYGFX_ERROR utConfigGetAttribute(UT_CONFIG_ATTR pname,
                                 CYGFX_U32*     pParam)
{
    CYGFX_ERROR ret    = CYGFX_OK;
    CYGFX_U32   result = 0;

    if (pParam == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("invalid specification for: pParam\n");
#endif
        return CYGFX_ERR;
    }

    switch (pname)
    {
        case UT_CONFIG_ATTR_RESOURCE_DISPLAY :
            ret = CyGfx_ConfigGetAttribute(CYGFX_CONFIG_ATTR_IPCONFIGURATION, pParam);
            if (ret == CYGFX_OK)
            {
                /* (( FPDLINK1 available ) || (Display1 TTL available)) + (FPDLINK0 available) */
                result = (((*pParam & 0x8u) >> 3) || ((*pParam & 0x20u) >> 5)) + ((*pParam & 0x4u) >> 2);
            }
            break;

        case UT_CONFIG_ATTR_RESOURCE_VRAM :
            result = utGetVRAMSize();
            break;

        default :
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("line: %u: invalid specification for: pname\n", __LINE__);
#endif
            ret = CYGFX_ERR;
            break;
    }

    *pParam = result;

    return ret;
}

