/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        ut_memman.c
 * \brief       This is the implementation of the memory management module.
 *              The video memory is managed via a first fit algorithm.
 *
 *              If the first block found "fits", it is split into two, just what
 *              is needed, and the remaining into a free block.
 *
 *              Align is implemented via adding the requested size and alignment
 *              and finding a block.  The space preceeding the alignment is then
 *              split into a new free block and added to the free list.
 *
 *              Free blocks are merged to adjacent free blocks when freed.
 *
 *              There is a linked list for "free blocks".  As each block is made
 *              free, it is added to the front of this list
 */

/*******************************************************************************
 Includes
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cygfx_driver_api.h"
#ifdef C_MODEL
#include "hw_interface.h"
#include "protocol.h"
#endif
#include "ut_memman.h"
#include "ut_compatibility.h"
#include "ut_errors.h"

#include "hweb.h"

/*******************************************************************************
 Macro Definitions
*******************************************************************************/

#ifdef UT_MEMMAN_CHECK
#define MML_UTMEM_MAGIC 0xaffeaffe
#endif

/*******************************************************************************
 Data Types
*******************************************************************************/
#define VRAM 0  //VRAM heapRegion id
#define  GFX_CACHE_LINE_SIZE 32
/**
 * Data structure which defines a heap region
 */
typedef struct heapRegion 
{
    struct heapRegion *next;           /**< Pointer to next block */
    UT_MEMMAN_HEAP_HANDLE heapHandle;  /**< heap region handler*/
    CYGFX_U32 id;                      /**< heap region id*/
    CYGFX_ADDR startAddr;              /**< heap region start address*/
    CYGFX_U32 size;                    /**< heap region size*/
} HeapRegion;

static HeapRegion *startRegion = 0;
static HeapRegion *currentRegion = 0;

struct BlockInfo;

/**
 * Data structure which tracks a block of memory in the heap
 */
typedef struct BlockInfo 
{
    struct BlockInfo *nextBlock;     /**< Pointer to next block */
    struct BlockInfo *prevBlock;     /**< Pointer to previous block */
    CYGFX_U32 size;                     /**< Size of block  */
    CYGFX_U32 *phyAddress;              /**< physical address of block */
    CYGFX_U32 offset;                   /**< offset of block from the beginning -- address = start + offset */
    CYGFX_U08 isUsed;                   /**< Is this block free or used? */
    struct BlockInfo *nextFreeBlock; /**< Pointer to next free block (only used in free blocks) */
    struct BlockInfo *prevFreeBlock; /**< Pointer to previous free block (only used in free blocks) */
} BlockInfo;

/**
 * Data structure which contains the information about a heap.
 */
typedef struct heapInfo 
{
    struct BlockInfo *startBlock;     /**< Initial block */
    struct BlockInfo *freeBlockRover; /**< Free block pointer */
    CYGFX_U32 size;                      /**< Initial amount of memory available to the user */
    CYGFX_U32 totalAllocated;            /**< Total memory allocated to user (what user thinks he/she has) */
    CYGFX_U32 totalFree;                 /**< Total free memory available to the user (what user thinks he/she can get) */
    CYGFX_U32 numFreeBlocks;             /**< Number of free blocks */
    CYGFX_U32 numAllocatedBlocks;        /**< Number of allocated blocks */
    CYGFX_U32 baseAddress;               /**< Base address of the heap */
} HeapInfo;


/*******************************************************************************
 Variables
*******************************************************************************/

#ifdef C_MODEL
/* Global variables that hold the handles to the memory heaps */
UT_MEMMAN_HEAP_HANDLE g_heapHandle_HOST = NULL;
UT_MEMMAN_HEAP_HANDLE g_heapHandle_FLASH = NULL;
#endif

#ifdef UT_MEMMAN_CHECK
CYGFX_BOOL g_mem_overwrite = CYGFX_FALSE;
#endif /* UT_MEMMAN_CHECK */

/*******************************************************************************
 Function Prototypes
*******************************************************************************/

static BlockInfo* s_obtainBlock( void *addr, HeapInfo* heap );
static void s_addToFreeList( BlockInfo* block, HeapInfo* heap );
static void s_removeFromFreeList( BlockInfo* block, HeapInfo* heap );

/*******************************************************************************
 Function Definitions
*******************************************************************************/
//OI : Shall we only reset VRAM or other memory region which might be present at that point of time ?
CYGFX_ERROR utMmanReset(void)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 sizeVRAM = utGetVRAMSize();
    CYGFX_U32 vram_baseAddress = utGetVRAMBaseAddr();

    HeapRegion *Region = startRegion;
    HeapRegion *tmpR;

    while(Region != NULL)
    {
        // delete recursive
        tmpR = Region->next;
        utMmanDestroyHeap(Region->heapHandle);
        utOsLibcFree(Region);
        Region = tmpR;
    }

    //create one region for VRAM
    startRegion = (HeapRegion*)utOsLibcMalloc(sizeof(HeapRegion));
    startRegion->id = VRAM;
    startRegion->startAddr = vram_baseAddress;
    startRegion->size = sizeVRAM;
    startRegion->next = 0;
    ret = utMmanCreateHeap(&startRegion->heapHandle, sizeVRAM, vram_baseAddress);
    // and set it as default
    currentRegion = startRegion;

    if (ret != CYGFX_OK) {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanReset: utMmanCreateHeap failed to create VRAM heap (0x%08x)\n", ret);
#endif
        return ret;
    }
#ifdef C_MODEL
    if (g_heapHandle_HOST != NULL) {
        utMmanDestroyHeap(g_heapHandle_HOST);
        g_heapHandle_HOST = NULL;
    }
    ret = utMmanCreateHeap(&g_heapHandle_HOST, IRIS_HOST_SIZE, IRIS_HOST_BASE);
    if (ret != CYGFX_OK) {
        printf("utMmanReset: utMmanCreateHeap failed to create HOST heap (0x%08x)\n", ret);
        return ret;
    }

    if (g_heapHandle_FLASH != NULL) {
        utMmanDestroyHeap(g_heapHandle_FLASH);
        g_heapHandle_FLASH = NULL;
    }
    /* Note: We can't use the flash memory from the very beginning, because
       IRIS_FLASH_BASE is 0x00000000, so the first call of FlashRamAlloc() would return 0 */
    ret = utMmanCreateHeap(&g_heapHandle_FLASH, IRIS_FLASH_SIZE - 32, IRIS_FLASH_BASE + 32);
    if (ret != CYGFX_OK) {
        printf("utMmanReset: utMmanCreateHeap failed to create FLASH heap (0x%08lx)\n", ret);
        return ret;
    }
#endif
    return CYGFX_OK;
}


CYGFX_ERROR utMmanAddRegion( CYGFX_U32 id, CYGFX_ADDR *pStartAddr, CYGFX_U32 size)
{
    HeapRegion *Region = startRegion; 
    HeapRegion *nextR = startRegion;
    HeapRegion *tmpR;
    //check if id is unused
    while(nextR)
    {
        if (nextR->id == id) 
        {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("[ERROR] utMmanAddRegion: MemRegion Id %d already added\n",id);
#endif
            return UTIL_ERR_MMAN_NO_MEMORY;
        }
        tmpR = nextR;
        nextR = nextR->next;
    }
    // tmpR is now the last one. Let us create a new entry
    Region = (HeapRegion*)utOsLibcMalloc(sizeof(HeapRegion));
    Region->id = id;
    Region->startAddr = *pStartAddr;
    Region->size = size;
    Region->next = 0;
    utMmanCreateHeap(&Region->heapHandle, size, *pStartAddr);
    tmpR->next = Region;
    
    return CYGFX_OK;
}

CYGFX_ERROR utMmanSelectRegion( CYGFX_U32 id)
{
    HeapRegion *Region = startRegion;
    //find id
    while(Region)
    {
        if (Region->id == id) 
            break;
        Region = Region->next;
    }
    if (Region == 0) 
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("[ERROR] utMmanSelectRegion: MemRegion Id %d not found\n",id);
#endif
        return UTIL_ERR_MMAN_NO_MEMORY;
    }
    // set a new default
    currentRegion = Region;
    return CYGFX_OK;
}

void * utVideoAlloc( CYGFX_U32 size, CYGFX_U32 alignment, CYGFX_ADDR *pAddr )
{
    CYGFX_ADDR ulRet = 0;
#ifdef C_MODEL
    void *add;
#endif

    CYGFX_ERROR ret = CYGFX_ERR;

    if(currentRegion !=0 )
    {
      // Force 32 byte alignment for all VRAM allocation
      alignment &= (CYGFX_U32)0xFFFFFFE0;
      if(0==alignment)
      {
          alignment = GFX_CACHE_LINE_SIZE;
      }
      ret = utMmanHeapAlloc(currentRegion->heapHandle, size, alignment, &ulRet);
    }
    if (ret != CYGFX_OK) 
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        if(currentRegion == 0)
        {
            printf("[ERROR] utVideoAlloc: currentRegion is NULL");
        }
        else
        {
          printf("[ERROR] utVideoAlloc: failed to allocate %d bytes (0x%08x)\n", size, ret);
        }
#endif
        return NULL;
    }
	//else
	//{
 //       printf("[INFO] utVideoAlloc:  allocated memory at %p  size %d\n", ulRet, size);
	//}

    if (pAddr)
    {
        *pAddr = ulRet;
    }
#ifdef C_MODEL
    hwi_Phys2Virt(ulRet, &add);
    return add;
#else
    return (void *)ulRet;
#endif

}


void utVideoFree(void* addr)
{
    CYGFX_ERROR ret;
    CYGFX_ADDR pPhys;
#ifdef C_MODEL
    hwi_Virt2Phys(addr, &pPhys);
#else
    pPhys = (CYGFX_ADDR)addr;
#endif
    ret = utMmanHeapFree(currentRegion->heapHandle, (void *)pPhys);
    if (ret != CYGFX_OK)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("[ERROR] utVideoFree: failed to free memory at (0x%08x)!\n", pPhys);
#endif
    }
    //else
    //{
    //    printf("[INFO] utVideoFree: free memory at %p\n", pPhys);
    //}
}

CYGFX_ERROR utVideoGetSize(CYGFX_U32 *size)
{
    return utMmanGetSize(currentRegion->heapHandle, size);
}

CYGFX_ERROR utVideoGetFreeTotal(CYGFX_U32 *size)
{
    return utMmanGetFree(currentRegion->heapHandle, size);
}

CYGFX_ERROR utVideoGetLargestBlock(CYGFX_U32 *size)
{
    return utMmanGetLargest(currentRegion->heapHandle, size);
}

#ifdef C_MODEL
void *HostRamAlloc(CYGFX_U32 size, CYGFX_ADDR *pAddr)
{
    CYGFX_ADDR ulRet = 0;
    void *add;

    CYGFX_ERROR ret = CYGFX_OK;

    ret = utMmanHeapAlloc(g_heapHandle_HOST, size, 1, &ulRet);
    if (ret != CYGFX_OK) {
        printf("HostRamAlloc: utMmanHeapAlloc failed with error code 0x%08lx\n", ret);
        return NULL;
    }

    if (pAddr)
    {
        *pAddr = ulRet;
    }
    hwi_Phys2Virt(ulRet, &add);
    return add;
}

void* utFlashRamAlloc(CYGFX_U32 size, CYGFX_U32 alignment, CYGFX_ADDR *pPhysAddr)
{
    CYGFX_ADDR ulRet = 0;
    void *pVirtAdd;

    CYGFX_ERROR ret;

    ret = utMmanHeapAlloc(g_heapHandle_FLASH, size, alignment, &ulRet);
    if (ret != CYGFX_OK) {
        printf("utFlashRamAlloc: utMmanHeapAlloc failed with error code 0x%08lx\n", ret);
        return NULL;
    }

    if (pPhysAddr)
    {
        *pPhysAddr = ulRet;
    }
    hwi_Phys2Virt(ulRet, &pVirtAdd);

    return pVirtAdd;
}

void utFlashRamFree(void* pVirtAddr)
{
    CYGFX_ERROR ret;
    CYGFX_ADDR pPhysAddr;

    hwi_Virt2Phys(pVirtAddr, &pPhysAddr);
    ret = utMmanHeapFree(g_heapHandle_FLASH, (void *)pPhysAddr);
    if (ret != CYGFX_OK) {
        printf("[ERROR] utFlashRamFree: failed to free memory at %p!\n", pPhysAddr);
//    } else {
//        printf("[INFO] utFlashRamFree: free memory at %p\n", pPhysAddr);
    }
}

CYGFX_ERROR utFlashRamGetSize(CYGFX_U32 *pSize)
{
    return utMmanGetSize(g_heapHandle_FLASH, pSize);
}

CYGFX_ERROR utFlashRamGetFreeTotal(CYGFX_U32 *pSize)
{
    return utMmanGetFree(g_heapHandle_FLASH, pSize);
}

#endif //#ifdef C_MODEL

CYGFX_ERROR utMmanCreateHeap(UT_MEMMAN_HEAP_HANDLE *hdlmem, CYGFX_U32 size, CYGFX_U32 baseAddress)
{

    HeapInfo* heap = (HeapInfo*)utOsLibcMalloc(sizeof(HeapInfo));

    if (heap == NULL)
    {
        return UTIL_ERR_MMAN_NO_MEMORY;
    }

    /* Initialize the heap structure */
    heap->size = size;
    heap->totalAllocated = 0U;
    heap->totalFree = size;
    heap->numFreeBlocks = 1U;
    heap->numAllocatedBlocks = 0U;
    heap->baseAddress = baseAddress;

    /* Create initial start block */
    heap->startBlock = (BlockInfo*)utOsLibcMalloc(sizeof(BlockInfo));
    if (heap->startBlock == NULL)
    {
        /* Free allocated memory */
        utOsLibcFree(heap);

        return UTIL_ERR_MMAN_NO_MEMORY;
    }

    heap->startBlock->nextBlock = 0;
    heap->startBlock->prevBlock = 0;
    heap->startBlock->size = size;
    heap->startBlock->offset = 0U;
    heap->startBlock->isUsed = 0;
    heap->startBlock->nextFreeBlock = 0;
    heap->startBlock->prevFreeBlock = 0;

    /* The first block is also the first "free" block in the free list */
    heap->freeBlockRover = heap->startBlock;

    *hdlmem = (UT_MEMMAN_HEAP_HANDLE)heap;

    return CYGFX_OK;
}

CYGFX_ERROR utMmanDestroyHeap(UT_MEMMAN_HEAP_HANDLE hdlmem)
{
    HeapInfo* heap = (HeapInfo*)hdlmem;

    if (heap == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanDestroyHeap: ERROR: hdlmem == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    utOsLibcFree(heap->startBlock);
    utOsLibcFree(heap);

    return CYGFX_OK;
}

CYGFX_ERROR utMmanHeapAlloc(UT_MEMMAN_HEAP_HANDLE hdlmem, CYGFX_U32 size, CYGFX_U32 alignment, CYGFX_ADDR *addr)
{
    BlockInfo *block;
    BlockInfo *newBlock;
    CYGFX_U32 alignRemainder;
    BlockInfo *newRemBlock = NULL;
    CYGFX_U08 mergedFlag = 0;
    HeapInfo* heap = (HeapInfo*)hdlmem;

    if (heap == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanHeapAlloc: ERROR: hdlmem == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    if (size == 0U)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanHeapAlloc: ERROR: size == 0\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    if (addr == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanHeapAlloc: ERROR: addr == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    *addr = 0;

    /* Make sure, allocated memory is always aligned to 4 bytes boundary */
    if (alignment == 0)
    {
        alignment = 4U;
    } else if (0 != (alignment & 1U))
    {
        alignment *= 4U;
    } else if (0 != (alignment & 2U))
    {
        alignment *= 2U;
    } else
    {
        /* Nothing to do */
    }

    /* Get a pointer to the first free block */
    block = heap->freeBlockRover;

#ifdef UT_MEMMAN_CHECK
    size += 4;
#endif

    /* Search for first available block that fits (first fit) */
    while (NULL != block )
    {
        /* Check how much extra memory we need due to alignment wastage */
        alignRemainder = (((heap->baseAddress + block->offset) + (alignment-1U)) &
                ~(alignment-1U)) - (heap->baseAddress + block->offset);

        if ( ( block->size  ) >= (size + alignRemainder))
        {
            /* We found a winner */
            if (alignRemainder > 0U)
            {
                /* Must Split block -- create the new block for the alignment remainder (which is to the left) */
                newRemBlock = (BlockInfo*)utOsLibcMalloc(sizeof(BlockInfo));
                if (newRemBlock == NULL)
                {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
                    printf("utMmanHeapAlloc: UTIL_ERR_MMAN_NO_MEMORY\r\n");
#endif
                    return UTIL_ERR_MMAN_NO_MEMORY;
                }
            }

            /* Check if its worth it to split the block */
            if ( (size + alignRemainder) < block->size )
            {
                /* Must Split block -- create the new empty block (which is to the right of the current block) */
                newBlock = (BlockInfo*)utOsLibcMalloc(sizeof(BlockInfo));
                if (newBlock == NULL)
                {
                    /* Free allocated memory */
                    if (newRemBlock != NULL)
                    {
                        utOsLibcFree(newRemBlock);
                    }
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
                    printf("utMmanHeapAlloc: UTIL_ERR_MMAN_NO_MEMORY\r\n");
#endif
                    return UTIL_ERR_MMAN_NO_MEMORY;
                }

                newBlock->nextBlock = block->nextBlock;
                newBlock->prevBlock = block;
                newBlock->size = block->size - (size + alignRemainder);
                newBlock->offset = block->offset + size + alignRemainder;
                newBlock->isUsed = 0;
                newBlock->nextFreeBlock = block->nextFreeBlock;
                newBlock->prevFreeBlock = block->prevFreeBlock;

                /* Adjust the current block size */
                block->nextBlock = newBlock;
                block->size = size + alignRemainder;

                /* Point the next block to the new block we just created */
                if (NULL != newBlock->nextBlock)
                {
                    newBlock->nextBlock->prevBlock = newBlock;
                }

                /* Must update links of free list to reflect the split */
                if (NULL != block->prevFreeBlock)
                {
                    block->prevFreeBlock->nextFreeBlock = newBlock;
                }
                if (NULL != block->nextFreeBlock)
                {
                    block->nextFreeBlock->prevFreeBlock = newBlock;
                }
                /* If the rover was set to the old block by chance, then update this */
                if (heap->freeBlockRover == block )
                {
                    heap->freeBlockRover = newBlock;
                }
                heap->numFreeBlocks++;
            } else
            {
                /* Just assign the block */
                /* Must update links of free list to reflect the assignment */
                if (NULL != block->prevFreeBlock)
                {
                    block->prevFreeBlock->nextFreeBlock = block->nextFreeBlock;
                }
                if (NULL != block->nextFreeBlock)
                {
                    block->nextFreeBlock->prevFreeBlock = block->prevFreeBlock;
                }
                /* If the rover was set to this block by chance, then update this */
                if (heap->freeBlockRover == block )
                {
                    heap->freeBlockRover = block->nextFreeBlock;
                }
            }

            /* If we have to align the block */

            if (alignRemainder > 0U)
            {
                newRemBlock->nextBlock = block;
                newRemBlock->prevBlock = block->prevBlock;
                newRemBlock->size = alignRemainder;
                newRemBlock->offset = block->offset;
                newRemBlock->isUsed = 0;
                newRemBlock->nextFreeBlock = 0;
                newRemBlock->prevFreeBlock = 0;

                /* Correct pointers from the previous block */
                if(NULL != block->prevBlock)
                {
                    block->prevBlock->nextBlock = newRemBlock;
                }
                /* Check to see if the block on the left is free, if so, merge with that block */
                if(NULL != newRemBlock->prevBlock)
                {
                    if (newRemBlock->prevBlock->isUsed == 0U)
                    {
                        newRemBlock->prevBlock->nextBlock = newRemBlock->nextBlock;
                        newRemBlock->prevBlock->size += newRemBlock->size;
                        utOsLibcFree(newRemBlock);
                        mergedFlag = 1;
                    }
                }
                if (mergedFlag == 0U)
                {
                    /* Add newly split block onto the free list */
                    s_addToFreeList(newRemBlock, heap);
                }
                /* Update current block */
                block->offset = (CYGFX_U32)block->offset + alignRemainder;
                block->size -= alignRemainder;
                block->prevBlock = newRemBlock;

                /* If the current block happened to be the start block */
                if (heap->startBlock == block)
                {
                    heap->startBlock = newRemBlock;
                }
                /* Update metrics */
                heap->numFreeBlocks++;
            }

            /* Mark the block as used */
            block->isUsed = 1;

            /* Update the heap information */
            heap->totalAllocated += size;
            heap->totalFree -= size;
            heap->numFreeBlocks--;
            heap->numAllocatedBlocks++;

            /* Return a pointer to the beginning of the block */
            *addr = heap->baseAddress + block->offset;

#ifdef UT_MEMMAN_CHECK
            {
                void* VirtAdd;
                const CYGFX_U32 val = MML_UTMEM_MAGIC;
                CYGFX_ADDR addCheck = ((*addr) + size) - 4;
                UT_HWEB_PHYS_TO_VIRT(addCheck, &VirtAdd);
                UT_HWEB_LOCK(VirtAdd, 4, UT_HWEB_MA_WRITE);
                memcpy(VirtAdd, &val, 4);
                UT_HWEB_UNLOCK(VirtAdd);
            }
#endif

            return CYGFX_OK;
        }

        /* Navigate to the next block */
        block = (BlockInfo*) block->nextFreeBlock;
    }

#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
    printf("utMmanHeapAlloc: UTIL_ERR_MMAN_NO_MEMORY\r\n");
#endif

    return UTIL_ERR_MMAN_NO_MEMORY;
}

CYGFX_ERROR utMmanHeapFree(UT_MEMMAN_HEAP_HANDLE hdlmem, void* addr)
{
    BlockInfo *block;
    BlockInfo *tempPtr;
    CYGFX_CHAR merged = 0;  /* Flag if we have performed a merge */
    HeapInfo* heap = (HeapInfo*)hdlmem;
#ifdef UT_MEMMAN_CHECK
    CYGFX_U32 orgSize;
#endif

    if (heap == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanHeapFree: ERROR: hdlmem == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    /* Obtain the block structure from the address */
    block = s_obtainBlock(addr, heap);
    if (block == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanHeapFree() - failed to find block with address 0x%08x\n", (CYGFX_U32)addr);
#endif
        return UTIL_ERR_MMAN_INVALID_MEMORY;
    }
    /* Check if the block is already free */
    if (block->isUsed == 0U)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanHeapFree() - attempt to free already free block at 0x%08x\n", (CYGFX_U32)addr);
#endif
        return UTIL_ERR_MMAN_INVALID_MEMORY;
    }

    /* Mark the block as not used */
    block->isUsed = 0;
    heap->totalAllocated -= ( block->size );
    heap->totalFree += block->size;
#ifdef UT_MEMMAN_CHECK
    orgSize = block->size;
#endif

    /* Check if block before and after are also free then lets merge */
    if (NULL != block->nextBlock )
    {
        tempPtr = block->nextBlock;
        /* Merge to the right */
        if ( tempPtr->isUsed == 0U )
        {
            /* Merge the blocks */
            block->size += tempPtr->size;

            /* Update free list pointers */
            if ( tempPtr->prevFreeBlock != 0 )
            {
                tempPtr->prevFreeBlock->nextFreeBlock = block;
            }
            if ( tempPtr->nextFreeBlock != 0 )
            {
                tempPtr->nextFreeBlock->prevFreeBlock = block;
            }
            block->nextFreeBlock = tempPtr->nextFreeBlock;
            block->prevFreeBlock = tempPtr->prevFreeBlock;

            /* Must update heap free block rover to reflect merge */
            if ( heap->freeBlockRover == tempPtr )
            {
                heap->freeBlockRover = block;
            }

            /* Update next/prev pointers */
            block->nextBlock = tempPtr->nextBlock;
            if (NULL != tempPtr->nextBlock )
            {
                tempPtr->nextBlock->prevBlock = block;
            }

            heap->numAllocatedBlocks--;
            utOsLibcFree(tempPtr);
            merged = 1;
        }
    }



    if (NULL != block->prevBlock )
    {
        tempPtr = block->prevBlock;
        /* Merge to the left */
        if ( tempPtr->isUsed == 0U )
        {
            tempPtr->size += block->size;
            heap->numAllocatedBlocks--;

            /* Update next/prev pointers */
            tempPtr->nextBlock = block->nextBlock;
            if (NULL != block->nextBlock )
            {
                block->nextBlock->prevBlock = tempPtr;
            }

            /* Three way merge */
            if (merged==1)
            {
                heap->numAllocatedBlocks++;
                heap->numFreeBlocks--;

                /* Remove the block from the free list */
                s_removeFromFreeList( block, heap );
            }

            utOsLibcFree(block);
            merged = 1;
        }
    }

    /* There was no merge that took place, so we just add this block to our list */
    if (0 == merged)
    {
        /* Add to free list */
        s_addToFreeList(block, heap);

        /* Update heap info */
        heap->numAllocatedBlocks--;
        heap->numFreeBlocks++;
    }
#ifdef UT_MEMMAN_CHECK
    {
        void* VirtAdd;
        CYGFX_U32 val;
        CYGFX_ADDR addCheck = (((CYGFX_ADDR)addr) + orgSize) - 4;
        UT_HWEB_PHYS_TO_VIRT(addCheck, &VirtAdd);
        UT_HWEB_LOCK(VirtAdd, 4, UT_HWEB_MA_READ);
        memcpy(&val, VirtAdd, 4);
        UT_HWEB_UNLOCK(VirtAdd);
        if (val != MML_UTMEM_MAGIC)
        {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("Error: Memory beyond allocated RAM was overwritten: Allocated block at %p (size %d bytes)\n", addr, orgSize - 4);
#endif
            g_mem_overwrite = CYGFX_TRUE;
            return UTIL_ERR_MMAN_CHECK_FAILED;
        }
    }
#endif

    return CYGFX_OK;
}

CYGFX_ERROR utMmanGetSize(UT_MEMMAN_HEAP_HANDLE  hdlmem, CYGFX_U32 *size)
{
    HeapInfo* heap = (HeapInfo*)hdlmem;

    if (heap == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanGetSize: ERROR: hdlmem == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    *size = heap->size;
    return CYGFX_OK;
}

CYGFX_ERROR utMmanGetFree(UT_MEMMAN_HEAP_HANDLE  hdlmem, CYGFX_U32 *size)
{
    HeapInfo* heap = (HeapInfo*)hdlmem;

    if (heap == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanGetFree: ERROR: hdlmem == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }
    if (size == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanGetFree: ERROR: size == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    *size = heap->totalFree;
    return CYGFX_OK;
}

CYGFX_ERROR utMmanGetLargest(UT_MEMMAN_HEAP_HANDLE  hdlmem, CYGFX_U32 *size)
{
    HeapInfo* heap = (HeapInfo*)hdlmem;
    BlockInfo *block;

     if (heap == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanGetLargest: ERROR: hdlmem == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    if (size == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("utMmanGetLargest: ERROR: size == NULL\r\n");
#endif
        return UTIL_ERR_MMAN_INVALID_PARAMETER;
    }

    /* init */
    *size = 0U;

    /* Get a pointer to the first free block */
    block = heap->freeBlockRover;

    /* Search free list for bigger block */
    while (NULL != block)
    {
        if (block->size > *size)
        {
            *size = block->size;
        }
        block = block->nextFreeBlock;
    }

    return CYGFX_OK;
}

/*
 * Add the specified block to the specified heap's free block list.
 *
 * param block Block to add to the free list.
 * param heap Heap to add block to.
 */
static void s_addToFreeList(BlockInfo* block, HeapInfo* heap)
{
    /* Add to free list */
    if(NULL != heap->freeBlockRover )
    {
        heap->freeBlockRover->prevFreeBlock = block;
        block->nextFreeBlock = heap->freeBlockRover;
    }
    block->prevFreeBlock = 0;
    heap->freeBlockRover = block;
}

/**
 * Remove the specified block from the specified heap's free block list.
 *
 * param block Block to remove from the free list.
 * param heap Heap to remove block from.
 */
static void s_removeFromFreeList(BlockInfo* block, HeapInfo* heap)
{
    /* Remove from free list */

    if( heap->freeBlockRover == block )
    {
        /* So happens to be the first free block */
        if (NULL != block->nextFreeBlock)
        {
            block->nextFreeBlock->prevFreeBlock = 0;
        }
        heap->freeBlockRover = block->nextFreeBlock;
    }
    else
    {
        if (NULL != block->nextFreeBlock)
        {
            block->nextFreeBlock->prevFreeBlock = block->prevFreeBlock;
        }
        block->prevFreeBlock->nextFreeBlock = block->nextFreeBlock;
    }
}

/**
 * Locates a BlockInfo structure associated with
 * the specified address in the specified heap.
 *
 * param addr Address which the block is associated with.
 * param heap Heap with contains the block.
 *
 * return Pointer to the BlockInfo structure, or NULL.
 */
static BlockInfo* s_obtainBlock(void *addr, HeapInfo* heap)
{
    BlockInfo *block = heap->startBlock;

    while (NULL != block )
    {
        if (block->offset == ((CYGFX_U32)addr - heap->baseAddress))
        {
            return block;
        }
        block = block->nextBlock;
    }
    return 0;
}
