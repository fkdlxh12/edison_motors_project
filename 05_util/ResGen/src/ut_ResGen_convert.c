/*******************************************************************************
* Copyright (C) 2013-2022, Cypress Semiconductor Corporation or a              *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

#include <string.h>
#include <stdio.h>

#include "cygfx_driver_api.h"
#include "sm_util.h"
#include "ut_ResGen_convert.h"
//#include "hweb.h"


void read_color(void *pIn,  unsigned int totalBitsIn,  unsigned int strideBitsIn, unsigned int colorBitsIn , unsigned int colorShiftIn,
                       unsigned int x, unsigned int y, unsigned char *r, unsigned char *g, unsigned char *b, unsigned char *a)
{
    unsigned int rgba;
    unsigned int nBits;
    unsigned int nShift;
    unsigned int i, j, col;
    unsigned int nBitPos = y * strideBitsIn + x * totalBitsIn;
    unsigned char *c[4];
    c[0] = a;
    c[1] = b;
    c[2] = g;
    c[3] = r;

    memcpy(&rgba, ((char*)pIn) + (nBitPos >> 3), 4UL);
    rgba >>= (nBitPos & 0x7UL);

    if (totalBitsIn < 32UL)
    {
        rgba &= (1UL<<totalBitsIn) - 1UL;
    }

    for (i = 0UL; i < 4UL; i++)
    {
        nBits = colorBitsIn & 0xffUL;
        nShift = colorShiftIn & 0xffUL;
        col = (rgba>>nShift) & ((1UL << nBits) -1UL);
        for (j = nBits; nBits && j < 8UL; j+=nBits)
        {
            col = (col << nBits) | col;
        }
        if (j > 8UL)
        {
            col >>= j - 8UL;
        }
        *c[i] = (unsigned char)(col & 0xffUL);
        colorBitsIn >>= 8UL;
        colorShiftIn >>= 8UL;
    }
}

void write_color(void* pOut, unsigned int totalBitsOut, unsigned int strideBitsOut, unsigned int colorBitsOut, unsigned int colorShiftOut,
                       unsigned int x, unsigned int y, unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    unsigned int rgba = 0UL, rgba_old, mask = 0UL;
    unsigned int nBits;
    unsigned int nShift;
    unsigned int nBitPos = y * strideBitsOut + x * totalBitsOut;

    nBits = (colorBitsOut>>24) & 0xffUL;
    nShift = (colorShiftOut>>24) & 0xffUL;
    r >>= 8UL - nBits;
    rgba |= r << nShift;

    nBits = (colorBitsOut>>16) & 0xffUL;
    nShift = (colorShiftOut>>16) & 0xffUL;
    g >>= 8UL - nBits;
    rgba |= g << nShift;

    nBits = (colorBitsOut>>8) & 0xffUL;
    nShift = (colorShiftOut>>8) & 0xffUL;
    b >>= 8UL - nBits;
    rgba |= b << nShift;

    nBits = (colorBitsOut>>0) & 0xffUL;
    nShift = (colorShiftOut>>0) & 0xffUL;
    a >>= 8UL - nBits;
    rgba |= a << nShift;

    if (totalBitsOut != 32UL)
    {
        mask = ((1 << totalBitsOut) - 1);
        mask <<= nBitPos & 0x7UL;
        mask = ~mask;
    }
    rgba <<= (nBitPos & 0x7UL);
    memcpy(&rgba_old, ((char*)pOut) + (nBitPos >> 3UL), 4UL);
    rgba_old = (rgba_old & mask) | rgba;
    memcpy(((char*)pOut) + (nBitPos >> 3), &rgba_old, 4UL);
}

void RequiredBitsForColor(unsigned char *bits, unsigned char val)
{
    if (*bits == 8)
        return;
    if (val == 0)
        return;
    for ( ; *bits < 8; (*bits)++)
    {
        unsigned int col = val;
        unsigned int mask = col >> (8 - *bits);
        int j;
        for (j = *bits; j < 8; j+=*bits)
        {
            mask = (mask << *bits) | mask;
        }
        if (j > 8U)
        {
            mask >>= j - 8U;
        }
        if (col == mask)
            return;
    }
    *bits = 8;
}

unsigned char FindBestRGBColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a, unsigned int nColsOut, void* pPalOut, unsigned char PalTypeOut)
{
    unsigned int i, nBest = 0UL;
    unsigned int nDiff, nBestDiff = (unsigned int)-1;
    int rr, gg, bb, aa;
    unsigned char *p = (unsigned char*)pPalOut;

    for (i = 0UL; i < nColsOut; i++)
    {
        nDiff = 0UL;
        if (PalTypeOut & PAL_TYPE_R)
        {
            rr = (int)(*p++) - r;
            nDiff += rr * rr;
        }
        if (PalTypeOut & PAL_TYPE_G)
        {
            gg = (int)(*p++) - g;
            nDiff += gg * gg;
        }
        if (PalTypeOut & PAL_TYPE_B)
        {
            bb = (int)(*p++) - b;
            nDiff += bb * bb;
        }
        if (PalTypeOut & PAL_TYPE_A)
        {
            aa = (int)(*p++) - a;
            nDiff += aa * aa;
        }
        if (nDiff == 0UL)
        {
            return i;
        }
        if (nBestDiff > nDiff)
        {
            nBestDiff = nDiff;
            nBest = i;
        }
    }

    return (unsigned char) nBest;
}


int util_convert(void *pIn,  unsigned int totalBitsIn,  unsigned int strideBitsIn,  unsigned int colorBitsIn , unsigned int colorShiftIn,
                 unsigned int nColsIn, void* pPalIn, unsigned char PalTypeIn,
                 void* pOut, unsigned int totalBitsOut, unsigned int strideBitsOut, unsigned int colorBitsOut, unsigned int colorShiftOut,
                 unsigned int nColsOut, void* pPalOut, unsigned char PalTypeOut,
                 unsigned int width, unsigned int height)
{
    unsigned int x, y;
    unsigned char r,g,b,a;
    unsigned int nBits;
    unsigned int nPalWidthIn = 0UL;

    if (PalTypeIn & PAL_TYPE_R)
    {
        nPalWidthIn++;
    }
    if (PalTypeIn & PAL_TYPE_G)
    {
        nPalWidthIn++;
    }
    if (PalTypeIn & PAL_TYPE_B)
    {
        nPalWidthIn++;
    }
    if (PalTypeIn & PAL_TYPE_A)
    {
        nPalWidthIn++;
    }

    if (nColsIn && (pPalIn == 0))
    {
#ifdef WIN32
        printf("Error util_convert: Wrong in palette\n");
#endif
        return 0;
    }
    if (nColsOut && (pPalOut == 0UL))
    {
#ifdef WIN32
        printf("Error util_convert: Wrong out palette\n");
#endif
        return 0;
    }


    for(y = 0UL; y < height; y++)
    {
        for(x = 0UL; x < width; x++)
        {
            read_color(pIn,  totalBitsIn,  strideBitsIn, colorBitsIn , colorShiftIn,
                           x, y, &r, &g, &b, &a);

            if (nColsIn) // this is a palette
            {
                // the previous function blows r always up to 8 bit but we need the
                // original value
                nBits = (colorBitsIn>>24UL) & 0xffUL;
                r >>= (8UL-nBits);

                if ((pPalIn == 0UL) || (r >= nColsIn))
                {
#ifdef WIN32
                    printf("Error util_convert: Color index larger than palette\n");
#endif
                    return 0UL;
                }
                if (PalTypeIn & PAL_TYPE_A)
                {
                    a = *((unsigned char*)pPalIn + nPalWidthIn * r + 3UL);
                }
                if (PalTypeIn & PAL_TYPE_B)
                {
                    b = *((unsigned char*)pPalIn + nPalWidthIn * r + 2UL);
                }
                if (PalTypeIn & PAL_TYPE_G)
                {
                    g = *((unsigned char*)pPalIn + nPalWidthIn * r + 1UL);
                }
                if (PalTypeIn & PAL_TYPE_R)
                {
                    r = *((unsigned char*)pPalIn + nPalWidthIn * r + 0UL);
                }
            }

            if (nColsOut)
            {
                r = FindBestRGBColor(r, g, b, a, nColsOut, pPalOut, PalTypeOut);
                nBits = (colorBitsOut>>24UL) & 0xffUL;
                r <<= (8UL-nBits);
            }
            write_color(pOut, totalBitsOut, strideBitsOut, colorBitsOut, colorShiftOut,
                        x, y, r, g, b, a);
        }
    }

    return 1;
}

unsigned int Enhance2PowerOf2(unsigned int var)
{
    if (var > 2UL && var < 4UL)
    {
        var = 4UL;
    }
    if (var > 4UL && var < 8UL)
    {
        var = 8UL;
    }
    if (var > 8UL && var < 16UL)
    {
        var = 16UL;
    }
    if (var > 16UL && var < 18UL)
    {
        var = 18UL;
    }
    if (var > 18UL && var < 24UL)
    {
        var = 24UL;
    }
    if (var > 24UL && var < 32UL)
    {
        var = 32UL;
    }
    return var;
}

int util_calc_format(CYGFX_SURFACE src, unsigned int *nColsOut, void* pPalOut,
                unsigned int *totalBitsOut, unsigned int *strideBytesOut, unsigned int *colorBitsOut, unsigned int *colorShiftOut, unsigned int *PalTypeOut)
{
    unsigned int x, y, i, j;
    unsigned char r,g,b,a;
    unsigned char mask[4];
    unsigned char first[4];
    unsigned char bIsGray = 1UL;
    unsigned char bNeedR = 0UL;
    unsigned char bNeedG = 0UL;
    unsigned char bNeedB = 0UL;
    unsigned char bNeedA = 0UL;
    unsigned char pRGB[257][3];
    unsigned char pRGBA[257][4];
    unsigned int nRGB = 0UL;
    unsigned int nRGBA = 0UL;
    unsigned int nRGBIndexBits, nRGBAIndexBits;
    CYGFX_U32 width, height;
    CYGFX_U32 colorBitsIn, bppIn;

    CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_WIDTH, &width);
    CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_HEIGHT, &height);
    CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COLORBITS, &colorBitsIn);
    CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_BITPERPIXEL, &bppIn);

    if(*nColsOut != 0)
    {
        nRGB = *nColsOut;
        nRGBA = *nColsOut;
        memcpy(pRGB, pPalOut, sizeof(pRGB[0]) * nRGB);
        memcpy(pRGBA, pPalOut, sizeof(pRGBA[0]) * nRGBA);
    }
    else
    {
        *PalTypeOut = 0;
    }

    for (i = 0; i < 4; i++)
    {
        mask[i] = 1;
    }

    for(y = 0UL; y < height; y++)
    {
        for(x = 0UL; x < width; x++)
        {
            utSurfGetPixel(src, x, y, &r, &g, &b, &a);

            if (bIsGray && (r != b || b!= g))
                bIsGray = 0;

            // here we check if we really code something (e.g. always alpha 255 can be ignored)
            if (x == 0 && y == 0)
            {
                first[3] = r;
                first[2] = g;
                first[1] = b;
                first[0] = a;
            }
            else
            {
                if (first[3] != r)
                    bNeedR = 1;
                if (first[2] != g)
                    bNeedG = 1;
                if (first[1] != b)
                    bNeedB = 1;
                if (first[0] != a)
                    bNeedA = 1;
            }

            // check RGB palette
            for (i = 0; nRGB < 257 && i < nRGB; i++)
            {
                if (pRGB[i][0] == r && pRGB[i][1] == g && pRGB[i][2] == b)
                    break;
            }
            if (i >= nRGB)
            {
                nRGB = i + 1;
                if (i < 257)
                {
                    pRGB[i][0] = r;
                    pRGB[i][1] = g;
                    pRGB[i][2] = b;
                }
            }
            // check RGBA palette
            for (i = 0; nRGBA < 257 && i < nRGBA; i++)
            {
                if (pRGBA[i][0] == r && pRGBA[i][1] == g && pRGBA[i][2] == b && pRGBA[i][3] == a)
                    break;
            }
            if (i >= nRGBA)
            {
                nRGBA = i + 1;
                if (i < 257)
                {
                    pRGBA[i][0] = r;
                    pRGBA[i][1] = g;
                    pRGBA[i][2] = b;
                    pRGBA[i][3] = a;
                }
            }

            RequiredBitsForColor(&mask[3], r);
            RequiredBitsForColor(&mask[2], g);
            RequiredBitsForColor(&mask[1], b);
            RequiredBitsForColor(&mask[0], a);
        }
    }

    /* if *PalTypeOut !=0, colors saved in previous shared palette */
    if (*PalTypeOut == PAL_TYPE_RGB)
    {
        for (i = 0; nRGB < 257 && i < nRGB; i++)
        {
            // here we check if we really code something (e.g. always alpha 255 can be ignored)
            if (i != 0)
            {
                if (pRGB[i][0] != pRGBA[0][0])
                    bNeedR = 1;
                if (pRGB[i][1] != pRGBA[0][1])
                    bNeedG = 1;
                if (pRGB[i][2] != pRGBA[0][2])
                    bNeedB = 1;
            }

            RequiredBitsForColor(&mask[3], pRGBA[i][0]);
            RequiredBitsForColor(&mask[2], pRGBA[i][1]);
            RequiredBitsForColor(&mask[1], pRGBA[i][2]);
            RequiredBitsForColor(&mask[0], pRGBA[i][3]);
        }
    }

    if (*PalTypeOut == PAL_TYPE_RGBA)
    {
        for (i = 0; nRGBA < 257 && i < nRGBA; i++)
        {
            // here we check if we really code something (e.g. always alpha 255 can be ignored)
            if (i != 0)
            {
                if (pRGBA[i][0] != pRGBA[0][0])
                    bNeedR = 1;
                if (pRGBA[i][1] != pRGBA[0][1])
                    bNeedG = 1;
                if (pRGBA[i][2] != pRGBA[0][2])
                    bNeedB = 1;
                if (pRGBA[i][3] != pRGBA[0][3])
                    bNeedA = 1;
            }

            RequiredBitsForColor(&mask[3], pRGBA[i][0]);
            RequiredBitsForColor(&mask[2], pRGBA[i][1]);
            RequiredBitsForColor(&mask[1], pRGBA[i][2]);
            RequiredBitsForColor(&mask[0], pRGBA[i][3]);
        }
    }

    // delete the mask if we found always the same value
    if ((bNeedR == 0) && mask[3])
        mask[3] = 0;
    if ((bNeedG == 0) && mask[2])
        mask[2] = 0;
    if ((bNeedB == 0) && mask[1])
        mask[1] = 0;
    if ((bNeedA == 0) && mask[0])
        mask[0] = 0;

    *totalBitsOut = 0;
    *colorBitsOut = 0;
    *colorShiftOut = 0;

    if (mask[0]) // has alpha
    {
        nRGBAIndexBits = 0;
        if (nRGBA <= 256)
        {
            nRGBAIndexBits = 1;
            while ((1 << nRGBAIndexBits) < (int)nRGBA)
                nRGBAIndexBits++;
        }
    }

    nRGBIndexBits = 0;
    if (nRGB <= 256)
    {
        nRGBIndexBits = 1;
        while ((1 << nRGBIndexBits) < (int)nRGB)
            nRGBIndexBits++;
    }

    *nColsOut = 0;
    for (i = 0; i < 4; i++)
    {
        (*colorBitsOut)  >>= 8;
        (*colorShiftOut) >>= 8;
        if (mask[i])
        {
            *colorShiftOut |= (*totalBitsOut) << 24;
            if (!bIsGray || (i == 0) || (i == 3))
                *totalBitsOut += mask[i];
            *colorBitsOut |= mask[i] << 24;
        }
    }

    *totalBitsOut = Enhance2PowerOf2(*totalBitsOut);

    if (mask[0]) // has alpha
    {
        if (bIsGray)
            printf("%d bit for gray (%d) and alpha (%d)\n", *totalBitsOut, mask[3], mask[0]);
        else
            printf("%d bit for color (r:%d g:%d b:%d) and alpha (%d)\n", *totalBitsOut, mask[3], mask[2], mask[1], mask[0]);
        if (nRGB <= 256)
        {
            unsigned int tmp = Enhance2PowerOf2(nRGBIndexBits + mask[0]);
            if (tmp < *totalBitsOut)
            {
                printf("%d bit for RGB index (%d) and alpha (%d)\n", tmp, nRGBIndexBits, mask[0]);
                *totalBitsOut = tmp;
                *colorBitsOut = (nRGBIndexBits << 24) | mask[0];
                *colorShiftOut = nRGBIndexBits;
                *nColsOut = nRGB,
                memcpy(pPalOut, pRGB, sizeof(pRGB[0]) * nRGB);
                *PalTypeOut = PAL_TYPE_RGB;
            }
        }
        if (nRGBA <= 256)
        {
            unsigned int tmp = Enhance2PowerOf2(nRGBAIndexBits);
            if (tmp < *totalBitsOut)
            {
                // check in we can use R6G6B6A6 as palette format */
                int OK = 1;
                for (i = 0; (i < nRGBAIndexBits) && (OK == 1); i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        unsigned char bits = 1;
                        RequiredBitsForColor(&bits, pRGBA[i][j]);

                        if (bits >= 6)
                        {
                            OK = 0;
                        }
                    }
                }
                if (OK == 1)
                {
                    printf("%d bit for RGBA index (%d)\n", tmp, nRGBAIndexBits);
                    *totalBitsOut = tmp;
                    *colorBitsOut = (nRGBAIndexBits << 24);
                    *colorShiftOut = 0;
                    *nColsOut = nRGBA,
                    memcpy(pPalOut, pRGBA, sizeof(pRGBA[0]) * nRGBA);
                    *PalTypeOut = PAL_TYPE_RGBA;
                }
            }
        }
    }
    else
    {
        if (bIsGray)
            printf("%d bit for gray\n", mask[3]);
        else
            printf("%d bit for color (r:%d g:%d b:%d)\n", mask[3] + mask[2] + mask[1] , mask[3], mask[2], mask[1]);
        if (nRGB <= 256)
        {
            unsigned int tmp = Enhance2PowerOf2(nRGBIndexBits);
            if (tmp < *totalBitsOut)
            {
                printf("%d bit for RGB index (%d)\n", tmp, nRGBIndexBits);
                *totalBitsOut = tmp;
                *colorBitsOut = (nRGBIndexBits << 24);
                *colorShiftOut = 0;
                *nColsOut = nRGB,
                memcpy(pPalOut, pRGB, sizeof(pRGB[0]) * nRGB);
                *PalTypeOut = PAL_TYPE_RGB;
            }
        }
    }

    *strideBytesOut = ((width * *totalBitsOut) + 7) >> 3;

    return bppIn > *totalBitsOut;
}
