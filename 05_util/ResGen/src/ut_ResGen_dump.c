/*******************************************************************************
* Copyright (C) 2013-2022, Cypress Semiconductor Corporation or a              *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "cygfx_driver_api.h"
#include "sm_util.h"
#include "ut_ResGen_dump.h"

/* The image is run length encoded */
#define SURF_PROP_RLD           0x01
/* The image uses a look up table */
#define SURF_PROP_CLUT          0x02
/* RLA */
#define SURF_PROP_RLA           0x04
/* RLAD */
#define SURF_PROP_RLAD          0x05
/* The image uses a look up table but in a splitted file */
#define SURF_PROP_SPLIT_CLUT    0x08

#define SURF_PROP_COMPRESSED   (SURF_PROP_RLD | SURF_PROP_RLA | SURF_PROP_RLAD)

#define DEPART_CLUT     (1u << 0u)
#define DEPART_SHARE    (1u << 1u)
#define DEPART_SHARE_CLUT (DEPART_CLUT & DEPART_SHARE)
struct SURF_BITMAP
{
    CYGFX_U32  nSize;
    CYGFX_U16  nWidth;
    CYGFX_U16  nHeight;
    CYGFX_U32  strideInByte:16; // or ObjectPartioning for compressed surfaces
    CYGFX_U32  totalBits:8;
    CYGFX_U32  flags:8;
    CYGFX_U32  ColorComponentBits;
    CYGFX_U32  ColorComponentShift;
};

/* optional CLUT description*/
struct SURF_BITMAP_CLUT
{
    CYGFX_U16 cl_count;
    CYGFX_U08 cl_mode;
    CYGFX_U08 cl_totalBits;
    CYGFX_U32 cl_colorBits;
    CYGFX_U32 cl_colorShift;
};

struct SURF_BITMAP_CLUT_FULL
{
    CYGFX_U32 nSize;
    CYGFX_U16 type; //set fix to 1
    CYGFX_U16 version; // currently only 0 supported
    CYGFX_U16 cl_count;
    CYGFX_U16 cl_totalBits;
    CYGFX_U32 cl_colorBits;
    CYGFX_U32 cl_colorShift;
};

CYGFX_U32 util_GetSurfCLUTSize(CYGFX_PALETTE_OBJECT_S* palt)
{
    CYGFX_U32 cl_count, cl_totalBits, nSize;

    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COUNT, &cl_count);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_BITPERPIXEL, &cl_totalBits);
    /* we align the buffer to make the live easy*/
    nSize =  cl_count * cl_totalBits / 8;
    return ((nSize + 3) / 4) * 4;
}


void PrintVoid(FILE *fp, unsigned char*p, int nSize)
{
    int i;
    for (i = 0; i < nSize; i++, p++)
    {
        if (i % 16 == 0)
            fprintf(fp, "\n    ");
        fprintf(fp, "0x%02x", *p);
        if (i != nSize -1)
            fprintf(fp,", ");
    }
}

unsigned char B(unsigned val, int pos)
{
    pos <<= 3;
    return (val>>pos)&0xff;
}

void storeDumpSurface(CYGFX_SURFACE_OBJECT_S* surf, CYGFX_PALETTE_OBJECT_S* palt, const char* szFileName, const char* szClutFileName, const char* szImageName, FILE* fp, const char* include_file, CYGFX_U32 departClut, CYGFX_U32 ClutMode)
{
    CYGFX_BOOL fpOwner = CYGFX_FALSE, cl_fpOwner = CYGFX_FALSE;
    unsigned char * p;
    int nWidth, nHeight;
    CYGFX_U32 flags, nSize, nClutSize, BufferSize, cl_count, compress, Stride_ObjectPartitioning, bpp, ColorComponentBits, ColorComponentShift, rlad_bits;
    CYGFX_U32 cl_mode = ClutMode, cl_colorBits, cl_colorShift, cl_totalBits;
    CYGFX_U32 add, cl_add, cl_size;
    CYGFX_U32 padding, size_in_word, i;
    CYGFX_U32 ObjectPartitioning = 0;
    FILE *cl_fp = 0, *tmp_fp = 0;
    char szUpper[64];

    if (fp == 0)
    {
        fp = fopen(szFileName, "w+");
        fpOwner = CYGFX_TRUE;
    }


    if (!fp)
    {
        return;
    }

    nWidth = utSurfWidth(surf);
    nHeight = utSurfHeight(surf);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &add);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &BufferSize);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE, &Stride_ObjectPartitioning);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_BITPERPIXEL, &bpp);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORBITS, &ColorComponentBits);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORSHIFT, &ColorComponentShift);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COMPRESSION_FORMAT, &compress);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COUNT, &cl_count);
    //CyGfx_PaletteGetAttribute(palt, MML_GDC_SURF_ATTR_CLUTMODE, &cl_mode);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_BITPERPIXEL, &cl_totalBits);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COLORBITS, &cl_colorBits);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COLORSHIFT, &cl_colorShift);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_PHYS_ADDRESS, &cl_add);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS, &rlad_bits);

    /* we align the buffer to make the live easy*/
    cl_size =  cl_count * cl_totalBits / 8;
    cl_size = ((cl_size + 3) / 4) * 4;


    flags = 0;
    nSize = sizeof(struct SURF_BITMAP) + BufferSize;

    if (cl_count)
    {
        if (departClut)
        {
            nClutSize = sizeof(struct SURF_BITMAP_CLUT_FULL) + cl_size;
            flags |= SURF_PROP_SPLIT_CLUT;
        }
        else
        {
            nSize += sizeof(struct SURF_BITMAP_CLUT) + cl_size;
            flags |= SURF_PROP_CLUT;
        }
    }
    if (compress == CYGFX_SM_COMP_RLC)
    {
        flags |= SURF_PROP_RLD;
    }
    if (compress == CYGFX_SM_COMP_RLA)
    {
        flags |= SURF_PROP_RLA;
        //to store the RLAD max bits we use the component bits:
        // it requires less than 4 bit for each component
        // so we can push the rlad_bits to the other 4 bits
        ColorComponentBits |= (rlad_bits)<<4;
    }
    if (compress == CYGFX_SM_COMP_RLAD)
    {
        flags |= SURF_PROP_RLAD;
        //to store the RLAD max bits we use the component bits:
        // it requires less than 4 bit for each component
        // so we can push the rlad_bits to the other 4 bits
        ColorComponentBits |= rlad_bits<<4;
    }
    if (compress == CYGFX_SM_COMP_RLAD_UNIFORM)
    {
        fclose(fp);
        printf("Error: CYGFX_SM_COMP_RLAD_UNIFORM not supported\n");
        return;
    }
    if (compress != CYGFX_SM_COMP_NONE)
    {
        CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, &ObjectPartitioning);
        Stride_ObjectPartitioning = ObjectPartitioning;
    }
    size_in_word = (nSize + 3u) / 4u;
    padding = (size_in_word * 4u) - nSize;
    nSize = size_in_word * 4u;
    i = 0;
    while(szImageName[i] && (i < 63))
    {
      szUpper[i] = toupper(szImageName[i]);
      i++;
      szUpper[i] = 0;
    }

    fprintf(fp, "extern const unsigned char %s[%d];\n\n" ,szImageName, nSize);

    fprintf(fp, "#ifndef %s\n", szUpper);
    fprintf(fp, "#define %s\n", szUpper);
    fprintf(fp, "#ifdef BITMAP_DATA\n\n");

    if (include_file == 0)
    {
        fprintf(fp, "#include \"flash_resource.h\"\n\n");
    }
    else
    {
        fprintf(fp, "#include \"%s\"\n\n", include_file);
    }

    fprintf(fp, "const unsigned char %s[%d] = {\n" ,szImageName, nSize);
    fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x, /* nSize in bytes (%d) */\n", B(nSize, 0), B(nSize, 1), B(nSize, 2), B(nSize, 3), nSize);
    fprintf(fp, "    0x%02x, 0x%02x, /* nWidth (%d) */\n", B(nWidth, 0), B(nWidth, 1), nWidth);
    fprintf(fp, "    0x%02x, 0x%02x, /* nHeight (%d) */\n", B(nHeight,0), B(nHeight,1), nHeight);
    //fprintf(fp, "    0x%02x, 0x%02x, /* strideInByte/Object Partitioning for compressed images (%d) */\n", B(Stride_ObjectPartitioning, 0), B(Stride_ObjectPartitioning, 1), Stride_ObjectPartitioning); 
    fprintf(fp, "    0x%02x, 0x%02x, /* strideInByte (%d) */\n", B(Stride_ObjectPartitioning, 0), B(Stride_ObjectPartitioning, 1), Stride_ObjectPartitioning); 
    fprintf(fp, "    0x%02x, /* totalBits */\n", bpp);
    fprintf(fp, "    0x%02x, /* flags */\n", flags);
    fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* ColorComponentBits (0x%08x) */\n", B(ColorComponentBits, 0),
        B(ColorComponentBits, 1), B(ColorComponentBits, 2), B(ColorComponentBits, 3), ColorComponentBits);
    fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* ColorComponentShift (0x%08x) */\n", B(ColorComponentShift,0),
        B(ColorComponentShift,1), B(ColorComponentShift,2), B(ColorComponentShift,3), ColorComponentShift);

    if (cl_count)
    {
        if (((departClut & DEPART_CLUT) == DEPART_CLUT) &&
           ((departClut & DEPART_SHARE) == 0))
        {
            if (cl_fp == 0)
            {
                cl_fp = fopen(szClutFileName, "w+");
                cl_fpOwner = CYGFX_TRUE;
            }
            if (!cl_fp)
            {
                return;
            
            }

            fprintf(cl_fp, "extern const unsigned char %s_palette[%d];\n\n" ,szImageName, nClutSize);

            fprintf(cl_fp, "#ifndef %s_CLUT\n", szUpper);
            fprintf(cl_fp, "#define %s_CLUT\n", szUpper);
            fprintf(cl_fp, "#ifdef BITMAP_DATA\n\n");

            if (include_file == 0)
            {
                fprintf(cl_fp, "#include \"flash_resource.h\"\n\n");
            }
            else
            {
                fprintf(cl_fp, "#include \"%s\"\n\n", include_file);
            }

            fprintf(cl_fp, "const unsigned char %s_palette[%d] = {\n" ,szImageName, nClutSize);
            fprintf(cl_fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x, /* nSize in bytes (%d) */\n", B(nClutSize, 0), B(nClutSize, 1), B(nClutSize, 2), B(nClutSize, 3), nClutSize);
            fprintf(cl_fp, "    0x%02x, 0x%02x, /* type */\n", 0x1, 0x0);
            fprintf(cl_fp, "    0x%02x, 0x%02x, /* version */\n", 0x0, 0x0);
            
            tmp_fp = cl_fp;
        }
        else
        {
            tmp_fp = fp;
        }
        
        if ((departClut & DEPART_SHARE) == 0)
        {
            /* optional */
            fprintf(tmp_fp, "    0x%02x, 0x%02x, /* cl_count (%d) */\n", B(cl_count, 0), B(cl_count, 1), cl_count);
            
            /* cl_mode is not used in new index palette */
            if((departClut & DEPART_CLUT) == 0)
                fprintf(tmp_fp, "    0x%02x, /* cl_mode */\n", cl_mode);

            if((departClut & DEPART_CLUT) == 0)
            {
                fprintf(tmp_fp, "    0x%02x, /* cl_totalBits (%d) */\n", cl_totalBits, cl_totalBits);
            } else
            {
                fprintf(tmp_fp, "    0x%02x, 0x%02x, /* cl_totalBits (%d) */\n", B(cl_totalBits, 0), B(cl_totalBits, 1), cl_totalBits);
            }
            
            fprintf(tmp_fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* cl_colorBits (0x%08x) */\n", B(cl_colorBits, 0),
                B(cl_colorBits, 1), B(cl_colorBits, 2), B(cl_colorBits, 3), cl_colorBits);
            fprintf(tmp_fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* cl_colorShift (0x%08x) */\n", B(cl_colorShift,0),
                B(cl_colorShift,1), B(cl_colorShift,2), B(cl_colorShift,3), cl_colorShift);
            fprintf(tmp_fp, "    %          /* CLUT data */");
            /* now color table */
            p = (unsigned char *)cl_add;
            PrintVoid(tmp_fp, p, util_GetSurfCLUTSize(palt));
            if((departClut & DEPART_CLUT) == 0)
                fprintf(tmp_fp, ",\n");
        }
        if (((departClut & DEPART_CLUT) == DEPART_CLUT) &&
           ((departClut & DEPART_SHARE) == 0))
        {
            fprintf(cl_fp, "\n};\n\n");

            fprintf(cl_fp, "#endif /* BITMAP_DATA */\n");
            fprintf(cl_fp, "#endif /* %s_CLUT */\n", szUpper);
            
            if (cl_fpOwner)
            {
                fclose(cl_fp);
            }
        }
    }

    p = (unsigned char *)add;
    if (ObjectPartitioning > 1)
    {
        fprintf(fp, "    /* Object Partitioning Information */\n");
    }
    for (i = 0; i < ObjectPartitioning; i++)
    {
        CYGFX_SURFACE_COLUMN_INFO_S column_info;
        memcpy(&column_info, p, sizeof(column_info));
        p += sizeof(column_info);
        BufferSize -= sizeof(column_info);
        fprintf(fp, "    0x%02x, 0x%02x, /* Column %d reseved */\n", B(column_info.reserved, 0), B(column_info.reserved, 1), i);
        fprintf(fp, "    0x%02x, 0x%02x, /* Column %d width (%d)*/\n", B(column_info.column_width, 0), B(column_info.column_width, 1), i, column_info.column_width);
        fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x, /* Column %d offset (0x%08x) */\n", B(column_info.column_address_offset, 0), B(column_info.column_address_offset, 1), B(column_info.column_address_offset, 2), B(column_info.column_address_offset, 3), i, column_info.column_address_offset);
    }
    fprintf(fp, "    /* color / index data */");
    PrintVoid(fp, p, BufferSize);
    for (i = 0; i < padding; i++)
    {
        if (i == 0)
        {
            fprintf(fp, ",\n    ");
        }
        fprintf(fp, "0x00");
        if (i != padding-1)
        {
            fprintf(fp, ", ");
        }
    }

    fprintf(fp, "\n};\n\n");

    fprintf(fp, "#endif /* BITMAP_DATA */\n");
    fprintf(fp, "#endif /* %s */\n", szUpper);


    if (fpOwner)
    {
        fclose(fp);
    }
}

void storeDumpSurfaceBin(CYGFX_SURFACE_OBJECT_S* surf, CYGFX_PALETTE_OBJECT_S* palt, const char* szFileName, FILE* fp, CYGFX_BOOL bAppend, CYGFX_U32 departClut, CYGFX_U32 ClutMode)
{
    CYGFX_BOOL fpOwner = CYGFX_FALSE;
    unsigned char * p;
    int nWidth, nHeight;
    CYGFX_U32 BufferSize, cl_count, compress, Stride_ObjectPartitioning, bpp, ColorComponentBits, ColorComponentShift;
    CYGFX_U32 cl_mode = ClutMode, cl_colorBits, cl_colorShift, cl_totalBits, rlad_bits;
    CYGFX_U32 add, cl_add, cl_size;
    struct SURF_BITMAP bmp;
    CYGFX_U32 padding, size_in_word, i;

    nWidth = utSurfWidth(surf);
    nHeight = utSurfHeight(surf);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &add);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &BufferSize);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE, &Stride_ObjectPartitioning);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_BITPERPIXEL, &bpp);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORBITS, &ColorComponentBits);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORSHIFT, &ColorComponentShift);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COMPRESSION_FORMAT, &compress);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COUNT, &cl_count);
   // CyGfx_PaletteGetAttribute(palt, MML_GDC_SURF_ATTR_CLUTMODE, &cl_mode);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_BITPERPIXEL, &cl_totalBits);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COLORBITS, &cl_colorBits);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_COLORSHIFT, &cl_colorShift);
    CyGfx_PaletteGetAttribute(palt, CYGFX_PALETTE_ATTR_PHYS_ADDRESS, &cl_add);
    CyGfx_PaletteGetAttribute(palt, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS, &rlad_bits);


    if (fp == 0)
    {
        if (bAppend)
        {
            fp = fopen(szFileName, "ab+");
        }
        else
        {
            fp = fopen(szFileName, "wb+");
        }
        fpOwner = CYGFX_TRUE;
    }


    if (!fp)
    {
        return;
    }

    bmp.nWidth = utSurfWidth(surf);
    bmp.nHeight = utSurfHeight(surf);
    bmp.flags = 0;
    cl_size = util_GetSurfCLUTSize(palt);
    bmp.nSize = sizeof(struct SURF_BITMAP) + BufferSize;
    if (cl_count)
    {
        if (departClut)
        {
            bmp.flags |= SURF_PROP_SPLIT_CLUT;
        }
        else
        {
            bmp.nSize += sizeof(struct SURF_BITMAP_CLUT) + cl_size;
            bmp.flags |= SURF_PROP_CLUT;
        }
    }
    if (compress == CYGFX_SM_COMP_RLC)
    {
        bmp.flags |= SURF_PROP_RLD;
    }
    if (compress == CYGFX_SM_COMP_RLA)
    {
        bmp.flags |= SURF_PROP_RLA;
        //to store the RLAD max bits we use the component bits:
        // it requires less than 4 bit for each component
        // so we can push the rlad_bits to the other 4 bits
        ColorComponentBits |= rlad_bits<<4;
    }
    if (compress == CYGFX_SM_COMP_RLAD)
    {
        bmp.flags |= SURF_PROP_RLAD;
        //to store the RLAD max bits we use the component bits:
        // it requires less than 4 bit for each component
        // so we can push the rlad_bits to the other 4 bits
        ColorComponentBits |= rlad_bits<<4;
    }
    if (compress == CYGFX_SM_COMP_RLAD_UNIFORM)
    {
        fclose(fp);
        printf("Error: CYGFX_SM_COMP_RLAD_UNIFORM not supported\n");
        return;
    }
    if (compress != CYGFX_SM_COMP_NONE)
    {
        CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, &Stride_ObjectPartitioning);
    }

    size_in_word = (bmp.nSize + 3u) / 4u;
    padding = (size_in_word * 4u) - bmp.nSize;
    bmp.nSize = size_in_word * 4u;

    bmp.strideInByte = (CYGFX_U16)Stride_ObjectPartitioning;
    bmp.totalBits = (CYGFX_U08)bpp;
    bmp.ColorComponentBits = ColorComponentBits;
    bmp.ColorComponentShift = ColorComponentShift;

    fwrite(&bmp, sizeof(bmp), 1, fp);

    if (cl_count)
    {
        if(departClut == 0)
        {
            struct SURF_BITMAP_CLUT clut;
            clut.cl_count = (CYGFX_U16)cl_count;
            clut.cl_mode = (CYGFX_U08)cl_mode;
            clut.cl_totalBits = (CYGFX_U08)cl_totalBits;
            clut.cl_colorBits = cl_colorBits;
            clut.cl_colorShift = cl_colorShift;
            fwrite(&clut, sizeof(clut), 1, fp);

            /* now color table */
            p = (unsigned char *)cl_add;
            fwrite(p, 1, cl_size, fp);
        }
    }

    p = (unsigned char *)add;
    fwrite(p, 1, BufferSize, fp);

    if (cl_count)
    {
        if (((departClut & DEPART_CLUT) == DEPART_CLUT) &&
            ((departClut & DEPART_SHARE) == 0))
        {
            struct SURF_BITMAP_CLUT_FULL clut_full;
            clut_full.nSize = sizeof(clut_full) + cl_size;
            clut_full.type = 0x1;
            clut_full.version = 0x0;
            clut_full.cl_count = (CYGFX_U16)cl_count;
            clut_full.cl_totalBits = (CYGFX_U08)cl_totalBits;
            clut_full.cl_colorBits = cl_colorBits;
            clut_full.cl_colorShift = cl_colorShift;
            fwrite(&clut_full, sizeof(clut_full), 1, fp);

            /* now color table */
            p = (unsigned char *)cl_add;
            fwrite(p, 1, cl_size, fp);
        }
    }

    for (i = 0; i < padding; i++)
    {
        CYGFX_U32 zero = 0;
        fwrite(&zero, 1, 1, fp);
    }

    if (fpOwner)
    {
        fclose(fp);
    }
}
