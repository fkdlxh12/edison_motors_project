/*******************************************************************************
* Copyright (C) 2013-2022, Cypress Semiconductor Corporation or a              *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

#include <stdio.h>
#include <png.h>
#ifndef __CC_ARM
#include <malloc.h>
#endif

#include "ut_ResGen_png.h"

#define debug printf

int png_Read(const char *filename, void **buffer, unsigned int *width, unsigned int *height, int *pFormat, unsigned int *pnColCnt, void **ppPallete)
{
    FILE *file;
    unsigned int i;
    png_structp png_ptr;
    png_infop info_ptr;
    //png_colorp palette;
    png_byte **row_pointers;
    char header[8];
    int bit_depth, color_type, interlace_type;
    png_uint_32 w, h;
    int Format = *pFormat;
    unsigned int nColCntIn = 0;

    // Open file.
    file = fopen(filename, "rb");
    if (file == NULL) {
        //ESTPrintf("ESTReadPng: Error opening file: %s\n", filename);
        return 0;
    }

    // Read file header.
    fread(header, 1, 8, file);
    if (png_sig_cmp(header, 0, 8)) {
        //ESTPrintf("ESTReadPng: Not a valid png file: %s\n", filename);
        return 0;
    }

    // Create read structure.
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fclose(file);
        return 0;
    }

    // Create info structure.
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fclose(file);
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        return 0;
    }

    // Setup the error handling.
    if (setjmp(png_jmpbuf(png_ptr))) {
        fclose(file);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return 0;
    }

    // Initialize png io.
    png_init_io(png_ptr, file);

    /* If we have already read some of the signature */
    png_set_sig_bytes(png_ptr, 8);

    // Read image info.
    png_read_info(png_ptr, info_ptr);

   png_get_IHDR(png_ptr, info_ptr, &w, &h, &bit_depth, &color_type,
       &interlace_type, int_p_NULL, int_p_NULL);

    if (width != NULL && *width > 0) {
        if (*width < w) {
            //ESTPrintf("ESTReadPng: Expected width (%d) doesn't match image width (%d) for image %s.\n", 
            //          *width, w, filename);
            fclose(file);
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return 0;
        }
    }
    if (height != NULL && *height > 0) {
        if (*height < h) {
            //ESTPrintf("ESTReadPng: Expected height (%d) doesn't match image height (%d) for image %s.\n", 
            //          *height, h, filename);
            fclose(file);
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return 0;
        }
    }
    if (width != NULL) {
        *width = w;
    }
    if (height != NULL) {
        *height = h;
    }

    if (Format != UPNG_UNKNOWN && Format != UPNG_A8B8G8R8)
    {
        printf("Only Format PNG_ANY, PNG_R8G8B8A8 as parameter allowed\n");
        fclose(file);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return 0;
    }

/* tell libpng to strip 16 bit depth files down to 8 bits */
    if (info_ptr->bit_depth == 16)
    {
        png_set_strip_16(png_ptr);
        bit_depth = 8;
    }
    if(pnColCnt)
    {
        nColCntIn = *pnColCnt;
        *pnColCnt = 0;
    }

    if (Format == UPNG_UNKNOWN)
        *pFormat = color_type | (bit_depth<<4);

    if (Format == UPNG_A8B8G8R8)
    {
        if (bit_depth < 8)
            png_set_packing(png_ptr);

    // Convert each color type to RGBA
        // minimum of a byte per pixel, then convert 1 byte to RGBA 
        if (color_type == PNG_COLOR_TYPE_GRAY) 
        {
            //debug("Color type GRAY converted to 8 bit, rgb, alpha\n");
            //Up to 8 bits if 1, 2 or 4
            if (bit_depth <8)
                png_set_gray_1_2_4_to_8(png_ptr);
            //Change gray to rgb
            png_set_gray_to_rgb(png_ptr);
            // pad with alpha = 255
            png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }

        // minimum of a byte per pixel, then convert 1 byte to RGB and add alpha 
        if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA) 
        {
            //debug("Color type GRAY ALPHA converted to 8 bit, rgb, alpha\n");
            //Up to 8 bits if 1, 2 or 4
            if (bit_depth <8)
                png_set_gray_1_2_4_to_8(png_ptr);
            //Change gray to rgb
            png_set_gray_to_rgb(png_ptr);
            // add alpha if any alpha found
            if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) 
            {
                png_set_tRNS_to_alpha(png_ptr);
            }
            //or pad with alpha = 255
            else
                png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }

        // minimum of a byte per pixel, then convert palettes to RGBA
        if (color_type == PNG_COLOR_TYPE_PALETTE)
        {	//debug("Color type PALETTE converted to 8 bit, rgb, alpha\n");
            //Up to 8 bits if 1, 2 or 4
            if (bit_depth <8)
                png_set_gray_1_2_4_to_8(png_ptr);
            //Change palette to rgb
            png_set_palette_to_rgb(png_ptr);
            // pad with alpha = 255
            png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }

        // Convert 3 byte RGB to 4 byte color with 4th byte = 255
        if (color_type == PNG_COLOR_TYPE_RGB)
        {	
            //debug("Color type RGB converted to 8 bit, rbg, alpha\n");
            png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }

        // Convert 16 bit RGBA to 8 bit RGBA
        if (color_type == PNG_COLOR_TYPE_RGB_ALPHA)
        {	
            //debug("Color type RGBA converted to 8 bit, rgb, alpha\n");
            // add alpha if any alpha found
            if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) 
            {
                png_set_tRNS_to_alpha(png_ptr);
            }
            //or pad with alpha = 255
            else
                png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }
    }
    else //PNG_ANY
    {
        png_set_packswap(png_ptr);
        if (color_type == PNG_COLOR_TYPE_GRAY) 
            png_set_packswap(png_ptr);

        if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA) 
        {
            png_set_packswap(png_ptr);

            if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) 
                png_set_tRNS_to_alpha(png_ptr);
            else
                png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }

        if (color_type == PNG_COLOR_TYPE_PALETTE)
        {	
            if (pnColCnt != NULL)
            {
                if (nColCntIn < info_ptr->num_palette)
                {
                    fclose(file);
                    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
                    return 0;
                }
                *pnColCnt = info_ptr->num_palette;
            }
            if (ppPallete != NULL && *ppPallete == NULL)
            {
                *ppPallete = (void**)malloc(3 * info_ptr->num_palette);
            }

            if (ppPallete == NULL || *ppPallete == NULL) {
                fclose(file);
                png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
                return 0;
            }

            memcpy(*ppPallete, info_ptr->palette, (*pnColCnt) * 3);
            
        }

        if (color_type == PNG_COLOR_TYPE_RGB)
        {	
            //debug("Color type RGB converted to 8 bit, rbg, alpha\n");
        }

        if (color_type == PNG_COLOR_TYPE_RGB_ALPHA)
        {	
            //debug("Color type RGBA converted to 8 bit, rgb, alpha\n");
            // add alpha if any alpha found
            if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) 
            {
                png_set_tRNS_to_alpha(png_ptr);
            }
            //or pad with alpha = 255
            else
                png_set_filler (png_ptr, 0xff, PNG_FILLER_AFTER);
        }
    }


    png_read_update_info(png_ptr, info_ptr);


    if (Format == UPNG_ERROR_FORMAT)
    {
        fclose(file);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return 0;
    }

    // Allocate buffer if needed.
    if (buffer == NULL) {
        fclose(file);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return 1;
    }
    if (*buffer == NULL) {
        *buffer = (char*)malloc(w * h * 4);
        if (*buffer == NULL) {
            fclose(file);
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return 0;
        }
    }

    // Set the row pointers.
    row_pointers = (png_byte**)malloc(sizeof(png_byte*) * h);
    for (i = 0; i < h; i++) {
        row_pointers[i] = ((char*)*buffer) + i * info_ptr->rowbytes;
    }

    // Read the image, finish the read, free the memory, and close the file.
    png_read_image(png_ptr, row_pointers);
    png_read_end(png_ptr, NULL);
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    free(row_pointers);
    fclose(file);

    // Return success.
    return 1;
}

int GetIrisParam(int nFormat, unsigned int width, unsigned int *pTotalBits, unsigned int *pStrideBits, unsigned int *pColorBits, unsigned int *pColorShift)
{
    *pTotalBits = 0; 
    *pColorBits = 0;
    *pColorShift = 0;

    if (UPNG_HAS_PALETTE(nFormat))
    {
        //r
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<24;
        *pColorShift|= *pTotalBits<<24;
        //g
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<16;
        *pColorShift|= *pTotalBits<<16;
        //b
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<8;
        *pColorShift|= *pTotalBits<<8;
        *pTotalBits += UPNG_GET_BIT_PER_CHANNEL(nFormat);
    }
    else if (UPNG_HAS_COLOR(nFormat))
    {
        //r
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<24;
        *pColorShift|= *pTotalBits<<24;
        *pTotalBits += UPNG_GET_BIT_PER_CHANNEL(nFormat);
        //g
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<16;
        *pColorShift|= *pTotalBits<<16;
        *pTotalBits += UPNG_GET_BIT_PER_CHANNEL(nFormat);
        //b
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<8;
        *pColorShift|= *pTotalBits<<8;
        *pTotalBits += UPNG_GET_BIT_PER_CHANNEL(nFormat);
    }
    else // GRAY
    {
        //r
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<24;
        *pColorShift|= *pTotalBits<<24;
        //g
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<16;
        *pColorShift|= *pTotalBits<<16;
        //b
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat)<<8;
        *pColorShift|= *pTotalBits<<8;
        *pTotalBits += UPNG_GET_BIT_PER_CHANNEL(nFormat);
    }
    if (UPNG_HAS_ALPHA(nFormat))
    {
        *pColorBits |= UPNG_GET_BIT_PER_CHANNEL(nFormat);
        *pColorShift|= *pTotalBits;
        *pTotalBits += UPNG_GET_BIT_PER_CHANNEL(nFormat);
    }

    *pStrideBits = *pTotalBits * width;
    while (*pStrideBits & 0x7)
        (*pStrideBits)++;

    return 1;
}

