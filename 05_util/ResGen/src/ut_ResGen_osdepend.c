﻿/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        ut_osdepend.c
 * \brief       This implements a sample memory manager.
 */

/*******************************************************************************
 Includes
*******************************************************************************/
#include <stdlib.h>         /* For _MAX_PATH definition */
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <winsock.h>

#include "cygfx_driver_api.h"

#include "ut_memman.h"
#include "ut_timer.h"
#include "ut_compatibility.h"
#include "ut_config.h"

/*******************************************************************************
 Macro Definitions
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Data Types
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Variables
*******************************************************************************/

HANDLE g_hSemaphore = NULL;

/*******************************************************************************
 Function Prototypes
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Definitions
*******************************************************************************/

CYGFX_U32 utGetVRAMSize()
{
#ifndef RES_GEN
    return hwi_GetRamSize();
#endif
}

CYGFX_U32 utGetVRAMBaseAddr()
{
#ifndef RES_GEN
    return hwi_GetRamBaseAddr();
#endif
}



void *utOsLibcMalloc(size_t mallocSize)
{
    void *ret = NULL;
    WaitForSingleObject(g_hSemaphore, INFINITE);
    ret = malloc(mallocSize);
    ReleaseSemaphore(g_hSemaphore, 1, 0);
    return ret;
}

void utOsLibcFree(void * pMemory)
{
    void *ret = NULL;
    WaitForSingleObject(g_hSemaphore, INFINITE);
    free(pMemory);
    ReleaseSemaphore(g_hSemaphore, 1, 0);
}

#ifndef RES_GEN
void erp_printf(const char *string)
{
    if (0U < strlen(string))
    {
#ifndef PRODUCTION
        if (cygfx_Interrupt_inISR == CYGFX_TRUE)
#else
        if (0)
#endif /* PRODUCTION */
        {
            FILE *kernel_log;
            WaitForSingleObject(g_hSemaphore, INFINITE);
            kernel_log = fopen("kernel.log", "a");
            if (kernel_log)
            {
                fprintf(kernel_log, "<IRQ> ");
                fprintf(kernel_log, string);
                fclose(kernel_log);
            }
            ReleaseSemaphore(g_hSemaphore, 1, 0);
        } else
        {
#if (defined (DEBUG) || defined (RELEASE))
            printf(string);
#endif
        }
    }
}
#endif


/* Timer functions */

CYGFX_ERROR utOsInitTimer()
{
    return CYGFX_OK;
}

CYGFX_ERROR utOsGetTime(UT_OS_SYSTIM *pSystim)
{
    #ifdef GetTickCount64
    *pSystim = GetTickCount64();
    #else 
        *pSystim = GetTickCount();
    #endif
    return CYGFX_OK;
}

CYGFX_FLOAT utOsDurationSec(UT_OS_SYSTIM *pStart,
                          UT_OS_SYSTIM *pStop)
{
    return (*pStop - *pStart)/1000.0f;
}

void utMmanCheckHeap(void)
{

}

void utMmanCheckStack(void)
{

}
