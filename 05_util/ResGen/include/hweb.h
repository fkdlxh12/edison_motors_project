/*******************************************************************************
* Warranty and Disclaimer
*
* This software product is property of Cypress Semiconductor Corporation or
* its subsidiaries.
* Any use and/or distribution rights for this software product are provided
* only under the Cypress Software License Agreement.
* Any use and/or distribution of this software product not in accordance with
* the terms of the Cypress Software License Agreement are unauthorized and
* shall constitute an infringement of Cypress intellectual property rights.
*/
/******************************************************************************/

/**
 * \file        ResGen/include/hweb.h
 *              Hardware exchange block for Resource Generator
 *              This file defines the interface for the Graphics Driver to
 *              Resource Generator. All read and write operations to registers 
 *              and memory are redirected by to nothing as resource Generator 
 *              doesn't have dependencies on Hardware exchange block.
 * \brief       Implementation of HW exchange block
 * \ingroup     bg_implementation_service
 * Implements Building Block: HWExchangeBlock
 *
 */

#ifndef HWEB_H
#define HWEB_H

/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include "hw_interface.h"

/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/
/*
    Define macros without functionality
*/
#define UT_HWEB_LOCK(ADD, CNT, ACCESS) 
#define UT_HWEB_UNLOCK(ADD)            

#define UT_HWEB_PHYS_TO_VIRT(phys, ppVirt) 
#define UT_HWEB_VIRT_TO_PHYS(pVirt, pPhys) 

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/


#endif /* HWEB_H */



