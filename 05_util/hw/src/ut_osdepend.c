﻿/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        ut_osdepend.c
 * \brief       This implements a sample memory manager.
 */

/*******************************************************************************
 Includes
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cygfx_driver_api.h"

#include "ut_memman.h"
#include "ut_printf.h"
#include "ut_timer.h"
#include "ut_compatibility.h"
#include "ut_config.h"

#include "hweb.h"

#include "cy_project.h"
#include "cy_device_headers.h"


/*******************************************************************************
 Macro Definitions
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Data Types
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Variables
*******************************************************************************/

/* The linker file must place the part of the VRAM that is accessible for the VRAM memory manager, to the end of the VRAM. e.g.:
    VRAM                 : ORIGIN = 0x24000000,   LENGTH = 0x00400000         //    4 MB
    DYNAMIC_VRAM_SECTION            ALIGN(16)                        : > VRAM
    __ghsbegin_DYNAMIC_VRAM_SECTION = MEMADDR(VRAM);
    __ghsend_DYNAMIC_VRAM_SECTION   = ENDADDR(VRAM);
    
*/
#ifdef __ghs__
extern char __ghsbegin_DYNAMIC_VRAM_SECTION[];
extern char __ghsend_DYNAMIC_VRAM_SECTION[];
extern char __ghsbegin_heap[];
extern char __ghsend_heap[];
extern char __ghsbegin_stack[];
extern char __ghsend_stack[];
#elif __ICCARM__

#pragma section = "HEAP_STACK"
extern char __gfx_vram_start_address[];
extern char __gfx_vram_end_address[];
extern char __gfx_heap_size[];
#else
#error "undefined sections"
#endif

/*******************************************************************************
 Function Prototypes
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Definitions
*******************************************************************************/


CYGFX_U32 utGetVRAMSize()
{
#ifdef __ghs__
#ifdef TVIIC2D6MDDR
    return (CYGFX_U32)__ghsend_DYNAMIC_LPDDR4_SECTION - (CYGFX_U32)__ghsbegin_DYNAMIC_LPDDR4_SECTION;
#else
    return (CYGFX_U32)__ghsend_DYNAMIC_VRAM_SECTION - (CYGFX_U32)__ghsbegin_DYNAMIC_VRAM_SECTION;
#endif

#elif __ICCARM__
    return (CYGFX_U32)__gfx_vram_end_address - (CYGFX_U32)__gfx_vram_start_address;
#else
    return 0;
#endif
}

CYGFX_U32 utGetVRAMBaseAddr()
{
#ifdef __ghs__
#ifdef TVIIC2D6MDDR
    return (CYGFX_U32)__ghsbegin_DYNAMIC_LPDDR4_SECTION;
#else
    return (CYGFX_U32)__ghsbegin_DYNAMIC_VRAM_SECTION;
#endif
#elif __ICCARM__
    return (CYGFX_U32)__gfx_vram_start_address;
#else
    return 0;
#endif
}

void *utOsLibcMalloc(size_t mallocSize)
{
    return malloc(mallocSize);
}

void utOsLibcFree(void * pMemory)
{
    free(pMemory);
}


#define KERNEL_LOG_SIZE 1024
CYGFX_CHAR kernel_log[KERNEL_LOG_SIZE] = {0};
void erp_printf(const char *string)
{   

    if (0 != string)
    {
        if (0 != string[0])
        {
#ifndef PRODUCTION
            if (cygfx_Interrupt_inISR == CYGFX_TRUE)
#else
            if (0)
#endif /* PRODUCTION */
            {
                /* We are at interrupt level.
                   Don't write to stdout in that case, in order to prevent deadlock!
                   Write to kernel_log buffer instead. */
                snprintf(&(kernel_log[strlen(kernel_log)]), KERNEL_LOG_SIZE - strlen(kernel_log) - 1, "<IRQ> %s", string);

                if (strlen(kernel_log) > KERNEL_LOG_SIZE - 6)
                {
                     strcpy (&kernel_log[KERNEL_LOG_SIZE-6],"...\r\n");
                }
            } 
            else
            {
                /* print out messages we held back during interrupt */
                if (kernel_log[0] != 0)
                {
                    printf(kernel_log);
                    kernel_log[0] = 0x00;
                }
                printf(string);
            }
        }
    }
}


/* Timer functions */

CYGFX_ERROR utOsInitTimer()
{
    return CYGFX_OK;
}

CYGFX_ERROR utOsGetTime(UT_OS_SYSTIM *pSystim)
{
    *pSystim = Cy_SwTmr_GetTickCountUs();
    return CYGFX_OK;
}

CYGFX_FLOAT utOsDurationSec(UT_OS_SYSTIM *pStart,
                            UT_OS_SYSTIM *pStop)
{
    return (*pStop - *pStart)/1000000.0f;
}

void utMmanCheckHeap(void)
{
    CYGFX_U32 j;
#ifdef __ghs__
    CYGFX_U32 heapStart = (CYGFX_U32)__ghsbegin_heap;
    CYGFX_U32 heapEnd   = (CYGFX_U32)__ghsend_heap;
#elif __ICCARM__
     CYGFX_U32 heapStart = (CYGFX_U32)__section_begin("HEAP_STACK");
    CYGFX_U32 heapEnd = heapStart + (CYGFX_U32)__gfx_heap_size;
#endif
    /* PLEASE NOTE: HEAP must be cleared (in ld-file) for a proper working order of this function! */
    /* get last used address of heap */
    for (j = heapEnd - 4; heapStart < j; j -= 4)
    {
        if ( 0 != UT_HWEB_READ32(NULL, j) ) break;
    }

#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
    printf("utMmanCheckHeap: %u %% usage ( %u of %u, 0x%08X ... 0x%08X)\n", 100 * (j - heapStart) / (heapEnd - heapStart), (j - heapStart), (heapEnd - heapStart), heapStart, heapEnd);
#endif

}

void utMmanCheckStack(void)
{
    CYGFX_U32 j;
#ifdef __ghs__
    CYGFX_U32 usrStart = (CYGFX_U32)__ghsbegin_stack;
    CYGFX_U32 usrEnd   = (CYGFX_U32)__ghsend_stack;
#elif __ICCARM__
    CYGFX_U32 usrStart = (CYGFX_U32)__section_begin("HEAP_STACK");
    usrStart = usrStart+ (CYGFX_U32)__gfx_heap_size;
    CYGFX_U32 usrEnd   =(CYGFX_U32)__section_end("HEAP_STACK");
#endif
    /* get last used address of stack */
    for (j = usrStart; j < usrEnd; j += 4)
    {
        if ( 0 != UT_HWEB_READ32(NULL, j) ) break;
    }

#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
    printf("utMmanCheckStack: %u %% usage ( %u of %u, 0x%08X ... 0x%08X)\n", 100 * (usrEnd - j) / (usrEnd - usrStart), (usrEnd - j), (usrEnd - usrStart), usrStart, usrEnd);
#endif
}


