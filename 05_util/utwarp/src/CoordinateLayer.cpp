/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        CoordinateLayer.cpp
 */

#if defined(C_MODEL) || defined(FPGA)
#include <cstdio>
#endif

#include <iostream>

#if defined(C_MODEL) || defined(FPGA)
#include <string>
#endif

#include <vector>

#if defined(C_MODEL) || defined(FPGA)
#include <ostream>
#endif

#include <fstream>
#include <sstream>

#if defined(C_MODEL) || defined(FPGA)
#include <cmath>
#include <cstdlib>
#include <stdexcept>
#endif

#include <iomanip>

#if defined(C_MODEL) || defined(FPGA)
#include <string.h>
#endif


#define _USE_MATH_DEFINES
#include <math.h>

#include "CoordinateLayer.h"

#ifdef __ICCARM__
#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif
#endif
namespace coordinate {

void CoordinateLayer::initialize() {
  this->chip = TRAVEO;
  this->width = 0;
  this->height = 0;
  this->m_sourceWidth = 0;
  this->m_sourceHeight = 0;
  this->notation = FLOAT;
  this->mode = PNT;
  this->target_mode = PNT;
  this->bpp = 0;
  this->offset = 0;
  this->max_grid_error = 0.0625;
  this->mse = 0.0;
  this->faulty = false;
  this->fixpoint_int = 0;
  this->fixpoint_dec = 0;
  this->p.clear();

  this->frameoffset.x = 0;
  this->frameoffset.y = 0;

  this->deltax.x = 0;
  this->deltax.y = 0;

  this->deltay.x = 0;
  this->deltay.y = 0;

  // default frameoffset register precision
  this->frameoffset_fixedpoint_int = 16;
  this->frameoffset_fixedpoint_dec = 5;
  this->frameoffset_isfixedpoint = 0;

  // default delta register precision
  this->delta_fixedpoint_int = 7;
  this->delta_fixedpoint_dec = 5;
  // or 7, 18
  // or 16, 5 and left shift by 13
  this->delta_isfixedpoint = 0;

  this->internal_int = 21;
  this->internal_dec = 5;

  this->ongrid = 0;
  this->error_cnt = 0;

  this->buf = NULL;
  this->buf_size = 0;
  this->buf_i = 0;

  this->stat_cnt_inside = 0;
  this->stat_avg_dy = 0;
}

CoordinateLayer::CoordinateLayer() {
  this->initialize();
}

CoordinateLayer::CoordinateLayer(unsigned int width, unsigned int height) {
  this->initialize();
  this->width = width;
  this->height = height;
  p.resize(this->width * this->height);
}

CoordinateLayer::CoordinateLayer(unsigned int width, unsigned int height, unsigned int sourceWidth, unsigned int sourceHeight) {
  this->initialize();
  this->width = width;
  this->height = height;
  this->m_sourceWidth = sourceWidth;
  this->m_sourceHeight = sourceHeight;
  p.resize(this->width * this->height);
}

CoordinateLayer::~CoordinateLayer() {
  p.clear();
  destroy_buf();
}

CoordinateLayer::CoordinateLayer(const CoordinateLayer& cp) {
  buf = NULL;
  operator=(cp);
}

CoordinateLayer& CoordinateLayer::operator= (const CoordinateLayer& cp)
{
  if (this == &cp) { // check identity
    return *this;
  }
  if (buf) {
    delete [] buf;
    buf = NULL;
    buf_size = 0;
    buf_i = 0;
  }
  chip = cp.chip;
  width = cp.width;
  height = cp.height;
  m_sourceWidth = cp.m_sourceWidth;
  m_sourceHeight = cp.m_sourceHeight;
  notation = cp.notation;
  mode = cp.mode;
  target_mode = cp.target_mode;
  bpp = cp.bpp;
  offset = cp.offset; 
  faulty = cp.faulty;
  ongrid = cp.ongrid;
  error_cnt = cp.error_cnt;
  fixpoint_int = cp.fixpoint_int;
  fixpoint_dec = cp.fixpoint_dec;
  p = cp.p;
  internal_int = cp.internal_int;
  internal_dec = cp.internal_dec;
  frameoffset = cp.frameoffset;
  frameoffset_fixedpoint_int = cp.frameoffset_fixedpoint_int;
  frameoffset_fixedpoint_dec = cp.frameoffset_fixedpoint_dec;
  frameoffset_isfixedpoint = cp.frameoffset_isfixedpoint;
  deltax = cp.deltax;
  deltay = cp.deltay;
  delta_fixedpoint_int = cp.delta_fixedpoint_int;
  delta_fixedpoint_dec = cp.delta_fixedpoint_dec;
  delta_isfixedpoint = cp.delta_isfixedpoint;
  max_grid_error = cp.max_grid_error;
  mse = cp.mse;
  //buf = cp.buf;
  buf_size = cp.buf_size;
  buf_i = cp.buf_i;

  if (cp.buf) {
    buf = new char [buf_size];
    memcpy(buf, cp.buf, buf_size);
  }

  return *this;
}

void CoordinateLayer::read(const std::string& filename) {
  std::string buffer;
  std::string buffer_tmp;
  unsigned int header = 0;
  double x, y;

  // reset properties
  this->fixpoint_int = 0;
  this->fixpoint_dec = 0;
  this->notation = FLOAT;
  this->faulty = false;

  p.clear();
  
  std::ifstream fs(filename.c_str());

  if (fs.is_open() != true) {
    std::cerr << "Error: Cannot open file '" << filename << "' for reading." << std::endl;
  }
  unsigned int i =0;
  while (getline(fs, buffer)) {
    //std::cout << i << ": " << buffer << std::endl;

    // remove comments
    buffer = buffer.substr(0, buffer.find("#"));

    //std::cout << i << " s: '" << buffer << "'"<< std::endl;

    // parse
    if (buffer.size() > 0) {
       std::stringstream istr1; // new stream for every conversion
       std::stringstream istr2; // new stream for every conversion
       x = 0;
       y = 0;

       //std::cout << "bs:"<< buffer.size() << std::endl;
       if (header == 0) {
         //buffer.compare
         ++header;
       } else if (header == 1) {
         //istr.str( buffer );
         istr1 << buffer;
         istr1 >> this->width >> this->height;
         ++header;
       } else if (header == 2) {
         this->notation = FLOAT;
         ++header;
       } else {
         // split buffer into two, remove whitespaces and parse double if applicable
         buffer_tmp = buffer.substr(0, buffer.find_first_of(" ") );
         if (buffer_tmp.size() == buffer.size())
           buffer_tmp = buffer.substr(0, buffer.find_first_of("\t") );

         remove_whitespace(buffer_tmp);
         if (buffer_tmp.size() == 0)
           continue;
         istr1.str(buffer_tmp);


         buffer_tmp = buffer.substr(buffer.find_first_of(" ")+1, buffer.size() );
         if (buffer_tmp.size() == buffer.size())
           buffer_tmp = buffer.substr(buffer.find_first_of("\t")+1, buffer.size() );

         remove_whitespace(buffer_tmp);
         if (buffer_tmp.size() == 0)
           continue;
         istr2.str( buffer_tmp);

         //std::cout << "istr1.str()=" << istr1.str() <<std::endl;
         //std::cout << ")istr2.str()=" << istr2.str() <<std::endl;

         istr1 >> x;
         istr2 >> y;
         if (!istr1.eof() || !istr2.eof()) {
           std::cerr << "Warning: Cannot convert value to double at input line " << i << "." << std::endl;
           continue;
         }

         //std::cout <<">> " << x << " : " << y<< std::endl<<std::endl;
         p.push_back(Coordinate(x, y));

       }
    }
    ++i;
  }
  fs.close();

  if (this->p.size() != (this->width * this->height)) {
    faulty = true;
    std::cerr << "Error: Meta info does not match actual size" << std::endl;
    std::cerr << "p.size()=" << p.size() << " width=" << width << " height=" << height << std::endl;
  }
}

//#define DEBUG_WRITE
void CoordinateLayer::write(const std::string& filename, int binary) {
  std::fstream fs;

  if (binary == 0) {
    fs.precision(15);
    fs.open(filename.c_str(), std::ios::out|std::ios::trunc );
  } else {
    fs.open(filename.c_str(), std::ios::out|std::ios::trunc|std::ios::binary );
  }
  if (fs.is_open() != true) {
    std::cerr << "Error: Cannot open file '" << filename << "' for writing." << std::endl;
  }

  if (0 == binary) {
    // header
    fs << "C1" << std::endl;
    fs << width << " " << height << std::endl;
    if (faulty)
      fs << "# Note: This file is broken" << std::endl;

    fs << this->notation;
    if (fixpoint_int !=0 || fixpoint_dec != 0) {
      fs << " # fixed point precision: " << fixpoint_int << " " << fixpoint_dec << std::endl;
    } else {
      fs << " # double precision" << std::endl;
    }
    fs << "# mode=" << mode << " notation=" << notation << std::endl;
    if (mode == D_PNT || mode == DD_PNT) {
      fs << "# Register settings:" << std::endl;
      fs << "#  frameoffset.x=" << frameoffset.x << " frameoffset.y=" << frameoffset.y << std::endl;
    }
    if (mode == DD_PNT) {
      fs << "#  v1: deltax.x=" << deltax.x << " deltax.y=" << deltax.y << std::endl;
      fs << "#  v4: deltay.x=" << deltay.x << " deltay.y=" << deltay.y << std::endl;
    }
    fs << "#" << std::endl;

    // data
    for (unsigned int i=0; i < p.size(); ++i) {
      //fs << p.at(i) << std::endl;
      fs << p.at(i).x << " " << p.at(i).y << std::endl;
      if ( fs.fail() ) {
        std::cerr << "Error: Failure on output stream." << std::endl;
        break;
      }
    }
  } else {
    //#define DEBUG_WRITE

    #ifdef DEBUG_WRITE
    std::cout << "Write binary mode=" << binary << std::endl;
    std::cout << "bpp=" << bpp << std::endl;
    std::cout << "width=" << width << " height=" << height << std::endl;
    #endif
    // check if fixed point
    if (notation != FIX) {
      double2fix();
    }

    create_buf();
    fs.write(buf, (std::streamsize)buf_size);
    if ( fs.fail() ) {
       std::cerr << "Error: Failure on output stream." << std::endl;
       return;
    }
    destroy_buf();

  } // else binary
  fs.close();
}

//#define DEBUG_BUFFER
int CoordinateLayer::get_buffer(void *buffer, size_t &size) {

  if (notation != FIX) {
    //std::cerr << "Error: Cannot convert non fix point to binary." << std::endl;
    double2fix();
  }

  if (bpp != 32 && bpp != 24 && bpp != 16 && bpp != 8 && bpp != 4 && bpp != 2 && bpp != 1) {
    std::cerr << "Error: BPP format not supported; Leaving buffer untouched." << std::endl;
    return 0;
  }

  size_t required_size = (size_t)(ceil(width*(double)bpp/8.0) * height);
  #ifdef DEBUG_BUFFER
  std::cout << "required size=" << required_size << " size=" << size << std::endl;
  #endif

  if (required_size > size) {
    std::cerr << "Error: Buffer too small." << std::endl;
    return 0;
  }

  unsigned int tsbuffer = 0;
  unsigned int tbuffer = 0;
  int tbuffer_bitcnt = 0;

  int toggle = 0;
  unsigned int buffer_index = 0;

  for (unsigned int i=0; i < p.size(); ++i) {
    #ifdef DEBUG_BUFFER
    std::cout << "i=" << std::dec << i << std::endl;
    #endif
    if (bpp > 8) {
      tbuffer = 0; // flush buffer for 8 bit aligned mode
    } 

    tsbuffer = 0;
    if (bpp > 1) {
      tsbuffer |= (unsigned int)p.at(i).x << bpp/2;
      tsbuffer |= (unsigned int)p.at(i).y;
        #ifdef DEBUG_BUFFER
        std::cout << "x=0x" << std::hex << (unsigned int)p.at(i).x ;
        std::cout << " y=0x" << std::hex <<(unsigned int)p.at(i).y ;
        std::cout << " b=0x" << std::hex <<(unsigned int)tsbuffer << std::endl;
        #endif
    } else {
      // alternate in horizontal direction
      if ((i % width) == 0) {
        #ifdef DEBUG_BUFFER
        std::cout << "linenumber=" << (i/width) << std::endl;
        #endif
        toggle = (i/width) & 1;
      }
      if (toggle) {
        #ifdef DEBUG_BUFFER
        std::cout << "y" << std::endl;
        #endif
        tsbuffer |= (unsigned int)p.at(i).y;
      } else {
        #ifdef DEBUG_BUFFER
        std::cout << "x" << std::endl;
        #endif
        tsbuffer |= (unsigned int)p.at(i).x;
      }
      toggle = !toggle;
    }
    tsbuffer <<= tbuffer_bitcnt;
    tbuffer |= tsbuffer;
    tbuffer_bitcnt += bpp;

    #ifdef DEBUG_BUFFER
    std::cout << " b=0x" << std::hex <<(unsigned int)tsbuffer ;
    std::cout << " tb=0x" << std::hex <<(unsigned int)tbuffer << std::endl;
    #endif


    // consume data in buffer 
    while (tbuffer_bitcnt > 7) {
      #ifdef DEBUG_BUFFER
      std::cout << "tbuffer_bitcnt=" << std::dec << tbuffer_bitcnt << std::endl;
      #endif
  
      ((char*)buffer)[buffer_index] = (char)(tbuffer & 0xff);
      #ifdef DEBUG_BUFFER
      std::cout << "write=" << std::hex << (unsigned int)(tbuffer & 0xff)<< std::endl;
      #endif
      tbuffer_bitcnt -= 8;
      tbuffer >>= 8;
  
      if (buffer_index < size) {
        buffer_index++;
      } else {
        std::cerr << "Error: Invalid access, index exceeds buffer size." << std::endl;
        return 0; 
      }
    }
    // handle non 8 bit aligned end of line, to work with byte aligned stride sizes
    if ((tbuffer_bitcnt > 0) && ((i+1) % width) == 0) {
      #ifdef DEBUG_BUFFER
      std::cout << std::endl << "--> i=" << std::dec << i << " width=" << std::dec << width << " at the end of " << std::endl;
      #endif

      #ifdef DEBUG_BUFFER
      std::cout << "tbuffer_bitcnt=" << std::dec << tbuffer_bitcnt << std::endl;
      #endif

      ((char*)buffer)[buffer_index] = (char)(tbuffer & 0xff);
      #ifdef DEBUG_BUFFER
      std::cout << "write=" << std::hex << (unsigned int)(tbuffer & 0xff)<< std::endl;
      #endif
      tbuffer_bitcnt = 0; 
      tbuffer = 0;
  
      if (buffer_index < size) {
        buffer_index++;
      } else {
        std::cerr << "Error: Invalid access, index exceeds buffer size." << std::endl;
        return 0; 
      }
    }
 


  }
  // handle non 8 bit aligned end of list
  if (tbuffer_bitcnt > 0) {
    #ifdef DEBUG_BUFFER
    std::cout << "Need to handle end of list, tbuffer_bitcnt=" << tbuffer_bitcnt << std::endl;
    #endif

    ((char*)buffer)[buffer_index] = (char)(tbuffer & 0xff);

    #ifdef DEBUG_BUFFER
    std::cout << "buffer_index=" << std::dec << buffer_index << " tbuffer_bitcnt=" << std::dec << tbuffer_bitcnt << " value =" << "0x" << std::hex << (tbuffer & 0xff) << std::endl;
    #endif
  }

  return 1;
}

//#define DEBUG_CREATEBUF
size_t CoordinateLayer::create_buf() {
  if (buf) { // 2nd call might have different settings/size
    destroy_buf(); // so delete old buffer first
  }
  buf_i = 0;
  buf_size = (size_t)(ceil((double)(width*bpp/8.0 ))*height);

  #ifdef DEBUG_CREATEBUF
  std::cout << "buf_size=" << buf_size << std::endl;
  #endif
  buf = new char [buf_size];
  if (buf == NULL){
    std::cerr << "Error: Cannot allocate buffer, new failed." << std::endl;
    return 0;
  }
  if (!get_buffer(buf, buf_size)) {
    buf_size = 0;
  }
  return buf_size;
}

size_t CoordinateLayer::get_buf_size() {
  if (!buf) { 
    create_buf();
  }
  return this->buf_size;
}

int CoordinateLayer::get_buf_end() {
  if (!buf) { 
    create_buf();
  }
  if (!(buf_i < buf_size)) // at the end
    destroy_buf();

  return (buf_i < buf_size ? 0 : 1);
}

char CoordinateLayer::get_buf_char() {
  char ret = '\0';
  if (!buf) {  // rare case 
    create_buf();
  }

  if (buf_i < buf_size) {
    ret = *(buf + buf_i);
    ++buf_i;
  }
  return ret;
}

void CoordinateLayer::destroy_buf() {
  if (buf) {
    delete [] buf;
    buf = NULL;
  }
  buf_size = 0;
  buf_i = 0;

  return;
}

void CoordinateLayer::remove_whitespace(std::string& buffer) {
  for (unsigned int i=0; i < buffer.size(); ++i) {
    if (buffer.at(i) == ' ' || buffer.at(i) == '\t' || buffer.at(i) == '\n' || buffer.at(i) == '\r' ) {
      buffer = buffer.erase(i, i+1);
      --i;
    }
  }
}

void CoordinateLayer::set_xy(unsigned int pos_x, unsigned int pos_y, double x, double y) {
  p.at(pos_x+pos_y*this->width).x = x;
  p.at(pos_x+pos_y*this->width).y = y;
}

unsigned int CoordinateLayer::get_width() {
  return this->width;
}

unsigned int CoordinateLayer::get_height() {
  return this->height;
}

void CoordinateLayer::defineSourceDimension(unsigned int srcWidth, unsigned int srcHeight) {
  this->m_sourceWidth = srcWidth;
  this->m_sourceHeight = srcHeight;
}

void CoordinateLayer::set_width(unsigned int width) {
  this->width = width;
  p.resize(this->width * this->height);
}

void CoordinateLayer::set_height(unsigned int height) {
  this->height = height;
  p.resize(this->width * this->height);
}


void CoordinateLayer::print() {
  for (unsigned int i=0; i < p.size(); ++i) {
   //std::cout << p.at(i).x << ", " << p.at(i).y << std::endl;
   std::cout << p.at(i) << std::endl;
  }
}

void CoordinateLayer::print_config() {
  std::cout << "WarpCoordinateMode=" << mode << std::endl;
  std::cout << "WarpBitsPerPixel=" << bpp << std::endl;
  std::cout << "WarpSymmetricOffset=" << offset << std::endl;
  std::cout << "frameoffset.x=" << frameoffset.x << std::endl;
  std::cout << "frameoffset.y=" << frameoffset.y << std::endl;
  std::cout << "deltax.x=" << deltax.x << std::endl;
  std::cout << "deltax.y=" << deltax.y << std::endl;
  std::cout << "deltay.x=" << deltay.x << std::endl;
  std::cout << "deltay.y=" << deltay.y << std::endl;

}
void CoordinateLayer::print_config_reg() {
  std::cout << std::showbase << std::internal << std::setfill('0');
  std::cout << "# Register aligned for FetchWarp (gen. 2012):" << std::endl; 
  std::cout << "# WarpControl:"<< std::endl << std::hex << std::setw(10) <<(int)(((unsigned)offset<<12)  | ((unsigned)mode<<8) | (unsigned)bpp) << std::endl; 
  std::cout << "# FrameXOffset:" << std::endl << std::hex << std::setw(10) << (int)(frameoffset.x*(1<<16)) << std::endl;
  std::cout << "# FrameYOffset:" << std::endl << std::hex << std::setw(10) << (int)(frameoffset.y*(1<<16)) << std::endl;
  std::cout << "# DeltaXX:" << std::endl << std::hex << std::setw(10) << (int)(deltax.x*(1<<18))  << std::endl;
  std::cout << "# DeltaXY:" << std::endl << std::hex << std::setw(10) << (int)(deltax.y*(1<<18))  << std::endl;
  std::cout << "# DeltaYX:" << std::endl << std::hex << std::setw(10) << (int)(deltay.x*(1<<18))  << std::endl;
  std::cout << "# DeltaYY:" << std::endl <<std::hex << std::setw(10) << (int)(deltay.y*(1<<18))  << std::endl;

}

void CoordinateLayer::set_WarpBitsPerPixel(unsigned int bpp)
{
  this->bpp = bpp;

  switch(bpp) {
    case 24:
      fixpoint_int =  8;
      fixpoint_dec =  4;
    break;
    case 16:
      fixpoint_int =  4;
      fixpoint_dec =  4;
    break;
    case 8:
      fixpoint_int =  0;
      fixpoint_dec =  4;
    break;
    case 4:
      fixpoint_int = -2;
      fixpoint_dec =  4;
    break;
    case 2:
      fixpoint_int = -3;
      fixpoint_dec =  4;
      offset = 1;
    break;
    case 1:
      fixpoint_int = -3;
      fixpoint_dec =  4;
      offset = 1;
    break;
    case 32:
    default:
      fixpoint_int = 12;
      fixpoint_dec =  4;
    break;
  }
}

unsigned int CoordinateLayer::get_WarpBitsPerPixel()
{
  return this->bpp;
}

void CoordinateLayer::set_WarpCoordinateMode(int mode)
{
  this->target_mode = (warpcoordinatemode_t) mode;
  //std::cout << "new target_mode=" << target_mode << std::endl;
}

unsigned int CoordinateLayer::get_WarpCoordinateMode()
{
  return this->mode;
}

int CoordinateLayer::get_WarpSymmetricOffset()
{
  return this->offset;
}

void CoordinateLayer::set_WarpSymmetricOffset(int offset)
{
  this->offset = offset;
}

void CoordinateLayer::set_max_grid_error(double error)
{
  this->max_grid_error = error;
}

double CoordinateLayer::get_max_grid_error()
{
  return this->max_grid_error;
}

double CoordinateLayer::get_mse()
{
  return this->mse;
}

int CoordinateLayer::get_error_cnt()
{
  return this->error_cnt;
}

unsigned int CoordinateLayer::get_packed(unsigned int pos)
{
  if (notation != FIX)
    double2fix();

  if (bpp > 1) {
    return (unsigned int) ( ((unsigned int)(p.at(pos).x) << (bpp/2)) |\
                             (unsigned int)(p.at(pos).y) );
  } else {
    int x=0;
    int y =0;
    y = (int) pos / width;
    x = pos - width*y;
    //std::cout << "pos=" << pos << " x=" << x << " y=" << y << std::endl;
    if ((x & 1) ^ (y & 1))
      return (unsigned int)(p.at(pos).y);
    else
      return (unsigned int)(p.at(pos).x);
  }
}

unsigned int CoordinateLayer::get_x(unsigned int pos)
{
  if (notation != FIX)
    double2fix();
  return (unsigned int) (p.at(pos).x);
}

unsigned int CoordinateLayer::get_y(unsigned int pos)
{
  if (notation != FIX)
    double2fix();
  return (unsigned int) (p.at(pos).y);
}


double CoordinateLayer::get_x(unsigned int x, unsigned int y)
{
    unsigned int pos = x+width*y;

    if (pos < p.size()) {
        return (p.at(pos).x);
    } else {
        std::cerr << "Position " << pos << " bigger than vector size " << p.size() << std::endl;
        return -1.0;
    }
}

double CoordinateLayer::get_y(unsigned int x, unsigned int y)
{
    unsigned int pos = x+width*y;

    if (pos < p.size()) {
        return (p.at(pos).y);
    } else {
        std::cerr << "Position " << pos << " bigger than vector size " << p.size() << std::endl;
        return -1.0;
    }
}

int CoordinateLayer::frameoffset_double2fix() {
  frameoffset_isfixedpoint = 1;
  return frameoffset.double2fix(frameoffset_fixedpoint_int, frameoffset_fixedpoint_dec);
}

int CoordinateLayer::frameoffset_double2fix(int num_int, int num_dec) {
  frameoffset_isfixedpoint = 1;
  frameoffset_fixedpoint_int = num_int;
  frameoffset_fixedpoint_dec = num_dec; // store for proper integer and decimal place extraction
  return frameoffset.double2fix(num_int, num_dec);
}

unsigned int CoordinateLayer::get_framexoffset() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix();

  return (unsigned int)frameoffset.x >> frameoffset_fixedpoint_dec;
}

unsigned int CoordinateLayer::get_framexoffsetdecimalplaces() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix();

  return (unsigned int) frameoffset.x & (unsigned int)(pow((double)2, frameoffset_fixedpoint_dec)-1);
}

unsigned int CoordinateLayer::get_frameyoffset() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix();

  return (unsigned int)frameoffset.y >> frameoffset_fixedpoint_dec;
}

unsigned int CoordinateLayer::get_frameyoffsetdecimalplaces() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix();

  return (unsigned int) frameoffset.y & (unsigned int)(pow((double)2, frameoffset_fixedpoint_dec)-1);
}
double CoordinateLayer::get_stat_cnt_inside() {
  return stat_cnt_inside;
}
double CoordinateLayer::get_stat_avg_dy() {
  return stat_avg_dy;
}
unsigned int CoordinateLayer::getreg_framexoffset() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix();

  return (unsigned int)frameoffset.x << 11;
}

unsigned int CoordinateLayer::getreg_frameyoffset() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix();

  return (unsigned int)frameoffset.y << 11;
}
unsigned int CoordinateLayer::get_arbdeltayy() {
  if (!delta_isfixedpoint)
    delta_double2fix(3,5);

  return (unsigned int)deltay.y;
}
unsigned int CoordinateLayer::get_arbdeltayx() {
  if (!delta_isfixedpoint)
    delta_double2fix(3,5);

  return (unsigned int)deltay.x;
}
unsigned int CoordinateLayer::get_arbdeltaxy() {
  if (!delta_isfixedpoint)
    delta_double2fix(3,5);

  return (unsigned int)deltax.y;
}
unsigned int CoordinateLayer::get_arbdeltaxx() {
  if (!delta_isfixedpoint)
    delta_double2fix(3,5);

  return (unsigned int)deltax.x;
}
unsigned int CoordinateLayer::get_arbstartx() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix(16,5);

  return (unsigned int)frameoffset.x;
}
unsigned int CoordinateLayer::get_arbstarty() {
  if (!frameoffset_isfixedpoint)
    frameoffset_double2fix(16,5);

  return (unsigned int)frameoffset.y;
}
unsigned int CoordinateLayer::getreg_arbstartx() {
  return (unsigned int)get_arbstartx();
}
unsigned int CoordinateLayer::getreg_arbstarty() {
  return (unsigned int)get_arbstarty();
}
int CoordinateLayer::delta_double2fix() {
  delta_isfixedpoint = 1;
  int error = 0;
  error = deltax.double2fix(delta_fixedpoint_int, delta_fixedpoint_dec);
  error += deltay.double2fix(delta_fixedpoint_int, delta_fixedpoint_dec);
  return (error > 0 ? 1 : 0);
}

int CoordinateLayer::delta_double2fix(int num_int, int num_dec) {
  delta_isfixedpoint = 1;
  this->delta_fixedpoint_int = num_int;
  this->delta_fixedpoint_dec = num_dec;
  return delta_double2fix();
}

unsigned int CoordinateLayer::get_deltaxx() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltax.x;
}

unsigned int CoordinateLayer::get_deltaxy() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltax.y;
}

unsigned int CoordinateLayer::get_deltayx() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltay.x;
}

unsigned int CoordinateLayer::get_deltayy() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltay.y;
}

unsigned int CoordinateLayer::getreg_deltaxx() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltax.x << 13;
}

unsigned int CoordinateLayer::getreg_deltaxy() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltax.y << 13;
}

unsigned int CoordinateLayer::getreg_deltayx() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltay.x << 13;
}

unsigned int CoordinateLayer::getreg_deltayy() {
  if (!delta_isfixedpoint)
    delta_double2fix();

  return (unsigned int)deltay.y << 13;
}

unsigned int CoordinateLayer::getreg_warpcontrol() {
  return (unsigned int)(((offset & 1) << 12) | ((mode & 3) << 8) | (bpp & 63));
}

int CoordinateLayer::double2fix() {
  if (offset)
    return double2fix(this->fixpoint_int, this->fixpoint_dec, offset);
  else
    return double2fix(this->fixpoint_int, this->fixpoint_dec);
}

int CoordinateLayer::double2fix(int num_int, int num_dec) {
  this->fixpoint_int = num_int;
  this->fixpoint_dec = num_dec;
  this->notation = FIX;
  int error = 0;
  for (unsigned int i=0; i < p.size(); ++i) {
    error += p.at(i).double2fix(fixpoint_int, fixpoint_dec);
  }
  return (error > 0 ? 1 : 0);
}

int CoordinateLayer::double2fix(int num_int, int num_dec, int offset) {
  this->fixpoint_int = num_int;
  this->fixpoint_dec = num_dec;
  this->notation = FIX;
  int error = 0;
  for (unsigned int i=0; i < p.size(); ++i) {
    error += p.at(i).double2fix(fixpoint_int, fixpoint_dec, offset);
  }
  return (error > 0 ? 1 : 0);
}

void CoordinateLayer::fix2double() {
  this->notation = FLOAT;
  //std::cout << "fixpoint_int=" << fixpoint_int << "  fixpoint_dec=" << fixpoint_dec << std::endl;
  for (unsigned int i=0; i < p.size(); ++i) {
    p.at(i).fix2double(fixpoint_int, fixpoint_dec);
  }
}

//#define DEBUG_OPTIMIZE
unsigned int CoordinateLayer::optimize_auto(int error_cnt) {
  unsigned int bpp[] = {1, 2, 4, 8, 16, 32};
  // convert object to grid
  if (!ongrid) {
    to_grid();
  }

  // allocate memory and copy object itself
  CoordinateLayer *ref;
  ref = new CoordinateLayer;
  if ( ref == NULL) {
    std::cerr << "Error: Cannot allocate memory." << std::endl;
    return 0;
  }
  *ref = *this; // store reference

  for (unsigned int i = 3; i > 0; --i) {
    //for (unsigned int bpp_i = 0; bpp_i < 7; ++bpp_i) {
    for (unsigned int bpp_i = 0; bpp_i < 6; ++bpp_i) {
      *this = *ref; // restore original coordinate layer

      set_WarpBitsPerPixel(bpp[bpp_i]);

      switch (i) {
       case 3:
         std::cout << "Trying 'delta increment' mode with bpp=" << std::setw(2) << bpp[bpp_i] <<std::endl;
         calculate_dd();
         break;
       case 2:
         std::cout << "Trying 'delta' mode with bpp=" << std::setw(2) << bpp[bpp_i] <<std::endl;
         calculate_d();
         break;
       default:
         std::cout << "Trying 'sample point' mode with bpp=" << std::setw(2) << bpp[bpp_i] <<std::endl;
         ;
      }
      #ifdef DEBUG_OPTIMIZE
      std::cout << "error_cnt=" << this->error_cnt << std::endl;
      #endif
      // found valid configuration: break inner loop
      if (this->error_cnt <= error_cnt) {
        break;
      }
    }
    // found valid configuration: break outer loop
    if (this->error_cnt <= error_cnt) {
      break;
    }
  }

  delete ref;
  return this->bpp;
}

unsigned int CoordinateLayer::optimize_manual(int error_cnt) {
  if (!ongrid) {
    to_grid();
  }

  switch (target_mode) {
    case DD_PNT: // delta_increment
      calculate_dd();
      break;
    case D_PNT: // delta mode
      calculate_d();
      break;
    default: // sample point
      ;
  }
  #ifdef DEBUG_OPTIMIZE
  std::cout << "error_cnt=" << this->error_cnt << std::endl;
  #endif
  if (this->error_cnt <= error_cnt) {
    return 1;
  }
   
  return 0;
}

void CoordinateLayer::to_grid() {
  if (mode != PNT) {
    std::cerr << "Error: Cannot convert to grid, coordinates are not sample points." << std::endl;
    return;
  }
  int error; // dummy, mse is always <= 1/16
  unsigned int i=0;
  for (unsigned int j=0; j < this->height; ++j) {
    for (i=0; i < this->width; ++i) {
        p.at(i+j*this->width).x = Coordinate::trunc2fix(p.at(i+j*this->width).x, 15, 4, error, 0);
        p.at(i+j*this->width).y = Coordinate::trunc2fix(p.at(i+j*this->width).y, 15, 4, error, 0);
    }
  }
  this->ongrid = 1;
}

//#define DEBUG_CONV_D
void CoordinateLayer::calculate_d() {
  if (mode != PNT) {
    std::cerr << "Error: Cannot convert to delta mode, coordinates are not sample points." << std::endl;
    return;
  }
  if (!ongrid) {
    to_grid();
  }
  notation = FLOAT;
  mode = D_PNT;

  frameoffset.x = p.at(0).x;
  frameoffset.y = p.at(0).y;
  int error;
  this->mse = 0;
  this->error_cnt = 0;
  double lx, ly;     // last x,y
  double x, y;       // current position on grid
  double x_e, y_e;   // error of current position
  double lrx, lry;   // last rows x,y
  double dx, dy;     // current values to be stored
  double dx_f, dy_f; // fixed point truncated values to be stored
  #ifdef DEBUG_CONV_D
  double dx_e, dy_e; // error of values to be stored
  #endif

  #ifdef DEBUG_CONV_D
  std::cout << "width=" << width << " height=" << height << std::endl;
  std::cout << "frameoffset=" << frameoffset.x << ", " << frameoffset.y << std::endl;
  #endif

  unsigned int i=0;
  for (unsigned int j=0; j < this->height; ++j) {
    for (i=0; i < this->width; ++i) {
      #ifdef DEBUG_CONV_D
      std::cout << "j=" << j << " i=" << i << std::endl;
      #endif
      x = p.at(i+j*this->width).x ;
      y = p.at(i+j*this->width).y ;

      if (i == 0 && j == 0) {
        dx = x - frameoffset.x;
        dy = y - frameoffset.y;
        lrx = x;
        lry = y;

        dx_f = Coordinate::trunc2fix(dx, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        dy_f = Coordinate::trunc2fix(dy, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        #ifdef DEBUG_CONV_D
        dx_e = dx - dx_f;
        dy_e = dy - dy_f;
        std::cout << "dx=" << dx << ", dx_f=" << dx_f << " dx_e=" << dx_e << std::endl;
        std::cout << "dy=" << dy << ", dy_f=" << dy_f << " dy_e=" << dy_e << std::endl;
        #endif

        lrx = lrx + dx_f;
        lry = lry + dy_f;
        lx = lrx;
        ly = lry;
        // error
        x_e = lx - x;
        y_e = ly - y;
        #ifdef DEBUG_CONV_D
        std::cout << ">>> x_e=" << x_e << " y_e=" << y_e << std::endl;
        #endif
      } else if (i == 0 && j > 0) {
        dx = x - lrx;
        dy = y - lry;

        dx_f = Coordinate::trunc2fix(dx, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        dy_f = Coordinate::trunc2fix(dy, this->fixpoint_int, this->fixpoint_dec, error, this->offset);

        #ifdef DEBUG_CONV_D
        dx_e = dx - dx_f;
        dy_e = dy - dy_f;
        std::cout << "dx=" << dx << ", dx_f=" << dx_f << " dx_e=" << dx_e << std::endl;
        std::cout << "dy=" << dy << ", dy_f=" << dy_f << " dy_e=" << dy_e << std::endl;
        #endif

        lrx = lrx + dx_f;
        lry = lry + dy_f;
        lx = lrx;
        ly = lry;
        // error
        x_e = lx - x;
        y_e = ly - y;
        #ifdef DEBUG_CONV_D
        std::cout << ">>> x_e=" << x_e << " y_e=" << y_e << std::endl;
        #endif
      } else {
        dx = x - lx;
        dy = y - ly;

        dx_f = Coordinate::trunc2fix(dx, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        dy_f = Coordinate::trunc2fix(dy, this->fixpoint_int, this->fixpoint_dec, error, this->offset);

        #ifdef DEBUG_CONV_D
        dx_e = dx - dx_f;
        dy_e = dy - dy_f;
        std::cout << "dx=" << dx << ", dx_f=" << dx_f << " dx_e=" << dx_e << std::endl;
        std::cout << "dy=" << dy << ", dy_f=" << dy_f << " dy_e=" << dy_e << std::endl;
        #endif

        lx = lx + dx_f;
        ly = ly + dy_f;

        // error
        x_e = lx - x;
        y_e = ly - y;
        #ifdef DEBUG_CONV_D
        std::cout << ">>> x_e=" << x_e << " y_e=" << y_e << std::endl;
        #endif
      }
      // sum up error terms
      this->mse += pow(x_e, 2.0) + pow(y_e, 2.0); // ||v||^2 = (sqrt(x^2+y^2))^2

      if (fabs(x_e) > this->max_grid_error) {
        this->error_cnt += 1;
        #ifdef DEBUG_CONV_D
        std::cout << "x_e=" << x_e << std::endl;
        #endif
      }
      if (fabs(y_e) > this->max_grid_error) {
        this->error_cnt += 1;
        #ifdef DEBUG_CONV_D
        std::cout << "y_e=" << y_e << std::endl;
        #endif
      }

      p.at(i+j*this->width).x = dx_f;
      p.at(i+j*this->width).y = dy_f;
    }  // i
  } // j

  this->mse /= this->p.size();
  //std::cout << "error_cnt=" << error_cnt << std::endl;
}

//#define DEBUG_CONV_DD
//#define DEBUG_CONV_DD_FIX
void CoordinateLayer::calculate_dd() {
  if (mode != PNT) {
    std::cerr << "Error: Cannot convert to delta increment mode, coordinates are not sample points." << std::endl;
    return;
  }
  if (!ongrid) {
    to_grid();
  }
  notation = FLOAT;
  mode = DD_PNT;
  this->mse = 0;
  this->error_cnt = 0;

  // Sample points on grid -> presets on grid
  if (this->height > 1) { // catch frames with height one
    // v4, vertical delta
    deltay.x = p.at(0+this->width).x - p.at(0).x;
    deltay.y = p.at(0+this->width).y - p.at(0).y;
  }

  frameoffset.x = p.at(0).x - deltay.x;
  frameoffset.y = p.at(0).y - deltay.y;
  deltay.x -= (get_WarpBitsPerPixel() == 1 ? 0.03125 : 0); // optional sub pixel error compensation to start with error 0
  if (this->width > 1) { // catch frames with width one
    // v1, horizontal delta
    deltax.x = p.at(1).x - p.at(0).x;
    deltax.y = p.at(1).y - p.at(0).y;
  }
  //deltax.y += (get_WarpBitsPerPixel() == 1 ? 0.03125 : 0); // optional sub pixel error compensation to start with error 0
  double lx, ly;       // last x,y
  double ldx, ldy;     // last dx,dy

  double lvdx, lvdy;   // last vertical dx,dy for 1st column
  double lhdx, lhdy;   // last horizontal dx,dy for 2nd column
  double lrx, lry;     // last rows x,y

  double x, y;         // current position on grid
  double x_e, y_e;     // error of current position
  double ddx, ddy;     // current calculated delta increment
  double ddx_f, ddy_f; // current calculated delta increment truncated to fixed point
  #ifdef DEBUG_CONV_DD
  double ddx_e, ddy_e; // current delta increment error
  #endif

  int error;
  int alt_x = 0;

  int count_ddy = 0;
  int count_ldy = 0;
  int count_inside = 0;
  double total_dy = 0;
/*
  std::cout << "width=" << width << " height=" << height << std::endl;
  std::cout << "frameoffset=" << frameoffset.x << ", " << frameoffset.y << std::endl;

  std::cout << "v4 (vertical) 1st column: deltay.x=" << deltay.x << " deltay.y=" << deltay.y << std::endl;
  std::cout << "v1 (horizontal) 2nd column:   deltax.x=" << deltax.x << " deltax.y=" << deltax.y << std::endl;
*/
  unsigned int i = 0;

  for (unsigned int j=0; j < this->height; ++j) {
    for (i=0; i < this->width; i++) {

      x = p.at(i+j*this->width).x;
      y = p.at(i+j*this->width).y;

      #ifdef DEBUG_CONV_DD
      std::cout << std::endl;
      std::cout << "i=" << i << " x=" << x << " y=" << y << std::endl;
      #endif

      // first row presets
      if (j == 0) {
        if (i == 0) {
          lvdx = deltay.x;
          lvdy = deltay.y;

          #ifdef DEBUG_CONV_DD
          std::cout << "lvdx=" << lvdx << std::endl;
          std::cout << "lvdy=" << lvdy << std::endl;
          #endif

          lrx = frameoffset.x;
          lry = frameoffset.y;

        } else if (i == 1) {
          lhdx = deltax.x;
          lhdy = deltax.y;

          #ifdef DEBUG_CONV_DD
          std::cout << "lhdx=" << lhdx << std::endl;
          std::cout << "lhdy=" << lhdy << std::endl;
          #endif
        }
      }
      if (i == 0) {
        alt_x = !(j & 1);
        #ifdef DEBUG_CONV_DD
        std::cout << "alt_x=" << alt_x << std::endl;
        #endif
        #ifdef DEBUG_CONV_DD
        std::cout << std::endl;
        std::cout << "lrx=" << lrx << " lvdx=" << lvdx << " ideal x=" << x << std::endl;
        std::cout << "lry=" << lry << " lvdy=" << lvdy << " ideal y=" << y << std::endl;
        #endif
        ddx = x - lrx - lvdx;
        ddy = y - lry - lvdy;

        if (get_WarpBitsPerPixel() == 1 && !alt_x)
          ddx_f = 0;
        else 
          ddx_f = Coordinate::trunc2fix(ddx, this->fixpoint_int, this->fixpoint_dec, error, this->offset);

        if (get_WarpBitsPerPixel() == 1 && alt_x)
          ddy_f = 0;
        else 
          ddy_f = Coordinate::trunc2fix(ddy, this->fixpoint_int, this->fixpoint_dec, error, this->offset);

        #ifdef DEBUG_CONV_DD
        ddx_e = ddx - ddx_f;
        ddy_e = ddy - ddy_f;
        std::cout << "ddx=" << ddx << ", ddx_f=" << ddx_f << " ddx_e=" << ddx_e << std::endl;
        std::cout << "ddy=" << ddy << ", ddy_f=" << ddy_f << " ddy_e=" << ddy_e << std::endl;
        #endif
        // new vertical delta for 1st column of next row
        lvdx = lvdx + ddx_f;
        lvdy = lvdy + ddy_f;

        #ifdef DEBUG_CONV_DD
        std::cout << "lvdx=" << lvdx << std::endl;
        std::cout << "lvdy=" << lvdy << std::endl;
        #endif

        // initalize
        ldx = 0;
        ldy = 0;

        #ifdef DEBUG_CONV_DD
        std::cout << ">>> lvdx=" << lvdx << " lvdy=" << lvdy << " <<<" << std::endl;
        #endif
        #ifdef DEBUG_CONV_DD_FIX
        std::cout << "f> lvdx=" << hex << (Coordinate::double2fix(lvdx,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        std::cout << "f> lvdy=" << hex << (Coordinate::double2fix(lvdy,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        #endif

        // current position becomes new position in next cycle
        lrx = lrx + lvdx;
        lry = lry + lvdy;
        #ifdef DEBUG_CONV_DD
        std::cout << ">>> lrx=" << lrx << " lry=" << lry << " <<<" << std::endl;
        std::cout << ">>>  x=" << x << "  y=" << y << " <<<" << std::endl;
        #endif

        lx = lrx;
        ly = lry;
        // error
        x_e = lx - x;
        y_e = ly - y;

        #ifdef DEBUG_CONV_DD
        std::cout << ">>> x_e=" << x_e << " y_e=" << y_e << std::endl;
        #endif
        #ifdef DEBUG_CONV_DD_FIX
        std::cout << "f> lx=" << hex << (Coordinate::double2fix(lx,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        std::cout << "f> ly=" << hex << (Coordinate::double2fix(ly,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        #endif
      } else if (i == 1) {
        #ifdef DEBUG_CONV_DD
        std::cout << std::endl;
        std::cout << "lx=" << lx << " lhdx=" << lhdx << " ideal x=" << x << std::endl;
        std::cout << "ly=" << ly << " lhdy=" << lhdy << " ideal y=" << y << std::endl;
        #endif
        ddx = x - lx - lhdx;
        ddy = y - ly - lhdy;
        #ifdef DEBUG_CONV_DD
        std::cout << "alt_x=" << alt_x << std::endl;
        #endif
        if (get_WarpBitsPerPixel() == 1 && !alt_x)
          ddx_f = 0;
        else 
          ddx_f = Coordinate::trunc2fix(ddx, this->fixpoint_int, this->fixpoint_dec, error, this->offset);

        if (get_WarpBitsPerPixel() == 1 && alt_x)
          ddy_f = 0;
        else 
          ddy_f = Coordinate::trunc2fix(ddy, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        #ifdef DEBUG_CONV_DD
        ddx_e = ddx - ddx_f;
        ddy_e = ddy - ddy_f;
        std::cout << "ddx=" << ddx << ", ddx_f=" << ddx_f << " ddx_e=" << ddx_e << std::endl;
        std::cout << "ddy=" << ddy << ", ddy_f=" << ddy_f << " ddy_e=" << ddy_e << std::endl;
        #endif
        ldx = lhdx + ddx_f;
        ldy = lhdy + ddy_f;

        // new horizontal delta for 2nd column of next row, include error for later compensation
        lhdx = lhdx + ddx_f;
        lhdy = lhdy + ddy_f;

        #ifdef DEBUG_CONV_DD
        std::cout << "lhdx=" << lhdx << std::endl;
        std::cout << "lhdy=" << lhdy << std::endl;
        #endif

        #ifdef DEBUG_CONV_DD
        std::cout << "ldx=" << ldx << std::endl;
        std::cout << "ldy=" << ldy << std::endl;
        std::cout << ">>> lhdx=" << lhdx << " lhdy=" << lhdy << " <<<" << std::endl;
        #endif
        #ifdef DEBUG_CONV_DD_FIX
        std::cout << "f> lhdx=" << hex << (Coordinate::double2fix(lhdx,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        std::cout << "f> lhdy=" << hex << (Coordinate::double2fix(lhdy,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        #endif
        // current position becomes new position in next cycle
        lx = lx + lhdx;
        ly = ly + lhdy;
        #ifdef DEBUG_CONV_DD
        std::cout << ">>> lx=" << lx << " ly=" << ly << " <<<" << std::endl;
        std::cout << ">>>  x=" << x << "  y=" << y << " <<<" << std::endl;
        #endif

        // error
        x_e = lx - x;
        y_e = ly - y;
        #ifdef DEBUG_CONV_DD
        std::cout << ">>> x_e=" << x_e << " y_e=" << y_e << std::endl;
        #endif
        #ifdef DEBUG_CONV_DD_FIX
        std::cout << "f> lx=" << hex << (Coordinate::double2fix(lx,this->fixpoint_int, this->fixpoint_dec+1, error) ) << std::endl;
        std::cout << "f> ly=" << hex << (Coordinate::double2fix(ly,this->fixpoint_int, this->fixpoint_dec+1, error) ) << std::endl;
        #endif
      } else {
        #ifdef DEBUG_CONV_DD
        std::cout << std::endl;
        std::cout << "lx=" << lx << " ldx=" << ldx << "  ideal x=" << x << std::endl;
        std::cout << "ly=" << ly << " ldy=" << ldy << "  ideal y=" << y << std::endl;
        #endif

        ddx = (x - (lx + ldx ));
        ddy = (y - (ly + ldy ));
        #ifdef DEBUG_CONV_DD
        std::cout << "alt_x=" << alt_x << std::endl;
        #endif
        if (get_WarpBitsPerPixel() == 1 && !alt_x)
          ddx_f = 0;
        else 
          ddx_f = Coordinate::trunc2fix(ddx, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        if (get_WarpBitsPerPixel() == 1 && alt_x)
          ddy_f = 0;
        else 
          ddy_f = Coordinate::trunc2fix(ddy, this->fixpoint_int, this->fixpoint_dec, error, this->offset);
        #ifdef DEBUG_CONV_DD
        ddx_e = ddx - ddx_f;
        ddy_e = ddy - ddy_f;
        std::cout << "new fixed point: " << std::endl;
        std::cout << "ddx_f=" << ddx_f << " (ddx=" << ddx << " ddx_e=" << ddx_e << ")" << std::endl;
        std::cout << "ddy_f=" << ddy_f << " (ddy=" << ddy << " ddy_e=" << ddy_e << ")" << std::endl;
        #endif

        // current delta becomes new delta in next cycle
        ldx = ldx + ddx_f;
        ldy = ldy + ddy_f;
        #ifdef DEBUG_CONV_DD
        std::cout << ">>> ldx=" << ldx << " ldy=" << ldy << " <<<" << std::endl;
        #endif

        // current position becomes new position in next cycle
        lx = lx + ldx;
        ly = ly + ldy;
        #ifdef DEBUG_CONV_DD
        std::cout << ">>> lx=" << lx << " ly=" << ly << " <<<" << std::endl;
        std::cout << ">>>  x=" << x << "  y=" << y << " <<<" << std::endl;
        #endif

        // error
        x_e = lx - x;
        y_e = ly - y;
        #ifdef DEBUG_CONV_DD
        std::cout << ">>> x_e=" << x_e << " y_e=" << y_e << std::endl;
        #endif
        #ifdef DEBUG_CONV_DD_FIX
        std::cout << "f> lx=" << hex << (Coordinate::double2fix(lx,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        std::cout << "f> ly=" << hex << (Coordinate::double2fix(ly,this->fixpoint_int, this->fixpoint_dec+1, error)) << std::endl;
        #endif
      }
      #ifdef DEBUG_CONV_DD
      std::cout << "-> x_e=" << x_e << std::endl;
      std::cout << "-> y_e=" << y_e << std::endl;
      #endif
      // sum up error terms
      this->mse += pow(x_e, 2.0) + pow(y_e, 2.0); // ||v||^2 = (sqrt(x^2+y^2))^2


      if (fabs(x_e) > this->max_grid_error) {
        this->error_cnt += 1;
        #ifdef DEBUG_CONV_DD
        std::cout << "----> x_e=" << x_e << std::endl;
        //std::cout << " (" << i << " " << j << ")" <<std::endl;
        #endif
      }
      if (fabs(y_e) > this->max_grid_error) {
        this->error_cnt += 1;
        #ifdef DEBUG_CONV_DD
        std::cout << "----> y_e=" << y_e << std::endl;
        //std::cout << " (" << i << " " << j << ")" <<std::endl;
        #endif
      }

      // alternating x y for 1bpp mode
      alt_x = !alt_x;

      p.at(i+j*this->width).x = ddx_f;
      p.at(i+j*this->width).y = ddy_f;

      // 
      if (fabs(ddy_f) > 0.03125)
        count_ddy += 1;
      if (fabs(ldy) > 0.03125)
        count_ldy += 1;

      total_dy += fabs(ldy);

      if ( lx >= 0 && lx < this->width && ly >= 0 && ly < this->height )
        count_inside +=1;

    }  // i
  } // j

  this->mse /= this->p.size();
  //std::cout << "error_cnt=" << error_cnt << std::endl;
  //std::cout << "count_ddy=" << count_ddy << " "  << (double)count_ddy/(double)(this->height * this->width) << std::endl;
  //std::cout << "count_ldy=" << count_ldy << " "  << (double)count_ldy/(double)(this->height * this->width) << std::endl;
  std::cout << "count_inside=" << count_inside << " "  << (double)count_inside/(double)(this->height * this->width) << std::endl;

  std::cout << "total_dy=" << total_dy << " "  << (double)total_dy/(double)(this->height * this->width) << std::endl;

  stat_cnt_inside = (double)count_inside/(double)(this->height * this->width);
  stat_avg_dy = (double)total_dy /(double)(this->height * this->width);
}
//-----------------------------------------------------------------------------
// Create coordinate layers

void CoordinateLayer::barrel(unsigned int width, unsigned int height, double xc, double yc, double k0, double k1, double k2, double p0, double p1, double p2)
 {
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;
  double minside = ( width > height ? height : width );
  //std::cout << "minside=" << minside << std::endl;

  // Normalize center offset according to ptlens
  double xcc = (xc - this->width * 0.5);
  double ycc = (yc - this->height * 0.5);
  
  if ((width == 0) || (height == 0))
  {
    return;
  }
  
  xcc = xcc * 2.0 / minside;
  ycc = ycc * 2.0 / minside;
  //std::cout << "Normalized center xcc=" << xcc << " ycc=" << ycc << std::endl;

   if (m_sourceWidth != 0 && m_sourceHeight != 0)
   {
        scaleWidth = (double) m_sourceWidth / (double) width;
        scaleHeight = (double) m_sourceHeight / (double) height;
   }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
        
      // normalized destination pixel coordinates
      double xd = (x - this->width * 0.5);
      double yd = (y - this->height * 0.5);
      xd = xd * 2.0 / minside;
      yd = yd * 2.0 / minside;

      // lens distortion model according to Brown and Conrady
      double xu = xd;
      double yu = yd;

      double r2 = pow(xd - xcc, 2.0) + pow(yd - ycc, 2.0);

      // radial component
      double r4 = pow(r2, 2.0);
      double c = (k0 + k1 * r2 + k2 * r4);

      xu += (xd - xcc) * c;
      yu += (yd - ycc) * c;

      // tangential component
      xu += (p0*(r2 + 2.0*pow(xd-xcc,2.0)) + 2.0*p1*(xd-xcc)*(yd-ycc)) * (1.0 + p2*r2);
      yu += (2.0*p0*(xd-xcc)*(yd-ycc) + p1*(r2 + 2.0*pow(yd-ycc,2.0))) * (1.0 + p2*r2);

      // Denormalize resulting coordinates
      xu = xu * minside * 0.5;
      yu = yu * minside * 0.5;
      xu = (xu + this->width * 0.5);
      yu = (yu + this->height * 0.5);

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}


void CoordinateLayer::neutral(unsigned int width, unsigned int height)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;
  double xu;
  double yu;
  if ((width == 0) || (height == 0))
  {
    return;
  }
  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
        // don't normalized to stay on integer grid       

        if (m_sourceWidth != 0 && m_sourceHeight != 0) {
            xu = x * scaleWidth; 
            yu = y * scaleHeight; 
        } else {
            xu = x;
            yu = y;
        }
        p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

//#define DEBUG_RANDOMGRID
void CoordinateLayer::random_grid(unsigned int width, unsigned int height, unsigned int seed, double xjitter, double yjitter)
{
  double scaleWidth;
  double scaleHeight;
  this->width = width;
  this->height = height;
  srand(seed);
  #ifdef DEBUG_RANDOMGRID
  std::cout << "seed=" << seed << std::endl;
  #endif

  if ((width == 0) || (height == 0))
  {
    return;
  }
  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
    // don't normalized to stay on integer grid

    double xoffset = ((rand()/(double)RAND_MAX) - 0.5) * 2 * xjitter;
    //std::cout << "xoffset= " << xoffset << std::endl;
    double yoffset = ((rand()/(double)RAND_MAX) - 0.5) * 2 * yjitter;
    //std::cout << "yoffset= " << yoffset << std::endl;
    double xu = x + xoffset;
    double yu = y + yoffset;
    #ifdef DEBUG_RANDOMGRID
    std::cout << "x=" << xu << " y=" << yu << std::endl;
    #endif
    //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 
    p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

void CoordinateLayer::wave(unsigned int width, unsigned int height, double xc, double yc, double a, double b, double c, double d)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;
  //double minside = ( width > height ? height : width );

  if ((width == 0) || (height == 0))
  {
    return;
  }
  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {

      // Normalize distorted coordinates according to ptlens
      double xu;
      double yu;



      double valx = c*sin(2*M_PI/(this->width)  * (a*(double)x - xc));
      double valy = d*sin(2*M_PI/(this->height) * (b*(double)y - yc));
      double val = valy + valx ;

      // limitation to have no offset at the border
      double valx_limit = sin(2*M_PI/(2*this->width)  * ((double)x + this->width ));
      double valy_limit = sin(2*M_PI/(2*this->height) * ((double)y + this->height));


      //xu = x + valx_limit * valy_limit * val;
      //yu = y + valy_limit * valx_limit * val;
      xu = x + valx_limit *  val;
      yu = y + valy_limit *  val;
      
      //if( y== 0)
      //  std::cout << "x=" << x << " val=" << val << " xu=" << xu << std::endl;
      
      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 
      // store new coordinates
      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}
void CoordinateLayer::centercrushing(unsigned int width, unsigned int height, double xc, double yc, double a)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;

  if ((width == 0) || (height == 0))
  {
    return;
  }
  double Amp = 1;

  Amp = -a * this->width / 180.0;
  //std::cout << "a= " << a << " Amp=" << Amp << std::endl;

  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {

      // Normalize distorted coordinates according to ptlens
      double xu;
      double yu;

      double valy = -Amp *sin(2*M_PI/(this->width)  * (0.5*(double)x - xc));
      double valy_limit = sin(2*M_PI/(2*this->height) * ((double)y  + this->height) + M_PI/2);

      xu = x ;
      yu = (double)y + valy * valy_limit;
      
      //if(y== 0 && x < 641)
      //  std::cout << "x=" << x << " valy=" << valy << " yu=" << yu << std::endl;

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      // store new coordinates
      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

void CoordinateLayer::ptlens_correct(unsigned int width, unsigned int height, double xc, double yc, double a, double b, double c)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;
  double minside = ( width > height ? height : width );
  //std::cout << "minside=" << minside << std::endl;

  // Normalize center offset according to ptlens
  double xcc = (xc - this->width * 0.5);
  double ycc = (yc - this->height * 0.5);
  
  if ((width == 0) || (height == 0))
  {
    return;
  }
  
  xcc = xcc * 2.0 / minside;
  ycc = ycc * 2.0 / minside;
  //std::cout << "Normalized center xcc=" << xcc << " ycc=" << ycc << std::endl;

  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
      // Normalize distorted coordinates according to ptlens
      double xd = (x - this->width * 0.5);
      double yd = (y - this->height * 0.5);
      xd = xd * 2.0 / minside;
      yd = yd * 2.0 / minside;

      double xu;
      double yu;

      // ptlens lens distortion model
      double d = 1;

      double r2 = pow(xd - xcc, 2.0) + pow(yd - ycc, 2.0);
      double r  = sqrt(pow(xd - xcc, 2.0) + pow(yd - ycc, 2.0));
      double r3 = pow(r, 3);

      // r^3
      const double poly = (a * r3 + b * r2 + c * r + d);
      // r^4 = r * r^3
      xu = xd * poly;
      yu = yd * poly;
      //std::cout << "(xu, yu)=" << xu << " " << yu << std::endl;

      // Denormalize resulting undistorted coordinates
      xu = xu * minside * 0.5;
      yu = yu * minside * 0.5;
      xu = (xu + this->width * 0.5);
      yu = (yu + this->height * 0.5);

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      // store new coordinates
      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

#define NEWTON_EPS 0.00001
#define EPSILON 0.00001
void CoordinateLayer::ptlens_apply(unsigned int width, unsigned int height, double xc, double yc, double a, double b, double c)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;
  double minside = (width > height ? height : width);
  //std::cout << "minside=" << minside << std::endl;

  // Normalized center offset according to ptlens
  double xcc = (xc - this->width * 0.5);
  double ycc = (yc - this->height * 0.5);
  
  if ((width == 0) || (height == 0))
  {
    return;
  }
  
  xcc = xcc * 2.0 / minside;
  ycc = ycc * 2.0 / minside;

  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
      // Normalize undistorted coordinates according to ptlens
      double xd = (x - this->width * 0.5);
      double yd = (y - this->height * 0.5);
      xd = xd * 2.0 / minside;
      yd = yd * 2.0 / minside;

      // results, fall back to source value, for the center point or negative values
      double xu = xd;
      double yu = yd;
      int valid = 1;

      double ru = sqrt(pow(xd - xcc, 2) + pow(yd - ycc, 2));

      if (fabs(ru - 0.0 ) > EPSILON) { // radius > 0 => not center point 
        // Solve non-linear equation using Newton's method
        double rd = ru;
  
        for (int n = 0; n < 6; ++n) { // n iterations
          valid = 1;
          double frd = rd * (a * rd * rd * rd + b * rd * rd + c * rd + 1.0) - ru;
          if (frd >= -NEWTON_EPS && frd < NEWTON_EPS) {
            break;
          }
          if (n == 5) { // no convergence
            valid = 0;
            //std::cout << "Note: Does not converge." << std::endl;
          }
          rd -= frd / (4 * a * rd * rd * rd + 3 * b * rd * rd + 2 * c * rd + 1.0);
        }
        if (valid == 1 && rd > 0.0) {
          //std::cout << "rd=" << rd  << " ru=" << ru << std::endl;
          rd /= ru;
          // obtain coordinates
          xu = xd * rd;
          yu = yd * rd;
        } // else use input values
      }

      // Denormalize resulting distorted coordinates
      xu = xu * minside * 0.5;
      yu = yu * minside * 0.5;
      xu = (xu + this->width * 0.5);
      yu = (yu + this->height * 0.5);

      /*
      // Debug: Check if resulting coordinate is outside valid range and set to -1
      if ((xu < 0) || (xu >= this->width-1) || (yu < 0) || (yu >= this->height-1)) {
        std::cerr << " xu out of range xu=" << xu << " yu=" << yu << std::endl;
        xu = -1.0;
        yu = -1.0;
      }
      */

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
         xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

void CoordinateLayer::fisheye_equidistant_correct(unsigned int width, unsigned int height, double focal, double cropfactor, double zoom)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
  double scaleHeight;
  double minside = ( width > height ? height : width );
  //std::cout << "minside=" << minside << std::endl;
  
  if ((width == 0) || (height == 0) || (zoom == 0.0))
  {
    return;
  }
  // focal * cropfactor equals fullframe(24mmx36mm) focal length
  // half of the small side 24mm/2 normalized to 1 
  //double inv_norm_f= 1/ (focal * cropfactor / 12);
  double inv_norm_f= 1/ (focal * cropfactor * zoom / 12);

  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
      // Normalize distorted coordinates according to ptlens
      double xd = (x - this->width * 0.5);
      double yd = (y - this->height * 0.5);
      xd = xd * 2.0 / minside;
      yd = yd * 2.0 / minside;

      double xu;
      double yu;

      double rs  = sqrt(pow(xd, 2.0) + pow(yd, 2.0)) * inv_norm_f;
      double theta = 1.0;
      if (rs != 0)
        theta = atan(rs) / rs;

      xu = xd * theta * 1/zoom;
      yu = yd * theta * 1/zoom;

      // Denormalize resulting undistorted coordinates
      xu = xu * minside * 0.5;
      yu = yu * minside * 0.5;
      xu = (xu + this->width * 0.5);
      yu = (yu + this->height * 0.5);

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      // store new coordinates
      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

void CoordinateLayer::fisheye_equisolid_correct(unsigned int width, unsigned int height, double focal, double cropfactor, double zoom)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
 double scaleHeight;
  double minside = ( width > height ? height : width );
  //std::cout << "minside=" << minside << std::endl;
  
  if ((width == 0) || (height == 0) || (zoom == 0.0))
  {
    return;
  }

  // focal * cropfactor equals fullframe(24mmx36mm) focal length
  // half of the small side 24mm/2 normalized to 1 
  //double inv_norm_f= 1/ (focal * cropfactor / 12);
  double inv_norm_f= 1/ (focal * cropfactor * zoom / 12);

  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
      // Normalize distorted coordinates according to ptlens
      double xd = (x - this->width * 0.5);
      double yd = (y - this->height * 0.5);
      xd = xd * 2.0 / minside;
      yd = yd * 2.0 / minside;

      double xu;
      double yu;

      double rs  = sqrt(pow(xd, 2.0) + pow(yd, 2.0)) * inv_norm_f;
      double theta = 1.0;
      if (rs != 0)
        theta = 2 * sin( atan(rs) / 2) / rs;

      xu = xd * theta * 1/zoom;
      yu = yd * theta * 1/zoom;

      // Denormalize resulting undistorted coordinates
      xu = xu * minside * 0.5;
      yu = yu * minside * 0.5;
      xu = (xu + this->width * 0.5);
      yu = (yu + this->height * 0.5);

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      // store new coordinates
      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
}

void CoordinateLayer::rotation(unsigned int width, unsigned int height, double angle, double zoom)
{
  this->width = width;
  this->height = height;
  double scaleWidth;
 double scaleHeight;
  double minside = ( width > height ? height : width );
  //std::cout << "minside=" << minside << std::endl;
  
  if ((width == 0) || (height == 0) || (zoom == 0.0))
  {
    return;
  }

  const double pi = 4 * atan(1.0);
  const double angle_rad = (angle / 180) * pi;
  //std::cout << "pi=" << pi << std::endl;
  //std::cout << "angle_rad=" << angle_rad << std::endl;

  if (m_sourceWidth != 0 && m_sourceHeight != 0)
  {
    scaleWidth = (double) m_sourceWidth / (double) width;
    scaleHeight = (double) m_sourceHeight / (double) height;
  }

  for (unsigned int y = 0; y < this->height; ++y) {
    for (unsigned int x = 0; x < this->width; ++x) {
      // Normalize coordinates according to ptlens
      double xd = (x - this->width * 0.5);
      double yd = (y - this->height * 0.5);
      xd = xd * 2.0 / minside;
      yd = yd * 2.0 / minside;

      double xu;
      double yu;

      double r  = sqrt(pow(xd, 2.0) + pow(yd, 2.0));
      double theta = atan2 (yd, xd); // obey correct quadrant
      theta += angle_rad; 

      xu = r * cos(theta) * 1/zoom;
      yu = r * sin(theta) * 1/zoom;

      // Denormalize resulting coordinates
      xu = xu * minside * 0.5;
      yu = yu * minside * 0.5;
      xu = (xu + this->width * 0.5);
      yu = (yu + this->height * 0.5);

      //scale
    if (m_sourceWidth != 0 && m_sourceHeight != 0) {
        xu = xu * scaleWidth; 
        yu = yu * scaleHeight; 
    } 

      // store new coordinates
      p.push_back(Coordinate(xu, yu));
    } // x
  } // y
} 
} // namespace
//-----------------------------------------------------------------------------

