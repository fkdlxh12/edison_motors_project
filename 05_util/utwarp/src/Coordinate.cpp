/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        Coordinate.cpp
 */

#if defined(C_MODEL) || defined(FPGA)
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <ostream>
#include <cmath>
#endif

#include <iomanip>

#include "Coordinate.h"

namespace coordinate {

std::ostream& operator << (std::ostream& os, const Coordinate& c) {
  os << c.x << " " << c.y;
  return os;
}

//#define DEBUG_D2F
unsigned int Coordinate::double2fix(double x, int num_int, int num_dec, int& error, int offset) {
  if (num_int == -3 && num_dec == 4) { // catch implied LSB set mode for 1 and 2 bpp mode
    return ( x < 0 ? 1 : 0);
  }
  if (offset == 0) {
    return double2fix(x, num_int, num_dec, error);
  }
  unsigned int x_i = 0;
  if (offset) {
    x_i = Coordinate::double2fix(x, num_int, num_dec+1, error, 0);
    //printbin(x_i);
    if (( x_i & 1)) {
      #ifdef DEBUG_D2F
      std::cout << "set: " << x << " subtract = " << (x - 0.03125) << endl;
      #endif
      x -= 0.03125;
    }
  }
  x_i = Coordinate::double2fix(x, num_int, num_dec+1, error, 0);
  return x_i >> 1;
}

#define EPSILON 0.00001
//#define DEBUG_DOUBLE2FIX
unsigned int Coordinate::double2fix(double x, int num_int, int num_dec, int& error)
{
  //int wl = num_dec + num_int;
  //int iwl = num_int;
  int do_round = 1;
  error = 0;
  double fixedprecision = pow(2.0, (-num_dec));
  //std::cout << "fixed precision=" << fixedprecision << endl;
  double max = 2 * pow(2.0, (num_dec + num_int - 2 - num_dec)) - pow(2.0, (-num_dec ));
  //std::cout << "max=" << max << endl;
  if (x > 0 && x > max + (do_round ? 0.5 *fixedprecision : 0)) {
    error = 1;
    #ifdef DEBUG_DOUBLE2FIX
    std::cout << "overflow " << setprecision(10) << x << " set x=" << max << endl;
    #endif
    x = max;
  }

  double min = - pow(2.0, (num_dec + num_int - 1 - num_dec));
  //std::cout << "min=" << min << endl;
  if (x < 0 && x < min - (do_round ? 0.5 *fixedprecision : 0)) {
    error = 2;
    #ifdef DEBUG_DOUBLE2FIX
    std::cout << "underflow " <<  setprecision(10) << x << " set x=" << min << endl;
    #endif
    x = min;
  }

  // catch small values, especially negative to not wrongly set signed bit
  if (fabs(x) <= pow(2.0, -num_dec-2))  {
    //std::cout << "almost zero" << endl;
    x = 0;
  }
  unsigned int result = 0;
  double tmp;

  //std::cout << "signed bit : " << pow(2.0, (num_dec + num_int - 1 - num_dec)) << endl;
  // convert -x and x to same range
  tmp = (x+fixedprecision/2.0 < 0 ? (x - min) : x);

  //std::cout << "x=" << x <<" tmp=" << tmp << endl;
  int exponent = num_dec + num_int - 2 - num_dec;
  // subtract weights excluding signed bit
  for (int n = num_dec+num_int - 2; n >= 0; --n, --exponent) {
      //printf ("n: %d exp: %d\n", n, exponent );
      double an = pow(2.0, exponent);
      //printf("\ta %f\n", an);
      if ( (an - tmp) <= 0 ) {
        //std::cout << "tmp=" << tmp << " n=" << n << endl;
        tmp -= an;
        result |= (1 << n);
      } 
  }
  //printbin(result);
  //std::cout << "tmp=" << tmp << endl;
  if (do_round) {
    //std::cout << "tmp=" << tmp << endl;
    if ( x > 0 && tmp > 0.5 *fixedprecision ) {
      //std::cout << "round up" << endl;
      result += 1;
    }
    if ( x < 0 && tmp > 0.5 *fixedprecision ) {
      //std::cout << "round up" << endl;
      result += 1;
    }
  }
  //printbin(result);

  if ( x < 0 ) {
    result |= (1 << (num_dec+num_int - 1));
  } 

  //printbin(result);

  return result;
}

//#define DEBUG_T2F
double Coordinate::trunc2fix(double x, int num_int, int num_dec, int& error, int offset) {
  unsigned int x_i = 0;
  double x_d = 0;

  x_i = Coordinate::double2fix(x, num_int, num_dec, error, offset);
  x_d = Coordinate::fix2double(x_i, num_int, num_dec, offset);
  #ifdef DEBUG_T2F
  std::cout << " x = " << x << " x_i = " << x_i << " x_d = " << x_d << endl;
  #endif

  return x_d;
}

//#define DEBUG_F2D 
double Coordinate::fix2double(unsigned int x, int num_int, int num_dec, int offset)
{
  //std::cout << "v is " << x << " (" << num_int << ", "  << num_dec << ")" << endl;
  if (num_int == -3 && num_dec == 4) { // catch implied LSB set mode
    return ( x >= 1 ? -0.03125 : 0.03125); 
  }
  // set implicit lsb bit for offset mode
  if (offset) {
    num_dec += 1;
    x = x << 1;
    x = x | 1;
  }

  double result = 0;
  
  int sign = (x & (1<< (num_int+num_dec-1)) ? -1 : 1 ) ;
  //printf("sign %d\n", sign);
  
  if (sign < 0) {
    x -= 1;
    x = ~x;
  }
  for (int n = (num_int-2) ; n >= -num_dec; --n) {
    //printf( "n=%d\n", n);
    if (x & (1 << (n+num_dec)) ) {
      result += pow((double)2, n);
    }
  }

  if (result != 0 )
    result *= (sign > 0 ? 1 : -1);
  //std::cout << "result=" << result << endl;

  return result;
}

double Coordinate::fix2double(unsigned int x, int num_int, int num_dec) {
  return fix2double(x, num_int, num_dec, 0);
}

//#define DEBUG_CONVERT
int Coordinate::double2fix(int num_int, int num_dec) {
  int yerror, xerror;
  this->x = (double)Coordinate::double2fix((double)this->x, (int)num_int, (int)num_dec, xerror);
  this->y = (double)Coordinate::double2fix((double)this->y, (int)num_int, (int)num_dec, yerror);
  #ifdef DEBUG_CONVERT
  printbin((unsigned int)this->x);
  printbin((unsigned int)this->y);
  #endif
  return (xerror > yerror ? xerror : yerror);
}

int Coordinate::double2fix(int num_int, int num_dec, int offset) {
  int yerror, xerror;
  this->x = (double)Coordinate::double2fix((double)this->x, (int)num_int, (int)num_dec, xerror, offset);
  this->y = (double)Coordinate::double2fix((double)this->y, (int)num_int, (int)num_dec, yerror, offset);
  #ifdef DEBUG_CONVERT
  printbin((unsigned int)this->x);
  printbin((unsigned int)this->y);
  #endif
  return (xerror > yerror ? xerror : yerror);
}

void Coordinate::fix2double(unsigned int num_int, unsigned int num_dec) {
  this->x = (double)Coordinate::fix2double((unsigned int)this->x, (int)num_int, (int)num_dec);
  this->y = (double)Coordinate::fix2double((unsigned int)this->y, (int)num_int, (int)num_dec);
}

#ifdef COORDINATE_DEBUG_FEATURES
void printbin (unsigned int x)
{
  printf("bit:   31 30 29 28 . 27 26 25 24 . 23 22 21 20 . 19 18 17 16 | 15 14 13 12 . 11 10  9  8 .  7  6  5  4 .  3  2  1  0\n"); 
  printf("value:  %d  %d  %d  %d .  %d  %d  %d  %d .  %d  %d  %d  %d .  %d  %d  %d  %d .  %d  %d  %d  %d .  %d  %d  %d  %d .  %d  %d  %d  %d .  %d  %d  %d  %d\n\n", \
         ((x & (1<<31))>> 31), 
         ((x & (1<<30))>> 30), 
         ((x & (1<<29))>> 29), 
         ((x & (1<<28))>> 28), 
         ((x & (1<<27))>> 27), 
         ((x & (1<<26))>> 26), 
         ((x & (1<<25))>> 25), 
         ((x & (1<<24))>> 24), 
         ((x & (1<<23))>> 23), 
         ((x & (1<<22))>> 22), 
         ((x & (1<<21))>> 21), 
         ((x & (1<<20))>> 20), 
         ((x & (1<<19))>> 19), 
         ((x & (1<<18))>> 18), 
         ((x & (1<<17))>> 17), 
         ((x & (1<<16))>> 16), 
         ((x & (1<<15))>> 15), 
         ((x & (1<<14))>> 14), 
         ((x & (1<<13))>> 13), 
         ((x & (1<<12))>> 12), 
         ((x & (1<<11))>> 11), 
         ((x & (1<<10))>> 10), 
         ((x & (1<<9)) >> 9), 
         ((x & (1<<8)) >> 8), 
         ((x & (1<<7)) >> 7), 
         ((x & (1<<6)) >> 6), 
         ((x & (1<<5)) >> 5), 
         ((x & (1<<4)) >> 4), 
         ((x & (1<<3)) >> 3), 
         ((x & (1<<2)) >> 2), 
         ((x & (1<<1)) >> 1), 
         (x & (1<<0)) );

}
#endif
} // namespace

