/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

#include <math.h>

#include "cygfx_driver_api.h"

#if (defined(UTIL_INCLUDE_FREETYPE) && UTIL_INCLUDE_FREETYPE==1)
#include "ut_freetype.h"
#include "sm_util.h"
#include "ut_compatibility.h"
#include "wchar.h"

/* These are referenced by freetype lib */
int z_verbose = 0;
void z_error (char *m) {}


static FT_Library g_lib = NULL;
static CYGFX_S32 g_libCnt = 0;
#ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER
static struct FT_MemoryRec_ g_memory;
#endif


static CYGFX_ERROR render_outline_glyph(FT_GlyphSlot glyph, CYGFX_BE_CONTEXT ctx);
static CYGFX_ERROR decompose_move_to(const FT_Vector *to, void *user);
static CYGFX_S32 decompose_line_to(const FT_Vector *to, void *user);
static CYGFX_S32 decompose_conic_to(const FT_Vector *control, const FT_Vector *to, void *user);
static CYGFX_S32 decompose_cubic_to(const FT_Vector *control1, const FT_Vector *control2, const FT_Vector *to, void *user);
#ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER
static void* utFt_Alloc( FT_Memory memory, long size);
static void utFt_Free( FT_Memory memory, void* block);
static void* utFt_Realloc(FT_Memory memory, long cur_size, long new_size, void* block);
#endif


CYGFX_ERROR utFtOpen(FT_CONTEXT ftCtx, CYGFX_BE_CONTEXT beCtx, const CYGFX_CHAR *font, CYGFX_U32 buffersize, CYGFX_U32 size)
{
    CYGFX_ERROR ret = CYGFX_OK;



    g_libCnt++;

    memset(ftCtx, 0, sizeof(FT_CONTEXT_CONTAINER));
    ftCtx->beCtx = beCtx;
    if (ftCtx->beCtx == NULL)
    {
        ret = CYGFX_ERR;
    }
    ftCtx->load_flags = FT_LOAD_NO_BITMAP | FT_LOAD_NO_HINTING;

    if (g_lib == NULL)
    {
#ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER
        g_memory.user    = 0;
        g_memory.alloc   = utFt_Alloc;
        g_memory.free    = utFt_Free;
        g_memory.realloc = utFt_Realloc;

        UTIL_SUCCESS(ret, FT_New_Library( &g_memory, &g_lib ));
        if( ret != CYGFX_OK) return ret;
        FT_Add_Default_Modules(g_lib);
#else
        UTIL_SUCCESS(ret, FT_Init_FreeType(&g_lib));
#endif /* #ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER */
    }

    if (buffersize > 0) /* size indicates a memory object */
    {
        UTIL_SUCCESS(ret, FT_New_Memory_Face( g_lib, (const FT_Byte*)font, buffersize, 0, &ftCtx->face));
    }
    else
    {
        UTIL_SUCCESS(ret, FT_New_Face(g_lib, font, 0, &ftCtx->face));
    }

    UTIL_SUCCESS(ret, FT_Select_Charmap(ftCtx->face, FT_ENCODING_UNICODE));

#ifndef UTIL_FREETYPE_USE_CHAR_SIZE
    UTIL_SUCCESS(ret, FT_Set_Pixel_Sizes(ftCtx->face, size, size));
#else
    UTIL_SUCCESS(ret, FT_Set_Char_Size(
            ftCtx->face,    /* handle to face object           */
            0,       /* char_width in 1/64th of points  */
            size*64,   /* char_height in 1/64th of points */
            300,     /* horizontal device resolution    */
            300 ));   /* vertical device resolution      */
#endif /* UTIL_FREETYPE_USE_CHAR_SIZE */

    if (ret != CYGFX_OK)
    {
       utFtClose(ftCtx);
       ftCtx = 0;
    }

    return ret;
}

void utFtSetContext(FT_CONTEXT ftCtx, CYGFX_BE_CONTEXT beCtx)
{
    ftCtx->beCtx = beCtx;
}

void utFtRotation(FT_CONTEXT ftCtx, Mat3x2 mat)
{

    FT_Matrix matrix; /* transformation matrix */
    FT_Vector pen;

    pen.x = (FT_Pos) mat[4];
    pen.y = (FT_Pos) mat[5];
    matrix.xx = (FT_Fixed) (mat[0] * 0x10000L);
    matrix.xy = (FT_Fixed) (mat[2] * 0x10000L);
    matrix.yx = (FT_Fixed) (mat[1] * 0x10000L);
    matrix.yy = (FT_Fixed) (mat[3] * 0x10000L);
    FT_Set_Transform(ftCtx->face, &matrix, &pen);
    return;

}

void utFtClose(FT_CONTEXT ftCtx)
{
    if (ftCtx != NULL)
    {
        if (NULL != ftCtx->face)
        {
            FT_Done_Face(ftCtx->face);
        }
        ftCtx->face = NULL;
    }
   g_libCnt--;
   if ((g_libCnt == 0) && (g_lib != NULL))
   {

#ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER
      FT_Done_Library(g_lib);
#else
      FT_Done_FreeType(g_lib);
#endif /* #ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER */
      g_lib = 0;
   }
}

CYGFX_ERROR utFtTextOutFast(FT_CONTEXT ftCtx, CYGFX_S32 x, CYGFX_S32 y, const wchar_t* pszString)
{
    CYGFX_ERROR     ret = CYGFX_OK;
    CYGFX_U32       u32Index;
    FT_Face         m_pstcFTFace;
    FT_UInt         uiGlyphIndex;    /* glyph index */
    FT_UInt         uiGlyphIndexPrevious = 0;
    FT_Vector       stcPenPos;
    CYGFX_BOOL      bUseKerning;

    wchar_t         key[] = L"\n";

    stcPenPos.x = x;
    stcPenPos.y = y;

    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(ftCtx->beCtx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_S16_6));

    m_pstcFTFace = ftCtx->face;
    bUseKerning = (FT_HAS_KERNING(m_pstcFTFace) != 0) ? CYGFX_TRUE : CYGFX_FALSE;

    for (u32Index = 0; (0 != pszString[u32Index]) && (ret == CYGFX_OK); u32Index++)
    {

        if (pszString[u32Index] == key[0])
        {
            stcPenPos.x = 0;
            stcPenPos.y -= ftCtx->face->height & 0xffffffc0U;
            uiGlyphIndexPrevious = 0;
            continue;
        }

        uiGlyphIndex = FT_Get_Char_Index(m_pstcFTFace, pszString[u32Index]);

        if ((bUseKerning == CYGFX_TRUE) && (uiGlyphIndexPrevious != 0) && (uiGlyphIndex != 0))
        {
            FT_Vector stcKerningDelta;
            FT_Get_Kerning(m_pstcFTFace, uiGlyphIndexPrevious, uiGlyphIndex, FT_KERNING_DEFAULT, &stcKerningDelta);
            stcPenPos.x += stcKerningDelta.x;
        }
        uiGlyphIndexPrevious = uiGlyphIndex;
        
        FT_Set_Transform(ftCtx->face, NULL, &stcPenPos);
        UTIL_SUCCESS(ret, FT_Load_Glyph(ftCtx->face, uiGlyphIndex, ftCtx->load_flags));

        switch (ftCtx->face->glyph->format)
        {
        case FT_GLYPH_FORMAT_OUTLINE:
            UTIL_SUCCESS(ret, render_outline_glyph(ftCtx->face->glyph, ftCtx->beCtx));
            break;
        case FT_GLYPH_FORMAT_BITMAP:
            /* not yet supported */
            ret = CYGFX_ERR;
            break;
        default:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("unsupported format for glyph '%c'\n", (CYGFX_S32)pszString[u32Index]);
#endif
            break;
        }
        stcPenPos.x += m_pstcFTFace->glyph->advance.x;
        stcPenPos.y += m_pstcFTFace->glyph->advance.y;
    }
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ftCtx->beCtx, (CYGFX_FLOAT)x, (CYGFX_FLOAT)y));
    return ret;
}


CYGFX_ERROR utFtTextOut(FT_CONTEXT ftCtx, CYGFX_S32 x, CYGFX_S32 y, const wchar_t *pszString)
{
    CYGFX_ERROR     ret = CYGFX_OK;
    CYGFX_U32       u32Index;
    FT_Face         m_pstcFTFace;
    FT_UInt         uiGlyphIndex;    /* glyph index */
    FT_UInt         uiGlyphIndexPrevious = 0;
    CYGFX_S32       stcPenPos[2];
    CYGFX_BOOL      bUseKerning;
    
    wchar_t         key[]=L"\n";

    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(ftCtx->beCtx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_S16_6));

    m_pstcFTFace = ftCtx->face;
    bUseKerning = (FT_HAS_KERNING(m_pstcFTFace) != 0) ? CYGFX_TRUE : CYGFX_FALSE;

    stcPenPos[0] = x * 64;
    stcPenPos[1] = y * 64;   

    for (u32Index = 0; (0 != pszString[u32Index]) && (ret == CYGFX_OK); u32Index++)
    {

        if (pszString[u32Index] == key[0])
        {
            stcPenPos[0] = x * 64;
            stcPenPos[1] -= ftCtx->face->height & 0xffffffc0U;
            uiGlyphIndexPrevious = 0;
            continue;
        }        
        
        uiGlyphIndex = FT_Get_Char_Index(m_pstcFTFace, pszString[u32Index]);

        if ((bUseKerning == CYGFX_TRUE) && (uiGlyphIndexPrevious != 0) && (uiGlyphIndex != 0))
        {
            FT_Vector stcKerningDelta;
            FT_Get_Kerning(m_pstcFTFace, uiGlyphIndexPrevious, uiGlyphIndex, FT_KERNING_DEFAULT, &stcKerningDelta);
            stcPenPos[0] += stcKerningDelta.x;
        }
        uiGlyphIndexPrevious = uiGlyphIndex;

        UTIL_SUCCESS(ret, FT_Load_Glyph(ftCtx->face, uiGlyphIndex, ftCtx->load_flags));

        switch (ftCtx->face->glyph->format)
        {
            case FT_GLYPH_FORMAT_OUTLINE:
                UTIL_SUCCESS(ret, render_outline_glyph(ftCtx->face->glyph, ftCtx->beCtx));
                UTIL_SUCCESS(ret, CyGfx_DeDraw(ftCtx->beCtx, (CYGFX_FLOAT)stcPenPos[0]/64.0f, (CYGFX_FLOAT)stcPenPos[1]/64.0f));
                break;
            case FT_GLYPH_FORMAT_BITMAP:
                /* not yet supported */
                ret = CYGFX_ERR;
                break;
            default:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
                printf("unsupported format for glyph '%c'\n", (CYGFX_S32)pszString[u32Index]);
#endif
                break;
        }
        stcPenPos[0] += m_pstcFTFace->glyph->advance.x;
        stcPenPos[1] += m_pstcFTFace->glyph->advance.y;
    }
    return ret;
}

CYGFX_ERROR utFtCalc(FT_CONTEXT ftCtx, CYGFX_U32 *w, CYGFX_U32 *h, const wchar_t *pszString)
{
    CYGFX_ERROR   ret = CYGFX_OK;
    CYGFX_U32     u32Index;
    FT_Face       m_pstcFTFace;
    FT_UInt       uiGlyphIndex;    /* glyph index */
    FT_UInt       uiGlyphIndexPrevious = 0;
    CYGFX_U32     stcPenPos[2];
    CYGFX_U32     max_x;
    CYGFX_BOOL    bUseKerning;

    wchar_t         key[]=L"\n";

    m_pstcFTFace = ftCtx->face;
    bUseKerning = (FT_HAS_KERNING(m_pstcFTFace) != 0) ? CYGFX_TRUE : CYGFX_FALSE;

    stcPenPos[0] = 0;
    stcPenPos[1] = 0;
    max_x = 0;

    for (u32Index = 0; (0 != pszString[u32Index]) && (ret == CYGFX_OK); u32Index++)
    {
        if (pszString[u32Index] == key[0])
        {
            if (max_x < stcPenPos[0])
            {
                max_x = stcPenPos[0];
            }
            stcPenPos[0] = 0;
            stcPenPos[1] += (CYGFX_U32)ftCtx->face->height & 0xffffffc0U;
            uiGlyphIndexPrevious = 0;
            continue;
        }
        uiGlyphIndex = FT_Get_Char_Index(m_pstcFTFace, pszString[u32Index]);

        if ((bUseKerning == CYGFX_TRUE) && (uiGlyphIndexPrevious != 0) && (uiGlyphIndex != 0))
        {
            FT_Vector stcKerningDelta;
            FT_Get_Kerning(m_pstcFTFace, uiGlyphIndexPrevious, uiGlyphIndex, FT_KERNING_DEFAULT, &stcKerningDelta);
            stcPenPos[0] += (CYGFX_U32)stcKerningDelta.x;
        }
        uiGlyphIndexPrevious = uiGlyphIndex;

        UTIL_SUCCESS(ret, FT_Load_Glyph(ftCtx->face, uiGlyphIndex, ftCtx->load_flags));

        switch (ftCtx->face->glyph->format)
        {
            case FT_GLYPH_FORMAT_OUTLINE:
                break;
            case FT_GLYPH_FORMAT_BITMAP:
                /* not yet supported */
                ret = CYGFX_ERR;
                break;
            default:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
                printf("unsupported format for glyph '%c'\n", (CYGFX_S32)pszString[u32Index]);
#endif
                break;
        }
        stcPenPos[0] += (CYGFX_U32)m_pstcFTFace->glyph->advance.x;
        stcPenPos[1] += (CYGFX_U32)m_pstcFTFace->glyph->advance.y;
    }
    if (ret == CYGFX_OK)
    {
        if (max_x < stcPenPos[0])
        {
            max_x = stcPenPos[0];
        }
        if (max_x > 0)
        {
            stcPenPos[1] += (CYGFX_U32)ftCtx->face->height & 0xffffffc0U;
        }
        if (w != NULL)
        {
            *w = max_x >> 6;
        }
        if (h != NULL)
        {
            *h = stcPenPos[1] >> 6;
        }
    }
    return ret;
}

CYGFX_ERROR utFtSetParam(FT_CONTEXT ftCtx, CYGFX_U32 load_flags)
{
    ftCtx->load_flags = load_flags;
    return CYGFX_OK;
}

static CYGFX_ERROR decompose_move_to(const FT_Vector *to, void *user)
{
    const CYGFX_U08 type = CYGFX_DE_INSTR_MOVE_TO_ABS;

    return CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 1, &type, to);
}

static CYGFX_S32 decompose_line_to(const FT_Vector *to, void *user)
{
    const CYGFX_U08 type = CYGFX_DE_INSTR_LINE_TO_ABS;

    return CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 1, &type, to);
}

static CYGFX_S32 decompose_conic_to(const FT_Vector *control, const FT_Vector *to, void *user)
{
    CYGFX_ERROR ret = CYGFX_OK;
    const CYGFX_U08 type = CYGFX_DE_INSTR_QUAD_TO_ABS;

    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 1, &type, NULL));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 2, NULL, control));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 2, NULL, to));

    return ret;
}

static CYGFX_S32 decompose_cubic_to(const FT_Vector *control1, const FT_Vector *control2,
                   const FT_Vector *to, void *user)
{
    CYGFX_ERROR ret = CYGFX_OK;
    const CYGFX_U08 type = CYGFX_DE_INSTR_CUBIC_TO_ABS;

    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 1, &type, NULL));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 2, NULL, control1));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 2, NULL, control2));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData((CYGFX_BE_CONTEXT)user, 2, NULL, to));

    return ret;
}

static CYGFX_ERROR render_outline_glyph(FT_GlyphSlot glyph, CYGFX_BE_CONTEXT ctx)
{
    CYGFX_ERROR ret = CYGFX_OK;

   FT_Outline_Funcs funcs =
   {
      &decompose_move_to,
      &decompose_line_to,
      &decompose_conic_to,
      &decompose_cubic_to,
      0, 0
   };

   UTIL_SUCCESS(ret, FT_Outline_Decompose(&glyph->outline, &funcs, (void *)ctx));

   return ret;
}

#ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER
static void* utFt_Alloc( FT_Memory memory, long size)
{
    /* This is just one possible implementation.
       Another option would be the use of HyperRAM
    */
    void*  p = utVideoAlloc((CYGFX_U32)size, 0, 0);
    memory=memory;
    return p;
}

static void utFt_Free( FT_Memory memory, void* block)
{
    memory=memory;
    utVideoFree(block);
}

static void* utFt_Realloc(FT_Memory memory, long cur_size, long new_size, void* block)
{
    void* p;

    /* The following lines can still be improved:
       Enlarge allocated memory if possible, i.e. memory is not allocated.
       Then copy is not needed.
    */
    p = utFt_Alloc( memory, new_size );
    if (p)
    {
        long  size;
        size = cur_size;
        if (new_size < size)
        {
            size = new_size;
        }
        memcpy( p, block, size );
        utFt_Free( memory, block );
    }

    return p;
}
#endif /* #ifdef UTIL_FREETYPE_LOCAL_MEMORY_MANAGER */
#endif //#if (defined(UTIL_INCLUDE_FREETYPE) && UTIL_INCLUDE_FREETYPE==1)
