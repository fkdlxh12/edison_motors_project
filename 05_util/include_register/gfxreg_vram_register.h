/*******************************************************************************
* Product: SW_TVII_VGFX_DRV
*
* (c) 2017-2022, Cypress Semiconductor Corporation. All rights reserved.
*
* Warranty and Disclaimer
*
* This software product is property of Cypress Semiconductor Corporation or
* its subsidiaries.
* Any use and/or distribution rights for this software product are provided
* only under the Cypress Software License Agreement.
* Any use and/or distribution of this software product not in accordance with
* the terms of the Cypress Software License Agreement are unauthorized and
* shall constitute an infringement of Cypress intellectual property rights.
*/
/******************************************************************************/

/*!
 * \version     0.1
 * \date        Tue May 26 11:58:42 2020
 * \file        gfxreg_vram_register.h
 *              This file was generated automatically by agentx 1.00.5.
 *              Design:   vram
 *              Source:   vram.component.xml
 *              Template: component_h.tpl
 * \brief      Iris vram register and field definition
 * Implements Building Block: HWExchangeBlock
 *
 *
 * \ingroup register_definitions
 */

#ifndef GFXREG_VRAM_REGISTER_H
#define GFXREG_VRAM_REGISTER_H

/*! \cond Hide for doxygen */

/******************************************************************************/
/* vram registers */
/******************************************************************************/

/* Deviation from MISRA C:2012 Dir-1.1.
   Justification: This cannot be avoided and is supported by the required compiler. */
/* PRQA S 0380 EOF */

/* Deviation from MISRA C:2012 Rule-1.3, Rule-5.2.
   Justification: This cannot be avoided and is supported by the required compiler. */
/* PRQA S 0779 EOF */

/* Deviation from MISRA C:2012 Dir-1.1.
   Justification: The choice of using a function-like macro here is done in order to 
                  allow an easier understanding of the code 
                  without incurring the overhead of a function call.    */
/* PRQA S 3453 EOF */


/* ARBITER_PRIORITY: Assigns arbitration priorities to each of the read/write ports. A port with a higher priority will always win over a port with a lower one. Ports having same priority will be round-robin arbitrated. The arbitration winner is determined based on the current bank arbitration state and the access requests from the slave interfaces.  The state machine tries to execute the round robin access sequence of S0 Write, S0 Read, S1 Read, S2 Read, S3 Read and S4 Read. If the preferred interface is not requesting, then the state machine skips to the next requesting interface. Currently 04 priority levels i.e. 0,1,2,3 are being supported. In order to represent 04 priority levels we need 2 bits.  As a result for each port there are two bits reserved in the arbiter_priority register. In priority levels, 3 represents the highest priority and 0 represents the lowest priority.  On reset, all ports will have same priority value of 0.     */
#define GFXREG_VRAM_ARBITER_PRIORITY 0x00000000U
/* PRIORITY_WR0: Priority for write slave interfaces 0. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_WR0_FSHIFT 0U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_WR0_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_WR0(val) (((CYGFX_U32)(val) & 0x3UL) )
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_WR0(val) (((CYGFX_U32)(val) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_WR0(val) (((CYGFX_U32)(val) ) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_WR0(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0x3UL) | (((CYGFX_U32)(val) ) & 0x3UL) )
#define GFXREG_VRAM_SET_PRIORITY_WR0(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0x3UL) | ((CYGFX_U32)(val) ) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_WR0(s) ((((s)->arbiter_priority) ) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_WR0_RESET 0U

/* PRIORITY_WR1: Priority for write slave interfaces 1. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_WR1_FSHIFT 2U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_WR1_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_WR1(val) (((CYGFX_U32)(val) & 0x3UL) << 2U)
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_WR1(val) (((CYGFX_U32)(val) << 2U) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_WR1(val) (((CYGFX_U32)(val) >> 2U) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_WR1(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0xcUL) | (((CYGFX_U32)(val) << 2U) & 0xcUL) )
#define GFXREG_VRAM_SET_PRIORITY_WR1(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0xcUL) | ((CYGFX_U32)(val) << 2U) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_WR1(s) ((((s)->arbiter_priority) >> 2U) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_WR1_RESET 0U

/* PRIORITY_RD0: Priority for read slave interface 0. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD0_FSHIFT 8U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD0_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_RD0(val) (((CYGFX_U32)(val) & 0x3UL) << 8U)
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_RD0(val) (((CYGFX_U32)(val) << 8U) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_RD0(val) (((CYGFX_U32)(val) >> 8U) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_RD0(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0x300UL) | (((CYGFX_U32)(val) << 8U) & 0x300UL) )
#define GFXREG_VRAM_SET_PRIORITY_RD0(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0x300UL) | ((CYGFX_U32)(val) << 8U) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_RD0(s) ((((s)->arbiter_priority) >> 8U) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD0_RESET 0U

/* PRIORITY_RD1: Priority for read slave interface 1. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD1_FSHIFT 12U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD1_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_RD1(val) (((CYGFX_U32)(val) & 0x3UL) << 12U)
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_RD1(val) (((CYGFX_U32)(val) << 12U) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_RD1(val) (((CYGFX_U32)(val) >> 12U) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_RD1(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0x3000UL) | (((CYGFX_U32)(val) << 12U) & 0x3000UL) )
#define GFXREG_VRAM_SET_PRIORITY_RD1(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0x3000UL) | ((CYGFX_U32)(val) << 12U) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_RD1(s) ((((s)->arbiter_priority) >> 12U) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD1_RESET 0U

/* PRIORITY_RD2: Priority for read slave interface 2. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD2_FSHIFT 16U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD2_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_RD2(val) (((CYGFX_U32)(val) & 0x3UL) << 16U)
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_RD2(val) (((CYGFX_U32)(val) << 16U) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_RD2(val) (((CYGFX_U32)(val) >> 16U) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_RD2(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0x30000UL) | (((CYGFX_U32)(val) << 16U) & 0x30000UL) )
#define GFXREG_VRAM_SET_PRIORITY_RD2(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0x30000UL) | ((CYGFX_U32)(val) << 16U) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_RD2(s) ((((s)->arbiter_priority) >> 16U) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD2_RESET 0U

/* PRIORITY_RD3: Priority for read slave interface 3. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD3_FSHIFT 20U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD3_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_RD3(val) (((CYGFX_U32)(val) & 0x3UL) << 20U)
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_RD3(val) (((CYGFX_U32)(val) << 20U) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_RD3(val) (((CYGFX_U32)(val) >> 20U) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_RD3(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0x300000UL) | (((CYGFX_U32)(val) << 20U) & 0x300000UL) )
#define GFXREG_VRAM_SET_PRIORITY_RD3(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0x300000UL) | ((CYGFX_U32)(val) << 20U) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_RD3(s) ((((s)->arbiter_priority) >> 20U) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD3_RESET 0U

/* PRIORITY_RD4: Priority for read slave interface 4. */
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD4_FSHIFT 24U
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD4_FMASK 0x3U
#define GFXREG_VRAM_ARBITER_PRIORITY_SETM_PRIORITY_RD4(val) (((CYGFX_U32)(val) & 0x3UL) << 24U)
#define GFXREG_VRAM_ARBITER_PRIORITY_SET_PRIORITY_RD4(val) (((CYGFX_U32)(val) << 24U) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__))
#define GFXREG_VRAM_ARBITER_PRIORITY_GET_PRIORITY_RD4(val) (((CYGFX_U32)(val) >> 24U) & 0x3UL)
#define GFXREG_VRAM_SETM_PRIORITY_RD4(s,val) ( (s)->arbiter_priority = ((s)->arbiter_priority & ~0x3000000UL) | (((CYGFX_U32)(val) << 24U) & 0x3000000UL) )
#define GFXREG_VRAM_SET_PRIORITY_RD4(s,val)  ( (s)->arbiter_priority = (((s)->arbiter_priority & ~0x3000000UL) | ((CYGFX_U32)(val) << 24U) ) | GCCGFX_GEN_ASSERT_FIELD_ACCESS(val, 0x3UL, __FILE__, __LINE__) )
#define GFXREG_VRAM_GET_PRIORITY_RD4(s) ((((s)->arbiter_priority) >> 24U) & 0x3UL)
#define GFXREG_VRAM_ARBITER_PRIORITY_PRIORITY_RD4_RESET 0U



/*! \endcond Hide for doxygen */

#endif /* GFXREG_VRAM_REGISTER_H */

