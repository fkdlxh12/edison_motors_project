/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        sm_util.c
 *              Surface Manager utility functions
 *
 *
 */

/*******************************************************************************
 Includes
*******************************************************************************/

/* just for debug */
#include <stdio.h>
#include <string.h>

#include "cygfx_driver_api.h"

#include "ut_compatibility.h"
#include "sm_util.h"
#include "ut_errors.h"
#include "hweb.h"

/*******************************************************************************
 Macro Definitions
*******************************************************************************/

/* The image is run length encoded */
#define SURF_PROP_RLD           0x01
/* The image uses a palette */
#define SURF_PROP_PALETTE       0x02
/* RLA */
#define SURF_PROP_RLA           0x04
/* RLAD */
#define SURF_PROP_RLAD          0x05

#define SURF_PROP_COMPRESSED   (SURF_PROP_RLD | SURF_PROP_RLA | SURF_PROP_RLAD)

/* Obsolete but remain for backward compatibility reason. It was CLUT mode IrisClutMode (typically IRIS_CLUT_MODE_INDEX_RGB or IRIS_CLUT_MODE_INDEX_RGBA) */
#define PALETTE_MODE_INDEX_NEUTRAL 0x00
#define PALETTE_MODE_INDEX_RGB     0x01
#define PALETTE_MODE_INDEX_RGBA    0x02
/*******************************************************************************
 Data Types
*******************************************************************************/

typedef struct
{
    CYGFX_U32  nSize;          /* Size of the whole image description (including this size filed) in bytes */
    CYGFX_U16  nWidth;         /* Width in pixel of the image */
    CYGFX_U16  nHeight;        /* Height in pixel of the image */
    CYGFX_U16  strideInByte;   /* Stride in size */
    CYGFX_U08  totalBits;      /* Number of bits used for one pixel */
    CYGFX_U08  flags;          /* Defines SURF_PROP_... flags */
    CYGFX_U32  ColorComponentBits; /* Defines the number of bits for each color component in the form 0xRRGGBBAA */
    CYGFX_U32  ColorComponentShift; /* Defines bits position for each color component in the form 0xRRGGBBAA */
} SURF_BITMAP;

/* Optional palette description. This structure is included if (flags & SURF_PROP_PALETTE) is true */
typedef struct
{
    CYGFX_U16 count;        /* Number of index colors */
    CYGFX_U08 mode;         /* Obsolete but remains for backward compatible reason. It was CLUT mode IrisClutMode (typically IRIS_CLUT_MODE_INDEX_RGB or IRIS_CLUT_MODE_INDEX_RGBA) */
    CYGFX_U08 totalBits;    /* Number of bits used for one color entry */
    CYGFX_U32 colorBits;    /* Defines the number of bits for each color component in the form 0xRRGGBBAA for one color entry*/
    CYGFX_U32 colorShift;   /* Defines bits position for each color component in the form 0xRRGGBBAA for one color entry*/
} SURF_BITMAP_PALETTE;

typedef struct
{
    CYGFX_U32 nSize;
    CYGFX_U16 type; //set fix to 1
    CYGFX_U16 version; // currently only 0 supported
    CYGFX_U16 count;
    CYGFX_U16 totalBits;
    CYGFX_U32 colorBits;
    CYGFX_U32 colorShift;
} SURF_BITMAP_PALETTE_FULL;

/*******************************************************************************
 Variables
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Prototypes
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Definitions
*******************************************************************************/

#ifdef C_MODEL

CYGFX_STATIC CYGFX_U08 B(CYGFX_U32 val, CYGFX_U32 pos)
{
    pos <<= 3;
    return (val>>pos)&0xff;
}

CYGFX_STATIC void PrintVoid(FILE *fp, unsigned char*p, CYGFX_U32 nSize)
{
    CYGFX_U32 i;
    for (i = 0; i < nSize; i++, p++)
    {
        if (i % 16 == 0)
        {
            fprintf(fp, "\n    ");
        }
        fprintf(fp, "0x%02x", *p);
        if (i != nSize -1)
        {
            fprintf(fp,", ");
        }
    }
}

void utSurfDumpHeader(CYGFX_SURFACE surf, CYGFX_PALETTE palette, const CYGFX_CHAR* szFileName, const CYGFX_CHAR* szImageName)
{
    CYGFX_U32 nWidth, nHeight;
    CYGFX_U32 flags, nSize, BufferSize, compress, Stride, bpp,
           ColorComponentBits, ColorComponentShift, rlad_bits, ColorFormat;
    CYGFX_U32 pal_count = 0, pal_mode = 0, pal_colorBits = 0;
    CYGFX_U32 pal_colorShift = 0, pal_totalBits = 0, pal_add = 0, pal_size = 0;
    CYGFX_U32 add, ObjectPartitioning, i;
    FILE *fp;
    unsigned char*p;

    fp = fopen(szFileName, "w+");

    if (fp == NULL)
    {
        return;
    }

    nWidth = utSurfWidth(surf);
    nHeight = utSurfHeight(surf);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &add);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &BufferSize);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE, &Stride);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_BITPERPIXEL, &bpp);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORBITS, &ColorComponentBits);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORSHIFT, &ColorComponentShift);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COMPRESSION_FORMAT, &compress);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS, &rlad_bits);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, &ObjectPartitioning);

    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLOR_FORMAT, &ColorFormat);
    if (ColorFormat != CYGFX_SM_COLOR_FORMAT_RGBA)
    {
        return;
    }

    if (palette != NULL)
    {
        CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_COUNT, &pal_count);
        CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_BITPERPIXEL, &pal_totalBits);
        CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_COLORBITS, &pal_colorBits);
        CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_COLORSHIFT, &pal_colorShift);
        CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_VIRT_ADDRESS, &pal_add);

        pal_mode = ((pal_colorBits & 0xffu) > 0u) ? PALETTE_MODE_INDEX_RGBA : PALETTE_MODE_INDEX_RGB;   //backword compatible reason
    }

    if ((compress != CYGFX_SM_COMP_NONE) && (ObjectPartitioning > 1))
    {
        Stride = ObjectPartitioning;
    }
    /* We align the buffer to make life easy */
    pal_size =  pal_count * pal_totalBits / 8;
    pal_size = ((pal_size + 3) / 4) * 4;


    flags = 0;
    nSize = sizeof(SURF_BITMAP) + pal_size + BufferSize;
    if (pal_count)
    {
        nSize += sizeof(SURF_BITMAP_PALETTE);
    }
    if (pal_count)
    {
        flags |= SURF_PROP_PALETTE;
    }
    if (compress == CYGFX_SM_COMP_RLC)
    {
        flags |= SURF_PROP_RLD;
    }
    if (compress == CYGFX_SM_COMP_RLA)
    {
        flags |= SURF_PROP_RLA;
    }
    if (compress == CYGFX_SM_COMP_RLAD)
    {
        flags |= SURF_PROP_RLAD;
        /* We store the RLAD max bits together with the component bits in the variable ColorComponentBits.
           It requires less than 4 bit for each component so we can push the rlad_bits to the other 4 bits. */
        ColorComponentBits |= (rlad_bits << 4);
    }
    if (compress == CYGFX_SM_COMP_RLAD_UNIFORM)
    {
        fclose(fp);
        printf("Error: CYGFX_SM_COMP_RLAD_UNIFORM not supported\n");
        return;
    }

    fprintf(fp, "extern const unsigned char %s[%d];\n\n", szImageName, nSize);

    fprintf(fp, "#ifdef BITMAP_DATA\n\n");

    fprintf(fp, "#include \"flash_resource.h\"\n\n");

    fprintf(fp, "const unsigned char %s[%d] = {\n", szImageName, nSize);
    fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x, /* nSize in bytes (%d) */\n", B(nSize, 0), B(nSize, 1), B(nSize, 2), B(nSize, 3), nSize);
    fprintf(fp, "    0x%02x, 0x%02x, /* nWidth (%d) */\n", B(nWidth, 0), B(nWidth, 1), nWidth);
    fprintf(fp, "    0x%02x, 0x%02x, /* nHeight (%d) */\n", B(nHeight,0), B(nHeight,1), nHeight);
    fprintf(fp, "    0x%02x, 0x%02x, /* strideInByte (%d) */\n", B(Stride, 0), B(Stride, 1), Stride);
    fprintf(fp, "    0x%02x, /* totalBits */\n", bpp);
    fprintf(fp, "    0x%02x, /* flags */\n", flags);
    fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* ColorComponentBits (0x%08x) */\n", B(ColorComponentBits, 0),
        B(ColorComponentBits, 1), B(ColorComponentBits, 2), B(ColorComponentBits, 3), ColorComponentBits);
    fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* ColorComponentShift (0x%08x) */\n", B(ColorComponentShift,0),
        B(ColorComponentShift,1), B(ColorComponentShift,2), B(ColorComponentShift,3), ColorComponentShift);

    if (pal_count)
    {
        /* Optional */
        fprintf(fp, "    0x%02x, 0x%02x, /* pal_count (%d) */\n", B(pal_count, 0), B(pal_count, 1), pal_count);
        fprintf(fp, "    0x%02x, /* pal_mode */\n", pal_mode);
        fprintf(fp, "    0x%02x, /* pal_totalBits (%d) */\n", pal_totalBits, pal_totalBits);
        fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* pal_colorBits (0x%08x) */\n", B(pal_colorBits, 0),
            B(pal_colorBits, 1), B(pal_colorBits, 2), B(pal_colorBits, 3), pal_colorBits);
        fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x,  /* pal_colorShift (0x%08x) */\n", B(pal_colorShift,0),
            B(pal_colorShift,1), B(pal_colorShift,2), B(pal_colorShift,3), pal_colorShift);
        fprintf(fp, "    %          /* Palette data */");
        /* Now the color table */
        UT_HWEB_LOCK((void*)pal_add, pal_size, UT_HWEB_MA_READ);
        PrintVoid(fp, (void*)pal_add, pal_size);
        UT_HWEB_UNLOCK((void*)pal_add);
        fprintf(fp, ",\n");
    }

    p = (unsigned char *)add;
    if ((compress != CYGFX_SM_COMP_NONE) && (ObjectPartitioning > 1))
    {
        /* Optional */
        fprintf(fp, "    /* Object Partitioning Information */\n");
        for (i = 0; i < ObjectPartitioning; i++)
        {
            CYGFX_SURFACE_COLUMN_INFO_S column_info;
            memcpy(&column_info, p, sizeof(column_info));
            p += sizeof(column_info);
            BufferSize -= sizeof(column_info);
            fprintf(fp, "    0x%02x, 0x%02x, /* Column %d reseved */\n", B(column_info.reserved, 0), B(column_info.reserved, 1), i);
            fprintf(fp, "    0x%02x, 0x%02x, /* Column %d width (%d)*/\n", B(column_info.column_width, 0), B(column_info.column_width, 1), i, column_info.column_width);
            fprintf(fp, "    0x%02x, 0x%02x, 0x%02x, 0x%02x, /* Column %d offset (0x%08x) */\n", B(column_info.column_address_offset, 0), B(column_info.column_address_offset, 1), B(column_info.column_address_offset, 2), B(column_info.column_address_offset, 3), i, column_info.column_address_offset);
        }
    }

    fprintf(fp, "    %          /* color / index data */");
    UT_HWEB_LOCK(p, BufferSize, UT_HWEB_MA_READ);
    PrintVoid(fp, p, BufferSize);
    UT_HWEB_UNLOCK(p);

    fprintf(fp, "\n};\n\n");

    fprintf(fp, "#endif /* BITMAP_DATA */\n");

    fclose(fp);
}

#endif /* C_MODEL */

CYGFX_STATIC CYGFX_ERROR utSurfReadPalette(CYGFX_PALETTE palette, void **pImage, CYGFX_U32 *baseAddr)
{
    CYGFX_U08 *addr;
    CYGFX_ERROR ret = CYGFX_OK;
    SURF_BITMAP_PALETTE_FULL *pal;

    if (pImage == 0)
    {
        return CYGFX_ERR;
    }
    if (*pImage == 0)
    {
        return CYGFX_ERR;
    }

    /* Cast the pointer to a SURF_BITMAP structure */
    pal = (SURF_BITMAP_PALETTE_FULL*)*pImage;

    /* only type 1 version 0 supported */
    if((pal->type != 1u) || (pal->version != 0u))
    {
        return CYGFX_ERR;
    }

    if (palette == 0)
    {
        return CYGFX_ERR;
    }
    CyGfx_PaletteReset(palette);

    addr = (CYGFX_U08*)*pImage;
    *pImage = (void*)((CYGFX_U32)*pImage + pal->nSize);

    /* Start calculation of palette data address */
    addr += sizeof(SURF_BITMAP_PALETTE_FULL);

    if (baseAddr != NULL)
    {
        *baseAddr = (CYGFX_U32)addr;
    }

    CyGfx_PaletteSetAttribute(palette, CYGFX_PALETTE_ATTR_COUNT, pal->count);
    CyGfx_PaletteSetAttribute(palette, CYGFX_PALETTE_ATTR_BITPERPIXEL, pal->totalBits);
    CyGfx_PaletteSetAttribute(palette, CYGFX_PALETTE_ATTR_COLORBITS, pal->colorBits);
    CyGfx_PaletteSetAttribute(palette, CYGFX_PALETTE_ATTR_COLORSHIFT, pal->colorShift);

    return ret;

}

CYGFX_ERROR utSurfLoadBitmapEx(CYGFX_SURFACE surface, const void *pImage, CYGFX_BOOL bCopyToVRAM, CYGFX_PALETTE_REGION region_palette)
{
    CYGFX_ERROR ret = CYGFX_OK;
    SURF_BITMAP* bmp;
    SURF_BITMAP_PALETTE *bmp_pal;
    CYGFX_U08 *pPaletteData, *pImageData;
    CYGFX_U32 nSize;
    CYGFX_PALETTE_OBJECT_S palette;
    void *pAddr;
    CYGFX_U32 userdefined = 0;

    if (pImage == 0)
    {
        return CYGFX_ERR;
    }
    if (surface == 0)
    {
        return CYGFX_ERR;
    }
#ifdef C_MODEL
    if (hwi_GetInterface() != HWI_TCP_IP_CMODEL)
    {
        bCopyToVRAM = CYGFX_TRUE;
    }
#endif

    /* Cast the pointer to a SURF_BITMAP structure */
    bmp = (SURF_BITMAP*)pImage;

    /* If (flags & SURF_PROP_PALETTE) is true the SURF_BITMAP_PALETTE structure follows */
    bmp_pal = (SURF_BITMAP_PALETTE*)(bmp + 1);

    CyGfx_SmResetSurfaceObject(surface);

    pImageData = (CYGFX_U08*)pImage + sizeof(SURF_BITMAP);

    if (0 != (bmp->flags & SURF_PROP_PALETTE))
    {
        /* If a palette is included we have to add the SURF_BITMAP_PALETTE size and the palette size */
        CYGFX_U32 nPalSize = (bmp_pal->count * bmp_pal->totalBits) / 8;
        nPalSize = ((nPalSize + 3) / 4) * 4;
        pPaletteData = pImageData + sizeof(SURF_BITMAP_PALETTE);
        pImageData = pPaletteData + nPalSize;
    }

    nSize = (bmp->nSize - (CYGFX_U32)(pImageData - (CYGFX_U08*)pImage));

    UTIL_SUCCESS(ret, CyGfx_SmAssignBuffer(surface, bmp->nWidth, bmp->nHeight, CYGFX_SM_FORMAT_R8G8B8A8, 0, 0));
    /* If the RLD flag is set we need to fill the RLELength field */
    switch (bmp->flags & SURF_PROP_COMPRESSED)
    {
    case SURF_PROP_RLD:
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_COMPRESSION_FORMAT, CYGFX_SM_COMP_RLC));
        break;
    case SURF_PROP_RLA:
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_COMPRESSION_FORMAT, CYGFX_SM_COMP_RLA));
        break;
    case SURF_PROP_RLAD:
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_COMPRESSION_FORMAT, CYGFX_SM_COMP_RLAD));
        break;
    default:
        /* MISRA */
        break;
    }
    if((bmp->flags & SURF_PROP_COMPRESSED) != 0)
    {
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_SIZEINBYTES, nSize));
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_OBJECT_PARTITIONING, bmp->strideInByte));      ///?????
    }
    else
    {
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_STRIDE, bmp->strideInByte));
    }
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_BITPERPIXEL, bmp->totalBits));
    /* This is because we save the RLA MAXBITS in the same variable as ColorComponentBits */
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_COLORBITS, bmp->ColorComponentBits & 0x0f0f0f0f));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS, (bmp->ColorComponentBits>>4) & 0x0f0f0f0f));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_COLORSHIFT, bmp->ColorComponentShift));

    pAddr = NULL;
    if (CYGFX_TRUE == bCopyToVRAM)
    {
        /* Allocate memory in VRAM and copy the data there */
        pAddr = utVideoAlloc(nSize, 8, 0);
        if (pAddr == 0)
        {
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        }
        userdefined |= UTIL_SM_USERDEFINED_VRAM;
    }
#ifdef C_MODEL
    else
    {
        pAddr = utFlashRamAlloc(nSize, 4u, NULL);
        if (pAddr == 0)
        {
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        }
        userdefined |= UTIL_SM_USERDEFINED_FLASH;
    }
#endif

    if (pAddr != NULL)
    {
        UT_HWEB_LOCK(pAddr, nSize, UT_HWEB_MA_WRITE);
        memcpy(pAddr, pImageData, nSize);
        UT_HWEB_UNLOCK(pAddr);
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_VIRT_ADDRESS, (CYGFX_U32)pAddr));
    }
    else
    {
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_PHYS_ADDRESS, (CYGFX_U32)pImageData));
    }

    /* Do the same for the palette if we have one  */
    if (0 != (bmp->flags & SURF_PROP_PALETTE))
    {
        pAddr = pPaletteData;
#ifdef C_MODEL
        /* The c-model requires a memory with virt address. That's way we use VRAM. But we need it only temporary. */
        nSize = bmp_pal->count * ((bmp_pal->totalBits + 7) / 8);
        pAddr = utVideoAlloc(nSize, 8, 0);
        if (pAddr == 0)
        {
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        }
        memcpy(pAddr, pPaletteData, nSize);
#endif
        UTIL_SUCCESS(ret, CyGfx_PaletteSetAttribute(&palette, CYGFX_PALETTE_ATTR_VIRT_ADDRESS, (CYGFX_U32)pAddr));
        UTIL_SUCCESS(ret, CyGfx_PaletteSetAttribute(&palette, CYGFX_PALETTE_ATTR_COUNT, bmp_pal->count));
        UTIL_SUCCESS(ret, CyGfx_PaletteSetAttribute(&palette, CYGFX_PALETTE_ATTR_BITPERPIXEL, bmp_pal->totalBits));
        UTIL_SUCCESS(ret, CyGfx_PaletteSetAttribute(&palette, CYGFX_PALETTE_ATTR_COLORBITS, bmp_pal->colorBits));
        UTIL_SUCCESS(ret, CyGfx_PaletteSetAttribute(&palette, CYGFX_PALETTE_ATTR_COLORSHIFT, bmp_pal->colorShift));
        UTIL_SUCCESS(ret, CyGfx_PaletteAlloc(&palette, region_palette));
        UTIL_SUCCESS(ret, CyGfx_SmPaletteAssign(surface, &palette));
        if ((region_palette & (CYGFX_PALETTE_REGION_DISPLAY | CYGFX_PALETTE_REGION_BLIT)) != 0)
        {
            userdefined |= UTIL_SM_USERDEFINED_PALETTE;
        }
        /* We can delete the buffer and the palette object because it owns the surface */
#ifdef C_MODEL
        utVideoFree(pAddr);
#endif
    }
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_USERDEFINED, userdefined));

    return ret;
}

CYGFX_ERROR utSurfLoadBitmap(CYGFX_SURFACE surface, const void *pImage, CYGFX_BOOL bCopyToVRAM)
{
    return utSurfLoadBitmapEx(surface, pImage, bCopyToVRAM, CYGFX_PALETTE_REGION_ALL);
}

CYGFX_ERROR utSurfLoadPalette(CYGFX_PALETTE palette, const void *pImage, CYGFX_PALETTE_REGION region_palette)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 bufAddr = 0;
    CYGFX_U32 cnt, bitPerPixel;
    #ifdef C_MODEL
    CYGFX_U32 bufferSize;
    void *pAdd1 = 0;
    #endif
    void *pIn = (void *)pImage;

    if (pImage == NULL)
    {
        return CYGFX_ERR;
    }

    /* Read bitmap and get buffer address and palette address */
    ret = utSurfReadPalette(palette, &pIn, &bufAddr);

    if (ret != CYGFX_OK)
    {
        return ret;
    }

    CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_COUNT, &cnt);
    CyGfx_PaletteGetAttribute(palette, CYGFX_PALETTE_ATTR_BITPERPIXEL, &bitPerPixel);

#ifdef C_MODEL
    bufferSize = (cnt * bitPerPixel) / 8;
    {
        /* Allocate memory in FLASH and copy the data there */
        pAdd1 = utFlashRamAlloc(bufferSize, 4u, NULL);
        if (pAdd1 == 0)
        {
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        } else
        {
            UT_HWEB_LOCK(pAdd1, bufferSize, UT_HWEB_MA_WRITE);
            memcpy(pAdd1, (void*)bufAddr, bufferSize);
            UT_HWEB_UNLOCK(pAdd1);
            bufAddr = (CYGFX_U32)pAdd1;
        }
    }
#endif /* C_MODEL */

    UTIL_SUCCESS(ret, CyGfx_PaletteSetAttribute(palette, CYGFX_PALETTE_ATTR_VIRT_ADDRESS, bufAddr));
    UTIL_SUCCESS(ret, CyGfx_PaletteAlloc(palette, region_palette));
    return ret;
}


CYGFX_S32 utSurfWidth(CYGFX_SURFACE surf)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32   val = 0;

    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_WIDTH, &val));
    return (CYGFX_S32)val;
}

CYGFX_S32 utSurfHeight(CYGFX_SURFACE surf)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32   val = 0;

    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_HEIGHT, &val));
    return (CYGFX_S32)val;
}

CYGFX_U32 utSurfAddress(CYGFX_SURFACE surf)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32   val = 0;

    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &val));
    return (CYGFX_U32)val;
}

CYGFX_ERROR utSurfCreateBuffer(CYGFX_SURFACE surf, CYGFX_U32 w, CYGFX_U32 h, CYGFX_SM_FORMAT eFormat)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 size;
    CYGFX_U32 stride;

    void* p;
    void* uv;

    UTIL_SUCCESS(ret, CyGfx_SmResetSurfaceObject(surf));
    UTIL_SUCCESS(ret, CyGfx_SmAssignBuffer(surf, w, h, eFormat, 0, 0));

    /* Use 64 byte alignment to avoid The JPEC driver problem, as  Addresses, Stride and Size must be a multiples of 64 bytes (Manual 2.2.1.4 CYJPG_JPGINFO_S).*/
    {
        UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE, &stride));
        CYGFX_U32 rem = stride % 64;
        if (rem != 0)
        {
            stride = stride + 64 - rem;
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_STRIDE, stride);
        }

        if (eFormat == CYGFX_SM_FORMAT_YUV420)
        {
            UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE_SEC, &stride));
            rem = stride % 64;
            if (rem != 0)
            {
                stride = stride + 64 - rem;
                CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_STRIDE_SEC, stride);
            }
        }
    }
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &size));
    if (size == 0)
    {
        return CYGFX_ERR;
    }
    /* use 8 byte alignment. Only then fast fill is possible. */
    /* use 32 byte alignment to avoid cache problem in TLM tests */
    /* use 64 byte alignment to avoid The JPEC driver problem, as  Addresses, Stride and Size must be a multiples of 64 bytes (Manual 2.2.1.4 CYJPG_JPGINFO_S).*/

    if ((eFormat >= CYGFX_SM_FORMAT_R8G8B8A8) && (eFormat < CYGFX_SM_FORMAT_YUV420))
    {
        p = utVideoAlloc( size, 64, 0 );
        if (p == NULL)
        {
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, 0U);
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        }

        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, (CYGFX_U32)p));
    }
    else if (eFormat == CYGFX_SM_FORMAT_YUV420)
    {
        /* YUV420 semi planar Y buffer */
        p = utVideoAlloc(w * h, 64, 0);
        if (p  == NULL)
        {
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, 0U);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, 0U);
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        }

        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, (CYGFX_U32)p));

        /* UV buffer */
        uv = utVideoAlloc(w * h / 2, 64, 0);
        if (uv  == NULL)
        {
            utVideoFree(p);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, 0U);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, 0U);
            return UTIL_ERR_SM_OUT_OF_MEMORY;
        }

        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, (CYGFX_U32)uv));
    }
    else
    {
        return CYGFX_ERR;
    }

    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_USERDEFINED, UTIL_SM_USERDEFINED_VRAM));
    return ret;
}

CYGFX_ERROR utSurfCreateCompressedBufferBySize(CYGFX_SURFACE surf, CYGFX_U32 w, CYGFX_U32 h, CYGFX_SM_FORMAT eFormat, CYGFX_U32 maxSizeInBytes)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 size, alignment;
    CYGFX_U08 ro, go, bo, ao;
    CYGFX_U08 rc, gc, bc, ac;
    CYGFX_U08 r = 8, g = 8, b = 8, a = 8;
    void* p;
    CYGFX_U32 i;

    UTIL_SUCCESS(ret, CyGfx_SmAssignBuffer(surf, w, h, eFormat, 0, 0));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORBITS, &size));
    ro = (CYGFX_U08)((size>>24) & 0xff);
    go = (CYGFX_U08)((size>>16) & 0xff);
    bo = (CYGFX_U08)((size>> 8) & 0xff);
    ao = (CYGFX_U08)(size & 0xff);
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &size));
    for (i = 0; (i < (32 - 4)) && (size > maxSizeInBytes); i++)
    {
        switch(i % 4)
        {
        case 0: a--;
            break;
        case 1: r--;
            break;
        case 2: b--;
            break;
        case 3: g--;
            break;
        default:
            break;
        }
        rc = CYGFX_MIN(r, ro);
        gc = CYGFX_MIN(g, go);
        bc = CYGFX_MIN(b, bo);
        ac = CYGFX_MIN(a, ao);
        /* Green channel gets the most bits */
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_COMPRESSION_FORMAT, CYGFX_SM_COMP_RLAD_UNIFORM));
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS,
            (((CYGFX_U32)rc)<<24) | (((CYGFX_U32)gc)<<16) | (((CYGFX_U32)bc)<<8) | (CYGFX_U32)ac));
        UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &size));
    }
    if (size > maxSizeInBytes)
    {
        return CYGFX_ERR;
    }

    if (size == 0)
    {
        return CYGFX_ERR;
    }

    alignment = 4;
    p = utVideoAlloc( size, alignment, 0 );
    if (p == NULL)
    {
        return CYGFX_ERR;
    }
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, (CYGFX_U32)p));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_USERDEFINED, UTIL_SM_USERDEFINED_VRAM));

    /* Size property update */
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, size));

    return ret;
}

void utSurfDeleteBuffer(CYGFX_SURFACE surf)
{
    CYGFX_U32 addr, userdefined;

    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_USERDEFINED, &userdefined);

    if ((userdefined & UTIL_SM_USERDEFINED_VRAM) != 0u)
    {
        CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &addr);
        if (addr != 0)
        {
            utVideoFree( (void*)addr);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, 0);
        }
        CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, &addr);
        if (addr != 0)
        {
            utVideoFree( (void*)addr);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, 0);
        }
    }
    if ((userdefined & UTIL_SM_USERDEFINED_FLASH) != 0u)
    {
#ifdef C_MODEL
        CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &addr);
        if (addr != 0)
        {
            utFlashRamFree( (void*)addr);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, 0);
        }
        CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, &addr);
        if (addr != 0)
        {
            utFlashRamFree( (void*)addr);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, 0);
        }
#endif
    }
    if ((userdefined & UTIL_SM_USERDEFINED_PALETTE) != 0)
    {
        CyGfx_SmPaletteFree(surf);
    }

}

#ifdef C_MODEL
CYGFX_ERROR utSurfSavePPM(CYGFX_SURFACE surf, const CYGFX_CHAR *pName)
{
    CYGFX_U32 nWidth, nHeight, x, y;
    FILE *fp;
    CYGFX_U08 r, g, b, a;

    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_WIDTH, &nWidth);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_HEIGHT, &nHeight);

    fp = fopen(pName, "w");
    if (NULL == fp)
    {
        return CYGFX_ERR;
    }

    fprintf(fp, "P3\n#Created by VIDEOSS util lib\n%u %u\n255\n", nWidth, nHeight);
    for (y = 0; y < nHeight; y++)
    {
        for (x = 0; x < nWidth; x++)
        {
            utSurfGetPixel(surf, x, y, &r, &g, &b, &a);
            fprintf(fp, "%d %d %d\n", (CYGFX_S32)r, (CYGFX_S32)g, (CYGFX_S32)b);
        }
    }
    fclose(fp);

    return CYGFX_OK;
}

CYGFX_ERROR utSurfSaveAlphaPPM(CYGFX_SURFACE surf, const CYGFX_CHAR *pName)
{
    CYGFX_U32 nWidth, nHeight, x, y;
    FILE *fp;
    CYGFX_U08 r, g, b, a;

    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_WIDTH, &nWidth);
    CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_HEIGHT, &nHeight);

    fp = fopen(pName, "w");
    if (NULL == fp)
    {
        return CYGFX_ERR;
    }

    fprintf(fp, "P3\n#Created by VIDEOSS util lib\n%u %u\n255\n", nWidth, nHeight);
    for (y = 0; y < nHeight; y++)
    {
        for (x = 0; x < nWidth; x++)
        {
            utSurfGetPixel(surf, x, y, &r, &g, &b, &a);
            fprintf(fp, "%d %d %d\n", (CYGFX_S32)a, (CYGFX_S32)a, (CYGFX_S32)a);
        }
    }
    fclose(fp);

    return CYGFX_OK;
}
#endif /* C_MODEL */

CYGFX_ERROR utSurfGetPixel(CYGFX_SURFACE src, CYGFX_U32 x, CYGFX_U32 y, CYGFX_U08 *r, CYGFX_U08 *g, CYGFX_U08 *b, CYGFX_U08 *a)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 rgba, add, add_sec, stride, w, h, bpp, colorBits, colorShift, comp, colorSpace, nBitPos, i, j, col, nBits, nShift;
    CYGFX_U08 *c[4];
    CYGFX_U32 bytes;

    c[0] = a;
    c[1] = b;
    c[2] = g;
    c[3] = r;

    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_VIRT_ADDRESS, &add));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, &add_sec));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_WIDTH, &w));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_HEIGHT, &h));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_STRIDE, &stride));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_BITPERPIXEL, &bpp));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COLORBITS, &colorBits));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COLORSHIFT, &colorShift));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COMPRESSION_FORMAT, &comp));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COLOR_SPACE, &colorSpace));

    if (ret != CYGFX_OK)
    {
        return ret;
    }

    if ((x >= w) || (y >= h))
    {
        return CYGFX_ERR;
    }

    if (comp != CYGFX_SM_COMP_NONE)
    {
        return CYGFX_ERR;
    }

    if (add_sec != 0)
    {
        return CYGFX_ERR;
    }

    nBitPos = (y * (stride*8)) + (x * bpp);

    if (colorSpace != CYGFX_SM_COLOR_SPACE_RGB)
    {
        /* YUV: read 2 pixels */
        if (x & 1)
        {
            nBitPos -= bpp;
        }
        bpp <<= 1;
    }

    if (bpp > 24)
    {
        bytes = 4;
    }
    else if (bpp > 16)
    {
        bytes = 3;
    }
    else if (bpp > 8)
    {
        bytes = 2;
    }
    else
    {
        bytes = 1;
    }

    UT_HWEB_LOCK(((CYGFX_U08*)add) + (nBitPos >> 3), bytes, UT_HWEB_MA_READ);
    memcpy(&rgba, ((CYGFX_U08*)add) + (nBitPos >> 3), bytes);
    UT_HWEB_UNLOCK(((CYGFX_U08*)add) + (nBitPos >> 3));
    rgba >>= (nBitPos & 0x7U);

    if (bpp < 32U)
    {
        rgba &= (1U<<bpp) - 1U;
    }
    for (i = 0U; i < 4U; i++)
    {
        nBits = colorBits & 0xffU;
        nShift = colorShift & 0xffU;
        col = (rgba>>nShift) & ((1U << nBits) -1U);
        for (j = nBits; ((0 != nBits) && (j < 8U)); j+=nBits)
        {
            col = (col << nBits) | col;
        }
        if (j > 8U)
        {
            col >>= j - 8U;
        }
        *c[i] = (CYGFX_U08)(col & 0xffU);
        colorBits >>= 8U;
        colorShift >>= 8U;
    }

    if (colorSpace != CYGFX_SM_COLOR_SPACE_RGB)
    {
        /* YUV */
        if (x & 1)
        {
            *r = *c[1]; /* Y1 */
        } else
        {
            *r = *c[3]; /* Y0 */
        }
        *g = *c[2]; /* U */
        *b = *c[0]; /* V */
    }
    return ret;
}

CYGFX_ERROR utSurfSetPixel(CYGFX_SURFACE src, CYGFX_U32 x, CYGFX_U32 y, CYGFX_U08 r, CYGFX_U08 g, CYGFX_U08 b, CYGFX_U08 a)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 add, add_sec, stride, w, h, bpp, colorBits, colorShift, comp, nBitPos, nBits, nShift;
    CYGFX_U32 rgba = 0U, rgba_old, mask = 0U;
    CYGFX_U32 bytes;

    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_VIRT_ADDRESS, &add));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, &add_sec));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_WIDTH, &w));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_HEIGHT, &h));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_STRIDE, &stride));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_BITPERPIXEL, &bpp));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COLORBITS, &colorBits));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COLORSHIFT, &colorShift));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(src, CYGFX_SM_ATTR_COMPRESSION_FORMAT, &comp));

    if (ret != CYGFX_OK)
    {
        return ret;
    }

    if ((x >= w) || (y >= h))
    {
        return CYGFX_ERR;
    }

    if (comp != CYGFX_SM_COMP_NONE)
    {
        return CYGFX_ERR;
    }

    if (add_sec != 0)
    {
        return CYGFX_ERR;
    }

    nBitPos = (y * (stride*8)) + (x * bpp);

    nBits = (colorBits>>24) & 0xffU;
    nShift = (colorShift>>24) & 0xffU;
    if(nBits > 8U)
    {
        rgba |= ( ( ((CYGFX_U32)r) << (nBits - 8U) ) | ( ((CYGFX_U32)r) >> (16 - nBits) ) ) << nShift;
    }
    else
    {
        rgba |= ( ((CYGFX_U32)r) >> (8U - nBits) ) << nShift;
    }

    nBits = (colorBits>>16) & 0xffU;
    nShift = (colorShift>>16) & 0xffU;
    if(nBits > 8U)
    {
        rgba |= ( ( ((CYGFX_U32)g) << (nBits - 8U) ) | ( ((CYGFX_U32)g) >> (16 - nBits) ) ) << nShift;
    }
    else
    {
        rgba |= ( ((CYGFX_U32)g) >> (8U - nBits) ) << nShift;
    }

    nBits = (colorBits>>8) & 0xffU;
    nShift = (colorShift>>8) & 0xffU;
    if(nBits > 8U)
    {
        rgba |= ( ( ((CYGFX_U32)b) << (nBits - 8U) ) | ( ((CYGFX_U32)b) >> (16 - nBits) ) ) << nShift;
    }
    else
    {
        rgba |= ( ((CYGFX_U32)b) >> (8U - nBits) ) << nShift;
    }

    nBits = (colorBits>>0) & 0xffU;
    nShift = (colorShift>>0) & 0xffU;
    if(nBits > 8U)
    {
        rgba |= ( ( ((CYGFX_U32)a) << (nBits - 8U) ) | ( ((CYGFX_U32)a) >> (16 - nBits) ) ) << nShift;
    }
    else
    {
        rgba |= ( ((CYGFX_U32)a) >> (8U - nBits) ) << nShift;
    }

    if (bpp != 32U)
    {
        mask = ((1U << bpp) - 1U);
        mask <<= nBitPos & 0x7U;
        mask = ~mask;
    }
    if (bpp > 24)
    {
        bytes = 4;
    }
    else if (bpp > 16)
    {
        bytes = 3;
    }
    else if (bpp > 8)
    {
        bytes = 2;
    }
    else
    {
        bytes = 1;
    }
    rgba <<= (nBitPos & 0x7U);
    UT_HWEB_LOCK(((CYGFX_U08*)add) + (nBitPos >> 3), bytes, UT_HWEB_MA_READ_WRITE);
    memcpy(&rgba_old, ((CYGFX_U08*)add) + (nBitPos >> 3), bytes);
    rgba_old = (rgba_old & mask) | rgba;
    memcpy(((CYGFX_U08*)add) + (nBitPos >> 3), &rgba_old, bytes);
    UT_HWEB_UNLOCK(((CYGFX_U08*)add) + (nBitPos >> 3));

    return ret;
}

CYGFX_ERROR utSurfGenerateGradient(CYGFX_SURFACE surf, CYGFX_U08 rs, CYGFX_U08 gs, CYGFX_U08 bs, CYGFX_U08 as,
    CYGFX_U08 rd, CYGFX_U08 gd, CYGFX_U08 bd, CYGFX_U08 ad, CYGFX_U08 dir)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 nWidth = utSurfWidth(surf);
    CYGFX_U32 nHeight = utSurfHeight(surf);
    CYGFX_U08 r, g, b, a;
    CYGFX_FLOAT v;
    CYGFX_U32 x, y;
    for (y = 0; y < nHeight; y++)
    {
        for (x = 0; x < nWidth; x++)
        {
            if (dir == 0)
                v = (CYGFX_FLOAT)y / (nHeight - 1);
            else
                v = (CYGFX_FLOAT)x / (nWidth - 1);
            r = (CYGFX_U08)(v * (rd-rs) + rs);
            g = (CYGFX_U08)(v * (gd-gs) + gs);
            b = (CYGFX_U08)(v * (bd-bs) + bs);
            a = (CYGFX_U08)(v * (ad-as) + as);
            UTIL_SUCCESS(ret, utSurfSetPixel(surf, x, y, r, g, b, a));
        }
    }
    return ret;
}


CYGFX_ERROR utSurfLoadYUV420Data(CYGFX_SURFACE surface, const void *pImage, CYGFX_U16 nWidth, CYGFX_U16 nHeight)
{
    CYGFX_ERROR ret = CYGFX_OK;
    void *pAddrY;
    void *pAddrUV;
    CYGFX_U08 *pYData, *pUVData;
    CYGFX_U32 userdefined = UTIL_SM_USERDEFINED_VRAM;
    if (pImage == 0)
    {
        return CYGFX_ERR;
    }
    if (surface == 0)
    {
        return CYGFX_ERR;
    }

    /* YUV420 planar input simulation: convert RGBA image to YUV420 planar */
    pAddrY = utVideoAlloc(nWidth * nHeight, 32, 0);
    if (pAddrY  == 0)
    {
        return UTIL_ERR_SM_OUT_OF_MEMORY;
    }
    pAddrUV = utVideoAlloc(nWidth * nHeight / 2, 32, 0);
    if (pAddrUV  == 0)
    {
        return UTIL_ERR_SM_OUT_OF_MEMORY;
    }
    UTIL_SUCCESS(ret, CyGfx_SmResetSurfaceObject(surface));
    UTIL_SUCCESS(ret, CyGfx_SmAssignBuffer(surface, nWidth, nHeight, CYGFX_SM_FORMAT_YUV420, (void*)pAddrY, 0));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_USERDEFINED, userdefined));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_VIRT_ADDRESS, (CYGFX_ADDR)pAddrY));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_VIRT_ADDRESS_SEC, (CYGFX_ADDR)pAddrUV));
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surface, CYGFX_SM_ATTR_COLOR_SPACE, CYGFX_SM_COLOR_SPACE_YUV_BT601_SWING_FULL));

    pYData = (CYGFX_U08 *)pImage;
    pUVData = pYData + (nWidth * nHeight);
    UT_HWEB_LOCK(pAddrY, nWidth * nHeight, UT_HWEB_MA_WRITE);
    memcpy(pAddrY, pYData, nWidth * nHeight);
    UT_HWEB_UNLOCK(pAddrY);

    UT_HWEB_LOCK(pAddrUV, nWidth * nHeight / 2, UT_HWEB_MA_WRITE);
    memcpy(pAddrUV, pUVData , nWidth * nHeight / 2);
    UT_HWEB_UNLOCK(pAddrUV);

    return ret;
}
