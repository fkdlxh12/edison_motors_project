/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

#include "ut_class_rlad.h"

//-----------------------------------------------------------------------------

RLAD::RLAD ()
{
    mode = MODE_RLAD;
    width = height = 1;
    for (unsigned c = 0; c < NUM_C; c++) bpc[c] = cbpc_max[c] = 8;
    dump = false;
    
    decode_BufferTooSmall = false;
    decode_BufferTooLarge = false;
}

//-----------------------------------------------------------------------------

bool RLAD::SetDumpFile (const char *name, bool pixels)
{
    of.open(name);
    dump = of.is_open();
    dump_pixels = dump && pixels;
    return dump;
}

//-----------------------------------------------------------------------------

unsigned RLAD::header_size () const
{
    unsigned size = 0;
    if (mode == MODE_RLA) size += 1 + cnt_width(); // type and cnt field
    for (unsigned i = 0; i < NUM_C; i++) size += cbpc_width(i) + bpc[i]; // cbpc and col_bias fields
    return size;
}

unsigned RLAD::buffer_size () const
{
    switch (mode)
    {
        case MODE_RLA:
        {
            return width * height * (header_size() + bpp());
        }
        default:
        {
            unsigned num_package = ((width / CNT_RLAD) + ((width % CNT_RLAD) ? 1 : 0)) * height;
            return num_package * header_size() + width * height * cbpp_max();
        }
    }
}

//-----------------------------------------------------------------------------

unsigned RLAD::BitStream::Size () const
{
    return (unsigned)((s.size() - 1) * 32 + wpos);
}

unsigned RLAD::BitStream::ReadAvail () const
{
    return Size() - (ridx * 32 + rpos);
}

void RLAD::BitStream::Push (unsigned bits, unsigned data)
{
    if (!bits) return;
    
    unsigned long long d = s.back();
    
    if (be) // big endian
    {
        d <<= 32; d |= (unsigned long long)data << (64 - wpos - bits);
        s.back() = (unsigned)(d >> 32);
        wpos += bits; if (wpos >= 32) { s.push_back((unsigned)d); wpos -= 32; }
    }
    else // little endian
    {
        d |= (unsigned long long)data << wpos;
        s.back() = (unsigned)d;
        wpos += bits; if (wpos >= 32) { s.push_back((unsigned)(d >> 32)); wpos -= 32; }
    }
}

void RLAD::BitStream::Clear ()
{
    s.resize(1);
    s[0] = 0;
    wpos = 0;
    ResetRead();
}

unsigned RLAD::BitStream::Read (unsigned bits, bool *err)
{
    if (!bits) return 0;

    unsigned avail = ReadAvail();
    if (!avail) { if (err) *err = true; return 0; }  // must not read from empty stream
    
    unsigned cbits = bits; if (bits > avail) // consider the case that more bits are read than the stream contains
    {
        cbits = avail;
        if (err) *err= true;
    }

    unsigned pos = rpos;
    unsigned long long d = s[ridx];
    rpos += cbits;
    unsigned data;
    
    if (be) // big endian
    {
        d <<= 32; if (rpos >= 32)
        {
            if (++ridx < s.size()) d |= (unsigned long long)s[ridx];
            rpos -= 32;
        }
        data = (unsigned)(d >> (64 - pos - cbits)) & ((1ll << cbits) - 1);
        
        if (bits > cbits) data <<= (bits - cbits); // msb align in case that more bits are read than extracted from the stream        
    }
    else // little endian
    {
        if (rpos >= 32)
        {
            if (++ridx < s.size()) d |= (unsigned long long)s[ridx] << 32;
            rpos -= 32;
        }
        data = (unsigned)(d >> pos) & ((1ll << cbits) - 1);
    }
    
    return data;
}

void RLAD::BitStream::ResetRead ()
{
    rpos = 0;
    ridx = 0;
}

//-----------------------------------------------------------------------------
// bit arithmetic stuff

unsigned RLAD::SpatialDither (unsigned data_in, unsigned size_in, unsigned size_out, unsigned x, unsigned y, bool exact)
{
    static int bayerMatrix[4][4] =
    {
        {  0,  8,  2, 10 },
        { 12,  4, 14,  6 },
        {  3, 11,  1,  9 },
        { 15,  7, 13,  5 }
    };
    unsigned dith = bayerMatrix[y&3][x&3];
    unsigned dith_bits = 4;

    unsigned data_out = data_in;
    
    // downscale range of possible color codes to fit number that can be
    // displayed by dithering
    if (!exact) data_out -= (data_in >> size_out);

    // align and add dither offset
    // note: no saturation to max code required due to downscaled code range
    int dith_shift = size_in - size_out - dith_bits;
    if (dith_shift < 0) data_out += (dith >> -dith_shift);
    else data_out += (dith << dith_shift);

    // reduce bit size by cutting of lsbits
    data_out >>= (size_in - size_out);
    
    return data_out;
}

unsigned RLAD::MSBitReplication (unsigned data_in, unsigned size_in, unsigned size_out)
{
    if (!size_in) return 0;

    unsigned data_out = 0;

    for (int i = size_out - size_in; i > -(int)size_in; i -= size_in)
    {
        data_out |= (i > 0) ? (data_in << i) : (data_in >> -i);
    }
    
    return data_out;
}

int RLAD::ClampToBpc (unsigned data_in, unsigned bpc)
{
    unsigned data_max = (1 << bpc) - 1;
    if (data_in > data_max) 
        return data_max;
    else
        return data_in;
}

unsigned RLAD::Log2 (unsigned t)
{
    unsigned r = 0;
    while (t) { t >>= 1; r++; };
    return r;
}

//-----------------------------------------------------------------------------
// compute number of bits per component that is actually stored in the stream

void RLAD::set_pbpc (unsigned *pbpc, unsigned *cbpc, unsigned& credit_cnt, unsigned pcnt)
{
    switch (mode)
    {
        case MODE_RL:
        case MODE_RLA:
        {
            for (unsigned c = 0; c < NUM_C; c++) pbpc[c] = cbpc[c];
            break;
        }
        case MODE_RLAD_UNIFORM:
        {
            for (unsigned c = 0; c < NUM_C; c++) pbpc[c] = cbpc_max[c];
            break;
        }
        case MODE_RLAD:
        {
            int d = 0;
            for (unsigned c = 0; c < NUM_C; c++) d += (int)cbpc[c] - (int)cbpc_max[c];
            d *= pcnt;
            
            if ((d <= 0) || (d <= (int)credit_cnt))
            {
                credit_cnt = (int)credit_cnt - d;
                for (unsigned c = 0; c < NUM_C; c++) pbpc[c] = cbpc[c]; 
            }
            else
            {
                for (unsigned c = 0; c < NUM_C; c++) pbpc[c] = cbpc_max[c];
            }
        }
    }
}

//-----------------------------------------------------------------------------

void RLAD::DumpHeader (const char *method)
{
    of << endl;
    of << "# Created by RLAD " << method << " (reference model v" << (double)RLAD_VERSION << ")" << endl;
    of << "#" << endl;
    of << "# CNT_RLAD = " << CNT_RLAD << endl;
    of << "# MAX_CNT_RLA = " << MAX_CNT_RLA << endl;
    of << "#" << endl;
    of << "# mode = " << mode << endl;
    of << "# width x height = " << width << " x " << height << endl;
    of << "# bpc = ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << bpc[c]; of << " )" << endl;
    of << "# cbpc_max = ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << cbpc_max[c]; of << " )" << endl;
    of << "#" << endl;
    of << "# image_size = " << image_size() << " bits" << endl; 
    of << "# buffer_size = " << buffer_size() << " bits (CR = " << compression_rate() << " %)" << endl; 
    of << "#" << endl;
    of << "# index [x,y] type pcnt ( cbpc0 cbpc1 cbpc2 cbpc3 ) credit ( pbpc0 pbpc1 pbpc2 pbpc3 ) ( col0_bias col1_bias col2_bias col3_bias )"; 
    if (dump_pixels) of << " # ( cdat0[0] cdat1[0] cdat2[0] cdat3[0] ) < col0[0] col1[0] col2[0] col3[0] > ... ( cdat0[pcnt-1] cdat1[pcnt-1] cdat2[pcnt-1] cdat3[pcnt-1] ) < col0[pcnt-1] col1[pcnt-1] col2[pcnt-1] col3[pcnt-1] >";
    of << endl;
    of << "#" << endl;
}

//-----------------------------------------------------------------------------

bool RLAD::Encode (Frame& f, BitStream& bs)
{
    if (dump) DumpHeader("Encoder");
    
    bool ok = false;
    
    switch (mode)
    {
        case MODE_RLA:
        {
            ok =  Encode_Lossless(f, bs);
            break;
        }
        case MODE_RLAD_UNIFORM:
        case MODE_RLAD:
        {
            ok = Encode_Lossy(f, bs);
            break;
        }
    }
    
    if (dump)
    {
        unsigned bits = bs.Size();
        unsigned words = (bits / 32) + ((bits % 32) ? 1 : 0);

        of << "#" << endl;
        of << "# stream_size = " << bits << " bits (" << words << " words) (CR = " << ((double)bs.Size() / image_size() * 100) << "%)" << endl;
        of << "#" << endl;
        for (unsigned i = 0; i < words; i++) { unsigned d = bs.Read(32);  char txt[32]; sprintf(txt, "%08X", d); of << txt << endl; }
        
        bs.ResetRead();
    }
    
    return ok;
}

//-----------------------------------------------------------------------------
// HW Encoder for RLA mode
//
// Note: This algorithm is not available yet in HW, but optimized for such implementation,
// because it requires a small pixel fifo only with MAX_CNT_RLA elements.
//
// An encoder algorithm for SW implementation could possibly achieve better results by
// some pre-analysis of the whole input image. By that it may optimize the package
// boundaries.

bool RLAD::Encode_Lossless (Frame& f, BitStream& bs)
{
    assert(!bs.IsBigEndian()); // RLA must be little endian
    
    queue<Frame::Pixel> fifo;
    Package nxt(this), sub(this), all(this);
    
    unsigned xin = 0, yin = 0;
    unsigned xout = 0, yout = 0;
    unsigned pkg = 0;
    
    while (true)
    {
        // read a pixel and put it into the fifo for later bit stream generation
    
        Frame::Pixel pix = f.Read();
        for (int c = 0; c< NUM_C; c++) pix.col[c] &= max_code(c);
        fifo.push(pix);
        
        if (++xin == width) { xin = 0; if (++yin == height) yin = 0; }
        bool end_of_line = (xin == 0), end_of_frame = end_of_line && (yin == 0);
        
        // update package parameters

        sub.Add(pix);

        if (sub.cbpp >= all.cbpp)
        {
            nxt = all;
            sub.Reset();
            sub.Add(pix);
        }

        all.Add(pix);
        
        // generate output package if it seems reasonable from compression rate point of view
        
        if ((nxt.pcnt >= 1) && ((nxt.pcnt + sub.pcnt) > 2) && ((nxt.size + sub.size) <= all.size))
        {
            nxt.Serialize(fifo, bs, pkg, xout, yout);
            all = nxt = sub;
            sub.Reset();
        }

        if ((all.pcnt == MAX_CNT_RLA) || end_of_line)
        {
            all.Serialize(fifo, bs, pkg, xout, yout);
            nxt.Reset(); sub.Reset(); all.Reset();
        }
        
        if (end_of_frame) break;
    }

    return true;
}

void RLAD::Package::Reset ()
{
    pcnt = 0;
    for (int c = 0; c < NUM_C; c++) 
    { 
        cofs[c] = 0;
        crange[c] = 0;
        cbpc[c] = 0;
        start[c] = 0;
        prev[c] = 0; 
        dmin[c] = 0;
        dmax[c] = 0; 
    }
    cbpp = 0;
    size = cfg->header_size();
}

void RLAD::Package::Add (const RLAD::Frame::Pixel& pix)
{
    pcnt++;
    
    for (int c = 0; c < NUM_C; c++)
    {
        if (pcnt == 1)
        {
            cofs[c] = pix.col[c];
            start[c] = pix.col[c];
        }
        else
        {
            // parameters for offset package
        
            unsigned comp = pix.col[c];
            unsigned cwrap = cfg->cwrap(c);
            
            if (comp < cofs[c]) comp += cwrap;
            
            if (comp > (cofs[c] + crange[c]))
            {
                unsigned d1 = (cofs[c] + cwrap) - comp;
                unsigned d2 = comp - (cofs[c] + crange[c]);
                if (d1 < d2) { crange[c] += d1; if (cofs[c] < d1) cofs[c] += cwrap; cofs[c] -= d1; }
                else crange[c] += d2;
            }
            
            // parameters for delta package
            
            int d = (int)pix.col[c] - (int)prev[c];
            if (d < dmin[c]) dmin[c] = d; if (d > dmax[c]) dmax[c] = d;
        }
        prev[c] = pix.col[c];
    }

    // compute package size
    
    unsigned cbpp_ofs = 0, cbpp_delta = 0;
    unsigned cbpc_ofs[NUM_C], cbpc_delta[NUM_C];

    for (int c = 0; c < NUM_C; c++)
    {
        cbpc_ofs[c] = Log2(crange[c]);
        cbpp_ofs += cbpc_ofs[c];
        
        cbpc_delta[c] = Log2((unsigned)(dmax[c] - dmin[c]));
        cbpp_delta += cbpc_delta[c];
    }
    
    delta = (cbpp_delta < cbpp_ofs);
    
    if (delta) for (int c = 0; c < NUM_C; c++) if (cbpc_delta[c] > cfg->bpc[c]) delta = false;
    
    for (int c = 0; c < NUM_C; c++) cbpc[c] = delta ? cbpc_delta[c] : cbpc_ofs[c];
    cbpp = delta ? cbpp_delta : cbpp_ofs;
    
    size = cfg->header_size() + pcnt * cbpp;
}

// write package data to the bit stream

void RLAD::Package::Serialize (queue<RLAD::Frame::Pixel>& fifo, RLAD::BitStream& bs, unsigned& pkg, unsigned& x, unsigned& y)
{
    ostream& of = cfg->of; bool dump = cfg->dump, dump_pixels = cfg->dump_pixels;

    if (dump) of << "# " << pkg << " [" << x << "," << y << "]";
    pkg++;

    // package header
    
    unsigned col_bias[NUM_C];
    for (int c = 0; c < NUM_C; c++) col_bias[c] = delta ? start[c] : cofs[c];

    bs.Push(1, (unsigned)delta); // package type
    bs.Push(cfg->cnt_width(), pcnt - 1); // package size
    for (int c = 0; c < NUM_C; c++) bs.Push(cfg->cbpc_width(c), cbpc[c]); // compressed bits per channel
    for (int c = 0; c < NUM_C; c++) bs.Push(cfg->bpc[c], col_bias[c]); // start values or offsets per channel

    if (dump)
    {
        of << " " << delta << " " << pcnt;
        of << " ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << cbpc[c]; of << " )";
        of << " ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << col_bias[c]; of << " )";
    }
    
    // pixel data

    if (dump_pixels) of << " #";
    
    Frame::Pixel prev;

    for (unsigned i = 0; i < pcnt; i++)
    {
        Frame::Pixel pix = fifo.front(); fifo.pop();
        
        if (dump_pixels) { of << " <"; for (unsigned c = 0; c < NUM_C; c++) of << ' ' << pix.col[c]; of << " > ("; }
        
        for (int c = 0; c < NUM_C; c++)
        {
            unsigned val; if (delta)
            {
                if (i) val = (unsigned)((int)pix.col[c] - (int)prev.col[c] - dmin[c]);
                else val = (unsigned)(-dmin[c]);
            }
            else
            {
                val = pix.col[c];
                if (val < cofs[c]) val += cfg->cwrap(c);
                val -= cofs[c];
            }
            
            bs.Push(cbpc[c], val);
            
            if (dump_pixels) of << ' ' << val;
        }
        
        prev = pix;
        
        if (dump_pixels) of << " )";

        if (++x == cfg->width) { x = 0; y++; }
    }

    if (dump) of << endl;
}

//-----------------------------------------------------------------------------
// HW Encoder for RLAD modes

bool RLAD::Encode_Lossy (Frame& f, BitStream& bs)
{
    assert(!bs.IsBigEndian()); // RLAD must be little endian
    
    unsigned x = 0, y = 0; // current frame position
    unsigned credit_cnt = 0; // credit counter
    unsigned pkg = 0;
    unsigned bpc_mask[NUM_C]; for (int c = 0; c < NUM_C; c++) bpc_mask[c] = (1 << bpc[c]) - 1;
    
    while (true) // package loop
    {
        if (dump) of << "# " << pkg << " [" << x << "," << y << "]";
        
        Frame::Pixel pix[CNT_RLAD];
        unsigned col_bias[NUM_C]; // minimum color code of all pixels in the package
        unsigned crange[NUM_C]; // distance to max color code (considering wrap-around) 
        unsigned pcnt = 0;

        // collect all pixels for one package
                
        for (unsigned i = 0; i < CNT_RLAD; i++)
        {
            pix[i] = f.Read(); pcnt++;
            
            for (unsigned c = 0; c < NUM_C; c++)
            {
                pix[i].col[c] &= bpc_mask[c];
                unsigned comp = pix[i].col[c];
            
                if (i == 0)
                {
                    col_bias[c] = comp;
                    crange[c] = 0;
                }
                else // Note: no wrap-around allowed for RLAD due to dithering
                {
                    if (comp < col_bias[c]) { crange[c] += col_bias[c] - comp; col_bias[c] = comp; }
                    else if (comp > (col_bias[c] + crange[c])) crange[c] = comp - col_bias[c];
                }
            }
            
            if ((x + i) == (width - 1)) break;
        }

        // compute package parameters
        
        unsigned cbpc[NUM_C];
        for (unsigned c = 0; c < NUM_C; c++) cbpc[c] = Log2(crange[c]);
        
        // the following prevents that contrast range gets lost by dithering in offset mode.
        // when, for example, the max color code is one that is dithered, overshoot codes are generated
        // which are clamped. in consequent the mixture of displayed colors darker than the original code.

        for (unsigned c = 0; c < NUM_C; c++) if ((col_bias[c] + (1 << cbpc[c])) > (unsigned)(1 << bpc[c]))
        {
            col_bias[c] = (1 << bpc[c]) - (1 << cbpc[c]);
        }
        
        // write header to bit stream

        for (unsigned c = 0; c < NUM_C; c++) bs.Push(cbpc_width(c), cbpc[c]);
        for (unsigned c = 0; c < NUM_C; c++) bs.Push(bpc[c], col_bias[c]);

        // compute number of bits to store per pixel into the stream

        unsigned pbpc[NUM_C];
        set_pbpc(pbpc, cbpc, credit_cnt, pcnt);
        
        if (dump) // package header
        {
            of << " ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << cbpc[c]; of << " )";
            of << " " << credit_cnt;
            of << " ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << pbpc[c]; of << " )";
            of << " ("; for (unsigned c = 0; c < NUM_C; c++) of << " " << col_bias[c]; of << " )";
        }
        
        // write pixel data
        
        if (dump_pixels) of << " #";
        
        for (unsigned i = 0; i < pcnt; i++)
        {
            if (dump_pixels) { of << " <"; for (unsigned c = 0; c < NUM_C; c++) of << ' ' << pix[i].col[c]; of << " > ("; }
        
            for (unsigned c = 0; c < NUM_C; c++)
            {
                // compute data to store

                unsigned cdat = 0; if (pbpc[c])
                {
                    cdat = pix[i].col[c];
                    if (cdat < col_bias[c]) cdat += (1 << bpc[c]); // wrap-around handling
                    cdat -= col_bias[c];
                    if (pbpc[c] < cbpc[c]) cdat = SpatialDither(cdat, cbpc[c], pbpc[c], x, y, false); // bit width reduction (lossy)
                }
                
                // put into bit stream

                if (dump_pixels) of << ' ' << cdat;
                
                bs.Push(pbpc[c], cdat);
            }

            if (dump_pixels) of << " )";
            
            if (++x == width) x = 0, ++y;
        }

        if (dump) of << endl;

        pkg++;
        if (y == height) break;
    }
    
    return true;
}

//-----------------------------------------------------------------------------
// reference implementation of a decoder for RLA and RLAD, which is
// suitable for HW and SW design

bool RLAD::Decode (BitStream& bs, Frame& f)
{
    assert((mode != MODE_RL) ^ bs.IsBigEndian()); // RL must be big endian, other modes little endian
    
    if (dump) DumpHeader("Decoder");

    decode_BufferTooSmall = false;
    decode_BufferTooLarge = false;

    unsigned x = 0, y = 0; // current frame position
    unsigned credit_cnt = 0; // credit counter
    unsigned pkg = 0;
    bool *err = &decode_BufferTooSmall;

    while (true) // package loop
    {
        // read header
        
        unsigned type, cnt;
        unsigned cbpc[NUM_C], col_bias[NUM_C];
        
        if (mode == MODE_RL) // map the standard RL format to RLAD parameters
        {
            unsigned header = bs.Read(8, err);
            unsigned rl_package = header >> 7;
            cnt = header & 127;
            type = 0; // offset type

            for (unsigned i = 0; i < NUM_C; i++)
            {
                if (rl_package) // run-length package
                {
                    col_bias[i] = bs.Read(bpc[i], err);
                    cbpc[i] = 0;
                }
                else // raw package
                {
                    col_bias[i] = 0;
                    cbpc[i] = bpc[i];
                }
            }
        }
        else
        {
            if (mode == MODE_RLA) { type = bs.Read(1, err); cnt = bs.Read(cnt_width(), err); }
            else { type = 0; cnt = CNT_RLAD - 1; }
        
            for (unsigned i = 0; i < NUM_C; i++)
            {
                cbpc[i] = bs.Read(cbpc_width(i), err);
                if (cbpc[i] > bpc[i]) cbpc[i] = bpc[i]; // required for defined behaviour in case of corrupted stream data
            }
        
            for (unsigned i = 0; i < NUM_C; i++) col_bias[i] = bs.Read(bpc[i], err);
        }
        
        // compute number of pixels that the package contains

        unsigned pcnt = 1 + cnt;
        
        if (mode != MODE_RL)
        {
            if ((x + pcnt) > width) pcnt = width - x; // package always ends at end of line
        }

        // compute number of bits that is stored per pixel in the stream

        unsigned pbpc[NUM_C];
        set_pbpc(pbpc, cbpc, credit_cnt, pcnt);
        
        // dump package header content
        
        if (dump)
        {
            of << "# " << pkg << " [" << x << "," << y << "] " << type << " " << pcnt << " (";
            for (unsigned c = 0; c < NUM_C; c++) of << " " << cbpc[c];
            of << " ) " << credit_cnt << " (";
            for (unsigned c = 0; c < NUM_C; c++) of << " " << pbpc[c];
            of << " ) (";
            for (unsigned c = 0; c < NUM_C; c++) of << " " << col_bias[c];
            of << " )"; if (dump_pixels) of << " #";
        }

        // read pixel data
        
        unsigned col_prev[NUM_C], sofs[NUM_C];

        for (unsigned i = 0; i < pcnt; i++)
        {
            Frame::Pixel pix;

            if (dump_pixels) of << " (";
        
            for (unsigned c = 0; c < NUM_C; c++)
            {
                unsigned cdat = bs.Read(pbpc[c], err);
                
                if (dump_pixels) of << " " << cdat;
                
                if (pbpc[c] < cbpc[c]) cdat = MSBitReplication(cdat, pbpc[c], cbpc[c]); // upscale when data was dithered by encoder
                
                if (type == 0) // offset type
                {
                    pix.col[c] = col_bias[c] + cdat;
                }
                else // delta type
                {
                    if (i == 0) pix.col[c] = col_bias[c], sofs[c] = cdat; // first pixel
                    else pix.col[c] = col_prev[c] + cdat - sofs[c]; // all subsequent pixels
                }
                
                if (mode == MODE_RLA) // wrap-around
                {
                    pix.col[c] &= max_code(c);
                }
                else // clamp
                {
                    if ((int)pix.col[c] < 0) pix.col[c] = 0;
                    else if (pix.col[c] > max_code(c)) pix.col[c] = max_code(c);
                }

                col_prev[c] = pix.col[c];
            }
            
            if (dump_pixels)
            {
                of << " ) <";
                for (unsigned c = 0; c < NUM_C; c++) of << " " << pix.col[c];
                of << " >";
            }
            
            f.Write(pix);

            if (++x == width)
            {
                x = 0; if (++y == height)
                {
                    unsigned av = bs.ReadAvail();
                    if (bs.rpos ? (av > (32 - bs.rpos)) : (av > 0)) decode_BufferTooLarge = true;
                    if (dump) of << endl;
                    return true; // exit
                }
            }
        }
        
        if (dump) of << endl;
        
        pkg++;
    }
}

//-----------------------------------------------------------------------------
