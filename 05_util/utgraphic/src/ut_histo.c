/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        ut_histo.c
 *              Some helper functions for the use of the histogram unit.
 */

/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include <stdio.h>

#include "cygfx_driver_api.h"

#include "ut_histo.h"


/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

#if 0
/* Debug prints on */
#define DBGPRINT(...) printf(__VA_ARGS__)
#endif

#if 1
/* Debug prints off */
#define DBGPRINT(...)
#endif
/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

extern CYGFX_ERROR utHisto2Clut(CYGFX_U32 *pComp0, CYGFX_U32 *pComp1, CYGFX_U32 *pComp2, CYGFX_S16 *pClutR, CYGFX_S16 *pClutG, CYGFX_S16 *pClutB)
{
    CYGFX_S32  i,
            sumTotal,
            sum;


    /* Check input parameters */
    if(NULL == pComp0)
    {
        return CYGFX_ERR;
    }
    if(NULL == pComp1)
    {
        return CYGFX_ERR;
    }
    if(NULL == pComp2)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutR)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutG)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutB)
    {
        return CYGFX_ERR;
    }


    /* Calculate a CLUT (using the histogram data) */
    /* first and last value of the CLUT */
    pClutR[0]  = 0;
    pClutR[32] = 1023;  /* 10 bit CLUT */

    /* other values of the CLUT */
    sumTotal = 0;
    for(i = 0; i < 64; i++)
    {
        sumTotal += (CYGFX_S32)(pComp0[i] + pComp1[i] + pComp2[i]);
    }
    DBGPRINT("sumTotal = %5i\n", sumTotal);

    if (sumTotal == 0)
    {
        return CYGFX_ERR;
    }

    sum = 0;
    for(i = 0; i <= 62; i += 2)
    {
        /* Add sum of next 2 bins */
        sum += (CYGFX_S32)(pComp0[i] + pComp1[i] + pComp2[i] + pComp0[i+1] + pComp1[i+1] + pComp2[i+1]);
        DBGPRINT("i = %i: sum = %5i, %5i + %5i + %5i + %5i + %5i + %5i\n", i, sum, pComp0[i], pComp1[i], pComp2[i], pComp0[i+1], pComp1[i+1], pComp2[i+1]);

        /* Calculate 1 CLUT value */
        pClutR[1 + (i/2)] = (CYGFX_S16)((sum * 1023) / sumTotal);
        DBGPRINT("i = %i: clut33Red[%i] = %i\n", i, (1 + i/2), pClutR[1 + i/2]);
    }


    /* "lower" the CLUT (decrease deviations from standard) */
    /* pClutR[32] keeps 1023 */
    for(i = 0; i < 32; i++)
    {
        pClutR[i] = (pClutR[i] + (CYGFX_S16)((i * 1023)/32) ) / 2;
    }

    /* use same CLUT for Red, Green and Blue */
    for(i = 0; i < 33; i++)
    {
        pClutG[i] = pClutR[i];
        pClutB[i] = pClutR[i];
        DBGPRINT("pClutR[%i] = %i\n", i, pClutR[i]);
    }


    return CYGFX_OK;
}


extern CYGFX_ERROR utHisto2BendsClut(CYGFX_U32 lowBend, CYGFX_U16 lowClut, CYGFX_U32 highBend, CYGFX_U16 highClut, CYGFX_S16 *pClutR, CYGFX_S16 *pClutG, CYGFX_S16 *pClutB)
{
    CYGFX_U32  i;


    /* Check input parameters */

    if( (lowBend < 1U) || (30U < lowBend) )
    {
        return CYGFX_ERR;
    }
    if( (highBend < 2U) || (31U < highBend) )
    {
        return CYGFX_ERR;
    }
    if(highBend <= lowBend)
    {
        return CYGFX_ERR;
    }
    if(highClut <= lowClut)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutR)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutG)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutB)
    {
        return CYGFX_ERR;
    }

    /* Section at the lower end */
    for(i = 0U; i <= lowBend; i++)
    {
        pClutR[i] = (CYGFX_S16)(   0U + (       (i * lowClut)/lowBend) );  /* 0, ... lowClut */
    }

    /* Section at the high end */
    for(i = 32U; highBend <= i; i--)
    {

        pClutR[i] = (CYGFX_S16)(1023U - ( ((32U - i) * (1023U - highClut))/(32U - highBend)) );  /* 1023, ...highClut */
    }

    /* Section in the middle (linear interpolation from lowClut to highClut) */
    for(i = (lowBend + 1U); i < highBend; i++)
    {
        pClutR[i] = (CYGFX_S16)(pClutR[lowBend] + ( ((CYGFX_S16)(i - lowBend) * (pClutR[highBend] - pClutR[lowBend])) / (CYGFX_S16)(highBend - lowBend)) );
    }

    for(i = 0U; i < 33U; i++)
    {
        pClutG[i] = pClutR[i];
        pClutB[i] = pClutR[i];
        DBGPRINT("pClutR[%i] = %i\n", i, pClutR[i]);
    }


    return CYGFX_OK;
}


extern CYGFX_ERROR utHistoGaussClut(CYGFX_U32 *pComp0, CYGFX_U32 *pComp1, CYGFX_U32 *pComp2, CYGFX_S16 *pClutR, CYGFX_S16 *pClutG, CYGFX_S16 *pClutB)
{
    CYGFX_U32  i,
            j,
            tmp,
            sum,
            sumTotal;


    /* Check input parameters */
    if(NULL == pComp0)
    {
        return CYGFX_ERR;
    }
    if(NULL == pComp1)
    {
        return CYGFX_ERR;
    }
    if(NULL == pComp2)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutR)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutG)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutB)
    {
        return CYGFX_ERR;
    }


    /* Calculate a CLUT (using the histogram data) */
    /* first and last value of the CLUT */
    pClutR[0]  = 0;
    pClutR[32] = 1023;  /* 10 bit CLUT */

    /* other values of the CLUT */
    sumTotal = 0;
    for(i = 0U; i < 64U; i++)
    {
        sumTotal += (pComp0[i] + pComp1[i] + pComp2[i]);
    }
    DBGPRINT("sumTotal = %5i\n", sumTotal);

    sum = 0U;
    for(i = 0U; i <= 62U; i += 2U)
    {
        /* Add sum of next 2 bins */
        sum += (pComp0[i] + pComp1[i] + pComp2[i] + pComp0[i+1] + pComp1[i+1] + pComp2[i+1]);
        DBGPRINT("i = %i: sum = %5i, %5i + %5i + %5i + %5i + %5i + %5i\n", i, sum, pComp0[i], pComp1[i], pComp2[i], pComp0[i+1], pComp1[i+1], pComp2[i+1]);

        /* Calculate 1 CLUT value */
        if (0 == sumTotal)
        {
            return CYGFX_ERR;
        }
        pClutR[1U + (i/2U)] = (CYGFX_S16)( (sum * 1023U) / sumTotal );
        DBGPRINT("i = %i: clut33Red[%i] = %i\n", i, (1 + i/2), pClutR[1 + i/2]);
    }

    for(i = 0U; i < 33U; i++)
    {
        pClutG[i] = pClutR[i];
        pClutB[i] = pClutR[i];
        DBGPRINT("pClutR[%i] = %i\n", i, pClutR[i]);
    }


    /* calculate a cumulative Gauss distribution */
    {
        CYGFX_DOUBLE mean = 153.0/8.0,  /* 153 is for 256 values */
                  sigma = 60.0/8.0;  /*  60 is for 256 values */
        CYGFX_DOUBLE fact = 1.0/(2.0 * (sigma/33.0) * (sigma/33.0));
        CYGFX_U32 total = 0U,
               gauss[33],
               cum[33] = {0U};

        /* find mean using the histogram data */
        {
            tmp = 0U;
            for(i = 1U; i < 33U; i++)
            {
                tmp += (CYGFX_U32)(pClutR[i] - pClutR[i - 1U]);
                DBGPRINT("i = %i tmp = %i, pClutR[i] = %i\n", i, tmp, pClutR[i]);
            }
            j = tmp/2U;
            tmp = 0U;
            for(i = 1U; i < 33U; i++)
            {
                tmp += (CYGFX_U32)(pClutR[i] - pClutR[i - 1U]);
                DBGPRINT("i = %i limit = %i tmp = %i, pClutR[i] = %i\n", i, j, tmp, pClutR[i]);
                if (j <= tmp)
                {
                    break;
                }
            }
            DBGPRINT("Mean =  %i limit = %i tmp = %i\n", i, j, tmp);
            mean = (CYGFX_DOUBLE)i;
        }
        /* find sigma using the histogram data */
        {
            j = i;  /* save mean */
            tmp = 0U;
            for(i = 0U; i < 33U; i++)
            {
                if (i >= j)
                {
                    tmp += ( (i - j) * (i - j) );
                    DBGPRINT("i = %i tmp = %i, (i - j) = %i\n", i, tmp, (i - j));
                }
                else
                {
                    tmp += ( (j - i) * (j - i) );
                    DBGPRINT("i = %i tmp = %i, (j - i) = %i\n", i, tmp, (j - i));
                }
            }
            sigma = sqrt( ((CYGFX_DOUBLE)tmp)/32.0 );
            DBGPRINT("sigma: %i\n", (CYGFX_U32)sigma);
        }
        fact = 1.0/(2.0 * (sigma/33.0) * (sigma/33.0));

        for(i = 0U; i < 33U; i++)
        {
            gauss[i] = (CYGFX_U16)((10230U) * exp( (-((((CYGFX_DOUBLE)i-mean)/33.0) * (((CYGFX_DOUBLE)i-mean)/33.0)) * fact) ) );
            total += gauss[i];
            cum[i] = total;
            DBGPRINT("%i %i %i\n", gauss[i], cum[i], total);
        }

        /* adjust CLUT (for every index of the CLUT (using the histogram data) find the index in the cumulative Gauss distribution) */
        for (i = 1U; i < 32U; i++)  /* 1...31 */
        {
            j = 0;
            while( (j < 33U) && (((1023 * (CYGFX_S32)cum[j])/(CYGFX_S32)cum[32]) <= pClutR[i]) )
            {
                DBGPRINT("i = %i, j = %i, cum[j] = %i, cum[32] = %i, (1023 * cum[j])/cum[32] = %i, pClutR[i] = %i\n", i, j, cum[j], cum[32], ((1023 * cum[j])/cum[32]), pClutR[i] );
                j++;
            }
            DBGPRINT("i: %i, j: %i -> %i\n", i, j, ((j * 1023)/32) );
            pClutG[i] = (CYGFX_S16)((j * 1023U)/32U);
        }
        for(i = 0U; i < 33U; i++)
        {
            pClutR[i] = pClutG[i];
            pClutB[i] = pClutG[i];
            DBGPRINT("pClutR[%i] = %i\n", i, pClutR[i]);
        }
    }


    return CYGFX_OK;
}


extern CYGFX_ERROR utGammaClut(CYGFX_FLOAT gammaValue, CYGFX_S16 *pClutR, CYGFX_S16 *pClutG, CYGFX_S16 *pClutB)
{
    CYGFX_U32 i;


    /* Check input parameters */
    if(gammaValue < 0.1f)
    {
        return CYGFX_ERR;
    }
    if(10.0f < gammaValue)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutR)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutG)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutB)
    {
        return CYGFX_ERR;
    }


    /* assign/calculate CLUT values */
    pClutR[0]  = 0;
    pClutR[32] = 1023;
    /* Formula: a**r = exp(r * ln(a)) */
    for(i = 1; i < 32; i++)
    {
        pClutR[i] = (CYGFX_S16)( exp( ((CYGFX_DOUBLE)gammaValue) * log(((CYGFX_DOUBLE)i)/32.0) ) * 1023.0);
    }

    for(i = 0U; i < 33U; i++)
    {
        pClutG[i] = pClutR[i];
        pClutB[i] = pClutR[i];
        DBGPRINT("pClutR[%i] = %i\n", i, pClutR[i]);
    }


    return CYGFX_OK;
}


extern CYGFX_ERROR ut2GammaClut(CYGFX_U32 x, CYGFX_U32 y, CYGFX_FLOAT gammaValue, CYGFX_S16 *pClutR, CYGFX_S16 *pClutG, CYGFX_S16 *pClutB)
{
    CYGFX_U32 i;


    /* Check input parameters */
    if( (x < 1U) || (31U < x) )
    {
        return CYGFX_ERR;
    }
    if( (y < 1U) || (1022U < y) )
    {
        return CYGFX_ERR;
    }
    if(gammaValue < 0.1f)
    {
        return CYGFX_ERR;
    }
    if(10.0f < gammaValue)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutR)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutG)
    {
        return CYGFX_ERR;
    }
    if(NULL == pClutB)
    {
        return CYGFX_ERR;
    }

    /* assign/calculate CLUT values */
    pClutR[0]  = 0;
    pClutR[x]  = (CYGFX_S16)y;
    pClutR[32] = 1023;

    /* Formula: a**r = exp(r * ln(a)) */
    for(i = 1; i < x; i++)
    {
        pClutR[i] = (CYGFX_S16)( exp( ((CYGFX_DOUBLE)gammaValue) * log(((CYGFX_DOUBLE)i) / ((CYGFX_DOUBLE)x)) ) * ((CYGFX_DOUBLE)y) );
    }
    for(i = (x + 1U); i < 32U; i++)
    {
        pClutR[i] = (CYGFX_S16)((CYGFX_DOUBLE)y + ( exp( (1.0/(CYGFX_DOUBLE)gammaValue) * log(((CYGFX_DOUBLE)(i - x)) / ((CYGFX_DOUBLE)(32 - x))) ) * ((CYGFX_DOUBLE)(1023 - y)) ) );
    }

    for(i = 0U; i < 33U; i++)
    {
        pClutG[i] = pClutR[i];
        pClutB[i] = pClutR[i];
        DBGPRINT("pClutR[%i] = %i\n", i, pClutR[i]);
    }


    return CYGFX_OK;
}

