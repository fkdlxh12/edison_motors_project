/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        de_util.c
 * \brief       Define helper functions for drawing.
 */

/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "cygfx_driver_api.h"

#include "de_util.h"
#include "sm_util.h"
#include "pe_matrix.h"
#include "ut_compatibility.h"


/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/
#if 0
/* Debug prints on */
#define DBGPRINT(...) printf(__VA_ARGS__)
#endif

#if 1
/* Debug prints off */
#define DBGPRINT(...)
#endif

#define AL 6
#define AW 3

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

static CYGFX_FLOAT DegreeToPI = 3.14159265f / 180.0f; /*(atan(1.0) * 4.0 / 180)*/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/
static CYGFX_ERROR util_GenerateRainbow(CYGFX_SURFACE surf);

CYGFX_ERROR utDePathMoveTo(CYGFX_BE_CONTEXT beCtx,
                                     CYGFX_FLOAT x0,
                                     CYGFX_FLOAT y0)
{
    CYGFX_ERROR ret = CYGFX_OK;
    static const CYGFX_U08 PathSegment = CYGFX_DE_INSTR_MOVE_TO_ABS;
    CYGFX_FLOAT values[2];

    DBGPRINT("# M %f, %f\n", x0, y0);

    values[0] = x0;
    values[1] = y0;
    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(beCtx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(beCtx, 1, &PathSegment, values));
    return ret;
}

CYGFX_ERROR utDePathLineTo(CYGFX_BE_CONTEXT beCtx,
                                     CYGFX_FLOAT x0,
                                     CYGFX_FLOAT y0)
{
    CYGFX_ERROR ret = CYGFX_OK;
    static const CYGFX_U08 PathSegment = CYGFX_DE_INSTR_LINE_TO_ABS;
    CYGFX_FLOAT values[2];

    DBGPRINT("# L %f, %f\n", x0, y0);

    values[0] = x0;
    values[1] = y0;
    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(beCtx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(beCtx, 1, &PathSegment, values));
    return ret;
}

CYGFX_ERROR utDePathQuadraticBezierTo(CYGFX_BE_CONTEXT beCtx,
                                                CYGFX_FLOAT x0,
                                                CYGFX_FLOAT y0,
                                                CYGFX_FLOAT x1,
                                                CYGFX_FLOAT y1)
{
    CYGFX_ERROR ret = CYGFX_OK;
    static const CYGFX_U08 PathSegment = CYGFX_DE_INSTR_QUAD_TO_ABS;
    CYGFX_FLOAT values[4];

    DBGPRINT("# Q %f, %f %f, %f\n", x0, y0, x1, y1);

    values[0] = x0;
    values[1] = y0;
    values[2] = x1;
    values[3] = y1;
    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(beCtx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(beCtx, 1, &PathSegment, values));
    return ret;
}

CYGFX_ERROR utDePathCubicBezierTo(CYGFX_BE_CONTEXT beCtx,
                                   CYGFX_FLOAT x0,
                                   CYGFX_FLOAT y0,
                                   CYGFX_FLOAT x1,
                                   CYGFX_FLOAT y1,
                                   CYGFX_FLOAT x2,
                                   CYGFX_FLOAT y2)
{
    CYGFX_ERROR ret = CYGFX_OK;
    static const CYGFX_U08 PathSegment = CYGFX_DE_INSTR_CUBIC_TO_ABS;
    CYGFX_FLOAT values[6];

    DBGPRINT("# C %f, %f %f, %f %f, %f\n", x0, y0, x1, y1, x2, y2);

    values[0] = x0;
    values[1] = y0;
    values[2] = x1;
    values[3] = y1;
    values[4] = x2;
    values[5] = y2;
    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(beCtx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));
    UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(beCtx, 1, &PathSegment, values));
    return ret;
}

CYGFX_ERROR utDeDrawRect(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT w, CYGFX_FLOAT h)
{
    CYGFX_ERROR ret = CYGFX_OK;
    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, 0, 0));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, 0,  0+h));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, 0+w, 0+h));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, 0+w, 0));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, 0, 0));
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, x, y));
    return ret;
}

CYGFX_ERROR utDeDrawRoundRect(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT w, CYGFX_FLOAT h, CYGFX_FLOAT r)
{
    CYGFX_ERROR ret = CYGFX_OK;
    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, 0, r));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, 0, 0, r, 0));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, w-r, 0));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, w, 0, w, r));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, w, h-r));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, w, h, w-r, h));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, r, h));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, 0, h, 0, h-r));
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, x, y));
    return ret;
}

CYGFX_ERROR utDeDrawCircle(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT r)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT angle = 0;
    static const CYGFX_U32 cnt = 8;
    CYGFX_U32 i;
    CYGFX_FLOAT x1, y1;
    CYGFX_FLOAT x2, y2;
    CYGFX_FLOAT r2;

    r2 = r / cosf((DegreeToPI * 180.0f) / (CYGFX_FLOAT)cnt);

    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x, y + r));

    for (i = 0; i < cnt; i++)
    {
        angle = (360.0f * ((CYGFX_FLOAT)i+0.5f)) / (CYGFX_FLOAT)cnt;
        x1 = x + (sinf(DegreeToPI * angle) * r2);
        y1 = y + (cosf(DegreeToPI * angle) * r2);

        angle = (360.0f * ((CYGFX_FLOAT)i+1)) / (CYGFX_FLOAT)cnt;
        x2 = x + (sinf(DegreeToPI * angle) * r);
        y2 = y + (cosf(DegreeToPI * angle) * r);
        UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1, y1, x2, y2));
    }
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}

CYGFX_ERROR utDeDrawArc(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT ri, CYGFX_FLOAT ro, CYGFX_FLOAT as, CYGFX_FLOAT ae)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT angle = 0;
    static const CYGFX_U32 cnt = 8;
    CYGFX_U32 i;
    CYGFX_FLOAT x1, y1;
    CYGFX_FLOAT x2, y2;
    CYGFX_FLOAT r2;
    CYGFX_FLOAT fCos;

    as *= DegreeToPI;
    ae *= DegreeToPI;

    if (as == ae)
    {
        return CYGFX_OK;
    }
    fCos = cosf((ae - as) / (CYGFX_FLOAT)(cnt * 2));
    if (fCos == 0.0f)
    {
        return CYGFX_ERR;
    }

    r2 = ro / fCos;

    angle = as;
    x2 = x + (sinf(angle) * ro);
    y2 = y + (cosf(angle) * ro);

    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x2, y2));

    for (i = 0; i < cnt; i++)
    {
        angle = as + (((ae - as) * ((CYGFX_FLOAT)i+0.5f)) / (CYGFX_FLOAT)cnt);
        x1 = x + (sinf(angle) * r2);
        y1 = y + (cosf(angle) * r2);

        angle = as + (((ae - as) * ((CYGFX_FLOAT)i+1.0f)) / (CYGFX_FLOAT)cnt);
        x2 = x + (sinf(angle) * ro);
        y2 = y + (cosf(angle) * ro);

        UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1, y1, x2, y2));
    }
    if (ri > 0)
    {
        r2 = ri / cosf((ae - as) / (CYGFX_FLOAT)(cnt * 2));
        angle = ae;
        x2 = x + (sinf(angle) * ri);
        y2 = y + (cosf(angle) * ri);
        UTIL_SUCCESS(ret, utDePathLineTo(ctx, x2, y2));
        for (i = 0; i < cnt; i++)
        {
            angle = ae + (((as - ae) * ((CYGFX_FLOAT)i+0.5f)) / (CYGFX_FLOAT)cnt);
            x1 = x + (sinf(angle) * r2);
            y1 = y + (cosf(angle) * r2);

            angle = ae + (((as - ae) * ((CYGFX_FLOAT)i+1.0f)) / (CYGFX_FLOAT)cnt);
            x2 = x + (sinf(angle) * ri);
            y2 = y + (cosf(angle) * ri);

            UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1, y1, x2, y2));
        }
    }
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}


CYGFX_ERROR utDeDrawFlower(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT r1, CYGFX_FLOAT r2, CYGFX_U32 cnt)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT angle = 0;
    CYGFX_U32 i;
    CYGFX_FLOAT x1, y1;
    CYGFX_FLOAT x2, y2;

    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x, y + r1));

    for (i = 0; i < cnt; i++)
    {
        angle = (360.0f * ((CYGFX_FLOAT)i+0.5f)) / (CYGFX_FLOAT)cnt;
        x1 = x + (sinf(DegreeToPI * angle) * r2);
        y1 = y + (cosf(DegreeToPI * angle) * r2);

        angle = (360.0f * ((CYGFX_FLOAT)i+1.0f)) / (CYGFX_FLOAT)cnt;
        x2 = x + (sinf(DegreeToPI * angle) * r1);
        y2 = y + (cosf(DegreeToPI * angle) * r1);
        UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1, y1, x2, y2));
    }
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}

CYGFX_ERROR utDeDrawStar(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT r1, CYGFX_FLOAT r2, CYGFX_U32 cnt)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT angle = 0;
    CYGFX_U32 i;
    CYGFX_FLOAT x1, y1;
    CYGFX_FLOAT x2, y2;

    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x, y + r1));

    for (i = 0; i < cnt; i++)
    {
        angle = (360.0f * ((CYGFX_FLOAT)i+0.5f)) / (CYGFX_FLOAT)cnt;
        x1 = x + (sinf(DegreeToPI * angle) * r2);
        y1 = y + (cosf(DegreeToPI * angle) * r2);
        UTIL_SUCCESS(ret, utDePathLineTo(ctx, x1, y1));

        angle = (360.0f * ((CYGFX_FLOAT)i+1.0f)) / (CYGFX_FLOAT)cnt;
        x2 = x + (sinf(DegreeToPI * angle) * r1);
        y2 = y + (cosf(DegreeToPI * angle) * r1);
        UTIL_SUCCESS(ret, utDePathLineTo(ctx, x2, y2));
    }
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}

CYGFX_ERROR utDeArrow(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x0, CYGFX_FLOAT y0, CYGFX_FLOAT x1, CYGFX_FLOAT y1, CYGFX_FLOAT w)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT dx = x1-x0;
    CYGFX_FLOAT dy = y1-y0;
    CYGFX_FLOAT len = (CYGFX_FLOAT)sqrt((dx*dx) + (dy*dy));
    
    if (len == 0.0f)
    {
        return CYGFX_ERR;
    }
    dx *= (w / len) / 2;
    dy *= (w / len) / 2;
    x0 += 0.5f;
    y0 += 0.5f;
    x1 += 0.5f;
    y1 += 0.5f;


    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x0 - dy, y0 + dx));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x1 - dy) - (AL * dx), (y1 + dx) - (AL * dy) ));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x1 - (AW * dy)) - (AL * dx), (y1 + (AW * dx)) - (AL * dy)));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x1, y1));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x1 + (AW * dy)) - ( AL * dx), (y1 - (AW * dx)) - (AL * dy)));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x1 + dy) - (AL * dx), (y1 - dx) - (AL * dy)));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x0 + dy, y0 - dx));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x0 - dy, y0 + dx));
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}

CYGFX_ERROR utDeLine(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x0, CYGFX_FLOAT y0, CYGFX_FLOAT x1, CYGFX_FLOAT y1, CYGFX_FLOAT w)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT dx = x1-x0;
    CYGFX_FLOAT dy = y1-y0;
    CYGFX_FLOAT len = (CYGFX_FLOAT)sqrt((dx*dx) + (dy*dy));

    if (len == 0.0f)
    {
        return CYGFX_ERR;
    }
    dx *= (w / len) / 2;
    dy *= (w / len) / 2;
    x0 += 0.5f;
    y0 += 0.5f;
    x1 += 0.5f;
    y1 += 0.5f;

    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x0 - dy, y0 + dx));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x1 - dy, y1 + dx));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x1 + dy, y1 - dx));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x0 + dy, y0 - dx));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x0 - dy, y0 + dx));
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}

CYGFX_ERROR utDeQuad(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x0, CYGFX_FLOAT y0, CYGFX_FLOAT x1, CYGFX_FLOAT y1, CYGFX_FLOAT x2, CYGFX_FLOAT y2, CYGFX_FLOAT w)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT dx1 = x1-x0;
    CYGFX_FLOAT dy1 = y1-y0;
    CYGFX_FLOAT len1 = (CYGFX_FLOAT)sqrt((dx1*dx1) + (dy1*dy1));
    CYGFX_FLOAT dx2 = x2-x1;
    CYGFX_FLOAT dy2 = y2-y1;
    CYGFX_FLOAT len2 = (CYGFX_FLOAT)sqrt((dx2*dx2) + (dy2*dy2));

    if ((len1 == 0.0f) || (len2 == 0.0f))
    {
        return CYGFX_ERR;
    }
    dx1 *= (w / len1) / 2;
    dy1 *= (w / len1) / 2;
    dx2 *= (w / len2) / 2;
    dy2 *= (w / len2) / 2;
    x0 += 0.5f;
    y0 += 0.5f;
    x1 += 0.5f;
    y1 += 0.5f;
    x2 += 0.5f;
    y2 += 0.5f;

    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x0 - dy1, y0 + dx1));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1 - ((dy2 + dy1) /2), y1 + ((dx2 + dx1)/2), x2 - dy2, y2 + dx2));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x2 + dy2, y2 - dx2));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1 + ((dy1 + dy2)/2), y1 - ((dx1 + dx2)/2), x0 + dy1, y0 - dx1));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x0 - dy1, y0 + dx1));
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}

CYGFX_ERROR utDeQuadArrow(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x0, CYGFX_FLOAT y0, CYGFX_FLOAT x1, CYGFX_FLOAT y1, CYGFX_FLOAT x2, CYGFX_FLOAT y2, CYGFX_FLOAT w)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT dx1 = x1-x0;
    CYGFX_FLOAT dy1 = y1-y0;
    CYGFX_FLOAT len1 = (CYGFX_FLOAT)sqrt((dx1*dx1) + (dy1*dy1));
    CYGFX_FLOAT dx2 = x2-x1;
    CYGFX_FLOAT dy2 = y2-y1;
    CYGFX_FLOAT len2 = (CYGFX_FLOAT)sqrt((dx2*dx2) + (dy2*dy2));

    if ((len1 == 0.0f) || (len2 == 0.0f))
    {
        return CYGFX_ERR;
    }

    dx1 *= (w / len1) / 2;
    dy1 *= (w / len1) / 2;
    dx2 *= (w / len2) / 2;
    dy2 *= (w / len2) / 2;
    x0 += 0.5f;
    y0 += 0.5f;
    x1 += 0.5f;
    y1 += 0.5f;
    x2 += 0.5f;
    y2 += 0.5f;


    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, x0 - dy1, y0 + dx1));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1 - ((dy2 + dy1) /2), y1 + ((dx2 + dx1)/2),
                    (x2 - dy2) - (AL * dx2), (y2 + dx2) - (AL * dy2)));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x2 - (AW * dy2)) - ( AL * dx2), (y2 + (AW * dx2)) - (AL * dy2)));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x2, y2));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x2 + (AW * dy2)) - ( AL * dx2), (y2 - (AW * dx2)) - (AL * dy2)));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, (x2 + dy2) - (AL * dx2), (y2 - dx2) - (AL * dy2)));
    UTIL_SUCCESS(ret, utDePathQuadraticBezierTo(ctx, x1 + ((dy1 + dy2)/2), y1 - ((dx1 + dx2)/2),
                    x0 + dy1, y0 - dx1));
    UTIL_SUCCESS(ret, utDePathLineTo(ctx, x0 - dy1, y0 + dx1));
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    return ret;
}


CYGFX_ERROR utDeDrawForm(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT xo, CYGFX_FLOAT yo, const CYGFX_CHAR* pFigure)
{
    CYGFX_ERROR ret = CYGFX_OK;

    CYGFX_U08 Mode = 0;
    const CYGFX_CHAR* ptr;

    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(ctx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));


    for(ptr = pFigure; (*ptr != 0) && (ret == CYGFX_OK); ptr++)
    {
        if (0 != isspace(*ptr))
        {
            continue;
        }
        if (*ptr == ',')
        {
            continue;
        }
        if (*ptr == 'M')
        {
            Mode = CYGFX_DE_INSTR_MOVE_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'm')
        {
            Mode = CYGFX_DE_INSTR_MOVE_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'L')
        {
            Mode = CYGFX_DE_INSTR_LINE_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'l')
        {
            Mode = CYGFX_DE_INSTR_LINE_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'H')
        {
            Mode = CYGFX_DE_INSTR_HLINE_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'h')
        {
            Mode = CYGFX_DE_INSTR_HLINE_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'V')
        {
            Mode = CYGFX_DE_INSTR_VLINE_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'v')
        {
            Mode = CYGFX_DE_INSTR_VLINE_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'Q')
        {
            Mode = CYGFX_DE_INSTR_QUAD_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'q')
        {
            Mode = CYGFX_DE_INSTR_QUAD_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'T')
        {
            Mode = CYGFX_DE_INSTR_SQUAD_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 't')
        {
            Mode = CYGFX_DE_INSTR_SQUAD_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'C')
        {
            Mode = CYGFX_DE_INSTR_CUBIC_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'c')
        {
            Mode = CYGFX_DE_INSTR_CUBIC_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 'S')
        {
            Mode = CYGFX_DE_INSTR_SCUBIC_TO_ABS;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if (*ptr == 's')
        {
            Mode = CYGFX_DE_INSTR_SCUBIC_TO_REL;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &Mode, 0));
        }
        else if ((*ptr == 'z') || (*ptr == 'Z'))
        {
        }
        else
        {
            CYGFX_CHAR *pEnd;
            CYGFX_FLOAT val = (CYGFX_FLOAT)strtod(ptr, &pEnd);
            if (ptr == pEnd)
            {
                return CYGFX_FALSE;
            }
            ptr = pEnd - 1;
            UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &val));
        }
    }
    if (Mode != 0)
    {
        UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, xo, yo));
    }
    return ret;
}

CYGFX_ERROR utDeDrawImage(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT xo, CYGFX_FLOAT yo, const DE_UTIL_GEOMETRY* pGeo, CYGFX_FLOAT *pMatrix)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 i;
    const DE_UTIL_PATH *pPath;
    Mat3x2 mat;

    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(ctx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));
    for (i = 0; i < pGeo->cnt; i++)
    {
        if (pMatrix != NULL)
        {
            utMat3x2Copy(mat, pMatrix);
        }
        else
        {
            utMat3x2LoadIdentity(mat);
        }
        pPath = (DE_UTIL_PATH*)pGeo->start_path + i;
        UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(pPath->r,pPath->g,pPath->b, pPath->a)));
        if (pPath->mat != NULL)
        {
            utMat3x2Multiply(mat, mat, pPath->mat);
        }
        CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_MASK, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);

        UTIL_SUCCESS(ret, utDeDrawForm(ctx, xo, yo, pPath->szPath));
    }
    return ret;
}


static CYGFX_ERROR util_GenerateRainbow(CYGFX_SURFACE surf)
{
    CYGFX_U32 width = utSurfWidth(surf);
    CYGFX_U32 height = utSurfHeight(surf);
    static const CYGFX_FLOAT multi  = 170.0f;
    static const CYGFX_FLOAT offset = 150.0f;
    static const CYGFX_FLOAT pi = 3.1415f;
    CYGFX_ERROR ret = CYGFX_OK;

    {
        CYGFX_U32 x, y;
        for (y = 0; y < height; ++y)
        {
            for (x = 0; x < width; ++x)
            {
                CYGFX_S32 r, g, b;
                CYGFX_FLOAT val = (((2.5f * pi) * (CYGFX_FLOAT)x) / (CYGFX_FLOAT)width)  + 3.3f;
                r = (CYGFX_S32)((sin(val +            0) * multi) + offset);
                g = (CYGFX_S32)((sin(val + ((pi * 2) / 3)) * multi) + offset);
                b = (CYGFX_S32)((sin(val + ((pi * 4) / 3)) * multi) + offset);
                if (r < 0)
                {
                    r = 0;
                }
                if (g < 0)
                {
                    g = 0;
                }
                if (b < 0)
                {
                    b = 0;
                }
                if (r > 255)
                {
                    r = 255;
                }
                if (g > 255)
                {
                    g = 255;
                }
                if (b > 255)
                {
                    b = 255;
                }
                UTIL_SUCCESS(ret, utSurfSetPixel(surf, x, y, (CYGFX_U08)r, (CYGFX_U08)g, (CYGFX_U08)b,255));
            }
        }
   }
   return ret;
}

CYGFX_ERROR utDeDrawLogo(CYGFX_BE_CONTEXT ctx, CYGFX_U32 x, CYGFX_U32 y, CYGFX_U32 w, CYGFX_U32 h)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_SURFACE sRainbow;
    Mat3x2 mat;
    static const CYGFX_U32 OrgWidth = 380;
    static const CYGFX_U32 OrgHeight = 300;

    static const CYGFX_CHAR *szDots =
        "M 322.574524, 208.235199 C 327.724487, 201.324127 327.854523, 192.915283 322.904999, 189.514069 C 317.970734, 186.111359 309.537384, 189.290710 304.067627, 196.535217 C 298.607056, 203.772095 298.452484, 212.027954 303.705017, 215.092651 C 308.974335, 218.157227 317.426025, 215.138580 322.574524, 208.235199 Z"
        "M 146.690308, 214.101288 C 142.054398, 207.055664 145.663681, 198.525879 155.224640, 195.176666 C 164.794800, 191.835114 176.349365, 195.107849 180.552246, 202.335571 C 184.750549, 209.574036 180.440552, 217.889587 171.300339, 221.053589 C 162.158585, 224.216125 151.323120, 221.153046 146.690308, 214.101288 Z"
        "M 342.141693, 110.616638 C 339.966034, 102.607117 333.113159, 99.404785 326.742218, 103.883087 C 320.381989, 108.355255 316.869141, 118.964294 319.069244, 127.084015 C 321.261780, 135.205231 328.301270, 137.706787 334.655396, 133.129028 C 341.018646, 128.529877 344.324982, 118.618500 342.141693, 110.616638 Z"
        "M 184.493546, 246.316895 C 176.996567, 242.846863 175.264572, 235.320801 181.092377, 229.448608 C 186.915558, 223.558167 198.180954, 221.595123 205.676407, 225.146240 C 213.182587, 228.688202 214.152618, 236.329041 208.298813, 242.143005 C 202.449615, 247.966248 191.972198, 249.789917 184.493546, 246.316895 Z"
        "M 317.321991, 67.697052 C 311.407013, 61.899902 302.959900, 63.628815 298.503021, 72.092743 C 294.040009, 80.562836 295.412445, 92.357574 301.521698, 97.879364 C 307.641693, 103.399567 316.024567, 100.885864 320.290192, 92.704865 C 324.561951, 84.507172 323.261444, 73.501862 317.321991, 67.697021 Z"
        "M 233.757980, 255.753967 C 225.324631, 256.130310 219.585587, 251.165466 221.222702, 244.542175 C 222.859818, 237.923401 231.531860, 232.028259 240.229904, 231.511108 C 248.934067, 230.992432 254.152908, 236.162231 252.248062, 242.921875 C 250.330994, 249.681458 242.183716, 255.380615 233.758026, 255.753967 Z"
        "M 283.233612, 242.074219 C 275.664703, 246.127136 267.572540, 244.846558 265.280579, 239.229919 C 263.003906, 233.605591 267.668915, 225.505859 275.592743, 221.159119 C 283.518127, 216.821533 291.371613, 218.148071 293.319336, 224.069153 C 295.250183, 229.977997 290.826965, 238.031921 283.233551, 242.074219 Z"
        "M 366.699707, 93.027802 C 365.414520, 88.635193 361.826660, 86.796112 358.696289, 89.011536 C 355.573578, 91.234650 354.061920, 96.718140 355.308899, 101.179626 C 356.575745, 105.634979 360.152832, 107.308777 363.318420, 105.029114 C 366.477875, 102.747833 368.000214, 97.418884 366.699707, 93.027802 Z"
        "M 227.191238, 282.538147 C 222.592041, 282.617676 219.452469, 279.883606 220.228210, 276.453369 C 221.026871, 273.006287 225.515884, 270.122192 230.179337, 269.964600 C 234.856552, 269.813110 237.878311, 272.524231 237.006195, 276.037109 C 236.152435, 279.539307 231.778198, 282.469238 227.191238, 282.538147 Z"
        "M 292.586487, 266.115112 C 288.470795, 268.226501 284.049133, 267.375793 282.759308, 264.262268 C 281.477142, 261.139526 283.879242, 256.857056 288.063843, 254.664551 C 292.239197, 252.464478 296.561432, 253.257019 297.777802, 256.454651 C 298.995667, 259.653870 296.694550, 263.991455 292.586487, 266.115112 Z"
        "M 330.834961, 33.371521 C 326.759033, 31.393219 322.771881, 33.607117 322.077240, 38.471039 C 321.384155, 43.342529 324.304901, 48.752625 328.438965, 50.440216 C 332.583771, 52.121674 336.350586, 49.699677 336.980957, 45.132660 C 337.611298, 40.550293 334.920074, 35.345245 330.834961, 33.371521 Z"
        "M 369.618927, 155.933716 C 368.218964, 151.776733 364.683136, 149.919250 361.747101, 151.883850 C 358.817139, 153.851440 357.547241, 158.967773 358.912018, 163.190582 C 360.293610, 167.410309 363.797302, 169.119324 366.785370, 167.085999 C 369.742889, 165.063293 371.024994, 160.113770 369.618927, 155.933807 Z"
        "M 344.023560, 156.271881 C 343.585968, 148.447418 337.912750, 144.151215 331.159332, 147.012299 C 324.399780, 149.899445 319.093781, 158.993713 319.517548, 166.916046 C 319.950531, 174.830688 325.948151, 178.514954 332.710724, 175.545258 C 339.479462, 172.566315 344.482544, 164.077911 344.023560, 156.271881 Z"
        "M 345.391388, 219.257355 C 346.829590, 215.256439 345.221558, 211.809357 341.740845, 211.624176 C 338.272308, 211.426758 334.178040, 214.615356 332.661835, 218.648407 C 331.162415, 222.670837 332.853088, 226.000061 336.396576, 226.157684 C 339.930878, 226.315308 343.930298, 223.264465 345.391449, 219.257355 Z"
        "M 162.147888, 268.560059 C 157.355942, 266.841919 155.168015, 262.793518 157.334518, 259.542297 C 159.502502, 256.287964 165.267563, 255.051758 170.152832, 256.716431 C 175.028961, 258.388733 177.108200, 262.374329 174.849945, 265.663879 C 172.584045, 268.941162 166.935257, 270.272278 162.147888, 268.560181 Z"
        "M 114.832611, 228.877960 C 109.765228, 226.746643 107.256058, 222.225464 109.309334, 218.815094 C 111.370216, 215.423126 117.291359, 214.402618 122.474998, 216.497131 C 127.643326, 218.587158 130.034744, 223.024109 127.874352, 226.468170 C 125.710930, 229.906097 119.906105, 231.006165 114.832657, 228.877960 Z"
        "M 294.554108, 188.712463 C 275.508667, 213.983521 241.798187, 224.014160 216.476654, 211.492584 C 202.342514, 204.505066 194.274857, 190.738159 193.927505, 174.518585 C 197.249130, 181.016510 202.521530, 186.373016 209.478424, 189.870605 C 230.977997, 200.670898 259.134613, 192.953583 274.697754, 172.376556 C 290.247131, 151.794952 289.188416, 123.684296 270.937012, 108.329193 C 267.769897, 105.663940 264.175934, 103.681030 260.306580, 102.333099 C 272.116638, 101.323364 283.247375, 104.227203 291.682251, 111.386108 C 313.030365, 129.486023 313.596466, 163.439728 294.554108, 188.712280 Z";

    if ((h == 0u) && (w > 0u))
    {
        h = (OrgHeight * w) / OrgWidth;
    }
    if ((w == 0u) && (h > 0u))
    {
        w = (OrgWidth * h) / OrgHeight;
    }
    if (w == 0u)
    {
        w = OrgWidth;
    }
    if (h == 0u)
    {
        h = OrgHeight;
    }

    utMat3x2LoadIdentity(mat);
    utMat3x2Translate(mat, (CYGFX_FLOAT)x, (CYGFX_FLOAT)y);
    utMat3x2Scale(mat, (CYGFX_FLOAT)w/(CYGFX_FLOAT)OrgWidth, (CYGFX_FLOAT)h/(CYGFX_FLOAT)OrgHeight);

    UTIL_SUCCESS(ret, utSmGenSurfaceObjects(1, &sRainbow));
    UTIL_SUCCESS(ret, utSurfCreateBuffer(sRainbow, w, 1, CYGFX_SM_FORMAT_R8G8B8A8));
    UTIL_SUCCESS(ret, util_GenerateRainbow(sRainbow));

    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(35, 32, 31, 255)));
    CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_MASK, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, sRainbow));
    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_TILE_MODE, CYGFX_BE_TILE_PAD));

    utMat3x2LoadIdentity(mat);
    utMat3x2Translate(mat, (CYGFX_FLOAT)x, (CYGFX_FLOAT)y);
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat));

    UTIL_SUCCESS(ret, utDeDrawForm(ctx, 0, 0, szDots));

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, 0));

    utSurfDeleteBuffer(sRainbow);
    utSmDeleteSurfaceObjects(1, &sRainbow);
    UTIL_SUCCESS(ret, CyGfx_BeFinish(ctx));
    return ret;
}


CYGFX_ERROR utDeDrawEllipse(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT cx, CYGFX_FLOAT cy, CYGFX_FLOAT halfWidth, CYGFX_FLOAT halfHeight)
{
    CYGFX_ERROR ret = CYGFX_OK;
    static CYGFX_U08 PathSegment = CYGFX_DE_INSTR_CUBIC_TO_ABS;
    CYGFX_FLOAT values[26];
    CYGFX_FLOAT offset[2];
    CYGFX_S32 i, index;

    static const CYGFX_FLOAT MagicConst = 0.2761423749154f;  /* 2/3*(sqrt(2)-1) */

    offset[0] = halfWidth * MagicConst * 2.0f;
    offset[1] = - (halfHeight * MagicConst * 2.0f);

    values[0]  = cx - halfWidth;
    values[2]  = cx - halfWidth;
    values[22] = cx - halfWidth;
    values[24] = cx - halfWidth;
    values[10] = cx + halfWidth;
    values[12] = cx + halfWidth;
    values[14] = cx + halfWidth;
    values[4]  = cx - offset[0];
    values[20] = cx - offset[0];
    values[8]  = cx + offset[0];
    values[16] = cx + offset[0];
    values[6]  = cx;
    values[18] = cx;

    values[5]  = cy + halfHeight;
    values[7]  = cy + halfHeight;
    values[9]  = cy + halfHeight;
    values[17] = cy - halfHeight;
    values[19] = cy - halfHeight;
    values[21] = cy - halfHeight;
    values[15] = cy + offset[1];
    values[23] = cy + offset[1];
    values[3]  = cy - offset[1];
    values[11] = cy - offset[1];
    values[1]  = cy;
    values[25] = cy;
    values[13] = cy;

    UTIL_SUCCESS(ret, CyGfx_DeSetAttribute(ctx, CYGFX_DE_ATTR_DATA_FORMAT, CYGFX_DE_DATA_FORMAT_FLOAT));
    
    UTIL_SUCCESS(ret, utDePathMoveTo(ctx, values[0], values[1]));   
    for (i = 0; i < 4; i++)
    {
        index = i * 3 * 2;
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, &PathSegment, 0));
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &values[index + 2]));
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &values[index + 3]));
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &values[index + 4]));
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &values[index + 5]));
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &values[index + 6]));
        UTIL_SUCCESS(ret, CyGfx_DeAppendPathData(ctx, 1, 0, &values[index + 7]));
    }
    UTIL_SUCCESS(ret, CyGfx_DeDraw(ctx, 0, 0));
    

    return ret;
}

CYGFX_ERROR utDeDrawButton(CYGFX_BE_CONTEXT ctx, CYGFX_SURFACE surf, CYGFX_U08 r)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT w = (CYGFX_FLOAT)utSurfWidth(surf);
    CYGFX_FLOAT h = (CYGFX_FLOAT)utSurfHeight(surf);
    CYGFX_SURFACE_OBJECT_S   mask;

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_STORE | CYGFX_BE_TARGET_DST, surf));
    UTIL_SUCCESS(ret, CyGfx_BeFill(ctx, 0, 0, (CYGFX_U32)w, (CYGFX_U32)h));

    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, 0x404040FF));
    UTIL_SUCCESS(ret, utDeDrawRoundRect(ctx, 0, 0, w, h, r));

    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, 0xd0d0d0FF));
    UTIL_SUCCESS(ret, utDeDrawRoundRect(ctx, 1.0f, r*2+1.0f, w-2, h-2*r-2, r));

    UTIL_SUCCESS(ret, utSurfCreateBuffer(&mask, 1, (CYGFX_U32)h, CYGFX_SM_FORMAT_RGB8));
    UTIL_SUCCESS(ret, utSurfGenerateGradient(&mask, 128, 128, 128, 255, 70, 70, 70, 255, 0));
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, &mask));
    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_TILE_MODE, CYGFX_BE_TILE_PAD));

    UTIL_SUCCESS(ret, utDeDrawRoundRect(ctx, 1, 1, w-2, h-1-2, r));

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, 0));
    UTIL_SUCCESS(ret, CyGfx_BeFinish(ctx));

    utSurfDeleteBuffer(&mask);

    return ret;
}


