/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        ut_compression.cpp
 *              compression utility functions
 */


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#ifdef FPGA
#include <string.h>
#endif
#include <stdlib.h>

#include "cygfx_driver_api.h"

#include "ut_class_rlad.h"
#include "sm_util.h"
#include "ut_rlc.h"
#include "ut_compression.h"
#include "ut_compatibility.h"

#include "hweb.h"
//#define CYGFX_OBJECT_PARTITIONING_TARGET_WIDTH 256
#define CYGFX_OBJECT_PARTITIONING_MAX_COUNT  7
#define CYGFX_SURFMAN_OBJECT_PARTITIONING_MIN_WIDTH 16
#define CYGFX_SM_OBJECT_PARTITIONING_BITS 3

extern "C"
{
static CYGFX_ERROR utSurfRLE(CYGFX_SURFACE surf);
CYGFX_U32 g_partitioning_target_width = 256;

static CYGFX_ERROR utSurfRLE(CYGFX_SURFACE surf)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 SizeOrg, RLEWords = 0, TotalBits, StrideBytes, add;
    CYGFX_U32 *pRLD_DATA, *p;
    CYGFX_U32 ObjectPartitioning;
    CYGFX_U32 column;
    CYGFX_U32 startx = 0;
    CYGFX_SURFACE_COLUMN_INFO_S column_info;
    std::vector<CYGFX_SURFACE_COLUMN_INFO_S> vColumnInfos;

    int width = utSurfWidth(surf);
    int height = utSurfHeight(surf);

    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &SizeOrg));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE, &StrideBytes));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_BITPERPIXEL, &TotalBits));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &add));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, &ObjectPartitioning));
    if (ret != CYGFX_OK)
        return ret;
    if (ObjectPartitioning == 1u)
    {
        ObjectPartitioning = (width / g_partitioning_target_width) + 1u;
        if (ObjectPartitioning > CYGFX_OBJECT_PARTITIONING_MAX_COUNT)
        { 
            ObjectPartitioning = CYGFX_OBJECT_PARTITIONING_MAX_COUNT;
        }
    }
    else if (ObjectPartitioning == 0u)
    {
        ObjectPartitioning++;
    }
    else
    {
        if (ObjectPartitioning >= (1 << CYGFX_SM_OBJECT_PARTITIONING_BITS))
        {
            printf("Too many columns (%d) for ObjectPartioning", ObjectPartitioning);
            return CYGFX_ERR;
        }
        if ((width / ObjectPartitioning) < CYGFX_SURFMAN_OBJECT_PARTITIONING_MIN_WIDTH)
        {
            printf("Wrongly set ObjectPartioning (%d) size for surface with small width (%d)", ObjectPartitioning, width);
            return CYGFX_ERR;
        }
    }
    UT_HWEB_LOCK((CYGFX_U32*)add, SizeOrg, UT_HWEB_MA_READ_WRITE);

    
    if (ObjectPartitioning > 1)
    {
        RLEWords += sizeof(column_info) * ObjectPartitioning;
    }
    else
    {
        // convert to word size
        SizeOrg >>= 2;
    }
    for (column = 0; column < ObjectPartitioning; column++)
    {
        if(column < ObjectPartitioning - 1)
        {
            column_info.column_width = width / ObjectPartitioning;
        }
        else
        {
            column_info.column_width = width - startx;
        }
        column_info.reserved = 0;
        if (ObjectPartitioning > 1)
        {
            column_info.column_address_offset = (RLEWords);
            RLEWords += utRldEncode((CYGFX_U32*)add, column_info.column_width, height, StrideBytes, startx, TotalBits, 0, 0) * 4;
            startx += column_info.column_width;
            vColumnInfos.push_back(column_info);
        }
        else
        {
            column_info.column_address_offset = (RLEWords << 2);
            RLEWords = utRldEncode((CYGFX_U32*)add, width, height, StrideBytes, startx, TotalBits, 0, 0);
        }
    }
    if (SizeOrg <= RLEWords)
    {
        UT_HWEB_UNLOCK((CYGFX_U32*)add);
        printf("Error: Compressed image has a bigger size as uncompressed!\n");
        return CYGFX_ERR;
    }
    if (SizeOrg > RLEWords)
    {
        UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, ObjectPartitioning == 1 ? 0 : ObjectPartitioning));
        if (ret != CYGFX_OK)
        return ret;
        if (ObjectPartitioning > 1)
        {
            pRLD_DATA = (CYGFX_U32*)utOsLibcMalloc(RLEWords);
        }
        else
        {
            pRLD_DATA = (CYGFX_U32*)utOsLibcMalloc(RLEWords << 2);
        }
        if (pRLD_DATA == 0) 
        {
#if (defined (DEBUG) || defined (RELEASE))
            printf("Failed to allocate memory in utSurfRLE\n");
#endif
            return CYGFX_ERR;
        }
        p = pRLD_DATA;
        if (ObjectPartitioning > 1)
        {
            for (column = 0; column < ObjectPartitioning; column++)
            {
                column_info = vColumnInfos[column];
                memcpy(p, &column_info, sizeof(column_info));
                p += (sizeof(column_info) / 4);
            }
        }
        startx = 0;
        for (column = 0; column < ObjectPartitioning; column++)
        {
            if (ObjectPartitioning > 1)
            {
                column_info = vColumnInfos[column];
                p = pRLD_DATA + column_info.column_address_offset / 4;
                utRldEncode((CYGFX_U32*)add, column_info.column_width, height, StrideBytes, startx, TotalBits, p, RLEWords / 4 - (pRLD_DATA - p));
                startx += column_info.column_width;
            }
            else
            {
                utRldEncode((CYGFX_U32*)add, width, height, StrideBytes, startx, TotalBits, pRLD_DATA, RLEWords);
            }
        }
        if (ObjectPartitioning > 1)
        {
            memcpy((CYGFX_U32*)add, pRLD_DATA, RLEWords);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, RLEWords);
        }
        else
        {
            memcpy((CYGFX_U32*)add, pRLD_DATA, RLEWords << 2);
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, RLEWords << 2);
        }
        
        CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_COMPRESSION_FORMAT, CYGFX_SM_COMP_RLC);
        utOsLibcFree(pRLD_DATA);
        UT_HWEB_UNLOCK((CYGFX_U32*)add);
        return CYGFX_OK;
    }
    UT_HWEB_UNLOCK((CYGFX_U32*)add);

    return CYGFX_ERR;
}


CYGFX_ERROR utSurfCompress(CYGFX_SURFACE surf, CYGFX_SM_COMP mode)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 SizeOrg, RLEWords = 0, TotalBits, StrideBytes, add, bits, rlad_bits;
    RLAD rlad;
    CYGFX_U08 r, g, b, a;
    CYGFX_U32 ObjectPartitioning;
    CYGFX_U32 column;
    CYGFX_U32 startx = 0;
    std::vector<RLAD::BitStream> vBs;
    static const int wsize = 32;
    CYGFX_SURFACE_COLUMN_INFO_S column_info;
    std::vector<CYGFX_SURFACE_COLUMN_INFO_S> vColumnInfos;
    CYGFX_U32 *p;

    switch (mode)
    {
    case CYGFX_SM_COMP_NONE: return CYGFX_OK;
    case CYGFX_SM_COMP_RLAD: rlad.mode = RLAD::MODE_RLAD; break;
    case CYGFX_SM_COMP_RLAD_UNIFORM: rlad.mode = RLAD::MODE_RLAD_UNIFORM; break;
    case CYGFX_SM_COMP_RLA: rlad.mode = RLAD::MODE_RLA; break;
    case CYGFX_SM_COMP_RLC: return utSurfRLE(surf);
    }

    int width = utSurfWidth(surf);
    int height = utSurfHeight(surf);
    
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, &SizeOrg));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_STRIDE, &StrideBytes));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_BITPERPIXEL, &TotalBits));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_VIRT_ADDRESS, &add));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_COLORBITS, &bits));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_RLAD_MAXCOLORBITS, &rlad_bits));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, &ObjectPartitioning));
	if (ret != CYGFX_OK)
        return ret;
    if (ObjectPartitioning == 1u)
    {
        ObjectPartitioning = (width / g_partitioning_target_width) + 1u;
        if (ObjectPartitioning > CYGFX_OBJECT_PARTITIONING_MAX_COUNT)
        { 
            ObjectPartitioning = CYGFX_OBJECT_PARTITIONING_MAX_COUNT;
        }
    }
    else if (ObjectPartitioning == 0u)
    {
        ObjectPartitioning++;
    }
    else
    {
        if (ObjectPartitioning >= (1 << CYGFX_SM_OBJECT_PARTITIONING_BITS))
        {
            printf("Too many columns (%d) for ObjectPartioning", ObjectPartitioning);
            return CYGFX_ERR;
        }
        if ((width / ObjectPartitioning) < CYGFX_SURFMAN_OBJECT_PARTITIONING_MIN_WIDTH)
        {
            printf("Wrongly set ObjectPartioning (%d) size for surface with small width (%d)", ObjectPartitioning, width);
            return CYGFX_ERR;
        }
    }
    UTIL_SUCCESS(ret, CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_OBJECT_PARTITIONING, ObjectPartitioning == 1 ? 0 : ObjectPartitioning));
    UT_HWEB_LOCK((CYGFX_U32*)add, RLEWords, UT_HWEB_MA_WRITE);
    if (ObjectPartitioning > 1)
    {
        RLEWords += sizeof(column_info) * ObjectPartitioning;
    }
    else
    {
        // convert to word size
        SizeOrg >>= 2;
    }
    for (column = 0; column < ObjectPartitioning; column++)
    {
        // setup encoder and compress frame
        if (column < ObjectPartitioning - 1)
        {
            column_info.column_width = width / ObjectPartitioning;
        }
        else
        {
            column_info.column_width = width - startx;
        }
        if (ObjectPartitioning > 1)
        {
            
            rlad.width = column_info.column_width;
        }
        else
        {
            rlad.width = width;
        }
        RLAD::Frame fdst(column_info.column_width, height);
        rlad.height = height;
        rlad.bpc[0] = (bits >> 24) & 0xf;
        rlad.bpc[1] = (bits >> 16) & 0xf;
        rlad.bpc[2] = (bits >> 8) & 0xf;
        rlad.bpc[3] = (bits >> 0) & 0xf;
        rlad.cbpc_max[0] = (rlad_bits >> 24) & 0xf;
        rlad.cbpc_max[1] = (rlad_bits >> 16) & 0xf;
        rlad.cbpc_max[2] = (rlad_bits >> 8) & 0xf;
        rlad.cbpc_max[3] = (rlad_bits >> 0) & 0xf;

        for (int y = 0; y < height; y++) for (int x = 0; x < rlad.width; x++)
        {
            utSurfGetPixel(surf, startx + x, y, &r, &g, &b, &a);
            RLAD::Frame::Pixel out;
            out.col[0] = r >> (8 - rlad.bpc[0]);
            out.col[1] = g >> (8 - rlad.bpc[1]);
            out.col[2] = b >> (8 - rlad.bpc[2]);
            out.col[3] = a >> (8 - rlad.bpc[3]);
            fdst.Write(out);
        }
        RLAD::BitStream bs;
        if (!rlad.Encode(fdst, bs))
        return CYGFX_ERR;

        vBs.push_back(bs);
        column_info.reserved = 0;
        column_info.column_address_offset = RLEWords;
        startx += column_info.column_width;
        vColumnInfos.push_back(column_info);
        // write compressed bit stream into destination buffer
        if (ObjectPartitioning > 1)
        {
            RLEWords += ((bs.Size() + wsize - 1) / wsize) * 4;
        }
        else
        {
            RLEWords = ((bs.Size() + wsize - 1) / wsize);
        }
    }
	if (SizeOrg <= RLEWords)
    {
        UT_HWEB_UNLOCK((CYGFX_U32*)add);
        printf("Error: Compressed image has a bigger size as uncopressed!\n");
        return CYGFX_ERR;
    }
    if (SizeOrg > RLEWords)
    {
        UT_HWEB_LOCK((CYGFX_U32*)add, RLEWords * 4, UT_HWEB_MA_WRITE);
        CYGFX_U32* p = (CYGFX_U32*)add;
        if (ObjectPartitioning > 1)
        {
            for (column = 0; column < ObjectPartitioning; column++)
            {
                column_info = vColumnInfos[column];
                memcpy(p, &column_info, sizeof(column_info));
                p += (sizeof(column_info) / 4);
            }
        }
        for (column = 0; column < ObjectPartitioning; column++)
        {
            RLAD::BitStream bs = vBs[column];
            CYGFX_U32 RLE_in_Word = ((bs.Size() + wsize - 1) / wsize);
            for (unsigned i = 0; i < RLE_in_Word; i++)
            {
                *p = bs.Read(wsize);
                 p++;
            }
        }
        if (ObjectPartitioning > 1)
        {
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, RLEWords);
        }
        else
        {
            CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_SIZEINBYTES, RLEWords * 4);
        }
        CyGfx_SmSetAttribute(surf, CYGFX_SM_ATTR_COMPRESSION_FORMAT, mode);
        UT_HWEB_UNLOCK((CYGFX_U32*)add);
#if (defined (DEBUG) || defined (RELEASE))
        printf("Compress: %.3fkB -> %.3fkB (%.1f%% of original)\n", SizeOrg * 4.0 / 1024, RLEWords * 4.0 / 1024, RLEWords * 100.0 / SizeOrg);
#endif

        return CYGFX_OK;
    }

    return CYGFX_ERR;
}

}
