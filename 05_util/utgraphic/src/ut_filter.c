/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/
#include <stdio.h>
#include <math.h>

#include "cygfx_driver_api.h"

#include "sm_util.h"
#include "ut_filter.h"


CYGFX_ERROR utFilterSetFilter(CYGFX_BE_CONTEXT beCtx, CYGFX_CM_FILTER_CHANNEL eChannel,
                               CYGFX_U08                    taps_h,
                               const CYGFX_FLOAT*           fCoefficients_h,
                               CYGFX_U08                    taps_v,
                               const CYGFX_FLOAT*           fCoefficients_v,
                               CYGFX_BOOL                   bNormalize)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 exponent_h = 0;
    CYGFX_U32 exponent_v = 0;
    CYGFX_S32 i;
    CYGFX_FLOAT fMax_h, fMax_v;
    CYGFX_FLOAT fFactor_h = 1.0f, fFactor_v = 1.0f;
    CYGFX_S08 fir_coefficients_h[15];
    CYGFX_S08 fir_coefficients_v[15];
    CYGFX_FLOAT fCoefficientsTemp_h[15];
    CYGFX_FLOAT fCoefficientsTemp_v[15];
    CYGFX_FLOAT sum;
    CYGFX_S08 correction = 0;

    if ((taps_h == 0u) || (taps_v == 0u))
    {
        return CYGFX_ERR;
    }
    if ((taps_h !=0u) && ((taps_h & 1u) != 1u))
    {
        return CYGFX_ERR;
    }
    if (taps_h > 15)
    {
        return CYGFX_ERR;
    }
    if ((taps_v != 0) && ((taps_v & 1u) != 1u))
    {
        return CYGFX_ERR;
    }
    if (taps_v > 15)
    {
        return CYGFX_ERR;
    }
    if ((taps_h != 0u) && (fir_coefficients_h == NULL))
    {
        return CYGFX_ERR;
    }
    if ((taps_v != 0u) && (fir_coefficients_v == NULL))
    {
        return CYGFX_ERR;
    }

    if (bNormalize)
    {
        sum = 0;
        if(fCoefficients_h != NULL)
        {
            for (i = 0; i < taps_h; i++)
            {
                sum += fCoefficients_h[i];
            }
            if (sum == 0)
            {
                return CYGFX_ERR;
            }
            for (i = 0; i < taps_h; i++)
            {
                fCoefficientsTemp_h[i] = fCoefficients_h[i] / sum;
            }
        }
        sum = 0;
        if(fCoefficients_v != NULL)
        {
            for (i = 0; i < taps_v; i++)
            {
                sum += fCoefficients_v[i];
            }
            if (sum == 0)
            {
                return CYGFX_ERR;
            }
            for (i = 0; i < taps_v; i++)
            {
                fCoefficientsTemp_v[i] = fCoefficients_v[i] / sum;
            }
        }
    }
    else
    {
        for (i = 0; i < taps_h; i++)
        {
            fCoefficientsTemp_h[i] = fCoefficients_h[i];
        }
        for (i = 0; i < taps_v; i++)
        {
            fCoefficientsTemp_v[i] = fCoefficients_v[i];
        }
    }

    /* hor */
    fMax_h = 0.0f;
    for ( i = 0; i < taps_h; i++)
    {
        if (fMax_h < fabs(fCoefficientsTemp_h[i]))
        {
            fMax_h = (CYGFX_FLOAT)fabs(fCoefficientsTemp_h[i]);
        }
    }
    while ((exponent_h < 16) && (fMax_h < 127))
    {
        exponent_h++;
        fMax_h *= 2;
        fFactor_h *= 2;
    }
    exponent_h--;
    fFactor_h /= 2;
    
    /* vert */
    fMax_v = 0.0f;
    for ( i = 0; i < taps_v; i++)
    {
        if (fMax_v < fabs(fCoefficientsTemp_v[i]))
        {
            fMax_v = (CYGFX_FLOAT)fabs(fCoefficientsTemp_v[i]);
        }
    }
    while ((exponent_v < 16) && (fMax_v < 127))
    {
        exponent_v++;
        fMax_v *= 2;
        fFactor_v *= 2;
    }
    exponent_v--;
    fFactor_v /= 2;

    /* We have currently only one exponent. Remove this if they are separate */
    if (exponent_h > exponent_v)
    {
        exponent_v = exponent_h;
        fFactor_v = fFactor_h;
    }
    if (exponent_h < exponent_v)
    {
        exponent_h = exponent_v;
        fFactor_h = fFactor_v;
    }
    
    sum = 0;
    for ( i = 0; i < taps_h; i++)
    {
        fir_coefficients_h[i] = (CYGFX_S08)(fCoefficientsTemp_h[i] * fFactor_h + 0.5f);
        /* printf("coeff[%d]: %d\n", i, fir_coefficients_h[i]); */
        sum += (CYGFX_FLOAT)fir_coefficients_h[i];
    }
    if (bNormalize)
    {
        correction = (CYGFX_S08)(fFactor_h - sum);
        /* add the difference to the central index */
        fir_coefficients_h[(taps_h-1)/2] += correction;
    }

    sum = 0;
    for ( i = 0; i < taps_v; i++)
    {
        fir_coefficients_v[i] = (CYGFX_S08)(fCoefficientsTemp_v[i] * fFactor_v + 0.5f);
        /* printf("coeff[%d]: %d\n", i, fir_coefficients_v[i]); */
        sum += (CYGFX_FLOAT)fir_coefficients_v[i];
    }
    if (bNormalize)
    {
        correction = (CYGFX_S08)(fFactor_h - sum);
        /* add the difference to the central index */
        fir_coefficients_v[(taps_v-1)/2] += correction;
    }

    UTIL_SUCCESS(ret, CyGfx_CmSetFilter(beCtx, eChannel, (CYGFX_U08)exponent_h, taps_h, fir_coefficients_h, taps_v, fir_coefficients_v));

    return ret;
}

CYGFX_ERROR utFilterSetGaussSharpen(CYGFX_BE_CONTEXT beCtx, CYGFX_CM_FILTER_CHANNEL eChannel, CYGFX_FLOAT sigma)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT fCoefficients[15];
    CYGFX_U32 i;

    if ( (sigma > 5.0) || (sigma <= 0) )
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("Warning: sigma in (0.0, 5.0) is recommended, because of gaussian function with limit block size.\n");
#endif
    }
    utCalculate1DGauss(fCoefficients, sigma);
    for (i = 0; i < 15; i++)
    {
        fCoefficients[i] = - fCoefficients[i];
        if (i == 7)
        {
            fCoefficients[i] += 2.0f;
        }
    }

    UTIL_SUCCESS(ret, utFilterSetFilter(beCtx, eChannel, 15, fCoefficients, 15, fCoefficients, CYGFX_TRUE));

    return ret;
}

CYGFX_ERROR utFilterSetLaplaceSharpen(CYGFX_BE_CONTEXT beCtx, CYGFX_CM_FILTER_CHANNEL eChannel)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT fCoefficients[3];

    fCoefficients[0] = -0.3f;
    fCoefficients[1] =  1.6f;
    fCoefficients[2] = -0.3f;

    UTIL_SUCCESS(ret, utFilterSetFilter(beCtx, eChannel, 3, fCoefficients, 3, fCoefficients, CYGFX_TRUE));

    return ret;
}

void utCalculate1DGauss(CYGFX_FLOAT fCoefficients[15], CYGFX_FLOAT sigma)
{
    CYGFX_S32 i;
    CYGFX_FLOAT d, d1, d2, val, sum, fexp;
    CYGFX_FLOAT PI = atanf(1.0f)*4.0f;

    d = sigma;
    d1 = 1 / (d * sqrtf(2*PI));
    d2 = 2 * d * d;
    sum = 0;
    for (i = 0; i < 15; i++)
    {
        fexp = (CYGFX_FLOAT)(((i-7)*(i-7))) / d2;
        /* restrict size otherwise we get a nan for different compilers / HW */
        if (fexp > 32)
            val = 0.0f;
        else
            val = expf(-fexp) * d1;
        fCoefficients[i] = (CYGFX_FLOAT)val;
        sum += val;
    }
}

CYGFX_ERROR utFilterSetGaussFilter(CYGFX_BE_CONTEXT beCtx, CYGFX_CM_FILTER_CHANNEL eChannel, CYGFX_FLOAT sigma)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT fCoefficients[15];

    if ( (sigma > 5.0) || (sigma <= 0) )
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("Warning: sigma in (0.0, 5.0) is recommended, because of gaussian function with limit block size.\n");
#endif
    }
    utCalculate1DGauss(fCoefficients, sigma);

    UTIL_SUCCESS(ret, utFilterSetFilter(beCtx, eChannel, 15, fCoefficients, 15, fCoefficients, CYGFX_TRUE));

    return ret;
}

CYGFX_ERROR utFilterFree(CYGFX_BE_CONTEXT beCtx) {
    CYGFX_ERROR ret = CYGFX_OK;
    UTIL_SUCCESS(ret, CyGfx_CmSetFilter(beCtx, CYGFX_CM_FILTER_CHANNEL_R, 0, 0, 0, 0, 0));
    return ret;
}
