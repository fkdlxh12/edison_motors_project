/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress�s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/
#include <stdio.h>
#include <string.h>

#include "cygfx_driver_api.h"
#include "ut_disp_panels.h"


CYGFX_DISP_TCON_PROPERTIES_S    s_panel_XXX_1600_480_tcon[] = {
        { 0x0408, 0x00000108}, /* CTRL (ENLVDS | BYPASS) */
        { 0x0410, 0x03040508}, /* MapBit3_0   ( R3, R4, R5, G0)     */
        { 0x0414, 0x11000102}, /* MapBit7_4   ( B1, R0, R1, R2)     */
        { 0x0418, 0x0B0C0D10}, /* MapBit11_8  ( G3, G4, G5, B0)     */
        { 0x041c, 0x191A090A}, /* MapBit15_12 ( VSYNC, EN, G1, G2)  */
        { 0x0420, 0x13141518}, /* MapBit19_16 ( B3, B4, B5, HSYNC)  */
        { 0x0424, 0x16171B12}, /* MapBit23_20 ( B6, B7, RES, B2)    */
        { 0x0428, 0x06070E0F}, /* MapBit27_24 ( R6, R7, G6, G7)     */
    };

CYGFX_DISP_PROPERTIES_S s_panel_XXX_1600_480 = {
    CYGFX_DISP_CONTROLLER_0,            /* CYGFX_DISP_CONTROLLER */
    CYGFX_DISP_MODE_SINGLE_SCREEN,      /* CYGFX_DISP_MODE */
    CYGFX_DISP_SYNC_MODE_NONE,          /* CYGFX_DISP_SYNC_MODE */
    ((60 * (2000 * 500))/1000000.0f),   /* pixelClock */ 
    1600,                               /* Hact */ 
    480,                                /* Vact */
    2000,                               /* Htot */
    500,                                /* Vtot */
    360,                                /* Hsbp */
    17,                                 /* Vsbp */
    160,                                /* Hsync */
    10,                                 /* Vsync */ 
    0,                                  /* HtotMin */
    0,                                  /* VtotMin */
    0,                                  /* HtotMax */
    0,                                  /* VtotMax */
    CYGFX_DISP_DCK_INVERT_ON,           /* DCKInvertEnable */
    CYGFX_GEN_POLARITY_HIGH,            /* polEn */
    CYGFX_GEN_POLARITY_LOW,             /* polHs */
    CYGFX_GEN_POLARITY_LOW,             /* polVs */
    CYGFX_DISP_RGB_LOW,                 /* pixInv */
    s_panel_XXX_1600_480_tcon, 
    sizeof(s_panel_XXX_1600_480_tcon) / sizeof(CYGFX_DISP_TCON_PROPERTIES_S)
};


static CYGFX_DISP_TCON_PROPERTIES_S    s_panel_XXX_800_480_tcon[] = {
        { 0x0408, 0x00000108}, /* CTRL (ENLVDS | BYPASS) */
        { 0x0410, 0x0506070a}, /* MapBit3_0   ( R3, R4, R5, G0)     */
        { 0x0414, 0x13020304}, /* MapBit7_4   ( B1, R0, R1, R2)     */
        { 0x0418, 0x0d0e0f12}, /* MapBit11_8  ( G3, G4, G5, B0)     */
        { 0x041c, 0x191a0b0c}, /* MapBit15_12 ( VSYNC, EN, G1, G2)  */
        { 0x0420, 0x15161718}, /* MapBit19_16 ( B3, B4, B5, HSYNC)  */
        { 0x0424, 0x10111b14}, /* MapBit23_20 ( B6, B7, RES, B2)    */
        { 0x0428, 0x00010809}, /* MapBit27_24 ( R6, R7, G6, G7)     */
    };

CYGFX_DISP_PROPERTIES_S s_panel_XXX_800_480 = {
    CYGFX_DISP_CONTROLLER_0,            /* CYGFX_DISP_CONTROLLER */
    CYGFX_DISP_MODE_SINGLE_SCREEN,      /* CYGFX_DISP_MODE */
    CYGFX_DISP_SYNC_MODE_NONE,          /* CYGFX_DISP_SYNC_MODE */
    ((60 * ( 992 * 500))/1000000.0f),   /* pixelClock */ 
    800,                                /* Hact */ 
    480,                                /* Vact */
    992,                                /* Htot */
    500,                                /* Vtot */
    168,                                /* Hsbp */
    17,                                 /* Vsbp */
    72,                                 /* Hsync */
    7,                                  /* Vsync */ 
    0,                                  /* HtotMin */
    0,                                  /* VtotMin */
    0,                                  /* HtotMax */
    0,                                  /* VtotMax */
    CYGFX_DISP_DCK_INVERT_OFF,          /* DCKInvertEnable */
    CYGFX_GEN_POLARITY_HIGH,            /* polEn */
    CYGFX_GEN_POLARITY_LOW,             /* polHs */
    CYGFX_GEN_POLARITY_LOW,             /* polVs */
    CYGFX_DISP_RGB_LOW,                 /* pixInv */
    (CYGFX_DISP_TCON_PROPERTIES_S*)s_panel_XXX_800_480_tcon, 
    sizeof(s_panel_XXX_800_480_tcon) / sizeof(CYGFX_DISP_TCON_PROPERTIES_S)
};


static CYGFX_DISP_TCON_PROPERTIES_S    s_panel_XXX_1280_800_tcon[] = {
        { 0x0408, 0x00000108}, /* CTRL (ENLVDS | BYPASS) */
        { 0x0410, 0x0506070a}, /* MapBit3_0   ( R3, R4, R5, G0)     */
        { 0x0414, 0x13020304}, /* MapBit7_4   ( B1, R0, R1, R2)     */
        { 0x0418, 0x0d0e0f12}, /* MapBit11_8  ( G3, G4, G5, B0)     */
        { 0x041c, 0x191a0b0c}, /* MapBit15_12 ( VSYNC, EN, G1, G2)  */
        { 0x0420, 0x15161718}, /* MapBit19_16 ( B3, B4, B5, HSYNC)  */
        { 0x0424, 0x10111b14}, /* MapBit23_20 ( B6, B7, RES, B2)    */
        { 0x0428, 0x00010809}, /* MapBit27_24 ( R6, R7, G6, G7)     */
    };

CYGFX_DISP_PROPERTIES_S s_panel_XXX_1280_800 = {
    CYGFX_DISP_CONTROLLER_0,            /* CYGFX_DISP_CONTROLLER */
    CYGFX_DISP_MODE_SINGLE_SCREEN,      /* CYGFX_DISP_MODE */
    CYGFX_DISP_SYNC_MODE_NONE,          /* CYGFX_DISP_SYNC_MODE */
    ((60 *( 1680 * 831))/1000000.0f),   /* pixelClock */ 
    1280,                               /* Hact */ 
    800,                                /* Vact */
    1680,                               /* Htot */
    831,                                /* Vtot */
    328,                                /* Hsbp */
    30,                                 /* Vsbp */
    128,                                /* Hsync */
    6,                                  /* Vsync */ 
    0,                                  /* HtotMin */
    0,                                  /* VtotMin */
    0,                                  /* HtotMax */
    0,                                  /* VtotMax */
    CYGFX_DISP_DCK_INVERT_OFF,          /* DCKInvertEnable */
    CYGFX_GEN_POLARITY_HIGH,            /* polEn */
    CYGFX_GEN_POLARITY_LOW,             /* polHs */
    CYGFX_GEN_POLARITY_LOW,             /* polVs */
    CYGFX_DISP_RGB_LOW,                 /* pixInv */
    (CYGFX_DISP_TCON_PROPERTIES_S*)s_panel_XXX_1280_800_tcon, 
    sizeof(s_panel_XXX_1280_800_tcon) / sizeof(CYGFX_DISP_TCON_PROPERTIES_S)
};

static CYGFX_DISP_TCON_PROPERTIES_S    s_panel_XXX_1920_720_tcon[] = {
        { 0x0408, 0x00000108}, /* CTRL (ENLVDS | BYPASS) */
        { 0x0410, 0x03040508}, /* MapBit3_0   ( R3, R4, R5, G0)     */
        { 0x0414, 0x11000102}, /* MapBit7_4   ( B1, R0, R1, R2)     */
        { 0x0418, 0x0b0c0d10}, /* MapBit11_8  ( G3, G4, G5, B0)     */
        { 0x041c, 0x191a090a}, /* MapBit15_12 ( VSYNC, EN, G1, G2)  */
        { 0x0420, 0x13141518}, /* MapBit19_16 ( B3, B4, B5, HSYNC)  */
        { 0x0424, 0x16171b12}, /* MapBit23_20 ( B6, B7, RES, B2)    */
        { 0x0428, 0x06070e0f}, /* MapBit27_24 ( R6, R7, G6, G7)     */
};

CYGFX_DISP_PROPERTIES_S s_panel_XXX_1920_720 = {
     CYGFX_DISP_CONTROLLER_0,             /* CYGFX_DISP_CONTROLLER */
     CYGFX_DISP_MODE_SINGLE_SCREEN,       /* CYGFX_DISP_MODE */
     CYGFX_DISP_SYNC_MODE_NONE,           /* CYGFX_DISP_SYNC_MODE */
     ((60 * (2200 * 820)) / 1000000.0f),  /* pixelClock */
     1920,                                /* Hact */
     720,                                 /* Vact */
     2200,                                /* Htot */
     820,                                 /* Vtot */
     220,                                 /* Hsbp */
     80,                                  /* Vsbp */
     200,                                 /* Hsync */
     60,                                  /* Vsync */
     0,                                   /* HtotMin */
     0,                                   /* VtotMin */
     0,                                   /* HtotMax */
     0,                                   /* VtotMax */
     CYGFX_DISP_DCK_INVERT_ON,            /* DCKInvertEnable */
     CYGFX_GEN_POLARITY_HIGH,             /* polEn */
     CYGFX_GEN_POLARITY_LOW,              /* polHs */
     CYGFX_GEN_POLARITY_LOW,              /* polVs */
     CYGFX_DISP_RGB_LOW,                  /* pixInv */
#ifndef FPGA
     s_panel_XXX_1920_720_tcon,
     sizeof(s_panel_XXX_1920_720_tcon) / sizeof(CYGFX_DISP_TCON_PROPERTIES_S)
#else
    NULL,
    0
#endif
};

static CYGFX_DISP_TCON_PROPERTIES_S    s_panel_XXX_1920_1080_tcon_dual[] = {
        { 0x0408, 0x00000109}, /* CTRL (ENLVDS | BYPASS | DUAL_INTERLACED) */
        { 0x0410, 0x03040508}, /* MapBit3_0   (R3,    R4,  R5, G0)    */
        { 0x0414, 0x11000102}, /* MapBit7_4   (B1,    R0,  R1, R2)    */
        { 0x0418, 0x0b0c0d10}, /* MapBit11_8  (G3,    G4,  G5, B0)    */
        { 0x041c, 0x191a090a}, /* MapBit15_12 (VSYNC, EN,  G1, G2)    */
        { 0x0420, 0x13141518}, /* MapBit19_16 (B3,    B4,  B5, HSYNC) */
        { 0x0424, 0x16171b12}, /* MapBit23_20 (B6,    B7, RES, B2)    */
        { 0x0428, 0x06070e0f}, /* MapBit27_24 (R6,    R7,  G6, G7)    */
        /* Dual Channel - Same values as for mapping above */
        { 0x042c, 0x03040508}, /* MapBit3_0_dual   = MapBit3_0   */
        { 0x0430, 0x11000102}, /* MapBit7_4_dual   = MapBit7_4   */
        { 0x0434, 0x0b0c0d10}, /* MapBit11_8_dual  = MapBit11_8  */
        { 0x0438, 0x191a090a}, /* MapBit15_12_dual = MapBit15_12 */
        { 0x043c, 0x13141518}, /* MapBit19_16_dual = MapBit19_16 */
        { 0x0440, 0x16171b12}, /* MapBit23_20_dual = MapBit23_20 */
        { 0x0444, 0x06070e0f}, /* MapBit27_24_dual = MapBit27_24 */
};

CYGFX_DISP_PROPERTIES_S s_panel_XXX_1920_1080_dual = {
    CYGFX_DISP_CONTROLLER_0,           /* CYGFX_DISP_CONTROLLER */
    CYGFX_DISP_MODE_DUAL_CHANNEL,      /* CYGFX_DISP_MODE */
    CYGFX_DISP_SYNC_MODE_NONE,         /* CYGFX_DISP_SYNC_MODE */
    ((50 * (2640 * 1125))/1000000.0f), /* pixelClock */
    1920,                              /* Hact */
    1080,                              /* Vact */
    2640,                              /* Htot */
    1125,                              /* Vtot */
    672,                               /* Hsbp */
    9,                                 /* Vsbp */
    44,                                /* Hsync */
    5,                                 /* Vsync */
    0,                                 /* HtotMin */
    0,                                 /* VtotMin */
    0,                                 /* HtotMax */
    0,                                 /* VtotMax */
    CYGFX_DISP_DCK_INVERT_ON,          /* DCKInvertEnable */
    CYGFX_GEN_POLARITY_HIGH,           /* polEn */
    CYGFX_GEN_POLARITY_LOW,            /* polHs */
    CYGFX_GEN_POLARITY_LOW,            /* polVs */
    CYGFX_DISP_RGB_LOW,                /* pixInv */
    (CYGFX_DISP_TCON_PROPERTIES_S*)s_panel_XXX_1920_1080_tcon_dual,
    sizeof(s_panel_XXX_1920_1080_tcon_dual) / sizeof(CYGFX_DISP_TCON_PROPERTIES_S)
};
