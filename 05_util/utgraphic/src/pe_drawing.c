/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        pe_drawing.c
 *              VIDEOSS simple drawing utility functions
 */

/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "cygfx_driver_api.h"

#include "sm_util.h"
#include "pe_matrix.h"
#include "pe_drawing.h"
#include "ut_compatibility.h"

/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/* This sample library does not support multi threading! We just save all settings as static variables. */

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/
static CYGFX_ERROR i_utSetProp(UTIL_CONTEXT *pUTCtx, CYGFX_BE_CONTEXT ctx, CYGFX_SURFACE src);

void utResetContext(UTIL_CONTEXT *pUTCtx)
{
    pUTCtx->r = 255;
    pUTCtx->g = 255;
    pUTCtx->b = 255;
    pUTCtx->a = 255;

    /* Store the target surface. */
    pUTCtx->sStore = 0;

    /* Store the mask (brush) surface. */
    pUTCtx->sMask = 0;

    /* Matrix for the mask (can be NULL) */
    pUTCtx->mBrushMat = 0;

    /* Store the font surface. */
    pUTCtx->sFont = 0;
    pUTCtx->nCharSpace = 2;
    pUTCtx->pCharSize = 0;

    /* Line width */
    pUTCtx->fLineWidth = 1.0f;

    /* Line end type */
    pUTCtx->line_end = UT_LINE_END_SQUARE;

    /* Filter mode */
    pUTCtx->filter = CYGFX_GEN_FILTER_BILINEAR;

    /* Point size for drawing */
    pUTCtx->fPointWidth = 1.0f;

    /* Generated point size. It stores the size of the 1 bpp circle used for all round corners. */
    pUTCtx->PointSize = 0;

    pUTCtx->attr_gamma = CYGFX_BE_GAMMA_CORRECTED;

    /* Generated point surface object. */
    pUTCtx->sPoint = 0;
}

void utTarget(UTIL_CONTEXT *pUTCtx, CYGFX_SURFACE pTarget)
{
    pUTCtx->sStore = pTarget;
}

void utColor(UTIL_CONTEXT *pUTCtx, CYGFX_U08 r, CYGFX_U08 g, CYGFX_U08 b, CYGFX_U08 a)
{
    pUTCtx->r = r;
    pUTCtx->g = g;
    pUTCtx->b = b;
    pUTCtx->a = a;
}

void utBrush(UTIL_CONTEXT *pUTCtx, CYGFX_SURFACE sBrush, Mat3x2 mBrushMat)
{
    pUTCtx->sMask = sBrush;
    pUTCtx->mBrushMat = mBrushMat;
}


void utFilter(UTIL_CONTEXT *pUTCtx, CYGFX_U32 filter)
{
    pUTCtx->filter = filter;
}

void utDrawGammaAttr(UTIL_CONTEXT *pUTCtx, CYGFX_BE_GAMMA gammaValue)
{
    pUTCtx->attr_gamma = gammaValue;
}

/* This helper function will be used for several drawing functions to prepare a context */
static CYGFX_ERROR i_utSetProp(UTIL_CONTEXT *pUTCtx, CYGFX_BE_CONTEXT ctx, CYGFX_SURFACE src)
{
    CYGFX_ERROR ret = CYGFX_OK;

    /* reset the context */
    UTIL_SUCCESS(ret, CyGfx_BeResetContext(ctx));
    /* set gamma correction */
    UTIL_SUCCESS(ret, CyGfx_BeSetAttribute(ctx, CYGFX_BE_CTX_ATTR_GAMMA  , (CYGFX_U32)pUTCtx->attr_gamma));
    /* set render mode */
    UTIL_SUCCESS(ret, CyGfx_BeSetAttribute(ctx, CYGFX_BE_CTX_ATTR_RENDER_MODE, CYGFX_BE_RENDER_MODE_MIXED));
    /* bind the given source surface */
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, src));
    /* bind the stored target as store and blend destination*/
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_STORE | CYGFX_BE_TARGET_DST, pUTCtx->sStore));
    /* Set the color */
    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, CYGFX_SM_COLOR_TO_RGBA(pUTCtx->r, pUTCtx->g, pUTCtx->b, pUTCtx->a)));
    /* in case of brush we assign a mask and set the ROP mode */
    if (NULL != pUTCtx->sMask)
    {
        UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_MASK, pUTCtx->sMask));
        UTIL_SUCCESS(ret, CyGfx_BeRopOperation(ctx, CYGFX_BE_ROP_MASKCOPY, CYGFX_BE_ROP_MASKCOPY, CYGFX_BE_ROP_MASKCOPY, CYGFX_BE_ROP_SRCCOPY));
        UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_MASK, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, pUTCtx->mBrushMat));
    }
    /* Finally we apply the filter mode */
    CyGfx_BeSetAttribute (ctx, CYGFX_BE_CTX_ATTR_FILTER, pUTCtx->filter);
    return ret;
}

void utLineEnd(UTIL_CONTEXT *pUTCtx, UTIL_LINE_END type)
{
    pUTCtx->line_end = type;
}

void utLineWidth(UTIL_CONTEXT *pUTCtx, CYGFX_FLOAT fWidth)
{
    pUTCtx->fLineWidth = fWidth;
}

CYGFX_ERROR utLine(UTIL_CONTEXT *pUTCtx, CYGFX_S32 x1, CYGFX_S32 y1, CYGFX_S32 x2, CYGFX_S32 y2)
{
    /* We won't implement 2 drawing functions. So we implicitly call the float version */
    return utLinef(pUTCtx, (CYGFX_FLOAT)x1 + 0.5f, (CYGFX_FLOAT)y1 + 0.5f, (CYGFX_FLOAT)x2 + 0.5f, (CYGFX_FLOAT)y2 + 0.5f);
}


CYGFX_ERROR utLinef(UTIL_CONTEXT *pUTCtx, CYGFX_FLOAT x1, CYGFX_FLOAT y1, CYGFX_FLOAT x2, CYGFX_FLOAT y2)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT fLenght, fLenghtCeil, fWidthCeil;
    CYGFX_SURFACE src;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    Mat3x2 mat, mat2;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }

    /* Our line drawing function based on a trick. We use a rectangle pointing to a
    virtual source buffer and rotate and scale this buffer to the requested line
    position. A surface without buffer forces the blit engine to use the
    constant color attribute of the surface instead of reading pixel data from memory. */

    /* Start with the calculation of the minimum buffer width */
    fWidthCeil = ceilf(pUTCtx->fLineWidth);
    /* Calculate the length. */
    fLenght = sqrtf(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
    /* Only for line end type "INCLUDE" we make the line one "line width" larger */
    if (pUTCtx->line_end == UT_LINE_END_SQUARE)
    {
        fLenght += pUTCtx->fLineWidth;
    }
    /* We need at least ... */
    fLenghtCeil = ceilf(fLenght);

    /* For round line ends we start to paint circles at the line end */
    if ((pUTCtx->line_end == UT_LINE_END_ROUND) && (pUTCtx->PointSize != 0) && (pUTCtx->fLineWidth > 1.0))
    {
        /* Prepare a context */
        UTIL_SUCCESS(ret, i_utSetProp(pUTCtx, &ctx, pUTCtx->sPoint));

        /* Prepare a matrix for our point surface */
        utMat3x2LoadIdentity(mat);
        /* It is important to scale it to the line width size */
        utMat3x2Scale(mat, (CYGFX_FLOAT)pUTCtx->fLineWidth / (CYGFX_FLOAT)pUTCtx->PointSize, (CYGFX_FLOAT)pUTCtx->fLineWidth / (CYGFX_FLOAT)pUTCtx->PointSize);
        /* Move it to the coordinate center */
        utMat3x2Translate(mat, -0.5f * (CYGFX_FLOAT)pUTCtx->PointSize, -0.5f * (CYGFX_FLOAT)pUTCtx->PointSize);
        /* Make a copy of this matrix so we can reuse it for the second point */
        utMat3x2Copy(mat2, mat);
        /* Move it to requested position.
        We use the ...Pre function otherwise we had to start with this calulation */
        utMat3x2TranslatePre(mat, x1, y1);
        /* Set the matrix */
        CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);

        if (pUTCtx->sMask == 0UL)
        {
            /* Set a different blend mode because the color is already filtered (= pre-multiplied alpha)*/

            CyGfx_BeBlendFunc(&ctx,
                CYGFX_BE_BF_GL_SRC_ALPHA, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA,
                CYGFX_BE_BF_GL_SRC_ALPHA, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA,
                CYGFX_BE_BF_GL_SRC_ALPHA, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA,
                CYGFX_BE_BF_GL_SRC_ALPHA, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA);
        }

        /* Paint it  */
        UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));

        /* the same for the second point */
        utMat3x2TranslatePre(mat2, x2, y2);
        CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat2);
        /* Paint it  */
        UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));
    }

    /* Prepare a "dummy" buffer surface */
    utSmGenSurfaceObjects(1, &src);
    CyGfx_SmAssignBuffer(src, (CYGFX_U32)fLenghtCeil, (CYGFX_U32)fWidthCeil, CYGFX_SM_FORMAT_R8G8B8A8, 0, 0);

    /* (Re-)start with a new context */
    i_utSetProp(pUTCtx, &ctx, src);

    /* We need a empty matrix */
    utMat3x2LoadIdentity(mat);
    /* rotate it in the line direction */
    utMat3x2Rot(mat, ((-atan2f(x2 - x1, y2 - y1) * 180.0f) / 3.1415f) + 90.0f);
    /* Scale it to the requested size */
    utMat3x2Scale(mat, fLenght / fLenghtCeil, pUTCtx->fLineWidth/fWidthCeil);
    if (pUTCtx->line_end == UT_LINE_END_SQUARE)
    {
        /* The rotation point must be inside for this mode */
        utMat3x2Translate(mat, -pUTCtx->fLineWidth * 0.5f, -fWidthCeil * 0.5f);
    }
    else
    {
        /* Otherwise we just need to center the line width */
        utMat3x2Translate(mat, 0, -fWidthCeil * 0.5f);
    }
    /* Move to the start point */
    utMat3x2TranslatePre(mat, x1, y1);
    CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);

    if (pUTCtx->sMask == 0UL)
    {
        /* Set a different blend mode because the color is already filtered (= pre-multiplied alpha)*/
        CyGfx_BeBlendFunc(&ctx,
                CYGFX_BE_BF_GL_ONE, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA,
                CYGFX_BE_BF_GL_ONE, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA,
                CYGFX_BE_BF_GL_ONE, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA,
                CYGFX_BE_BF_GL_ONE, CYGFX_BE_BF_GL_ONE_MINUS_SRC_ALPHA);

    }

    /* Paint it  */
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));
    utSmDeleteSurfaceObjects(1, &src);

    return ret;
}

void utSetFont(UTIL_CONTEXT *pUTCtx, CYGFX_SURFACE pFontSurf)
{
    pUTCtx->sFont = pFontSurf;
}

void utSetCharacterSpace(UTIL_CONTEXT *pUTCtx, CYGFX_U32 nCharSpace)
{
    pUTCtx->nCharSpace = nCharSpace;
}

void utSetCharacterSize(UTIL_CONTEXT *pUTCtx, const CYGFX_U32 *pCharWidth)
{
    pUTCtx->pCharSize = pCharWidth;
}

CYGFX_ERROR utCalcCharacterSize(UTIL_CONTEXT *pUTCtx)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 c, x, y, lw, lh, lht, ok, sx, sy;
    CYGFX_U08 r, g, b, a;
    if(pUTCtx->sFont == 0)
    {
        return CYGFX_ERR;
    }
    /* We assume the font defined 16*16 letters with identical size */
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(pUTCtx->sFont, CYGFX_SM_ATTR_WIDTH, &lw));
    UTIL_SUCCESS(ret, CyGfx_SmGetAttribute(pUTCtx->sFont, CYGFX_SM_ATTR_HEIGHT, &lht));
    lw = lw >> 4;
    lh = lht >> 4;
    for (c = 0; c < 256; c++)
    {
        ok = 1;
        sx = c & 0xf;
        sy = c >> 4;
        for (x = lw - 1; (x > 2) && (ok == 1); x--)
        {
            for (y = 0; (y < lh) && (ok == 1); y++)
            {
                UTIL_SUCCESS(ret, utSurfGetPixel(pUTCtx->sFont, x + (sx * lw), y + (sy * lh), &r, &g, &b, &a));
                if ((r != 0) || (g != 0) || (b != 0) || (a != 0))
                {
                    ok = 0;
                }
            }
        }
        pUTCtx->utCharSize[c] = x + 2;
    }

    if (ret == CYGFX_OK)
    {
        pUTCtx->pCharSize = pUTCtx->utCharSize;
    }
    else
    {
        pUTCtx->pCharSize = 0;
    }
    return ret;
}

CYGFX_ERROR utTextOut(UTIL_CONTEXT *pUTCtx, CYGFX_S32 x, CYGFX_S32 y, CYGFX_CHAR* text)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    CYGFX_U32 sx, sy, lw, lh, lht, lwr;
    Mat3x2 m;
    CYGFX_S32 startx;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }

    i_utSetProp(pUTCtx, &ctx, pUTCtx->sFont);

    /* We assume the font defined 16*16 letters with identical size */
    CyGfx_SmGetAttribute(pUTCtx->sFont, CYGFX_SM_ATTR_WIDTH, &lw);
    CyGfx_SmGetAttribute(pUTCtx->sFont, CYGFX_SM_ATTR_HEIGHT, &lht);
    lw = lw >> 4;
    lh = lht >> 4;

    startx = x;

    /* Loop as long as no 0 comes */
    while (*text != 0)
    {
        CYGFX_CHAR c = (CYGFX_CHAR)*text;
        /* in case of \n we step in the next line */
        if (c == '\n')
        {
            x = startx;
            y += (CYGFX_S32)lh;
            text++;
            continue;
        }
        /* Calculate the letter position */
        sx = c & 0xf;
        sy = c >> 4;

        if (NULL != pUTCtx->pCharSize)
        {
            lwr = pUTCtx->pCharSize[c];
        }
        else
        {
            lwr = lw;
        }

        /* restrict the surface to this area */
        CyGfx_BeActiveArea(&ctx, CYGFX_BE_TARGET_SRC, (CYGFX_S16)(sx*lw), (CYGFX_S16)((lht - lh) - (sy*lh)), (CYGFX_U16)lwr, (CYGFX_U16)lh);
        /* Prepare a related matrix */
        utMat3x2LoadIdentity(m);
        utMat3x2Translate(m, -(CYGFX_FLOAT)(sx*lw), -(CYGFX_FLOAT)((lht - lh) - (sy*lh)));
        utMat3x2TranslatePre(m, (CYGFX_FLOAT)x, (CYGFX_FLOAT)y);
        CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, m);
        /* Paint it */
        UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));
        x += (CYGFX_S32)(lwr + pUTCtx->nCharSpace);
        text++;
    }

    return ret;
}

CYGFX_ERROR utTextOutEx(UTIL_CONTEXT *pUTCtx, CYGFX_S32 x, CYGFX_S32 y, CYGFX_FLOAT fsx, CYGFX_FLOAT fsy, UTIL_TEXT_POS Pos, CYGFX_CHAR* text)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    CYGFX_U32 sx, sy, lw, lh, lht, lwr, first;
    Mat3x2 m;
    CYGFX_FLOAT startx, fx, fy;
    CYGFX_CHAR* p = text;
    CYGFX_U32 nWidth, nHeight;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }

    i_utSetProp(pUTCtx, &ctx, pUTCtx->sFont);

    /* We assume the font defined 16*16 letters with identical size */
    CyGfx_SmGetAttribute(pUTCtx->sFont, CYGFX_SM_ATTR_WIDTH, &lw);
    CyGfx_SmGetAttribute(pUTCtx->sFont, CYGFX_SM_ATTR_HEIGHT, &lht);
    lw = lw >> 4;
    lh = lht >> 4;

    nWidth = 0;
    nHeight = 0;
    first = 1;
    lwr = 0;
    while (*p != 0)
    {
        CYGFX_CHAR c = (CYGFX_CHAR)*p;
        if (first == 1)
        {
            first = 0;
            nHeight += lh;
        }
        if (c == '\n')
        {
            if (nWidth < lwr)
            {
                nWidth = lwr;
            }
            lwr = 0;
            first = 1;
        }
        if (NULL != pUTCtx->pCharSize)
        {
            lwr += pUTCtx->pCharSize[c];
        }
        else
        {
            lwr += lw;
        }
        lwr += pUTCtx->nCharSpace;

        p++;
    }
    if (nWidth < lwr)
    {
        nWidth = lwr;
    }
    switch(Pos)
    {
    case UT_TEXT_POS_MIDDLE_LEFT:
        fx = (CYGFX_FLOAT)x;
        fy = (CYGFX_FLOAT)y - (((CYGFX_FLOAT)nHeight * 0.5f) * fsy);
        break;
    case UT_TEXT_POS_BOTTOM_LEFT:
        fx = (CYGFX_FLOAT)x;
        fy = (CYGFX_FLOAT)y;
        break;
    case UT_TEXT_POS_TOP_MIDDLE:
        fx = (CYGFX_FLOAT)x - (((CYGFX_FLOAT)nWidth * 0.5f) * fsx);
        fy = (CYGFX_FLOAT)y - ((CYGFX_FLOAT)nHeight * fsy);
        break;
    case UT_TEXT_POS_CENTER:
        fx = (CYGFX_FLOAT)x - (((CYGFX_FLOAT)nWidth * 0.5f) * fsx);
        fy = (CYGFX_FLOAT)y - ((CYGFX_FLOAT)nHeight * 0.5f * fsy);
        break;
    case UT_TEXT_POS_BOTTOM_MIDDLE:
        fx = (CYGFX_FLOAT)x - (((CYGFX_FLOAT)nWidth * 0.5f) * fsx);
        fy = (CYGFX_FLOAT)y;
        break;
    case UT_TEXT_POS_TOP_RIGHT:
        fx = (CYGFX_FLOAT)x - ((CYGFX_FLOAT)nWidth * fsx);
        fy = (CYGFX_FLOAT)y - ((CYGFX_FLOAT)nHeight * fsy);
        break;
    case UT_TEXT_POS_MIDDLE_RIGHT:
        fx = (CYGFX_FLOAT)x - ((CYGFX_FLOAT)nWidth * fsx);
        fy = (CYGFX_FLOAT)y - (((CYGFX_FLOAT)nHeight * 0.5f) * fsy);
        break;
    case UT_TEXT_POS_BOTTOM_RIGHT:
        fx = (CYGFX_FLOAT)x - ((CYGFX_FLOAT)nWidth * fsx);
        fy = (CYGFX_FLOAT)y;
        break;
    default:
        fx = (CYGFX_FLOAT)x;
        fy = (CYGFX_FLOAT)y - ((CYGFX_FLOAT)nHeight * fsy);
        break;
    }
    startx = fx;

    /* Loop as long as no 0 comes */
    while (*text != 0)
    {
        CYGFX_CHAR c = (CYGFX_CHAR)*text;
        /* in case of \n we step in the next line */
        if (c == '\n')
        {
            fx = startx;
            fy += (CYGFX_FLOAT)lh;
            text++;
            continue;
        }
        /* Calculate the letter position */
        sx = c & 0xf;
        sy = c >> 4;

        if (NULL != pUTCtx->pCharSize)
        {
            lwr = pUTCtx->pCharSize[c];
        }
        else
        {
            lwr = lw;
        }

        /* restrict the surface to this area */
        CyGfx_BeActiveArea(&ctx, CYGFX_BE_TARGET_SRC, (CYGFX_S16)(sx*lw), (CYGFX_S16)((lht - lh) - (sy*lh)), (CYGFX_U16)lwr, (CYGFX_U16)lh);
        /* Prepare a related matrix */
        utMat3x2LoadIdentity(m);
        utMat3x2Translate(m, (CYGFX_FLOAT)fx, (CYGFX_FLOAT)fy);
        utMat3x2Scale(m, fsx, fsy);
        utMat3x2Translate(m, -(CYGFX_FLOAT)(sx*lw), -(CYGFX_FLOAT)((lht - lh) - (sy*lh)));
        CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, m);
        /* Paint it */
        UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));
        fx += ((CYGFX_FLOAT)lwr * fsx) + (CYGFX_FLOAT)pUTCtx->nCharSpace;
        text++;
    }

    return ret;
}

CYGFX_ERROR utInitPoint(UTIL_CONTEXT *pUTCtx, CYGFX_U08 nSize)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U08 x, y;
    CYGFX_FLOAT c, d, r;

    /* This function creates a surface and render a circle in it.
    We need this circle to draw round corners points and so on. A
    one bit per pixel surface is large enough. The dimension is defined
    by the caller and should be twice the size of the biggest required
    circle to the a smooth anti aliased rounding */

    if (pUTCtx->sPoint != 0)
    {
        utSurfDeleteBuffer(pUTCtx->sPoint);
        utSmDeleteSurfaceObjects(1, &pUTCtx->sPoint);
        pUTCtx->sPoint = 0;
    }

    if (nSize == 0)
    {
        return CYGFX_OK;
    }

    utSmGenSurfaceObjects(1, &pUTCtx->sPoint);
    if (utSurfCreateBuffer(pUTCtx->sPoint, nSize, nSize, CYGFX_SM_FORMAT_A1) != CYGFX_OK)
    {
        return CYGFX_ERR;
    }

    /* Fill the buffer with the circle */
    c = ((CYGFX_FLOAT)nSize * 0.5f) - 0.5f;
    r = (c * c) + 0.5f;

    for (y = 0; y < nSize; y++)
    {
        for (x = 0; x < nSize; x++)
        {
            d = ((c - (CYGFX_FLOAT)x)*(c - (CYGFX_FLOAT)x)) + ((c - (CYGFX_FLOAT)y)*(c - (CYGFX_FLOAT)y));

            if (d <= r)
            {
                utSurfSetPixel(pUTCtx->sPoint, x, y, 255, 255, 255, 255);
            }
            else
            {
                utSurfSetPixel(pUTCtx->sPoint, x, y, 0, 0, 0, 0);
            }
        }
    }

    /* Store the circle size */
    pUTCtx->PointSize = nSize;

    return ret;
}

void utPointSize(UTIL_CONTEXT *pUTCtx, CYGFX_FLOAT fWidth)
{
    pUTCtx->fPointWidth = fWidth;
}

CYGFX_ERROR utPoint(UTIL_CONTEXT *pUTCtx, CYGFX_S32 x, CYGFX_S32 y)
{
    /* Like for line we just call the float version */
    return utPointf(pUTCtx, (CYGFX_FLOAT)x + 0.5f, (CYGFX_FLOAT)y + 0.5f);
}

CYGFX_ERROR utPointf(UTIL_CONTEXT *pUTCtx, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    Mat3x2 mat;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }
    if (pUTCtx->PointSize == 0)
    {
        return CYGFX_ERR;
    }

    /* Prepare a context */
    i_utSetProp(pUTCtx, &ctx, pUTCtx->sPoint);

    /* Prepare a matrix */
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, (CYGFX_FLOAT)pUTCtx->fPointWidth / (CYGFX_FLOAT)pUTCtx->PointSize, (CYGFX_FLOAT)pUTCtx->fPointWidth / (CYGFX_FLOAT)pUTCtx->PointSize);
    utMat3x2Translate(mat, -0.5f * (CYGFX_FLOAT)pUTCtx->PointSize, -0.5f * (CYGFX_FLOAT)pUTCtx->PointSize);
    utMat3x2TranslatePre(mat, x, y);
    CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);

    /* Paint it */
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));

    return ret;
}

CYGFX_ERROR utRect(UTIL_CONTEXT *pUTCtx, CYGFX_S32 x, CYGFX_S32 y, CYGFX_U32 w, CYGFX_U32 h)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    CYGFX_SURFACE src;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }

    /* A rect is very simple so we don't need comments */

    utSmGenSurfaceObjects(1, &src);
    CyGfx_SmAssignBuffer(src, (CYGFX_U32)x+w, (CYGFX_U32)y+h, CYGFX_SM_FORMAT_R8G8B8A8, 0, 0);
    i_utSetProp(pUTCtx, &ctx, src);
    CyGfx_BeActiveArea(&ctx, CYGFX_BE_TARGET_SRC, x, y, w, h);

    /* switch off blend */
    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(&ctx, CYGFX_BE_TARGET_DST, 0));

    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0,0));
    utSmDeleteSurfaceObjects(1, &src);

    return ret;
}

CYGFX_ERROR utRoundRect(UTIL_CONTEXT *pUTCtx, CYGFX_S32 x, CYGFX_S32 y, CYGFX_S32 w, CYGFX_S32 h, CYGFX_S32 rx, CYGFX_S32 ry)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    CYGFX_SURFACE src;
    Mat3x2 mat, mat2;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }
    if (pUTCtx->PointSize == 0)
    {
        return CYGFX_ERR;
    }

    /* To draw a round rect we paint 4 circles in the corners (O)
       and afterwards 2 rectangles (1 and 2, X is drawn twice):
            O11111O
            2XXXXX2
            2XXXXX2
            O11111O
    */

    /* Prepare the point context */
    i_utSetProp(pUTCtx, &ctx, pUTCtx->sPoint);

    /* Paint the corners */
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, (2.0f * (CYGFX_FLOAT)rx) / (CYGFX_FLOAT)pUTCtx->PointSize, (2.0f * (CYGFX_FLOAT)ry) / (CYGFX_FLOAT)pUTCtx->PointSize);
    utMat3x2Translate(mat, -0.5f * (CYGFX_FLOAT)pUTCtx->PointSize, -0.5f * (CYGFX_FLOAT)pUTCtx->PointSize);
    utMat3x2Copy(mat2, mat);
    utMat3x2TranslatePre(mat, (CYGFX_FLOAT)(x + rx), (CYGFX_FLOAT)(y + ry));
    CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));

    utMat3x2Copy(mat, mat2);
    utMat3x2TranslatePre(mat, (CYGFX_FLOAT)((x + w) - rx), (CYGFX_FLOAT)(y + ry));
    CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));

    utMat3x2Copy(mat, mat2);
    utMat3x2TranslatePre(mat, (CYGFX_FLOAT)((x + w) - rx), (CYGFX_FLOAT)((y + h) - ry));
    CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));

    utMat3x2Copy(mat, mat2);
    utMat3x2TranslatePre(mat, (CYGFX_FLOAT)(x + rx), (CYGFX_FLOAT)((y + h) - ry));
    CyGfx_BeSetGeoMatrix(&ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat);
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0, 0));

    /* And now the 2 rectangles */
    utSmGenSurfaceObjects(1, &src);
    CyGfx_SmAssignBuffer(src, (CYGFX_U16)(x+w), (CYGFX_U16)(y+h), CYGFX_SM_FORMAT_R8G8B8A8, 0, 0);

    i_utSetProp(pUTCtx, &ctx, src);

    CyGfx_BeActiveArea(&ctx, CYGFX_BE_TARGET_SRC, (CYGFX_S16)(x + rx), (CYGFX_S16)y, (CYGFX_U16)(w - rx - rx), (CYGFX_U16)h);
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0,0));
    CyGfx_BeActiveArea(&ctx, CYGFX_BE_TARGET_SRC, (CYGFX_S16)x, (CYGFX_S16)(y + ry), (CYGFX_U16)w, (CYGFX_U16)(h - ry - ry));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(&ctx, 0,0));
    utSmDeleteSurfaceObjects(1, &src);

    return ret;
}

CYGFX_ERROR utClear(UTIL_CONTEXT *pUTCtx)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S ctx;
    CYGFX_U32 w, h;

    if(pUTCtx->sStore == 0)
    {
        return CYGFX_ERR;
    }

    i_utSetProp(pUTCtx, &ctx, 0);

    CyGfx_SmGetAttribute(pUTCtx->sStore, CYGFX_SM_ATTR_WIDTH, &w);
    CyGfx_SmGetAttribute(pUTCtx->sStore, CYGFX_SM_ATTR_HEIGHT, &h);

    CyGfx_BeSetAttribute(&ctx, CYGFX_BE_CTX_ATTR_COLOR, 0);
    UTIL_SUCCESS(ret, CyGfx_BeFill(&ctx, 0, 0, w, h));

    return ret;
}
