/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        pe_matrix.c
 *              Matrix utility functions
 */


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include <math.h>

#include "cygfx_driver_api.h"

#include "pe_matrix.h"



/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

#ifndef TRACE
#define TRACE printf
#endif /* TRACE */

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

static CYGFX_FLOAT DegreeToPI = 3.14159265f / 180.0f; /*(atan(1.0) * 4.0 / 180)*/

static CYGFX_FLOAT TYCbCr[] =
{
    +0.299f, -0.169f, +0.500f,
    +0.587f, -0.331f, -0.419f,
    +0.114f, +0.500f, -0.081f,
    +0.000f, +0.000f, +0.000f
 /* +0.000f, +0.500f, +0.000 */
};

static CYGFX_FLOAT TYCbCr_1[] =
{
    +1.000f, +1.000f, +1.000f,
    -0.000f, -0.344f, +1.772f,
    +1.402f, -0.714f, +0.000f,
    +0.000f, +0.000f, +0.000f
 /* +0.000f, +0.172f, -0.886f */
};

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

static CYGFX_FLOAT cot(CYGFX_FLOAT d);
static void Gfx_MathMultiplyXxX(CYGFX_FLOAT *dst, const CYGFX_FLOAT *src1, const CYGFX_FLOAT *src2, CYGFX_U32 nCols, CYGFX_U32 nRows);

/*
OpenGL always count matrices column first
e.g.
    1   4   7   10
    2   5   8   11
    3   6   9   12
*/

void utMat3x2Copy(Mat3x2 dst, const Mat3x2 src)
{
    dst[ 0] = src[ 0];
    dst[ 1] = src[ 1];
    dst[ 2] = src[ 2];
    dst[ 3] = src[ 3];
    dst[ 4] = src[ 4];
    dst[ 5] = src[ 5];
}

void utMat3x2Multiply(Mat3x2 dst, const Mat3x2 src1, const Mat3x2 src2)
{
    /* just in case dst is the same buffer ... */
    if ((dst == src1) || (dst == src2))
    {
        Mat3x2 tmp;
        utMat3x2Multiply(tmp, src1, src2);
        utMat3x2Copy(dst, tmp);
        return;
    }
    /* We calculate dst = src1 * src2
    *  we assume identity matrix for missing elements
    */
    dst[0] = (src1[0] * src2[0]) + (src1[2] * src2[1]);
    dst[1] = (src1[1] * src2[0]) + (src1[3] * src2[1]);

    dst[2] = (src1[0] * src2[2]) + (src1[2] * src2[3]);
    dst[3] = (src1[1] * src2[2]) + (src1[3] * src2[3]);

    dst[4] = (src1[0] * src2[4]) + (src1[2] * src2[5]) + src1[4];
    dst[5] = (src1[1] * src2[4]) + (src1[3] * src2[5]) + src1[5];
}


void utMat3x2LoadIdentity(Mat3x2 m)
{
    m[ 0] = 1.0F;
    m[ 1] = 0.0F;
    m[ 2] = 0.0F;
    m[ 3] = 1.0F;
    m[ 4] = 0.0F;
    m[ 5] = 0.0F;
}

void utMat3x2Translate(Mat3x2 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    m[4] = (m[0] * x) + (m[2] * y) + m[4];
    m[5] = (m[1] * x) + (m[3] * y) + m[5];
}

void utMat3x2TranslatePre(Mat3x2 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    m[4] += x;
    m[5] += y;
}

void utMat3x2Scale(Mat3x2 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    m[0] *= x;
    m[1] *= x;

    m[2] *= y;
    m[3] *= y;
}

void utMat3x2ScalePre(Mat3x2 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    m[0] *= x;
    m[1] *= y;

    m[2] *= x;
    m[3] *= y;

    m[4] *= x;
    m[5] *= y;
}

void utMat3x2Rot(Mat3x2 m, CYGFX_FLOAT f)
{
    CYGFX_FLOAT c, s, m0, m1, m2, m3;

    f *= DegreeToPI;
    c = (CYGFX_FLOAT)cos((CYGFX_DOUBLE)f);
    s = (CYGFX_FLOAT)sin((CYGFX_DOUBLE)f);

    m0 =  (m[0] * c) + (m[2] * s);
    m1 =  (m[1] * c) + (m[3] * s);
    m2 = -(m[0] * s) + (m[2] * c);
    m3 = -(m[1] * s) + (m[3] * c);

    m[0] = m0;
    m[1] = m1;
    m[2] = m2;
    m[3] = m3;
}

void utMat3x2RotPre(Mat3x2 m, CYGFX_FLOAT f)
{
    CYGFX_FLOAT c, s, m0, m1, m2, m3, m4;

    f *= DegreeToPI;
    c = (CYGFX_FLOAT)cos((CYGFX_DOUBLE)f);
    s = (CYGFX_FLOAT)sin((CYGFX_DOUBLE)f);

    m0   = (c * m[0]) - (s * m[1]);
    m1   = (s * m[0]) + (c * m[1]);
    m2   = (c * m[2]) - (s * m[3]);
    m3   = (s * m[2]) + (c * m[3]);
    m4   = (c * m[4]) - (s * m[5]);
    m[5] = (s * m[4]) + (c * m[5]);

    m[0] = m0;
    m[1] = m1;
    m[2] = m2;
    m[3] = m3;
    m[4] = m4;
}

CYGFX_ERROR utMat3x2Invert(Mat3x2 m)
{
    Mat3x2 dst;
    CYGFX_FLOAT v = (CYGFX_FLOAT)((m[2] * m[1]) - (m[0] * m[3]));

    if (v == 0.0F)
    {
        return CYGFX_ERR;
    }

    dst[0] = (CYGFX_FLOAT)(-m[3] / v);
    dst[2] = (CYGFX_FLOAT)(m[2] / v);
    dst[4] = (CYGFX_FLOAT)(((m[4] * m[3]) - (m[2] * m[5])) / v);

    dst[1] = (CYGFX_FLOAT)(m[1] / v);
    dst[3] = (CYGFX_FLOAT)(-m[0] / v);
    dst[5] = (CYGFX_FLOAT)(((m[0] * m[5]) - (m[4] * m[1])) / v);
    utMat3x2Copy(m, dst);
    return CYGFX_OK;
}

void utMat3x2GetXY(const Mat3x2 m, const CYGFX_FLOAT x, const CYGFX_FLOAT y, CYGFX_FLOAT *xout, CYGFX_FLOAT *yout)
{
    CYGFX_FLOAT tmp;
    /* using a temp var in case of &x = xout in the calling function */
    tmp = (m[0] * x) + (m[2] * y) + m[4];
    if (yout != NULL)
    {
        *yout = (m[1] * x) + (m[3] * y) + m[5];
    }
    if (xout != NULL)
    {
        *xout = tmp;
    }
}


#ifdef TRACE_MATRIX
void matTrace(const CYGFX_CHAR *szName, const CYGFX_FLOAT *m, CYGFX_U32 nCols, CYGFX_U32 nRows)
{
    CYGFX_U32 x, y;
    if (NULL != szName)
    {
        TRACE(szName);
        TRACE(":\n");
    }
    for (y = 0; y < nRows; y++)
    {
        if (y == 0)
        {
            TRACE("/ ");
        }
        else if (y == (nRows -1))
        {
            TRACE("\\ ");
        }
        else
        {
            TRACE("| ");
        }
        for (x = 0; x < nCols; x++)
        {
            TRACE("%8.3f ", m[y+(x*nCols)]);
        }
        if (y == 0)
        {
            TRACE("\\\n");
        }
        else if (y == (nRows -1))
        {
            TRACE("/\n");
        }
        else
        {
            TRACE("|\n");
        }
    }
}
#endif /* TRACE_MATRIX */

static void Gfx_MathMultiplyXxX(CYGFX_FLOAT *dst, const CYGFX_FLOAT *src1, const CYGFX_FLOAT *src2, CYGFX_U32 nCols, CYGFX_U32 nRows)
{
    /* We calculate dst = src1 * src2.
    *  If nCols != nRows, we assume a transposed matrix.
    */
    CYGFX_U32 i, j, k;
    CYGFX_FLOAT val;

    /* the calling function should take care that we have a separate dst buffer */
    if ((dst == src1) || (dst == src2))
    {
        return;
    }

    for (i = 0; i<nRows; i++)
    {
        for (j = 0; j<nCols; j++)
        {
            val = 0.0f;
            for (k = 0; k<nCols; k++)
            {
                val += src1[i + (k * nRows)] * src2[k+ (j * nRows)];
            }
            dst[i + (j * nRows)] = val;
        }
    }
}

#ifdef TRACE_MATRIX
void utMat4x4Trace(const CYGFX_CHAR *szName, const Mat4x4 m)
{
    matTrace(szName, m, 4, 4);
}
#endif /* TRACE_MATRIX */

void utMat4x4LoadIdentity(Mat4x4 m)
{
    m[ 0] = 1.0f;
    m[ 1] = 0.0f;
    m[ 2] = 0.0f;
    m[ 3] = 0.0f;
    m[ 4] = 0.0f;
    m[ 5] = 1.0f;
    m[ 6] = 0.0f;
    m[ 7] = 0.0f;
    m[ 8] = 0.0f;
    m[ 9] = 0.0f;
    m[10] = 1.0f;
    m[11] = 0.0f;
    m[12] = 0.0f;
    m[13] = 0.0f;
    m[14] = 0.0f;
    m[15] = 1.0f;
}

void utMat4x4Translate(Mat4x4 m, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT z)
{
    Mat4x4 tmp, dst;
    CYGFX_U32 i;

    utMat4x4LoadIdentity(tmp);

    tmp[12 + 0] = x;
    tmp[12 + 1] = y;
    tmp[12 + 2] = z;

    Gfx_MathMultiplyXxX(dst, m, tmp, 4, 4);
    for (i = 0; i < 16; i++)
    {
        m[i] = dst[i];
    }
}


void utMat4x4Scale(Mat4x4 m, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT z)
{
    Mat4x4 tmp, dst;
    CYGFX_U32 i;

    utMat4x4LoadIdentity(tmp);

    tmp[ 0] = x;
    tmp[ 5] = y;
    tmp[10] = z;

    Gfx_MathMultiplyXxX(dst, m, tmp, 4, 4);
    for (i = 0; i < 16; i++)
    {
        m[i] = dst[i];
    }
}

void utMat4x4RotX(Mat4x4 m, CYGFX_FLOAT f)
{
    Mat4x4 tmp, dst;
    CYGFX_U32 i;

    f *= DegreeToPI;
    utMat4x4LoadIdentity(tmp);
    tmp[5] = tmp[10] = cosf(f);
    tmp[9] = -sinf(f);
    tmp[6] = -tmp[9];
    Gfx_MathMultiplyXxX(dst, m, tmp, 4, 4);
    for (i = 0; i < 16; i++)
    {
        m[i] = dst[i];
    }
}
/*
Structure of a 4x4 matrix:
0    4    8    12
1    5    9    13
2    6    10    14
3    7    11    15
*/
void utMat4x4RotY(Mat4x4 m, CYGFX_FLOAT f)
{
    Mat4x4 tmp, dst;
    CYGFX_U32 i;

    f *= DegreeToPI;
    utMat4x4LoadIdentity(tmp);
    tmp[0] = tmp[10] = cosf(f);
    tmp[2] = -sinf(f);
    tmp[8] = -tmp[2];
    Gfx_MathMultiplyXxX(dst, m, tmp, 4, 4);
    for (i = 0; i < 16; i++)
    {
        m[i] = dst[i];
    }
}

void utMat4x4RotZ(Mat4x4 m, CYGFX_FLOAT f)
{
    Mat4x4 tmp, dst;
    CYGFX_U32 i;

    f *= DegreeToPI;
    utMat4x4LoadIdentity(tmp);
    tmp[0] = tmp[5] = cosf(f);
    tmp[4] = -sinf(f);
    tmp[1] = -tmp[4];
    Gfx_MathMultiplyXxX(dst, m, tmp, 4, 4);
    for (i = 0; i < 16; i++)
    {
        m[i] = dst[i];
    }
}

static CYGFX_FLOAT cot(CYGFX_FLOAT d)
{
  return (1 / tanf(d));
}
CYGFX_ERROR utMat4x4Perspective(Mat4x4 m, CYGFX_FLOAT fovy, CYGFX_FLOAT aspect, CYGFX_FLOAT zNear, CYGFX_FLOAT zFar)
{
    Mat4x4 tmp, dst;
    CYGFX_U32 i;
    CYGFX_FLOAT f, s;

    fovy *= DegreeToPI;

    f = cot(fovy / 2);
    if ((f == 0.0f) || ((zNear - zFar) == 0.0f) || (aspect == 0.0f))
    {
        return CYGFX_ERR;
    }
    s = 1 / (zNear - zFar);

    for (i = 0; i < 16; i++)
    {
        tmp[i] = 0;
    }
    /* (x << 2) + y == [x][y] */
    tmp[(0<<2)+0] = f / aspect;
    tmp[(1<<2)+1] = f;
    tmp[(2<<2)+2] = (zFar + zNear) * s;
    tmp[(3<<2)+2] = (2 * zFar * zNear) * s;
    tmp[(2<<2)+3] = -1.0f;
    Gfx_MathMultiplyXxX(dst, m, tmp, 4, 4);
    for (i = 0; i < 16; i++)
    {
        m[i] = dst[i];
    }
    return CYGFX_OK;
}

void utMat4x4Copy(Mat4x4 dst, const Mat4x4 src)
{
    dst[ 0] = src[ 0];
    dst[ 1] = src[ 1];
    dst[ 2] = src[ 2];
    dst[ 3] = src[ 3];
    dst[ 4] = src[ 4];
    dst[ 5] = src[ 5];
    dst[ 6] = src[ 6];
    dst[ 7] = src[ 7];
    dst[ 8] = src[ 8];
    dst[ 9] = src[ 9];
    dst[10] = src[10];
    dst[11] = src[11];
    dst[12] = src[12];
    dst[13] = src[13];
    dst[14] = src[14];
    dst[15] = src[15];
}

void utMat4x4Multiply(Mat4x4 dst, const Mat4x4 src1, const Mat4x4 src2)
{
    Mat4x4 tmp;
    if ((dst == src1) || (dst == src2))
    {
        Gfx_MathMultiplyXxX(tmp, src1, src2, 4, 4);
        utMat4x4Copy(dst, tmp);
    }
    Gfx_MathMultiplyXxX(dst, src1, src2, 4, 4);
}

/*
Structure of a 4x4 matrix:
0   4    8   12
1   5    9   13
2   6   10   14
3   7   11   15
*/
CYGFX_ERROR utMat4x4GetXYZ(const Mat4x4 m, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_FLOAT z, CYGFX_FLOAT *xout, CYGFX_FLOAT *yout, CYGFX_FLOAT *zout)
{
    CYGFX_FLOAT x_tmp, y_tmp;
    CYGFX_FLOAT d =    (m[(0<<2)+3] * x) + (m[(1<<2)+3] * y) + (m[(2<<2)+3] * z) + m[(3<<2)+3];

    if (d == 0.0f)
    {
        return CYGFX_ERR;
    }
    x_tmp = ((  (m[(0<<2)+0] * x) + (m[(1<<2)+0] * y) + (m[(2<<2)+0] * z) + m[(3<<2)+0]) / d);
    y_tmp = ((  (m[(0<<2)+1] * x) + (m[(1<<2)+1] * y) + (m[(2<<2)+1] * z) + m[(3<<2)+1]) / d);
    if (zout != NULL)
    {
        *zout = ((  (m[(0<<2)+2] * x) + (m[(1<<2)+2] * y) + (m[(2<<2)+2] * z) + m[(3<<2)+2]) / d);
    }
    if (xout != NULL)
    {
        *xout = x_tmp;
    }
    if (xout != NULL)
    {
        *yout =  y_tmp;
    }
    return CYGFX_OK;
}

#ifdef TRACE_MATRIX
void utMat3x3Trace(const CYGFX_CHAR * szName, const Mat3x3 m)
{
    matTrace(szName, m, 3, 3);
}
#endif /* TRACE_MATRIX */

void utMat3x3LoadIdentity(Mat3x3 m)
{
    m[ 0] = 1.0f;
    m[ 1] = 0.0f;
    m[ 2] = 0.0f;
    m[ 3] = 0.0f;
    m[ 4] = 1.0f;
    m[ 5] = 0.0f;
    m[ 6] = 0.0f;
    m[ 7] = 0.0f;
    m[ 8] = 1.0f;
}

void utMat3x3Copy(Mat3x3 dst, const Mat3x3 src)
{
    dst[ 0] = src[ 0];
    dst[ 1] = src[ 1];
    dst[ 2] = src[ 2];
    dst[ 3] = src[ 3];
    dst[ 4] = src[ 4];
    dst[ 5] = src[ 5];
    dst[ 6] = src[ 6];
    dst[ 7] = src[ 7];
    dst[ 8] = src[ 8];
}

void utMat3x3Multiply(Mat3x3 dst, const Mat3x3 src1, const Mat3x3 src2)
{
    Mat3x3 tmp;
    if ((dst == src1) || (dst == src2))
    {
        Gfx_MathMultiplyXxX(tmp, src1, src2, 3, 3);
        utMat3x3Copy(dst, tmp);
    }
    Gfx_MathMultiplyXxX(dst, src1, src2, 3, 3);
}
void utMat3x3Translate(Mat3x3 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    Mat3x3 tmp, dst;
    CYGFX_U32 i;

    utMat3x3LoadIdentity(tmp);

    tmp[6 + 0] += x;
    tmp[6 + 1] += y;

    Gfx_MathMultiplyXxX(dst, m, tmp, 3, 3);
    for (i = 0; i < 9; i++)
    {
        m[i] = dst[i];
    }
}

void utMat3x3TranslatePre(Mat3x3 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    Mat3x3 tmp, dst;
    CYGFX_U32 i;

    utMat3x3LoadIdentity(tmp);

    tmp[6 + 0] += x;
    tmp[6 + 1] += y;

    Gfx_MathMultiplyXxX(dst, tmp, m, 3, 3);
    for (i = 0; i < 9; i++)
    {
        m[i] = dst[i];
    }
}

void utMat3x3Scale(Mat3x3 m, CYGFX_FLOAT x, CYGFX_FLOAT y)
{
    Mat3x3 tmp, dst;
    CYGFX_U32 i;

    utMat3x3LoadIdentity(tmp);

    tmp[ 0] = x;
    tmp[ 4] = y;

    Gfx_MathMultiplyXxX(dst, m, tmp, 3, 3);
    for (i = 0; i < 9; i++)
    {
        m[i] = dst[i];
    }
}


void utMat3x3RotX(Mat3x3 m, CYGFX_FLOAT f)
{
    Mat3x3 tmp, dst;
    CYGFX_U32 i;

    f *= DegreeToPI;
    utMat3x3LoadIdentity(tmp);
    tmp[4] = cosf(f);
    tmp[7] = -sinf(f);
    Gfx_MathMultiplyXxX(dst, m, tmp, 3, 3);
    for (i = 0; i < 9; i++)
    {
        m[i] = dst[i];
    }
}

void utMat3x3RotZ(Mat3x3 m, CYGFX_FLOAT f)
{
    Mat3x3 tmp, dst;
    CYGFX_U32 i;

    f *= DegreeToPI;
    utMat3x3LoadIdentity(tmp);
    tmp[0] = tmp[4] = cosf(f);
    tmp[3] = -sinf(f);
    tmp[1] = -tmp[3];
    Gfx_MathMultiplyXxX(dst, m, tmp, 3, 3);
    for (i = 0; i < 9; i++)
    {
        m[i] = dst[i];
    }
}



/* 3 * 2 matrix
 * order:
 *   0   2   4
 *   1   3   5
*/


void utMat3x2ToMat4x4(const Mat3x2 src, Mat4x4 dst)
{
    dst[ 0] = src[0];
    dst[ 1] = src[1];
    dst[ 2] = 0.0f;
    dst[ 3] = 0.0f;
    dst[ 4] = src[2];
    dst[ 5] = src[3];
    dst[ 6] = 0.0f;
    dst[ 7] = 0.0f;
    dst[ 8] = 0.0f;
    dst[ 9] = 0.0f;
    dst[10] = 1.0f;
    dst[11] = 0.0f;
    dst[12] = src[4];
    dst[13] = src[5];
    dst[14] = 0.0f;
    dst[15] = 1.0f;
}


void utMat3x3ToMat4x4(const Mat3x3 src, Mat4x4 dst)
{
    dst[ 0] = src[0];
    dst[ 1] = src[1];
    dst[ 2] = 0.0f;
    dst[ 3] = src[2];
    dst[ 4] = src[3];
    dst[ 5] = src[4];
    dst[ 6] = 0.0f;
    dst[ 7] = src[5];
    dst[ 8] = 0.0f;
    dst[ 9] = 0.0f;
    dst[10] = 1.0f;
    dst[11] = 0.0f;
    dst[12] = src[6];
    dst[13] = src[7];
    dst[14] = 0.0f;
    dst[15] = src[8];
}

void utMat4x4ToMat3x3(const Mat4x4 src, Mat3x3 dst)
{
    dst[0] = src[ 0];
    dst[1] = src[ 1];
    dst[2] = src[ 3];
    dst[3] = src[ 4];
    dst[4] = src[ 5];
    dst[5] = src[ 7];
    dst[6] = src[12];
    dst[7] = src[13];
    dst[8] = src[15];
}

CYGFX_ERROR utMat4x4ToMat3x2(const Mat4x4 src, Mat3x2 dst)
{
    CYGFX_FLOAT f;

    if (src[15] == 0.0F)
    {
        return CYGFX_ERR;
    }
    /* Such a conversion assumes that the 4x4 matrix realizes only 
       affine transformations. Beside this the z-coordinate will be removed. */
    f = 1.0f / src[15];
    dst[0] = src[ 0] * f;
    dst[1] = src[ 1] * f;
    dst[2] = src[ 4] * f;
    dst[3] = src[ 5] * f;
    dst[4] = src[12] * f;
    dst[5] = src[13] * f;
    return CYGFX_OK;
}


void utMat4x3LoadIdentity(Mat4x3 m)
{
    m[ 0] = 1.0F;
    m[ 1] = 0.0F;
    m[ 2] = 0.0F;
    m[ 3] = 0.0F;
    m[ 4] = 1.0F;
    m[ 5] = 0.0F;
    m[ 6] = 0.0F;
    m[ 7] = 0.0F;
    m[ 8] = 1.0F;
    m[ 9] = 0.0F;
    m[10] = 0.0F;
    m[11] = 0.0F;
}

void utMat5x4LoadIdentity(Mat5x4 m)
{
    m[ 0] = 1.0F;
    m[ 1] = 0.0F;
    m[ 2] = 0.0F;
    m[ 3] = 0.0F;
    m[ 4] = 0.0F;
    m[ 5] = 1.0F;
    m[ 6] = 0.0F;
    m[ 7] = 0.0F;
    m[ 8] = 0.0F;
    m[ 9] = 0.0F;
    m[10] = 1.0F;
    m[11] = 0.0F;
    m[12] = 0.0F;
    m[13] = 0.0F;
    m[14] = 0.0F;
    m[15] = 1.0F;
    m[16] = 0.0F;
    m[17] = 0.0F;
    m[18] = 0.0F;
    m[19] = 0.0F;
}

void utMat4x3Copy(Mat4x3 dst, const Mat4x3 src)
{
    dst[ 0] = src[ 0];
    dst[ 1] = src[ 1];
    dst[ 2] = src[ 2];
    dst[ 3] = src[ 3];
    dst[ 4] = src[ 4];
    dst[ 5] = src[ 5];
    dst[ 6] = src[ 6];
    dst[ 7] = src[ 7];
    dst[ 8] = src[ 8];
    dst[ 9] = src[ 9];
    dst[10] = src[10];
    dst[11] = src[11];
}

void utMat4x3Multiply(Mat4x3 dst, const Mat4x3 src1, const Mat4x3 src2)
{
    Mat4x3 MTmp;
    CYGFX_U32 i, k, l;

    for ( i = 0; i < 3; i++ )
    {
        for (k = 0; k < 4; k++)
        {
            MTmp[i + ( k * 3)] = 0.0f;
            for (l = 0; l < 3; l++)
            {
                MTmp[i + (k * 3)] += src1[i + (l * 3)] * src2[l + (k * 3)];
            }
        }
        MTmp[i + 9] += src1[i + 9];
    }
    utMat4x3Copy(dst, MTmp);
}

void utMat4x3CalcColMatrix(Mat4x3 dst, CYGFX_FLOAT fContrast, CYGFX_FLOAT fBrightness, CYGFX_FLOAT fSaturation, CYGFX_FLOAT fHue )
{
    CYGFX_U32 i;

    /* reset the matrix */
    utMat4x3LoadIdentity(dst);

    if( ( fHue != 0.0f ) || ( fSaturation != 1.0f ) )
    {
        fHue *= DegreeToPI;
        dst[8] = fSaturation * (CYGFX_FLOAT)cos((CYGFX_DOUBLE)fHue);
        dst[4] = dst[8];
        dst[7] = fSaturation * (CYGFX_FLOAT)sin((CYGFX_DOUBLE)fHue);
        dst[5] = -dst[7];

        utMat4x3Multiply( dst, dst, TYCbCr );
        utMat4x3Multiply( dst, TYCbCr_1, dst );
    }

    for ( i = 0; i < 9; i++)
    {
        dst[i] = dst[i] * fContrast;
    }

    dst[ 9] += fBrightness;
    dst[10] += fBrightness;
    dst[11] += fBrightness;

    return;
}
