/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/
#include <stdio.h>
#include <string.h>

#include "cygfx_driver_api.h"
#include "ut_disp.h"
#include "sm_util.h"

#if (defined (CY_USE_PSVP) && !CY_USE_PSVP)

#include "cy_project.h"
#include "cy_device_headers.h"
#include "fpdlink\cy_fpdlink.h"

#endif


/* The following defines control the number and type of PLLs that are used in driving the display outputs. */
/* There are three choices:                                                                                */
/*  1) One 400MHz PLL used to drive display 0 and one 200MHz PLL used to drive display 1                   */
/*  2) Two 400MHz PLL used to drive display 0 and display 1                                                */
/*  3) One 400MHz PLL drives both display outputs (0 and 1) with the same pixel clock                      */
#define PLL400MHZ_DISP0_PLL200MHZ_DISP1     (1u)
#define PLL400MHZ_DISP0_PLL400MHZ_DISP1     (2u)
#define PLL400MHZ_USED_FOR_BOTH_DISPLAYS    (3u)

/* Here we select the PLL combination */
#define PLL_COMBINATIONS  (PLL400MHZ_USED_FOR_BOTH_DISPLAYS)

static const CYGFX_DISP_TIMING_S     sTimings[] =
{
          /*                 pixelClock,                    Hact,  Vact,   Htot,   Vtot,  Hsbp,  Vsbp,  Hsync, Vsync, HtotMin, VtotMin, HtotMax, VtotMax,            DCKInvertEnable,                polEn,                   polHs,                   polVs,                pixInv */
        { /*   160x120@60 */ ((60*( 592* 286))/1000000.0f),  160,   120,    192,    133,    24,    10,      8,     4,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*   320x240@60 */ ((60*( 592* 286))/1000000.0f),  320,   240,    400,    253,    72,    10,     32,     4,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*   480x272@60 */ ((60*( 592* 286))/1000000.0f),  480,   272,    592,    286,    96,    11,     40,     5,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*   640x480@60 */ ((60*( 800* 500))/1000000.0f),  640,   480,    800,    500,   144,    17,     64,     4,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*   720x576@50 */ ((50*( 864* 625))/1000000.0f),  720,   576,    864,    625,   124,    43,      4,     5,     848,     621,     880,     629,  CYGFX_DISP_DCK_INVERT_ON,    CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*   800x480@60 */ ((60*( 992* 500))/1000000.0f),  800,   480,    992,    500,   168,    17,     72,     7,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*   800x600@60 */ ((60*(1024* 624))/1000000.0f),  800,   600,   1024,    624,   192,    21,     80,     4,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1024x768@60 */ ((60*(1328* 798))/1000000.0f), 1024,   768,   1328,    798,   256,    27,    104,     4,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1280x720@60 */ ((60*(1664* 748))/1000000.0f), 1280,   720,   1664,    748,   320,    25,    128,     5,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1280x800@60 */ ((60*(1680* 831))/1000000.0f), 1280,   800,   1680,    831,   328,    30,    128,     6,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1440x540@60 */ ((60*(1824* 562))/1000000.0f), 1440,   540,   1824,    562,   336,    19,    144,    10,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1600x480@60 */ ((60*(2000* 500))/1000000.0f), 1600,   480,   2000,    500,   360,    17,    160,    10,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_ON,    CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1600x720@60 */ ((60*(2080* 748))/1000000.0f), 1600,   720,   2080,    748,   336,    25,    144,    10,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        { /*  1920x720@60 */ ((60*(2496* 748))/1000000.0f), 1920,   720,   2496,    748,   480,    25,    192,    10,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW},
        /* Use Reduced Blanking for 2880x1080. Warning: Requires dual channel output */
        { /* 2880x1080@60 */ ((60*(3040*1111))/1000000.0f), 2880,  1080,   3040,   1111,   112,    28,     32,    10,       0,       0,       0,       0,  CYGFX_DISP_DCK_INVERT_OFF,   CYGFX_GEN_POLARITY_HIGH,    CYGFX_GEN_POLARITY_LOW,    CYGFX_GEN_POLARITY_LOW,    CYGFX_DISP_RGB_LOW}
};

static struct TimeData tData;

CYGFX_EXTERN void utDispStorePllData(CYGFX_DISP_CONTROLLER outputController, CYGFX_U32 pllClock, CYGFX_U08 divider)
{
    if (outputController == CYGFX_DISP_CONTROLLER_0)
    {
        tData.pllClock0 = pllClock;
        tData.divider0 = divider;
    }
    if (outputController == CYGFX_DISP_CONTROLLER_1)
    {
        tData.pllClock1 = pllClock;
        tData.divider1 = divider;
    }
}


CYGFX_EXTERN void utDispRestorePllData(CYGFX_DISP_CONTROLLER outputController, CYGFX_U32* pllClock, CYGFX_U08* divider)
{
    if (outputController == CYGFX_DISP_CONTROLLER_0)
    {
        *pllClock = tData.pllClock0;
        *divider = tData.divider0;
    }
    if (outputController == CYGFX_DISP_CONTROLLER_1)
    {
        *pllClock = tData.pllClock1;
        *divider = tData.divider1;
    }
}

CYGFX_ERROR utDispGetTiming(CYGFX_U32 xResolution, CYGFX_U32 yResolution, CYGFX_DISP_TIMING_S *timing)
{
    CYGFX_U08 i = 0;
    CYGFX_BOOL res_found = CYGFX_FALSE;

    if (timing == NULL)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("ERROR: NULL pointer given for timing!\n");
#endif
        return CYGFX_ERR;
    }

    for(i=0; i<CYGFX_ARRAY_LENGTH(sTimings); i++)
    {
        if ((xResolution == sTimings[i].Hact) && (yResolution == sTimings[i].Vact))
        {
            memcpy(timing, &sTimings[i], sizeof(CYGFX_DISP_TIMING_S));
            res_found = CYGFX_TRUE;
            break;
        }
    }

    if (res_found != CYGFX_TRUE)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("ERROR: given resolution not found!\n");
#endif
        return CYGFX_ERR;
    }

    return CYGFX_OK;
}


CYGFX_ERROR utDispGetPll(CYGFX_FLOAT pixelClock, CYGFX_DISP_MODE displayMode, CYGFX_U08 *divider, CYGFX_U32 *pll)
{
    const CYGFX_U08 kDivMax = 3U;

    CYGFX_ERROR     ret = CYGFX_OK;

    CYGFX_U08       ClkDiv;
    CYGFX_U32       pixelClockHz;
    CYGFX_U32       displayClock;

    if ((pll == NULL) || (divider == NULL))
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("One of the arguments is invalid! ");
        printf("Valid values are: pixelClock >= %3.0f and pixelClock <= %3.0f, divider != NULL, pll != NULL.\n", CYGFX_DISP_MIN_PIXEL_CLOCK, CYGFX_DISP_MAX_PIXEL_CLOCK);
#endif
        ret = CYGFX_ERR;
    }
    else if (pixelClock == 0.0f)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("WARNING: Supplied pixelClock is 0 -> Skipping Pll calculation!\n");
#endif

        displayClock = 0U;
        ClkDiv = 0U;

        ret = CYGFX_OK;
    }
    else
    {
        pixelClockHz = (CYGFX_U32)(pixelClock * 1000000.0f);

        /* Single Screen Mode - Display clock = twice the PixeClock
            Dual Channel Mode - Display clock = PixelClock
         */
        if (displayMode == CYGFX_DISP_MODE_SINGLE_SCREEN)
        {
            pixelClockHz *= 2U;
        }

        for (ClkDiv = 0; ClkDiv <= kDivMax; ClkDiv++)
        {
            displayClock = (CYGFX_U32)pixelClockHz * (1U << ClkDiv);
            if ((displayClock >= CYGFX_SYS_INIT_DISP_PLL_MIN) && (displayClock <= CYGFX_SYS_INIT_DISP_PLL_MAX))
            {
                break;
            }
        }

        if (ClkDiv > kDivMax)
        {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("Clock divider exceeds maximum allowed value: [%d]!\n", ClkDiv);
            printf("Consider providing a bigger pixelClock value.\n");
#endif
            ret = CYGFX_ERR;
        }
    }

    if (ret == CYGFX_OK)
    {
        /* Save resulted values */
        *pll = displayClock;
        *divider = ClkDiv;
    }

    return ret;
}

CYGFX_ERROR utDispEnablePll(CYGFX_U32 pll, CYGFX_DISP_CONTROLLER outputController)
{
    CYGFX_ERROR ret = CYGFX_OK;
    /* The following code is only for Silicion and requires SDL. */
#if (defined (CY_USE_PSVP) && !CY_USE_PSVP)
    const CYGFX_U32 waitForStabilization = 10000ul;
    cy_en_sysclk_status_t status = CY_SYSCLK_SUCCESS;

    cy_stc_pll_400M_config_t stcPllCfgDisp0 =
    {
    #if CY_SYSTEM_USE_CLOCK == CY_SYSTEM_USE_ECO
        CY_CLK_ECO_FREQ_HZ,
    #elif CY_SYSTEM_USE_CLOCK == CY_SYSTEM_USE_IMO
        CY_CLK_IMO_FREQ_HZ,
    #else
        #error "Please check CY_SYSTEM_USE_CLOCK define!"
    #endif
        0u,
        CY_SYSCLK_FLLPLL_OUTPUT_AUTO,
        true,
        true,
        true,
        true,
        CY_SYSCLK_SSCG_DEPTH_MINUS_2_0,
        CY_SYSCLK_SSCG_RATE_DIV_512,
    };

#if (PLL_COMBINATIONS == PLL400MHZ_DISP0_PLL400MHZ_DISP1)
    cy_stc_pll_400M_config_t stcPllCfgDisp1 =
    {
    #if CY_SYSTEM_USE_CLOCK == CY_SYSTEM_USE_ECO
        CY_CLK_ECO_FREQ_HZ,
    #elif CY_SYSTEM_USE_CLOCK == CY_SYSTEM_USE_IMO
        CY_CLK_IMO_FREQ_HZ,
    #else
        #error "Please check CY_SYSTEM_USE_CLOCK define!"
    #endif
        0u,
        CY_SYSCLK_FLLPLL_OUTPUT_AUTO,
        true,
        true,
        true,
        true,
        CY_SYSCLK_SSCG_DEPTH_MINUS_2_0,
        CY_SYSCLK_SSCG_RATE_DIV_512,
    };
#endif

#if (PLL_COMBINATIONS == PLL400MHZ_DISP0_PLL200MHZ_DISP1)
    cy_stc_pll_config_t pll200_0_Config =
    {
    #if CY_SYSTEM_USE_CLOCK == CY_SYSTEM_USE_ECO
        CY_CLK_ECO_FREQ_HZ,
    #elif CY_SYSTEM_USE_CLOCK == CY_SYSTEM_USE_IMO
        CY_CLK_IMO_FREQ_HZ,
    #else
        #error "Please check CY_SYSTEM_USE_CLOCK define!"
    #endif
        0u,
        0u,
        CY_SYSCLK_FLLPLL_OUTPUT_AUTO,
    };
#endif


    if (pll == 0u)
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("WARNING: Skipping Pll Configuration!\n");
#endif
        ret = CYGFX_ERR;
    }
    else
    {
        if (outputController == CYGFX_DISP_CONTROLLER_0)
        {
            UTIL_SUCCESS(status, Cy_SysClk_Pll400MDisable(CY_SYSCLK_HFCLK_IN_CLKPATH5)); /* 400 MHz PLL */
            UTIL_SUCCESS(status, Cy_SysClk_HfClockSetSource(CY_SYSCLK_HFCLK_11, CY_SYSCLK_HFCLK_IN_CLKPATH5));

            stcPllCfgDisp0.outputFreq = pll;

            UTIL_SUCCESS(status, Cy_SysClk_Pll400MConfigure(CY_SYSCLK_HFCLK_IN_CLKPATH5, &stcPllCfgDisp0));
            UTIL_SUCCESS(status, Cy_SysClk_Pll400MEnable(CY_SYSCLK_HFCLK_IN_CLKPATH5, waitForStabilization));
        }
        else
        {
    #if (PLL_COMBINATIONS == PLL400MHZ_DISP0_PLL200MHZ_DISP1)
            UTIL_SUCCESS(status, Cy_SysClk_PllDisable(CY_SYSCLK_HFCLK_IN_CLKPATH8)); /* 200 MHz PLL */
            UTIL_SUCCESS(status, Cy_SysClk_HfClockSetSource(CY_SYSCLK_HFCLK_12, CY_SYSCLK_HFCLK_IN_CLKPATH8));

            pll200_0_Config.outputFreq = pll;

            UTIL_SUCCESS(status, Cy_SysClk_PllConfigure(CY_SYSCLK_HFCLK_IN_CLKPATH8, &pll200_0_Config));
            UTIL_SUCCESS(status, Cy_SysClk_PllEnable(CY_SYSCLK_HFCLK_IN_CLKPATH8, waitForStabilization));
    #endif /* PLL400MHZ_DISP0_PLL200MHZ_DISP1 */

    #if (PLL_COMBINATIONS == PLL400MHZ_DISP0_PLL400MHZ_DISP1)
            UTIL_SUCCESS(status, Cy_SysClk_Pll400MDisable(CY_SYSCLK_HFCLK_IN_CLKPATH3)); /* 400 MHz PLL */
            UTIL_SUCCESS(status, Cy_SysClk_HfClockSetSource(CY_SYSCLK_HFCLK_12, CY_SYSCLK_HFCLK_IN_CLKPATH3));

            stcPllCfgDisp1.outputFreq = pll;

            UTIL_SUCCESS(status, Cy_SysClk_Pll400MConfigure(CY_SYSCLK_HFCLK_IN_CLKPATH3, &stcPllCfgDisp1));
            UTIL_SUCCESS(status, Cy_SysClk_Pll400MEnable(CY_SYSCLK_HFCLK_IN_CLKPATH3, waitForStabilization));

    #endif /* PLL400MHZ_DISP0_PLL400MHZ_DISP1 */

    #if (PLL_COMBINATIONS == PLL400MHZ_USED_FOR_BOTH_DISPLAYS )
        Cy_SysClk_HfClockSetSource(CY_SYSCLK_HFCLK_12, CY_SYSCLK_HFCLK_IN_CLKPATH5);
    #endif /* PLL400MHZ_USED_FOR_BOTH_DISPLAYS */
        }
    }
#endif

    return ret;
}

/* This function will be replaced later by utDispSetFpdLink */
CYGFX_ERROR utDispEnableFpdLink(CYGFX_U08 pllDivider, CYGFX_DISP_MODE displayMode, cy_fpdlink_en_instance_t fpdLink)
{
    /* The following code is only for Silicion and requires SDL. */
#if (defined (CY_USE_PSVP) && !CY_USE_PSVP)
    cy_fpdlink_en_result_t ret;

    cy_fpdlink_stc_cfg_t stcFpdCfg =
    {
        FpdlinkMsbFirst,
        FpdlinkCurrent3m36,
        (cy_fpdlink_en_pll_out_div_t)(pllDivider),
        FpdlinkPllBandwidthDefault,
    };

#ifdef VIDEOSS0_FPDLINK1
    if ((displayMode == CYGFX_DISP_MODE_DUAL_CHANNEL) && (fpdLink != (CYGFX_U08)FpdlinkDual01))
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("Dual channel requires only FpdLink0 to be programmed!\n");
#endif
        return CYGFX_ERR;
    }
    else
#endif
    {
        ret = Cy_Fpdlink_Init((cy_fpdlink_en_instance_t)fpdLink, &stcFpdCfg);
    }

    return (ret != CY_FPDLINK_SUCCESS) ? CYGFX_ERR : CYGFX_OK;
#else
    return CYGFX_OK;
#endif 
}

CYGFX_ERROR utDispSetFpdLink(CYGFX_DISP_CONTROLLER outputController, CYGFX_DISP_MODE displayMode, CYGFX_U08 pllDivider)
{
    CYGFX_ERROR ret = CYGFX_OK;

    /* The following code is needed for Silicon and requires SDL. 
        Better solution is to have one version for all compilers but VIDEOSS0_FPDLINK1 definition is used from SDL include and does not exist for model.
    */
#if (defined (CY_USE_PSVP) && !CY_USE_PSVP)
    cy_fpdlink_en_instance_t fpdlink_en;

    if (outputController == CYGFX_DISP_CONTROLLER_0)
    {
        if (displayMode == CYGFX_DISP_MODE_SINGLE_SCREEN)
        {
            fpdlink_en = Fpdlink0;
        }
        else
        {
#ifdef VIDEOSS0_FPDLINK1
            fpdlink_en = FpdlinkDual01;
#else
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("This device doesn't support FpdLink1 required for Dual channel!\n");
#endif
            return CYGFX_ERR;
#endif
        }
    }
    else
    {
#ifdef VIDEOSS0_FPDLINK1
        fpdlink_en = Fpdlink1;
#else
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("FpdLink1 is not supported!\n");
#endif
        /* CYGFX_OK is accepted because 4MAx display 1 output is TTL */
        return CYGFX_OK;
#endif
    }


    cy_fpdlink_stc_cfg_t stcFpdCfg =
    {
        FpdlinkMsbFirst,
        FpdlinkCurrent3m36,
        (cy_fpdlink_en_pll_out_div_t)(pllDivider),
        FpdlinkPllBandwidthDefault,
    };

    return (Cy_Fpdlink_Init(fpdlink_en, &stcFpdCfg) != CY_FPDLINK_SUCCESS) ? CYGFX_ERR : CYGFX_OK;
#else
    return CYGFX_OK;
#endif 
}
