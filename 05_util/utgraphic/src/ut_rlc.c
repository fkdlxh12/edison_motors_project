/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/**
 * \file        ut_rlc.c
 * \brief       This sample code can be used to create a run-length encoded buffer.
 */

/*******************************************************************************
 Includes
*******************************************************************************/

#include "cygfx_driver_api.h"

#include "ut_rlc.h"
#include "string.h"

/*******************************************************************************
 Macro Definitions
*******************************************************************************/

#define BITMASK(m) ((CYGFX_U32) (((m) < 32) ? ((1U << (m)) - 1U) : 0xffffffffU))

/*******************************************************************************
 Data Types
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Variables
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Prototypes
*******************************************************************************/

/* N/A */

/*******************************************************************************
 Function Definitions
*******************************************************************************/

static CYGFX_U32 utRldGetPixel(CYGFX_U32* pixeldata, CYGFX_U32 width, CYGFX_U32 height, CYGFX_U32 strideBytes, CYGFX_U32 x_offs, CYGFX_U32 dataBpp, CYGFX_U32 pos);
static void utRldWriteBits(CYGFX_U32* rld, CYGFX_U32 rldCount, CYGFX_U32* pBitPos, CYGFX_U32 nBits, CYGFX_U32 value);
static CYGFX_U32 utRldEncodeEx(CYGFX_U32* pixeldata, CYGFX_U32 unWidth, CYGFX_U32 unHeight, CYGFX_U32 strideBytes, CYGFX_U32 x_offs, CYGFX_U32 dataBpp, CYGFX_U32 endian, CYGFX_U32 count_offset, CYGFX_U32* rld, CYGFX_U32 rldCount);

static CYGFX_U32 utRldGetPixel(CYGFX_U32* pixeldata, CYGFX_U32 width, CYGFX_U32 height, CYGFX_U32 strideBytes, CYGFX_U32 x_offs, CYGFX_U32 dataBpp, CYGFX_U32 pos)
{
    CYGFX_U32 pixel, bitpos;
    if (pos > (width * height))
    {
        return 0;
    }
    bitpos = ((pos / width) * (strideBytes * 8)) + (((pos % width) + x_offs) * dataBpp);

    memcpy(&pixel, (( CYGFX_CHAR*)pixeldata) + (bitpos >> 3), 4);
    pixel >>= (bitpos & 0x7);

    if (dataBpp < 32)
    {
        pixel &= (1U << dataBpp) - 1U;
    }
    return pixel;
}


static void utRldWriteBits(CYGFX_U32* rld, CYGFX_U32 rldCount, CYGFX_U32* pBitPos, CYGFX_U32 nBits, CYGFX_U32 value)
{
    /* r bits left in the current word k */
    CYGFX_U32 kWords = *pBitPos / 32;
    CYGFX_U32 rBits = 32 - (*pBitPos % 32); /* 1..32 */
    CYGFX_U32 dBits;

    nBits = (nBits >= 32)? 32: nBits;
    value &= BITMASK(nBits);

    if ( (kWords < rldCount) && (nBits != 0) )
    {
        /* r >= n, n = r - d:
        * put n bits to current word at offset d
        */
        if (rBits >= nBits)
        {
            dBits = rBits-nBits; /*  0..31 */
            rld[kWords] &= ~(BITMASK(nBits)<<dBits);
            rld[kWords] |= (value<<dBits);
        }
        /* r < n, n = r + d:
        * put r bits to current word at offset 0
        * put d bits to next word at offset 32-d
        */
        else
        {
            dBits = nBits-rBits; /* 1..31 */
            rld[kWords] &= ~BITMASK(rBits);
            rld[kWords] |= (value >> dBits);
            if ((kWords+1) < rldCount)
            {
                rld[kWords+1] &= ~(BITMASK(dBits)<<(32-dBits));
                rld[kWords+1] |= ((value&BITMASK(dBits))<<(32-dBits));
            }
        }
    }

    *pBitPos += nBits;
}

static CYGFX_U32 utRldEncodeEx(CYGFX_U32* pixeldata, CYGFX_U32 unWidth, CYGFX_U32 unHeight,
                   CYGFX_U32 strideBytes, CYGFX_U32 x_offs, CYGFX_U32 dataBpp, CYGFX_U32 endian, CYGFX_U32 count_offset,
                   CYGFX_U32* rld, CYGFX_U32 rldCount)
{
    CYGFX_U32 pixel, prevPixel = 0;
    CYGFX_U32 total = unWidth * unHeight;      /* pixels */
    CYGFX_U32 start = 0, compr, curr, count, p;    /* pixels */
    CYGFX_U32 minRun;              /* pixels */
    CYGFX_U32 rldPos = 0;     /* bits */

    if ((dataBpp == 0) || (pixeldata == NULL))
    {
        return 0;
    }

    minRun = (8/dataBpp) + 2;              /* pixels */

    /*
    * We use a very simple algorithm here: When we detect a run of
    * minRun or more identical pixels, it's compressed; everything
    * else is left uncompressed.
    *
    * minRun is calculated so that the number of bits used to encode
    * the run is smaller than the number of original bits:
    * 8 + bpp < minRun * bpp. For 1, 2, 4, 8, 16, 24, 32 bits per
    * pixel, the minimal run is 10, 6, 4, 3, 2, 2, 2 pixels, resp.
    */

    while (start < total)
    {
        /*
         * pixels from 'start' to 'compr'-1 are uncompressed
         * pixels from 'compr' to 'curr'-1 are compressed
         */

        for (curr = compr = start; ; curr++)
        {
            /* end of pixeldata */
            if (curr >= total)
            {
                if (curr < (compr + minRun))
                {
                    compr = curr;
                }
                break;
            }

            /* next pixel */
            pixel = utRldGetPixel(pixeldata, unWidth, unHeight, strideBytes, x_offs, dataBpp, curr);
            if ( (curr > compr) && (pixel != prevPixel) )
            {
                if (curr >= (compr+minRun))
                {
                    break; /* end of run */
                }
                compr = curr;
            }
            prevPixel = pixel;
        }

        /* encode uncompressed */
        while (compr > start)
        {
            count = compr - start;
            if (count > (128 - count_offset))
            {
                count = 128 - count_offset;
            }
            utRldWriteBits(rld, rldCount, &rldPos, 8, (count - 1) + count_offset);
            for (p = start; p < (start+count); p++)
            {
                pixel = utRldGetPixel(pixeldata, unWidth, unHeight, strideBytes, x_offs, dataBpp, p);
                utRldWriteBits(rld, rldCount, &rldPos, dataBpp, pixel);
            }
            start += count;
        }

        /* encode compressed */
        while (curr > compr)
        {
            count = curr - compr;
            if ( (count < minRun) && (curr < total) )
            {
                curr = compr; break;
            }
            if (count > (128 - count_offset))
            {
                count = 128 - count_offset;
            }
            utRldWriteBits(rld, rldCount, &rldPos, 8, 0x80|((count - 1) + count_offset));
            pixel = utRldGetPixel(pixeldata, unWidth, unHeight, strideBytes, x_offs, dataBpp, compr);
            utRldWriteBits(rld, rldCount, &rldPos, dataBpp, pixel);
            compr += count;
        }

        /* next round */
        start = compr = curr;
    }

    /* add trailing fill bits */
    while ((rldPos % 32) != 0)
    {
        utRldWriteBits(rld, rldCount, &rldPos, 1, 0);
    }

    if (endian == 1)
    {
        if (rldCount > (rldPos / 32))
        {
            rldCount = rldPos / 32;
        }
        for (start = 0; start < rldCount; start++)
        {
            pixel = rld[start];
            rld[start] = ((pixel & 0xff000000U) >> 24)
                | ((pixel & 0x00ff0000) >> 8)
                | ((pixel & 0x0000ff00) << 8)
                | ((pixel & 0x000000ff) << 24);
        }
    }

    return rldPos / 32;
}

CYGFX_U32 utRldEncode(CYGFX_U32* pixeldata, CYGFX_U32 unWidth, CYGFX_U32 unHeight,
                   CYGFX_U32 strideBytes, CYGFX_U32 x_offs, CYGFX_U32 dataBpp,
                   CYGFX_U32* rld, CYGFX_U32 rldCount)
{
    return utRldEncodeEx(pixeldata, unWidth, unHeight,
                   strideBytes, x_offs, dataBpp, 0, 0,
                   rld, rldCount);
}

