/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

#include <stdlib.h>

#include "cygfx_driver_api.h"

#include "ut_compatibility.h"
#include "ut_errors.h"

CYGFX_ERROR utSmGenSurfaceObjects(CYGFX_U32 uCnt, CYGFX_SURFACE *pSurfaces)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 i;
    CYGFX_SURFACE pSurf = 0;

    if (pSurfaces == 0)
    {
        return CYGFX_ERR;
    }

    for (i = 0U; i < uCnt; i++)
    {
        /* Create the surface object. */
        pSurf = (CYGFX_SURFACE)utOsLibcMalloc(sizeof(CYGFX_SURFACE_OBJECT_S));
        if (pSurf == NULL)
        {
            ret = CYGFX_ERR;
            pSurfaces[i] = (CYGFX_SURFACE)0;
        }
        else
        {
            CyGfx_SmResetSurfaceObject(pSurf);
            pSurfaces[i] = pSurf;
        }
    }
    if (ret != CYGFX_OK)
    {
        (void)utSmDeleteSurfaceObjects(uCnt, pSurfaces);
    }

    return ret;
}


CYGFX_ERROR utSmDeleteSurfaceObjects(CYGFX_U32 uCnt, CYGFX_SURFACE *pSurfaces)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 i;
    CYGFX_SURFACE_OBJECT_S *pSurf;

    if (pSurfaces == 0)
    {
        return CYGFX_ERR;
    }

    for (i = 0U; i < uCnt; i++)
    {
        pSurf = (CYGFX_SURFACE_OBJECT_S*)(void*)pSurfaces[i];
        pSurfaces[i] = 0;
        utOsLibcFree(pSurf);
    }
    return ret;
}

CYGFX_ERROR utPeGenContext(CYGFX_BE_CONTEXT *pBeCtx)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_BE_CONTEXT_OBJECT_S *pCtx;

    if (pBeCtx == 0)
    {
        return CYGFX_ERR;
    }
    /* Create the surface object. */
    pCtx = (CYGFX_BE_CONTEXT_OBJECT_S*)utOsLibcMalloc(sizeof(CYGFX_BE_CONTEXT_OBJECT_S));
    if (NULL != pCtx)
    {
        CyGfx_BeResetContext(pCtx);
    }
    else
    {
        ret = CYGFX_ERR;
    }
    *pBeCtx = (CYGFX_BE_CONTEXT)(void*)pCtx;

    return ret;
}

void utPeDeleteContext(CYGFX_BE_CONTEXT beCtx)
{
    CYGFX_BE_CONTEXT_OBJECT_S *pCtx = (CYGFX_BE_CONTEXT_OBJECT_S*)(void*)beCtx;

    utOsLibcFree(pCtx);
}


CYGFX_ERROR utSyncCreate(CYGFX_U32 uCnt, CYGFX_SYNC* pSyncObjects)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 i;
    CYGFX_SYNC pSync = 0;

    if (pSyncObjects == NULL)
    {
        return CYGFX_ERP_ERR_SYNC_INVALID_PARAMETER;
    }

    for (i = 0U; i < uCnt; i++)
    {
        /* Create the surface object. */
        pSync = (CYGFX_SYNC)utOsLibcMalloc(sizeof(CYGFX_SYNC_OBJECT_S));
        if (pSync == NULL)
        {
            ret = UTIL_ERR_SYNC_OUT_OF_MEMORY;
            pSyncObjects[i] = (CYGFX_SYNC)0;
        }
        else
        {
            CyGfx_SyncReset(pSync);
            pSyncObjects[i] = (CYGFX_SYNC)(void*)pSync;
        }
    }
    if(ret != CYGFX_OK)
    {
        (void)utSyncDelete(uCnt, pSyncObjects);
    }

    return ret;
}

CYGFX_ERROR utSyncDelete(CYGFX_U32 uCnt, CYGFX_SYNC* pSyncObjects)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_U32 i;
    CYGFX_SYNC_OBJECT_S *pSync;

    if (pSyncObjects == NULL)
    {
        return CYGFX_ERP_ERR_SYNC_INVALID_PARAMETER;
    }

    for (i = 0U; i < uCnt; i++)
    {
        pSync = (CYGFX_SYNC_OBJECT_S*)(void*)pSyncObjects[i];
        pSyncObjects[i] = 0;
        utOsLibcFree(pSync);
    }
    return ret;
}

