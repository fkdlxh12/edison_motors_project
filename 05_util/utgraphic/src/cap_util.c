/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        cap_util.c
 * \brief       Define helper functions for capture.
 */

/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "cygfx_driver_api.h"
#include "cap_util.h"


/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

#define UT_HWEB_WRITE32(ADD, VAL)    (*((volatile CYGFX_U32*)(ADD)) = (VAL))
#define UT_HWEB_READ32(ADD)          (*((volatile CYGFX_U32 *)(ADD)))


#define GFX_FRAMECAP_CAPTURE_MODE      (0x40A81400)
#define GFX_CAPTUREMODE_ENHSVS_32BIT   (0U)
#define GFX_CAPTUREMODE_ITU656_10BIT   (1U)
#define GFX_CAPTUREMODE_ITU656_8BIT    (2U)

#define GFX_FRAMECAP_CTR               (0x40A81404)
#define GFX_FRAMECAP_MDR               (0x40A8140C)

#define GFX_FRAMECAP_STSCLR            (0x40A81424)
#define GFX_FRAMECAP_STS               (0x40A81428)

#define GFX_FRAMECAP_MDSTS0            (0x40A8142C)

#define GFX_FRAMECAP_FRLINECOUNT       (0x40A81438)
#define GFX_FRAMECAP_CMSTS1            (0x40A8143C)
#define GFX_FRAMECAP_CMSTS2            (0x40A81440)


#define CAP_TIMEOUT 1000000

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/
CYGFX_ERROR utCapCheckSourceTiming(CYGFX_FLOAT GfxPll, CYGFX_CAP_MEASUREMENT_RESULT_S *meas, CYGFX_FLOAT *pPixClk)
{
    CYGFX_U32   regValue = 0;
    CYGFX_U32   i = 0;
    CYGFX_CAP_MEASUREMENT_RESULT_S measurement = { 0 };
    CYGFX_CONFIG_DEVICE activeDevice = CYGFX_CONFIG_DEVICE_UNDEF;

    volatile CYGFX_U32  stsVal;

    if ((GfxPll < 100.0f) || (meas == NULL) || (pPixClk == NULL))
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("Invalid arguments\n");
#endif
        return CYGFX_ERR;
    }

    (void)CyGfx_ConfigGetAttribute(CYGFX_CONFIG_ATTR_DEVICE_NAME, &activeDevice);

    /* Stop Capture before using frame measurement */
    UT_HWEB_WRITE32(GFX_FRAMECAP_CTR, 0U);

    /* The capture input format is configured or statically set by the embodying system.
       Read out the currently active format from CaptureMode */
    regValue = UT_HWEB_READ32(GFX_FRAMECAP_CAPTURE_MODE);
    switch (regValue)
    {
        case GFX_CAPTUREMODE_ENHSVS_32BIT:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("Capture mode = 0 (32 bit Parallel or MIPI_CSI2 input). \n");
#endif
            break;
        case GFX_CAPTUREMODE_ITU656_10BIT:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("Capture mode = 1 (ITU656 10 bit)\n");
#endif
            break;
        case GFX_CAPTUREMODE_ITU656_8BIT:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("Capture mode = 2 (ITU656 8 bit)\n");
#endif
            break;
        default:
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("Capture mode = 0x%08x (illegal value) => Abort.\n", regValue);
#endif
            return CYGFX_ERR;
    }

    /* Clear all status registers */
    UT_HWEB_WRITE32(GFX_FRAMECAP_STSCLR, 1U);

    /* Configured Frame size tolerance and Mode Detection */
    UT_HWEB_WRITE32(GFX_FRAMECAP_MDR, 0xFFFFFFFE | 0x01U);

    /* Wait for interrupt */
    do
    {
        stsVal = UT_HWEB_READ32(GFX_FRAMECAP_STS);

        /* Take only MDRCMRDONE and SyncStat */
        stsVal &= 0x120U;
        i++;
        if (i > CAP_TIMEOUT)
        {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("ERROR: Measurement timeout!\n");
#endif
            return CYGFX_ERR;
        }
    }while (stsVal == 0U);

    /* Measurement done -> calculate PixelClock */
    regValue = UT_HWEB_READ32(GFX_FRAMECAP_MDSTS0);
    measurement.ActWidth = (CYGFX_U16)(regValue & 0x7FFF);
    measurement.ActHeight = (CYGFX_U16)((regValue >> 16U) & 0x7FFF);

    regValue = UT_HWEB_READ32(GFX_FRAMECAP_CMSTS1);
    measurement.FrameTime = (CYGFX_U32)(regValue & 0x3FFFFFFF);

    regValue = UT_HWEB_READ32(GFX_FRAMECAP_CMSTS2);
    measurement.TotWidth_time = (CYGFX_U32)(regValue & 0x7FFFF);
    measurement.ActWidth_time = (CYGFX_U32)((regValue >> 15U) & 0x3FFF);


    regValue = UT_HWEB_READ32(GFX_FRAMECAP_FRLINECOUNT);

    measurement.TotWidth_time |= (CYGFX_U32)(((regValue >> 16) & 0x1F) << 15);
 	measurement.ActWidth_time |= (CYGFX_U32)(((regValue >> 24) & 0x3F) << 14);
    

    measurement.TotWidth = (CYGFX_U16)
        ((measurement.TotWidth_time * measurement.ActWidth) / measurement.ActWidth_time);

    measurement.TotHeight = (CYGFX_U16)
        ((CYGFX_U32)measurement.FrameTime / measurement.TotWidth_time);

    /* Finally Calculate Pixel frame rate */
    measurement.PixelFrameRate = (CYGFX_FLOAT)
        (((CYGFX_FLOAT)measurement.TotWidth * (CYGFX_FLOAT)measurement.TotHeight) / (CYGFX_FLOAT)measurement.FrameTime);

    /* Disable measurement */
    UT_HWEB_WRITE32(GFX_FRAMECAP_MDR, 0x0U);

    /* Clear all status registers */
    UT_HWEB_WRITE32(GFX_FRAMECAP_STSCLR, 1U);

    /* Copy back the results */
    (void)memcpy(meas, (void*)&measurement, sizeof(CYGFX_CAP_MEASUREMENT_RESULT_S));

    *pPixClk = (GfxPll * measurement.PixelFrameRate);

#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
    printf("PixelClock = %5.6f MHz)\n", *pPixClk);
#endif

    return CYGFX_OK;
}

