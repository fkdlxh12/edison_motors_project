/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress's             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        ut_blit.c
 * \brief       Define helper functions for blitting.
 */

/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/

#include <stdio.h>

#include "cygfx_driver_api.h"

#include "ut_blit.h"
#include "sm_util.h"
#include "pe_matrix.h"


/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** GLOBAL VARIABLES ********************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/

/* Blit 9 slice button example. A good explanation is found here https://en.wikipedia.org/wiki/9-slice_scaling 
    \param ctx  The context object
    \param x    The desination x coordinate
    \param y    The desination y coordinate
    \param w    The desination width
    \param h    The desination height
    \param surf The surface to be blitted
    \param x1   x position of the first split
    \param y1   y position of the first split
    \param x2   x position of the second split
    \param y2   y position of the second split
    \param transparence Over all transparence
*/
CYGFX_ERROR utBe9SliceScaling(CYGFX_BE_CONTEXT ctx, CYGFX_FLOAT x, CYGFX_FLOAT y, CYGFX_U32 w, CYGFX_U32 h, CYGFX_SURFACE surf, CYGFX_U32 x1, CYGFX_U32 y1, CYGFX_U32 x2, CYGFX_U32 y2, CYGFX_U08 transparence)
{
    CYGFX_ERROR ret = CYGFX_OK;
    Mat3x2      mat;
    CYGFX_U32   sw = utSurfWidth(surf);
    CYGFX_U32   sh = utSurfHeight(surf);
    CYGFX_U32   msw = x2-x1;
    CYGFX_U32   msh = y2-y1;
    CYGFX_U32   mdw = w-(sw-msw);
    CYGFX_U32   mdh = h-(sh-msh);

    UTIL_SUCCESS(ret, CyGfx_BeBindSurface(ctx, CYGFX_BE_TARGET_SRC, surf));
    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_ALPHAMULTI, CYGFX_TRUE));
    UTIL_SUCCESS(ret, CyGfx_BeSetSurfAttribute(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_SURF_ATTR_COLOR, transparence));
    /* middle */
    UTIL_SUCCESS(ret, CyGfx_BeSelectArea(ctx, CYGFX_BE_TARGET_STORE));
    utMat3x2Scale(mat, (float)mdw / msw, (float)mdh / msh);
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x+x1, (CYGFX_S32)y+y1, mdw, mdh));
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, (float)mdw / msw, (float)mdh / msh);
    utMat3x2Translate(mat, -(float)x1, -(float)y1);
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x + x1, y + y1));
    /* upper */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x+x1, (CYGFX_S32)y, mdw, y1));
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, (float)mdw / msw, 1.0f);
    utMat3x2Translate(mat, -(float)x1, 0);
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x + x1, y));
    /* lower */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x+x1, (CYGFX_S32)y+y1+mdh, mdw, sh-y2));
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, (float)mdw / msw, 1.0f);
    utMat3x2Translate(mat, -(float)x1, -(float)y2);
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x + x1, y+y1+mdh));
    /* left */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x, (CYGFX_S32)y+y1, x1, mdh));
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, 1.0f, (float)mdh / msh);
    utMat3x2Translate(mat, 0, -(float)y1);
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x, y + y1));
    /* right */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x+x1+mdw, (CYGFX_S32)y+y1, sw-x2, mdh));
    utMat3x2LoadIdentity(mat);
    utMat3x2Scale(mat, 1.0f, (float)mdh / msh);
    utMat3x2Translate(mat, -(float)x2, -(float)y1);
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, mat));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x+x1+mdw, y + y1));
    /* up left */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x, (CYGFX_S32)y, x1, y1));
    UTIL_SUCCESS(ret, CyGfx_BeSetGeoMatrix(ctx, CYGFX_BE_TARGET_SRC, CYGFX_BE_GEO_MATRIX_FORMAT_3X2, 0));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x, y));
    /* up right */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x + mdw + x1, (CYGFX_S32)y, x1, y1));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x + mdw + x1 - x2, y));
    /* low left */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x, (CYGFX_S32)y + mdh + y1, x1, y1));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x, y + mdh + y1 - y2));
    /* low right */
    UTIL_SUCCESS(ret, CyGfx_BeActiveArea(ctx, CYGFX_BE_TARGET_STORE, (CYGFX_S32)x + mdw + x1, (CYGFX_S32)y + mdh + y1, x1, y1));
    UTIL_SUCCESS(ret, CyGfx_BeBlt(ctx, x + mdw + x1 - x2, y + mdh + y1 - y2));

    UTIL_SUCCESS(ret, CyGfx_BeSelectArea(ctx, CYGFX_BE_TARGET_SRC));

    return ret;
}



/* 
   Perform a utilization counter read for all fields and display the values in
   human readable format.

   \param inputValues Pointer to an utilization counter variable where to perform the read

*/
CYGFX_ERROR utReadUCount (CYGFX_BE_U_COUNT_S *inputValues)
{
    CYGFX_ERROR ret = CYGFX_OK;
    CYGFX_FLOAT be_lbo;
    CYGFX_FLOAT be_ibo;
    CYGFX_FLOAT be_idle;
    CYGFX_FLOAT tb0_active_counter;
    CYGFX_FLOAT tb1_active_counter;
    CYGFX_FLOAT tb2_active_counter;
    CYGFX_FLOAT tb3_active_counter;
    CYGFX_FLOAT tb4_active_counter;
    CYGFX_FLOAT tb5_active_counter;
    CYGFX_FLOAT tb6_active_counter;
    CYGFX_FLOAT tb7_active_counter;
    CYGFX_DOUBLE sum;

    if (inputValues==NULL)
    {
        ret = CYGFX_ERR;
    } else
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("\n****************************Performance Counters ************************************");
#endif
        if (inputValues->clock_counter !=0 )
        {
            be_lbo                = ((CYGFX_FLOAT)(inputValues->lbo_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter))*100.0f;
            be_ibo                = ((CYGFX_FLOAT)(inputValues->ibo_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter))*100.0f;
            be_idle               = (((CYGFX_FLOAT)(inputValues->clock_counter) - (CYGFX_FLOAT)(inputValues->ibo_active_counter) - (CYGFX_FLOAT)(inputValues->lbo_active_counter))/(CYGFX_FLOAT)(inputValues->clock_counter))*100.0f;       
            tb0_active_counter    = ((CYGFX_FLOAT)(inputValues->tb0_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb1_active_counter    = ((CYGFX_FLOAT)(inputValues->tb1_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb2_active_counter    = ((CYGFX_FLOAT)(inputValues->tb2_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb3_active_counter    = ((CYGFX_FLOAT)(inputValues->tb3_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb4_active_counter    = ((CYGFX_FLOAT)(inputValues->tb4_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb5_active_counter    = ((CYGFX_FLOAT)(inputValues->tb5_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb6_active_counter    = ((CYGFX_FLOAT)(inputValues->tb6_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;
            tb7_active_counter    = ((CYGFX_FLOAT)(inputValues->tb7_active_counter) / (CYGFX_FLOAT)(inputValues->clock_counter)) * 100.0f;

#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("\ntb0_active_counter                 :%.1f%%", tb0_active_counter);
            printf("\ntb1_active_counter                 :%.1f%%", tb1_active_counter); 
            printf("\ntb2_active_counter                 :%.1f%%", tb2_active_counter);
            printf("\ntb3_active_counter                 :%.1f%%", tb3_active_counter);
            printf("\ntb4_active_counter                 :%.1f%%", tb4_active_counter);
            printf("\ntb5_active_counter                 :%.1f%%", tb5_active_counter);
            printf("\ntb6_active_counter                 :%.1f%%", tb6_active_counter);
            printf("\ntb7_active_counter                 :%.1f%%", tb7_active_counter);
            printf("\nBlit Engine utilization -> IDLE: %.1f%% | IBO: %.1f%% | LBO: %.1f%%",be_idle, be_ibo, be_lbo);  
#endif
        }

        if (inputValues->lbo_active_counter != 0)
        {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
            printf("\nLBO time all_fetches_active: %.1f%%, wait_bliteng_ressources: %.1f%%, rest: %.1f%%",
                ((double)(inputValues->lbo_all_fetches_active_counter) / (double)inputValues->lbo_active_counter) * 100,
                ((double)(inputValues->lbo_wait_bliteng_resources_counter) / (double)inputValues->lbo_active_counter) * 100,
                ((double)(inputValues->lbo_active_counter - inputValues->lbo_all_fetches_active_counter - inputValues->lbo_wait_bliteng_resources_counter) / (double)inputValues->lbo_active_counter) * 100);            
#endif
        }

        sum = (inputValues->lbo_source_pixel_rgba_counter + inputValues->lbo_source_pixel_alpha_counter) * 16.0;
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("\nPixel/clock IBO: %.3f, LBO Source: %.3f (%.1f%% RGBA, %.1f%% alpha), LBO Destination: %.3f\n",
        (inputValues->ibo_active_counter == 0) ? 0.0 : (double)inputValues->ibo_destination_pixel_counter  / inputValues->ibo_active_counter,
        (inputValues->lbo_active_counter == 0) ? 0.0 : sum / inputValues->lbo_active_counter,
        (sum         == 0) ? 0.0 : inputValues->lbo_source_pixel_rgba_counter  * 16.0 * 100.0 / sum,
        (sum         == 0) ? 0.0 : inputValues->lbo_source_pixel_alpha_counter * 16.0 * 100.0 / sum,
        (inputValues->lbo_active_counter == 0) ? 0.0 : (double)inputValues->lbo_destination_pixel_counter  * 2.0 / inputValues->lbo_active_counter);

        printf("Pixels: IBO %.0f, LBO Source %.0f, LBO Destination %.0f\n", inputValues->ibo_destination_pixel_counter * 16.0, sum * 16.0, inputValues->lbo_destination_pixel_counter * 16 * 2.0);
        printf("\n*************************************************************************************\n");
#endif
    }

    return ret;
}

