/*******************************************************************************
* (c) 2018-2022, Cypress Semiconductor Corporation or a                        *
* subsidiary of Cypress Semiconductor Corporation.  All rights reserved.       *
*                                                                              *
* This software, including source code, documentation and related              *
* materials ("Software"), is owned by Cypress Semiconductor Corporation or     *
* one of its subsidiaries ("Cypress") and is protected by and subject to       *
* worldwide patent protection (United States and foreign), United States       *
* copyright laws and international treaty provisions. Therefore, you may use   *
* this Software only as provided in the license agreement accompanying the     *
* software package from which you obtained this Software ("EULA").             *
*                                                                              *
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,     *
* non-transferable license to copy, modify, and compile the                    *
* Software source code solely for use in connection with Cypress’s             *
* integrated circuit products.  Any reproduction, modification, translation,   *
* compilation, or representation of this Software except as specified          *
* above is prohibited without the express written permission of Cypress.       *
*                                                                              *
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO                         *
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING,                         *
* BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED                                 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A                              *
* PARTICULAR PURPOSE. Cypress reserves the right to make                       *
* changes to the Software without notice. Cypress does not assume any          *
* liability arising out of the application or use of the Software or any       *
* product or circuit described in the Software. Cypress does not               *
* authorize its products for use in any products where a malfunction or        *
* failure of the Cypress product may reasonably be expected to result in       *
* significant property damage, injury or death ("High Risk Product"). By       *
* including Cypress's product in a High Risk Product, the manufacturer         *
* of such system or application assumes all risk of such use and in doing      *
* so agrees to indemnify Cypress against all liability.                        *
*******************************************************************************/

/*!
 * \file        ut_capringbuffer.c
 *              Function to calculate the required ring buffer size for capture-to-window
 */


/*****************************************************************************/
/*** INCLUDES ****************************************************************/
/*****************************************************************************/
#include <stdio.h>
#include "cygfx_driver_api.h"
#include "ut_capringbuffer.h"


/*****************************************************************************/
/*** DEFINITIONS *************************************************************/
/*****************************************************************************/
#define ALIGN(value, align)  (((((value) + (align)) - 1) / (align)) * (align))
#define UT_GDC_CAPTURE_SAFETY_MARGIN        (16 * 1024)
#define UT_CAPRINGBUFFER_VERBOSE 0 /* Debug print verbosity: off if 0, otherwise full. */

#if 0 != UT_CAPRINGBUFFER_VERBOSE
#define UT_CAPRINGBUFFER_PRINT(X) printf X
#else
#define UT_CAPRINGBUFFER_PRINT(X)
#endif

/*****************************************************************************/
/*** TYPES / STRUCTURES ******************************************************/
/*****************************************************************************/
/**
 * Clock properties, used for Ring Buffer setup
 */
typedef struct
{
    CYGFX_U32   m_Hact;
    CYGFX_U32   m_Vact;
    CYGFX_U32   m_blanking_height;
    CYGFX_U32   m_Htot;
    CYGFX_U32   m_Vtot;
    CYGFX_FLOAT m_pixelClock;         /* pixel clock in Hz */
} GFX_CAP_RB_CLOCK_S;

/**
 * Helper struct to calculate Ring Buffer properties
 */
typedef struct
{
    GFX_CAP_RB_CLOCK_S cap;           /**< timing parameters of capture stream */
    GFX_CAP_RB_CLOCK_S dsp;           /**< timing parameters of display stream */
    CYGFX_U32 m_Hact;              /**< stored width */
    CYGFX_U32 m_Vact;             /**< stored height */
    CYGFX_U32 m_w_yoffset;                 /**< y offset of cropping rectangle */
    CYGFX_U32 m_w_height;                  /**< height of cropping rectangle */
    CYGFX_U32 m_r_yoffset;                 /**< y offset of capture window */
    CYGFX_U32 m_r_height;                  /**< height of capture window */
    CYGFX_U32 m_bpp;                       /**< bit per pixel in ring buffer. 24 for R8G8B8, 16 for YUV422. */
    CYGFX_U32 m_burstlength;               /**< burst length in bytes (maximum of Fetch and Store) */
    CYGFX_U32 m_stride;                    /**< stride in ring buffer (must be a multiple of burst length) */
    CYGFX_U32 m_bufsize;                   /**< size of 1 uncompressed frame in bytes */
    CYGFX_U32 m_ringbufsize;               /**< minimum uncompressed ring buffer size in bytes */
    CYGFX_FLOAT m_tw_line;                 /**< time to write one line (incl. horizontal blanking) in seconds */
    CYGFX_FLOAT m_tw_act;                  /**< time of active capture frame in seconds */
    CYGFX_FLOAT m_tw_height;               /**< time to write cropping window in seconds */
    CYGFX_FLOAT m_tw_vblk;                 /**< time of vertical blanking in seconds */
    CYGFX_FLOAT m_tw_blk;                  /**< time per frame while nothing is written in seconds */
    CYGFX_FLOAT m_tw_blk2;                 /**< time from capture frame complete interrupt until Store starts to write the following frame */
    CYGFX_FLOAT m_tw_ofs;                  /**< time until cropping window starts (Note: during this time, the Store write pointer will still point to the last written pixel) */
    CYGFX_FLOAT m_tw_tot;                  /**< duration of one capture frame including blanking in seconds */
    CYGFX_FLOAT m_tw_buf;                  /**< time from framecomplete interrupt (end of crop area) until next frame is written (end of crop area) */
    CYGFX_FLOAT m_tr_line;                 /**< time to read one line (incl. horizontal blanking) in seconds */
    CYGFX_FLOAT m_tr_act;                  /**< time for active display frame in seconds */
    CYGFX_FLOAT m_tr_height;               /**< time to read window in seconds */
    CYGFX_FLOAT m_tr_vblk;                 /**< time of vertical blanking in seconds */
    CYGFX_FLOAT m_tr_blk;                  /**< time per frame while nothing is read in seconds */
    CYGFX_FLOAT m_tr_blk2;                 /**< time from display frame complete interrupt until Fetch starts to read the following frame */
    CYGFX_FLOAT m_tr_ofs;                  /**< time until window starts (Note: during this time, the Fetch read pointer will still point to the last read pixel) */
    CYGFX_FLOAT m_tr_tot;                  /**< duration of one display frame including blanking in seconds */
    CYGFX_FLOAT m_tr_buf;                  /**< time from framecomplete interrupt (end of active area) until next frame is completely read (end of window) */
} GFX_CAP_RB_SETUP_S;

/*****************************************************************************/
/*** VARIABLES ***************************************************************/
/*****************************************************************************/
GFX_CAP_RB_SETUP_S    s_rb_setup;

CYGFX_BOOL bNeedRepeat;    /* display is faster than capture => consider repeating frames */
CYGFX_BOOL bNeedDrop;      /* capture is faster than display => consider dropping frames */
CYGFX_U32 aw_delta;        /* minimum number of pixels written by Store unit during the period from display frame complete interrupt until the following frame is read by Fetch unit */
CYGFX_U32 ar_delta;        /* minimum number of pixels read by Fetch unit during the period from capture frame complete interrupt until the following frame is written by Store unit */
CYGFX_U32 aw_delta_blk;    /* Extra space required for Store, when Fetch repeats a frame.
                           case 1: Fetch reads faster than Store writes:
                                   Maximum number of pixels written by Store during the period from display frame complete interrupt until Fetch starts to read the following frame.
                           case 2: Store writes faster than Fetch reads:
                                   Minimum number of pixels read by Fetch while Store writes a frame, i.e. extra space required for Store, when Fetch repeats a frame. */
CYGFX_U32 ar_delta_blk;    /* Extra space required for Fetch, when Store drops a frame.
                           case 1: Store writes faster than Fetch reads:
                                   Maximum number of pixels read by Fetch during the period from capture frame complete interrupt until Store starts to write the following frame.
                           case 2: Fetch reads faster than Store writes:
                                   1 - (Minimum number of pixels written by Store while Fetch reads a frame), i.e. extra space required for Fetch, when Store repeats a frame. */

/*****************************************************************************/
/*** LOCAL FUNCTIONS *********************************************************/
/*****************************************************************************/
/**
 * Calculate stride for ring buffer setup.
 */
static void Gfx_CapCalcStride(void)
{
    s_rb_setup.m_stride = ALIGN((s_rb_setup.m_Hact * s_rb_setup.m_bpp)/8, s_rb_setup.m_burstlength * 8);
}

/**
 * Based on the associated clocks, dimensions and offset, calculate the delta values for extrapolation.
 * NOTE: The algorithm just counts in lines and doesn't care about horizontal position,
 *       so in all calculations we add or subtract the duration of one line, whatever
 *       would be the worst case. We also add an additional margin for run-in delay of
 *       Fetch or Store.
 */
static void Gfx_CapCalcDelta(void)
{
    CYGFX_U32 delta_2_tmp;

    CYGFX_S32 runin_correction = 385 * s_rb_setup.m_bpp/8;

    s_rb_setup.m_bufsize = s_rb_setup.m_Vact * s_rb_setup.m_stride;

    if (s_rb_setup.m_tw_tot < s_rb_setup.m_tr_tot + 2 * s_rb_setup.m_tr_line)
    {
        /* Store is faster than Fetch (higher frame rate).
           Capture must take care not to cross read pointer, i.e. drop frames, if necessary. */
        /* Note: The check is also enabled, if Store and Fetch run approximately at the same
                 speed (2 lines safety margin per frame), because the real frequency might differ
                 from the value provided by the application, e.g. due to tolerance of the oscillator */
        bNeedDrop = 1;

        /* Calculate constants for capture stream */
        /* Minimum number of pixels read by Fetch unit during the period from capture frame complete interrupt until the following frame is written by Store unit */
        ar_delta = (CYGFX_U32)CYGFX_MAX(((((CYGFX_FLOAT)s_rb_setup.m_bufsize * ((s_rb_setup.m_tw_buf - s_rb_setup.m_tw_line) - (s_rb_setup.m_tr_blk + s_rb_setup.m_tr_line))) / s_rb_setup.m_tr_height) - (CYGFX_FLOAT)runin_correction), 0.0f);
        if (s_rb_setup.m_tw_height < s_rb_setup.m_tr_height)
        {
            /* Store writes faster than Fetch reads */
            UT_CAPRINGBUFFER_PRINT(("Store writes faster than Fetch reads\n"));
            /* Maximum number of pixels read by Fetch unit during the period from capture frame complete interrupt until the Store starts to write the following frame */
            ar_delta_blk = (CYGFX_U32)(((CYGFX_FLOAT)s_rb_setup.m_bufsize * (s_rb_setup.m_tw_blk2 + s_rb_setup.m_tw_line)) / (s_rb_setup.m_tr_height - s_rb_setup.m_tr_line)) + (CYGFX_U32)runin_correction;
        } else
        {
            /* Fetch reads faster than Store writes */
            UT_CAPRINGBUFFER_PRINT(("Fetch reads faster than Store writes\n"));
            /*
               Worst case: Fetch just starts to read
                 Fetch reads a frame: m_tr_height
                 Store writes: 1. Nothing during tw_blk2
                               2. Something during (tr_height - tw_blk2)
                               => Store advances bufsize * (tr_height - tw_blk2) / tw_height
                               => Fetch advances bufsize
                               => Extra margin: bufsize * (1 - (tr_height - tw_blk2) / tw_height) = bufsize * ((tw_height - tr_height + tw_blk2) / tw_height)
               All other cases: Fetch reads less, Store doesn't write that much less, because Fetch reads faster than Store writes
            */
            ar_delta_blk = (CYGFX_U32)(((CYGFX_FLOAT)s_rb_setup.m_bufsize * ((s_rb_setup.m_tw_height - s_rb_setup.m_tr_height) + s_rb_setup.m_tw_blk2 + s_rb_setup.m_tw_line)) / (s_rb_setup.m_tw_height - s_rb_setup.m_tw_line)) + (CYGFX_U32)runin_correction;

            /* We must also adjust ar_delta, to avoid that Store crosses read pointer while Fetch doesn't proceed, although Fetch will be (again) ahead of Store, when next frame is written.
               Worst case: Fetch just finished reading
                 Fetch does not advance during: tr_blk
                 Store writes: 1. Nothing during tw_blk2
                               2. Something during (tr_blk - tw_blk2)
                               => Store advances bufsize * (tr_blk - tw_blk2) / tw_height
                               => Fetch must already be (bufsize * (tr_blk - tw_blk2) / tw_height) ahead.
                               => Maximum ar_delta: bufsize * (1 - (tr_blk - tw_blk2) / tw_height) = bufsize * ((tw_height - tr_blk + tw_blk2) / tw_height)
            */
            delta_2_tmp = (CYGFX_U32)CYGFX_MAX(((((CYGFX_FLOAT)s_rb_setup.m_bufsize * (((s_rb_setup.m_tw_height - s_rb_setup.m_tr_blk) + s_rb_setup.m_tw_blk2) - s_rb_setup.m_tw_line)) / (s_rb_setup.m_tw_height + s_rb_setup.m_tw_line)) - (CYGFX_FLOAT)runin_correction), 0.0f);
            if (delta_2_tmp < ar_delta)
            {
                UT_CAPRINGBUFFER_PRINT(("=> adjust ar_delta: %d -> %d\n", ar_delta, delta_2_tmp));
                ar_delta = delta_2_tmp;
            }
        }
        if (ar_delta_blk > s_rb_setup.m_bufsize)
        {
            ar_delta_blk = s_rb_setup.m_bufsize;
        }
    } else
    {
        /* Store is not faster than Fetch.
           Capture never needs to drop frames
           and therefore ar_delta and ar_delta_blk needs not be considered. */
        bNeedDrop = 0;
        ar_delta = 0;
        ar_delta_blk = 0;
    }

    if (s_rb_setup.m_tr_tot < s_rb_setup.m_tw_tot + 2 * s_rb_setup.m_tw_line)
    {
        /* Fetch is faster than Store (higher frame rate).
           Display must take care not to cross write pointer, i.e. repeat frames, if necessary. */
        /* Note: The check is also enabled, if Store and Fetch run approximately at the same
                 speed (2 lines safety margin per frame), because the real frequency might differ
                 from the value provided by the application */
        bNeedRepeat = 1;

        /* Calculate constants for display stream */
        /* Minimum number of pixels written by Store unit during the period from display frame complete interrupt until the following frame is read by Fetch unit */
        aw_delta = (CYGFX_U32)CYGFX_MAX(((((CYGFX_FLOAT)s_rb_setup.m_bufsize * ((s_rb_setup.m_tr_buf - s_rb_setup.m_tr_line) - (s_rb_setup.m_tw_blk + s_rb_setup.m_tw_line))) / s_rb_setup.m_tw_height) - (CYGFX_FLOAT)runin_correction), 0.0f);
        if (s_rb_setup.m_tr_height < s_rb_setup.m_tw_height)
        {
            /* Fetch reads faster than Store writes */
            UT_CAPRINGBUFFER_PRINT(("Fetch reads faster than Store writes\n"));
            /* Maximum number of pixels written by Store unit during the period from display frame complete interrupt until Fetch starts to read the following frame */
            aw_delta_blk = (CYGFX_U32)(((CYGFX_FLOAT)s_rb_setup.m_bufsize * (s_rb_setup.m_tr_blk2 + s_rb_setup.m_tr_line)) / (s_rb_setup.m_tw_height - s_rb_setup.m_tw_line)) + (CYGFX_U32)runin_correction;
        } else
        {
            /* Store writes faster than Fetch reads */
            UT_CAPRINGBUFFER_PRINT(("Store writes faster than Fetch reads\n"));
            /*
               Worst case: Store just starts to write
                 Store writes a frame: m_tw_height
                 Fetch reads: 1. Nothing during tr_blk2
                              2. Something during (tw_height - tr_blk2)
                              => Fetch advances bufsize * (tw_height - tr_blk2) / tr_height
                              => Store advances bufsize
                              => Extra margin: bufsize * (1 - (tw_height - tr_blk2) / tr_height) = bufsize * ((tr_height - tw_height + tr_blk2) / tr_height)
               All other cases: Store writes less, Fetch doesn't read that much less, because Store writes faster than Fetch reads
            */
            aw_delta_blk = (CYGFX_U32)(((CYGFX_FLOAT)s_rb_setup.m_bufsize * ((s_rb_setup.m_tr_height - s_rb_setup.m_tw_height) + s_rb_setup.m_tr_blk2 + s_rb_setup.m_tr_line)) / (s_rb_setup.m_tr_height - s_rb_setup.m_tr_line)) + (CYGFX_U32)runin_correction;

            /* We must also adjust aw_delta, to avoid that Fetch crosses write pointer while Store doesn't proceed, although Store will be (again) ahead of Fetch, when next frame is read.
               Worst case: Store just finished writing
                 Store does not advance during: tw_blk
                 Fetch reads: 1. Nothing during tr_blk2
                              2. Something during (tw_blk - tr_blk2)
                              => Fetch advances bufsize * (tw_blk - tr_blk2) / tr_height
                              => Store must already be (bufsize * (tw_blk - tr_blk2) / tr_height) ahead.
                              => Maximum aw_delta: bufsize * (1 - (tw_blk - tr_blk2) / tr_height) = bufsize * ((tr_height - tw_blk - tr_blk2) / tr_height)
            */
            delta_2_tmp = (CYGFX_U32)CYGFX_MAX(((((CYGFX_FLOAT)s_rb_setup.m_bufsize * (((s_rb_setup.m_tr_height - s_rb_setup.m_tw_blk) + s_rb_setup.m_tr_blk2) - s_rb_setup.m_tr_line)) / (s_rb_setup.m_tr_height + s_rb_setup.m_tr_line)) - (CYGFX_FLOAT)runin_correction), 0.0f);
            if (delta_2_tmp < aw_delta)
            {
                UT_CAPRINGBUFFER_PRINT(("=> adjust aw_delta: %d -> %d\n", aw_delta, delta_2_tmp));
                aw_delta = delta_2_tmp;
            }
        }
        if (aw_delta_blk > s_rb_setup.m_bufsize)
        {
            aw_delta_blk = s_rb_setup.m_bufsize;
        }
    } else
    {
        /* Fetch is not faster than Store.
           Display never needs to repeat frames
           and therefore aw_delta and aw_delta_blk needs not be considered. */
        bNeedRepeat = 0;
        aw_delta = 0;
        aw_delta_blk = 0;
    }

    /* In case of downscaling, the Store4 writes pixels at a lower frequency than the capture clock!
       In case of upscaling, the FetchDecode4 reads pixels at a lower frequency than the display clock!
       To be on the safe side, we set the deltas to 0, i.e. we don't rely on the other pointer to
       advance at all, when determining whether to repeat a frame or not. */
    if (s_rb_setup.m_Vact < s_rb_setup.m_r_height) /* Upscale */
    {
        if (bNeedDrop == 1) /* Capture is faster than display */
        {
            UT_CAPRINGBUFFER_PRINT(("Upscale => ar_delta = 0 (was %d)\n", ar_delta));
            ar_delta = 0;
        }
    } else if (s_rb_setup.m_w_height > s_rb_setup.m_Vact) /* Downscale */
    {
        if (bNeedRepeat == 1) /* Display is faster than capture */
        {
            UT_CAPRINGBUFFER_PRINT(("Downscale => aw_delta = 0 (was %d)\n", aw_delta));
            aw_delta = 0;
        }
    }

    UT_CAPRINGBUFFER_PRINT(("Gfx_CapCalcDelta(): m_bufsize = %d, bNeedDrop = %d, bNeedRepeat = %d, ar_delta = %d, aw_delta = %d, ar_delta_blk = %d, aw_delta_blk = %d\n",
        s_rb_setup.m_bufsize, bNeedDrop, bNeedRepeat, ar_delta, aw_delta, ar_delta_blk, aw_delta_blk));
}

/**
 * Based on single buffer size and deltas, calculate the required ring buffer size without compression.
 */
static void Gfx_CapCalcSize(void)
{
    if (bNeedDrop && bNeedRepeat)
    {
        /* We must care about both read and write deltas, so consider the worst case */
        s_rb_setup.m_ringbufsize = (2 * s_rb_setup.m_bufsize) - CYGFX_MIN(ar_delta, aw_delta) + CYGFX_MAX(ar_delta_blk, aw_delta_blk) + (2 * UT_GDC_CAPTURE_SAFETY_MARGIN);
        UT_CAPRINGBUFFER_PRINT(("Gfx_CapCalcSize(): m_ringbufsize = 2 * %d - MIN(%d, %d) + MAX(%d, %d) + 2 * %d = %d\n", s_rb_setup.m_bufsize, ar_delta, aw_delta, ar_delta_blk, aw_delta_blk, UT_GDC_CAPTURE_SAFETY_MARGIN, s_rb_setup.m_ringbufsize));
    } else if (bNeedDrop)
    {
        /* We must only care about read deltas */
        s_rb_setup.m_ringbufsize = (2 * s_rb_setup.m_bufsize) - ar_delta + ar_delta_blk + (2 * UT_GDC_CAPTURE_SAFETY_MARGIN);
        UT_CAPRINGBUFFER_PRINT(("Gfx_CapCalcSize(): m_ringbufsize = 2 * %d - %d + %d + 2 * %d = %d\n", s_rb_setup.m_bufsize, ar_delta, ar_delta_blk, UT_GDC_CAPTURE_SAFETY_MARGIN, s_rb_setup.m_ringbufsize));
    } else /* bNeedRepeat */
    {
        /* We must only care about write deltas */
        s_rb_setup.m_ringbufsize = (2 * s_rb_setup.m_bufsize) - aw_delta + aw_delta_blk + (2 * UT_GDC_CAPTURE_SAFETY_MARGIN);
        UT_CAPRINGBUFFER_PRINT(("Gfx_CapCalcSize(): m_ringbufsize = 2 * %d - %d + %d + 2 * %d = %d\n", s_rb_setup.m_bufsize, aw_delta, aw_delta_blk, UT_GDC_CAPTURE_SAFETY_MARGIN, s_rb_setup.m_ringbufsize));
    }

    /* Ring buffer size must be a multiple of stride */
    s_rb_setup.m_ringbufsize = ALIGN(s_rb_setup.m_ringbufsize, s_rb_setup.m_stride);
    UT_CAPRINGBUFFER_PRINT(("ALIGNED => m_ringbufsize   = %d\n", s_rb_setup.m_ringbufsize));
}

/*****************************************************************************/
/*** FUNCTIONS ***************************************************************/
/*****************************************************************************/
CYGFX_U32 CalculateRingbufferSize(
    CYGFX_U32 cap_width,
    CYGFX_U32 cap_height,
    CYGFX_U32 cap_totalwidth,
    CYGFX_U32 cap_totalheight,
    CYGFX_U32 cap_pclk,
    CYGFX_U32 disp_width,
    CYGFX_U32 disp_height,
    CYGFX_U32 disp_totalwidth,
    CYGFX_U32 disp_totalheight,
    CYGFX_U32 disp_pclk,
    CYGFX_U32 crop_width,
    CYGFX_U32 crop_height,
    CYGFX_U32 crop_yoffset,
    CYGFX_U32 win_width,
    CYGFX_U32 win_height,
    CYGFX_U32 win_yoffset,
    CYGFX_BOOL bYUV422)
{
    CYGFX_U32 Hact;        /* Width of the frame stored in the ringbuffer */
    CYGFX_U32 Vact;       /* Height of the frame stored in the ringbuffer */

    /* Sanity check */
    if ((cap_width == 0) ||
        (cap_height == 0) ||
        (cap_totalwidth <= cap_width) ||
        (cap_totalheight <= cap_height) ||
        (cap_pclk <= 100000) ||
        (disp_width == 0) ||
        (disp_height == 0) ||
        (disp_totalwidth <= disp_width) ||
        (disp_totalheight <= disp_height) ||
        (disp_pclk <= 100000) ||
        (crop_width == 0) ||
        (crop_height == 0) ||
        (win_width == 0) ||
        (win_height == 0) ||
        (cap_width < crop_width) ||
        (cap_height < (crop_yoffset + crop_height)) ||
        (disp_height <= win_yoffset))
    {
#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
        printf("CalculateRingbufferSize: illegal parameter!\n");
#endif
        return 0xffffffff;
    }



    if ((crop_width != win_width) || (crop_height != win_height))
    {
        /* In case of scaling, the data will always be stored as RGB in ring buffer */
        bYUV422 = CYGFX_FALSE;
    }

    if ((win_height > crop_height) ||                               /* vertical upscale or */
        (win_height == crop_height) && (win_width > crop_width))    /* only horizontal upscale */
    {
        /* upscaling is done in display path */
        Hact = crop_width;
        Vact = crop_height;
    } else                      /* vertical downscale or only horizontal downscale or no scale */
    {
        /* downscaling is done in capture path, if necessary */
        Hact = win_width;
        Vact = win_height;
    }

    /* Set up the ring buffer properties, based on the associated clocks, dimensions and offset */
    s_rb_setup.m_Hact = Hact;
    s_rb_setup.m_Vact = Vact;
    s_rb_setup.m_w_yoffset = crop_yoffset;
    s_rb_setup.m_w_height = crop_height;
    s_rb_setup.m_r_yoffset = win_yoffset;
    s_rb_setup.m_r_height = win_height;
    s_rb_setup.m_bpp = (bYUV422 == CYGFX_TRUE) ? 16 : 24;
    s_rb_setup.m_burstlength = 8;

    Gfx_CapCalcStride();

    /* Calculate timing parameters */
    s_rb_setup.m_tw_line          = (CYGFX_FLOAT)cap_totalwidth / (CYGFX_FLOAT)cap_pclk;
    s_rb_setup.m_tw_ofs           = s_rb_setup.m_tw_line * (CYGFX_FLOAT)s_rb_setup.m_w_yoffset;
    s_rb_setup.m_tw_height        = s_rb_setup.m_tw_line * (CYGFX_FLOAT)s_rb_setup.m_w_height;
    s_rb_setup.m_tw_act           = s_rb_setup.m_tw_line * (CYGFX_FLOAT)cap_height;
    s_rb_setup.m_tw_vblk          = s_rb_setup.m_tw_line * (CYGFX_FLOAT)(cap_totalheight - cap_height);
    s_rb_setup.m_tw_tot           = s_rb_setup.m_tw_act + s_rb_setup.m_tw_vblk;
    s_rb_setup.m_tw_blk           = s_rb_setup.m_tw_tot - s_rb_setup.m_tw_height;
    s_rb_setup.m_tw_blk2          = s_rb_setup.m_tw_blk;                        /* interrupt is at end of cropping window */
    s_rb_setup.m_tw_buf           = s_rb_setup.m_tw_tot;

    s_rb_setup.m_tr_line          = (CYGFX_FLOAT)disp_totalwidth / (CYGFX_FLOAT)disp_pclk;
    s_rb_setup.m_tr_ofs           = s_rb_setup.m_tr_line * (CYGFX_FLOAT)s_rb_setup.m_r_yoffset;
    s_rb_setup.m_tr_height        = s_rb_setup.m_tr_line * (CYGFX_FLOAT)s_rb_setup.m_r_height;
    s_rb_setup.m_tr_act           = s_rb_setup.m_tr_line * (CYGFX_FLOAT)disp_height;
    s_rb_setup.m_tr_vblk          = s_rb_setup.m_tr_line * (CYGFX_FLOAT)(disp_totalheight - disp_height);
    s_rb_setup.m_tr_tot           = s_rb_setup.m_tr_act + s_rb_setup.m_tr_vblk;
    s_rb_setup.m_tr_blk           = s_rb_setup.m_tr_tot - s_rb_setup.m_tr_height;
    s_rb_setup.m_tr_blk2          = s_rb_setup.m_tr_vblk + s_rb_setup.m_tr_ofs; /* interrupt is at end of active frame */
    s_rb_setup.m_tr_buf           = s_rb_setup.m_tr_vblk + s_rb_setup.m_tr_ofs + s_rb_setup.m_tr_height;

    Gfx_CapCalcDelta();
    Gfx_CapCalcSize();

#if (defined (DEBUG) || defined (RELEASE) || defined (FPGA))
    printf("Minimum ringBufferSize: %d bytes  (FACTOR = %d%%)\n", 
        s_rb_setup.m_ringbufsize, 
        s_rb_setup.m_ringbufsize * 100 / s_rb_setup.m_bufsize);
#endif

    return s_rb_setup.m_ringbufsize;
}
